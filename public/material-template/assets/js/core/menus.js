let base_url = window.location.origin;
const menuNavbar = document.querySelector('#menu-navbar');
const submenuNavbar = document.querySelector('#submenus-navbar');
const currentMenu = sessionStorage.getItem('menu');
const listadoObtenido = sessionStorage.getItem('lista');


$('#menu-navbar').on('click', 'li', function () {
    sessionStorage.setItem('menu', $(this).data('menu'));
    sessionStorage.setItem('icono', $(this).data('icono'));
    window.location.replace(contextPath + '/' + $(this).data('enlace'));


});
if (listadoObtenido !== undefined && listadoObtenido !== null) {
    let menuJson = JSON.parse(listadoObtenido);
    fillMainMenu(menuJson);
} else {
    ajaxGET('/Menu/getMenuUsuario', null).done(function (data) {
        sessionStorage.getItem('lista');
        if (data === undefined || data === null || data.length === 0) {
            showAlert(
                'No se pudieron obtener los menús',
                'warning',
                'Ocurrio un problema obteniendo la lista de menus',
                true,
                false
            );
        } else {
            sessionStorage.setItem('lista', JSON.stringify(data));
            fillMainMenu(data);
            document.getElementById('current-menu-icon').classList.add(data[0]['icono'] + '-lg');
            fillSubmenus(data[0]['id_menu']);
        }
    });
}


if (currentMenu !== undefined && currentMenu !== null) {
    document.getElementById('current-menu-icon').classList.add(sessionStorage.getItem('icono') + '-lg');
    fillSubmenus(currentMenu);
}


function fillMainMenu(data) {
    let menusContent = '';
    data.forEach(function (menu) {
        menusContent = menusContent + `
        <li 
        class="nav-item d-flex align-items-center" 
        id="menu-${menu.id_menu}" 
        data-menu="${menu.id_menu}" 
        data-enlace="${menu.enlace}" 
        data-icono="${menu.icono}" 
        data-etiqueta="${menu.etiqueta}" 
        data-bs-toggle="tooltip" 
        data-bs-placement="bottom" 
        title="${menu.descripcion === null ? '...' : menu.descripcion}">
          <a href="javascript:void(0)" class="text-center">          
            <strong class="font-weight-bolder text-wrap mb-0"><i class="${menu.icono}"></i>${menu.etiqueta}</strong>
          </a>
        </li>`;
    });
    menuNavbar.innerHTML = menusContent;
}

function fillSubmenus(currentMenu) {
    ajaxGET('/RestMenu/findAllSubmenusByMenu/' + currentMenu, {id_menu: currentMenu}).done(function (data) {
        if (data === undefined || data === null) {
            showAlert(
                'No se pudo obtener el menú',
                'warning',
                'Ocurrio un problema obteniendo el menú',
                true,
                false
            );
        } else {
            let submenusContent = '';
            data.forEach(function (submenu) {
                submenusContent = submenusContent + `
                <li class="nav-item" style="font-size: 12px;">
                    <a class="nav-link" href="/${submenu.enlace}" target="_self">                       
                        <span class="nav-link-text text-wrap"><i class="${submenu.icono}" style="line-height: 18px;"></i>${submenu.etiqueta}</span>
                    </a>
                </li>
                `;
            });
            submenuNavbar.innerHTML = submenusContent;
        }
    });
}
