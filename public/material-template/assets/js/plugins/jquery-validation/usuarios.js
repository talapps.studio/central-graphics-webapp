//$(document).ready(function () {
//    $("#pass").on("change", function () {
//        validarPass("/password", $("#pass").val(), $("#nombreUsuario").val());
//    });
//
//});
//function validarPass(url, password, username) {
//    var result = getRequest(url, {password: password, username: username});
//    result.done(function (data) {
//        if (data.data === false) {
//            Swal.fire({
//                icon: 'warning',
//                title: "Contraseña incorrecta",
//                showConfirmButton: true
//            });
//        }
//    });
//}
function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    }).done(function (data) {

    })
            .fail(function (xhr, status, errorThrown) {
            });
}

$("#modalForm").validate({
    errorClass: 'text-danger',
    rules: {
        passModificada: {
            required: true,
            minlength: 8,
            maxlength: 60
        },
        passModificadaConfirmacion: {
            required: true,
            minlength: 8,
            equalTo: "#passModificada"
        },
        pass: {
            required: true,
            minlength: 8,
            remote: {
                url: "/password",
                type: "GET",
                data: {
                    password: function () {
                        return $("#pass").val();
                    },
                    username: function () {
                        return $("#nombreUsuario").val();
                    }
                }
            }
        }
    }

});

function buildOptionListRoles(url, selectRef, roles) {
    var result = getRequest(url);
    result.done(function (data) {
        var options = "";
        var i = 0;
        $.each(data.data, function (key, item) {
            if (item.id === roles.map(obj => obj.id)[i]) {
                options += "<option selected value='" + item.id + "'>" + item.tipoRol + "</option>";
                i++;
            } else
                options += "<option value='" + item.id + "'>" + item.tipoRol + "</option>";
        });
        selectRef.html(options);
    });
}
