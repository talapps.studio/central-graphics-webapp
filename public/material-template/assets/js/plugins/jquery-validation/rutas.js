$(document).ready(function () {
    $(".pais-origen-select-list").on("change", function () {
        buildOptionListEstado(apiContextPath +"/ruta/estadoPorPais", $(".pais-origen-select-list").val(), $(".estado-origen-select-list"));
    });
    $(".pais-destino-select-list").on("change", function () {
        buildOptionListEstado(apiContextPath +"/ruta/estadoPorPais", $(".pais-destino-select-list").val(), $(".estado-destino-select-list"));
    });
});

function buildOptionListEstado(url, identifier, selectRef, idEstado) {
    var result = getRequest(url, {id: identifier});
    result.done(function (data) {
        var options = "";
        $.each(data.data, function (key, item) {
            if (item.id === idEstado) {
                options += "<option selected value='" + item.id + "'>" + item.nombreEstado + "</option>";
            } else {
                options += "<option value='" + item.id + "'>" + item.nombreEstado + "</option>";
            }
        });
        selectRef.html(options);
    });
}
function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    }).done(function (data) {
     
    }).fail(function (xhr, status, errorThrown){});
}