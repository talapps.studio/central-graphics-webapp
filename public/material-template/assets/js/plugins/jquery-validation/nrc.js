$.validator.addMethod( "nrc", function( value, element ) {
    return this.optional(element) || /^\d{6}[-]\d{1}$/.test(value);
}, "El formato del NRC no es correcto, ejemplo(000000-0)");
