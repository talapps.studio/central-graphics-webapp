/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

$(document).ajaxSend(function (e, xhr, options) {
    xhr.setRequestHeader(header, token);
});*/

function buildOptionListDocumentosBase(url, selectRef, documentos) {
    var result = getRequest(url);
    var documentosOrdenados = documentos.sort(function (a, b) {
        if (a.id > b.id) {
            return 1;
        }
        if (a.id < b.id) {
            return -1;
        }
        return 0;
    });
    result.done(function (data) {
        var options = "";
        var i = 0;
        $.each(data.data, function (key, item) {
            if (item.id === documentos.map(doc => doc.id)[i]) {
                options += "<option selected value='" + item.id + "'>" + item.nombre + "</option>";
                i++;
            } else
                options += "<option value='" + item.id + "'>" + item.nombre + "</option>";
        });
        selectRef.html(options);
    });
    function getRequest(url, data, successMessage) {
        return $.get({
            url: url,
            data: data
        }).done(function (data) {

        })
                .fail(function (xhr, status, errorThrown) {
                });
    }
}

