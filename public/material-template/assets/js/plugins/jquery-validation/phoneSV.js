$.validator.addMethod( "sphone", function( value, element ) {
    return this.optional(element) || /^\d{4}[-]\d{4}$/.test(value);
}, "El formato de tel\u00E9fono no es correcto, ejemplo( 0000-0000 )");
