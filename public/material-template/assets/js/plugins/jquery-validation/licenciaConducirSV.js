$.validator.addMethod( "licencia", function( value, element ) {
    return this.optional(element) || /^\d{4}[-]\d{6}[-]\d{3}[-]\d{1}$/.test(value);
}, "El formato de la licencia no es correcto, ejemplo(0000-000000-000-0");
