$.validator.addMethod( "dui", function( value, element ) {
    return this.optional(element) || /^\d{8}[-]\d{1}$/.test(value);
}, "El formato del DUI no es correcto, ejemplo(00000000-0)");
$.validator.addMethod( "edui", function( value, element ) {
    return this.optional(element) || /^\d{8}[-]\d{1}$/.test(value);
}, "El formato del DUI no es correcto, ejemplo(00000000-0)");