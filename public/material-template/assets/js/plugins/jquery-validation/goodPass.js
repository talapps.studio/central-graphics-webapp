$.validator.addMethod( "goodPass", function( value, element ) {
    return this.optional(element) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,25}$/.test(value);
}, "La contrase&ntilde;a no cumple con los requerimientos m&iacute;nimos de seguridad (May&uacute;sculas y min&uacute;sculas, caracteres especiales y n&uacute;meros).");
