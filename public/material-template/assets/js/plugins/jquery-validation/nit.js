$.validator.addMethod( "nit", function( value, element ) {
    return this.optional(element) || /^\d{4}[-]\d{6}[-]\d{3}[-]\d{1}$/.test(value);
}, "El formato del NIT no es correcto, ejemplo(0000-000000-000-0)");
