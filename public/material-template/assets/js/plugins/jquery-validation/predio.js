/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    $(".pais-select-list").on("change", function () {
        buildOptionListEstado(apiContextPath + "/ruta/estadoPorPais", $(".pais-select-list").val(), $(".estado-select-list"));
    });
    
});

function buildOptionListEstado(url, identifier, selectRef, idEstado) {
    var result = getRequest(url, {id: identifier});

    result.done(function (data) {        
        var options = "";
        $.each(data.data, function (key, item) {
            if (item.id === idEstado) {
                options += "<option selected value='" + item.id + "'>" + item.nombreEstado + "</option>";
            } else {
                options += "<option value='" + item.id + "'>" + item.nombreEstado + "</option>";
            }
        });
        selectRef.html(options);
    });
}
function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    }).done(function (data) {      
    })
            .fail(function (xhr, status, errorThrown) {
            });
}


