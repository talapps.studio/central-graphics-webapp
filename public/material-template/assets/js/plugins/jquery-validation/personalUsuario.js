/*
$(document).ready(function () {
    $("#pass").on("change", function () {
        validarPass("/password", $("#pass").val(), $("#nombreUsuario").val());
    });

});
function validarPass(url, password, username) {
    var result = getRequest(url, {password: password, username: username});
    result.done(function (data) {
        if (data.data === true) {

        } else {
            Swal.fire({
                  icon: 'warning',
                  title: "Contraseña incorrecta",
                  showConfirmButton: true
          });
        }
    });
};*/
function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    }).done(function (data) {

    })
            .fail(function (xhr, status, errorThrown) {
            });
}

$("#modalForm").validate({
    rules: {
        passModificada: {
            required: true,
            minlength: 6,
            maxlength: 60
        },
        passModificadaConfirmacion: {
            required: true,
            minlength: 6,
            equalTo: "#passModificada"
        },
        pass: {
            required: true,
            minlength: 8,
            remote: {
                url: "/password",
                type: "GET",
                data: {
                    password: function () {
                        return $("#pass").val();
                    },
                    username: function () {
                        return $("#nombreUsuario").val();
                    }
                }
            }
        }
    }
});



