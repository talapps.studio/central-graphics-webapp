/*
* APP CONSTANT
* */
const contextPath = window.location.origin;
//const contextPath = "";
const ajaxRequestTimeout = 60000;

const defSuccessMessage = 'Cambios guardados correctamente';
const defErrorMessage = 'Error al intentar guardar cambios';

let mainDatatable;
let datatableData = {};


jQuery.validator.setDefaults({
    errorElement: 'span',
    errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-success');
        $(element).closest('.form-group').addClass('has-error');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).closest('.form-group').removeClass('has-error');
        $(element).closest('.form-group').addClass('has-success');
    }
});

/*
* */

function ajaxGET(url, data) {
    return $.ajax({
        url: contextPath + url,
        type: 'GET',
        data: JSON.stringify(data),
        contentType: "application/json",
        dataType: 'json',
        timeout: ajaxRequestTimeout
    });
}

function ajaxMethod(type, url, dataObject, successMessage = defSuccessMessage, errorMessage = defErrorMessage) {
    return $.ajax({
        url: contextPath + url,
        type: type,
        data: JSON.stringify(dataObject),
        contentType: "application/json",
        dataType: 'json',
        timeout: ajaxRequestTimeout
    });
}

function showAlert(title, icon, html, showCloseBtn, showCancelBtn) {
    Swal.fire({
        title: title,
        icon: icon,
        html: html,
        showCloseButton: showCloseBtn,
        showCancelButton: showCancelBtn,
        focusConfirm: false,
        confirmButtonText: '<i class="fa fa-thumbs-up"></i> Aceptar!',
        confirmButtonAriaLabel: 'Aceptar!',
        cancelButtonText: '<i class="fa fa-thumbs-down"></i> Cancelar',
        cancelButtonAriaLabel: 'Cancelar'
    })
}

function showDeleteAlert(title, url) {
    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-danger',
            cancelButton: 'btn btn-secondary'
        },
        buttonsStyling: true
    });
    swalWithBootstrapButtons.fire({
        title: title,
        text: "¿Desea eliminar el elemento seleccionado?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar',
        reverseButtons: true
    }).then((result) => {
        if (result.isConfirmed) {
            ajaxMethod('DELETE', url, {}).done(function () {
                swalWithBootstrapButtons.fire(
                    'Eliminado!',
                    'El elemento seleccionado fue eliminado correctamente.',
                    'success'
                )
            }).fail(function () {
                swalWithBootstrapButtons.fire(
                    'Error!',
                    'No se pudo eliminar el elemento seleccionado.',
                    'error'
                )
            }).always(function () {reloadMainDatatable()});
        }
    });
}

function initMainDatatable(type, url, data, columns, columnDefs, funcRow) {
    mainDatatable = $('#mainTable').DataTable({
        "language": {
            "url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"
        },
        dom: 'Blfrtip',
        buttons: [
            { "extend": 'csv', "text":'Exporta CSV',"className": 'btn btn-outline-secondary btn-outline', exportOptions: {
                    columns: "thead th:not(.noExport)"
                } },
            { "extend": 'excel', "text":'Exportar Excel',"className": 'btn btn-outline-secondary btn-outline', exportOptions: {
                    columns: "thead th:not(.noExport)"
                } },
            { "extend": 'pdf', orientation: 'landscape', "text":'Exportar PDF',"className": 'btn btn-outline-secondary btn-outline', exportOptions: {
                    columns: "thead th:not(.noExport)"
                } }
        ],
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Todos"]],
        "processing": true,
        "ajax": {
            "dataType": 'json',
            "type": type,
            "url": contextPath + url,
            data() {
                return datatableData;
            },
            dataSrc: ''
        },
        "aaSorting": [],
        "columns": columns,
        "columnDefs": [
            {
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "data": null,
                "width": "10%",
                "className": "text-center",
                "defaultContent": columnDefs
            },
            {
                targets: '_all',
                defaultContent: '<span class="badge badge-pill badge-secondary">Vacío</span>'
            }
        ],
        "fnRowCallback": validateDatatableRowFunction(funcRow)
    });
}

function reloadMainDatatable() {
    mainDatatable.ajax.reload();
}
function getDataRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data
    })
        .done(function (data) {
        })
        .fail(function (xhr, status, errorThrown) {
        });
}
function getRequest(url, data) {
    $("#preloader").show();
    return ajaxGET(url, data)
        .fail(function (xhr, status, errorThrown) {
            toastr.warning('No se pudo realizar la conección con el servidor correctamente.', 'Error de conección', {timeOut: 5000});
        }).always(function () {
            $("#preloader").hide();
        });
}

function ajaxRequest(type, url, dataObject, successMessage = defSuccessMessage, errorMessage = defErrorMessage) {
    $("#preloader").show();
    $(".modal-footer button").addClass("disabled");
    return ajaxMethod(type, url, dataObject, successMessage, errorMessage)
        .done(function (data) {
            showAlert(successMessage, 'success', null, false, false, 1500, null);
        })
        .fail(function (xhr, status, errorThrown) {
            showAlert(errorMessage, 'error', null, false, false, 1500, null)
        })
        .always(function() {
            hideAllModals();
            reloadMainDatatable();
            $(".modal-footer button").removeClass("disabled");
            $("#preloader").hide();
        });
}

function hideAllModals() {
    $('.modal').modal('hide');
}

function clearValidation(formElement){
    document.querySelectorAll('.CRUD-element').forEach(item => {
        item.value = null;
        $(item).closest('.form-group').removeClass('has-success');
        $(item).closest('.form-group').removeClass('has-error');
    });
}
function clearColorValidation(form){
    $(form).validate().resetForm();
    document.querySelectorAll('.clear-element').forEach(item=>{
        item.value = null;
        $(item).closest('.form-group').removeClass('has-success');
        $(item).closest('.form-group').removeClass('has-error');
    });
}
function validateDatatableRowFunction(f) {
    if (f !== null && f !== undefined) {
        return f;
    } else {
        return function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if (aData['visible'] === false){
                $('td', nRow).addClass("table-danger");
                $('td:last-child', nRow).empty();
                $('td:last-child', nRow).append('<span class="badge badge-pill badge-danger">No disponible</span>')
            }
        }
    }
}

function validEmptyField(field) {
    if (field === undefined || field === null || field.length === 0)
        return "N/A";
    return field;
}

document.querySelectorAll('.modal-cancel-btn').forEach(item => {
    item.addEventListener('click', evt => {
        clearValidation(saveForm);
        clearValidation(editForm);
    });
});

let forms = document.querySelectorAll('form');
for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('input', function (e) {
        if (e.target.classList.contains('CRUD-element')) {
            CRUDModel[e.target.getAttribute("name")] = e.target.value;
        }
    });
}

function removeOptions(selectElement) {
    let i, L = selectElement.options.length - 1;
    for(i = L; i >= 0; i--) {
        selectElement.remove(i);
    }
}

function changeSelectedOption(element, selectedId) {
    let options = element.options;
    element.value = selectedId;
    for (let i = 0; i < options.length; i++) {
        if (options[i].value == selectedId) {
            options[i].setAttribute('selected', 'true');
        } else {
            options[i].removeAttribute('selected');
        }
    }
}

function fillSelectOptions(element, data, selectedId) {
    element.innerHTML = '<option value="" selected disabled>Seleccione una opción</option>';
    if (data !== undefined && data !== null) {
        data.forEach(function (item) {
            let opt = document.createElement('option');
            opt.value = item.ID;
            opt.innerHTML = item.NOMBRE;
            opt.selected = item.ID === selectedId;
            element.appendChild(opt);
        });
    }
}

function addShowAllOption(element)
{
    let opt = document.createElement('option');
    opt.value = '';
    opt.innerHTML = 'SELECCIONAR TODOS';
    element.appendChild(opt);
}

function random_rgba() {
    const o = Math.round, r = Math.random, s = 200;
    return 'rgba(' + o(r()*s) + ',' + o(r()*s) + ',' + o(r()*s) + ',' + 1 + ')';
}

function updateCombobox(url, name, element, selectedId, showAllOption) {
    getRequest(url).done(function (data) {
        if (data.length === 0) {
            showAlert('Búsqueda sin resultado', 'info', 'No se encontraron '+ name +' relacionadas a la gerencia seleccionada.', true, false);
        } else {
            fillSelectOptions(element, data, selectedId);
        }
        if (showAllOption) {
            addShowAllOption(element);
        }
    });
}

function fillComboboxWaterfall(selectedId, toFindList, valueName, toUpdateElement) {
    for (let i = 0; i < toFindList.length; i++) {
        if (toFindList[i]['ID'] === selectedId) {
            changeSelectedOption(toUpdateElement, toFindList[i][valueName]);
            return toFindList[i][valueName];
        }
    }
}

function makeid(length) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;
    for ( let i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

function onLogout() {
    Swal.fire({
        title: 'Confirmación',
        text: "¿Esta seguro que desea cerrar la sesión?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Cerrar sesión',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            sessionStorage.clear();
            window.location.href = "/logout";
        }
    });
}

$(document).ajaxStart(function(){
    $("#preloader").show();
});
$(document).ajaxStop(function(){
    $("#preloader").hide();
});
/*const opts = {
    lines: 13, // The number of lines to draw
    length: 38, // The length of each line
    width: 17, // The line thickness
    radius: 45, // The radius of the inner circle
    scale: 1, // Scales overall size of the spinner
    corners: 1, // Corner roundness (0..1)
    speed: 1, // Rounds per second
    rotate: 0, // The rotation offset
    animation: 'spinner-line-shrink', // The CSS animation name for the lines
    direction: 1, // 1: clockwise, -1: counterclockwise
    color: '#4d4d4d', // CSS color or array of colors
    fadeColor: 'transparent', // CSS color or array of colors
    top: '50%', // Top position relative to parent
    left: '50%', // Left position relative to parent
    shadow: '0 0 1px transparent', // Box-shadow for the lines
    zIndex: 2000000000, // The z-index (defaults to 2e9)
    className: 'spinner', // The CSS class to assign to the spinner
    position: 'absolute', // Element positioning
};

const loaderSpinnerTarget = document.getElementById('loader-spinner');
const spinner = new Spinner(opts).spin(loaderSpinnerTarget);

spinner.stop();


function showLoaderSpinner() {
    document.getElementById('mainBody').classList.add('loading-spinner');
    spinner.spin(loaderSpinnerTarget);
}

function stopLoaderSpinner() {
    document.getElementById('mainBody').classList.remove('loading-spinner');
    spinner.stop();
}
*/