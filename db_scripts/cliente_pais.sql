-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-04-2022 a las 21:40:49
-- Versión del servidor: 10.4.22-MariaDB
-- Versión de PHP: 7.3.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `seguimiento_10x`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_pais`
--

CREATE TABLE `cliente_pais` (
  `id_pais_cliente` int(11) NOT NULL,
  `abreviatura` varchar(10) NOT NULL,
  `nombre_pais` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente_pais`
--

INSERT INTO `cliente_pais` (`id_pais_cliente`, `abreviatura`, `nombre_pais`) VALUES
(1, 'sv', 'El Salvador'),
(2, 'hn', 'Honduras'),
(3, 'cr', 'Costa Rica');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cliente_pais`
--
ALTER TABLE `cliente_pais`
  ADD PRIMARY KEY (`id_pais_cliente`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cliente_pais`
--
ALTER TABLE `cliente_pais`
  MODIFY `id_pais_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
