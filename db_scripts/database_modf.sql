/*
    Actualiza tamaño de la columna contraseña para que sea de 60 caracteres (Bcrypt hash size 60 characters)
*/
alter table usuario modify contrasena varchar (60) default '' not null;

/*
    Agrega tabla cliente_pais
*/
create table cliente_pais
(
    id_pais_cliente int         not null,
    abreviatura     varchar(10) not null,
    nombre_pais     varchar(50) not null
);

/*
    Agrega las columnas descripcion e icono a la tabla de menu
*/
alter table menu
    add descripcion varchar(150) null;

alter table menu
    add icono varchar(50) null;

/*
  Se agrega la columna token y expiration_token a la tabla de usuarios
*/
alter table usuario
    add token varchar(200)
alter table usuario
    add expiration_token datetime

/* se agrega columna desactivado a la tabla de inventario_proveedor
*/
--
-- Estructura de tabla para la tabla `inventario_proveedor`
--

CREATE TABLE `inventario_proveedor`
(
    `id_inventario_proveedor` int(11) NOT NULL,
    `proveedor_nombre`        varchar(250) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
    `usuario`                 varchar(10) COLLATE utf8_unicode_ci  NOT NULL DEFAULT '',
    `contrasena`              varchar(30) COLLATE utf8_unicode_ci  NOT NULL DEFAULT '',
    `id_grupo`                int(4) NOT NULL,
    `desactivado`             bit(1)                               NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Se crea tabla para la cotizacion de servicios predeterminados 
*/
CREATE TABLE `seguimiento_10x`.`cotizacion_servicios_predeterminados`
(
    `id_servicios_predeterminados` INT(11) NOT NULL AUTO_INCREMENT,
    `descripcion`                  VARCHAR(200)  NOT NULL,
    `costo`                        DECIMAL(5, 2) NOT NULL,
    PRIMARY KEY (`id_servicios_predeterminados`)
) ENGINE = InnoDB;
/*Se crea tabla para productos_predeterminados
*/
--
-- Estructura de tabla para la tabla `productos_predeterminados`
--

CREATE TABLE `productos_predeterminados`
(
    `id_productos_predeterminados` int(11) NOT NULL,
    `descripcion`                  varchar(200)  NOT NULL,
    `costo`                        decimal(5, 2) NOT NULL,
    `activo`                       bit(1)        NOT NULL DEFAULT b'1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
/*Se agrega campo activo en tabla inventario_material
*/
ALTER TABLE `inventario_material`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `mat_pais`;
/*Se agrega campo activo en tabla cliente_pais
*/
ALTER TABLE `cliente_pais`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `nombre_pais`;
/*Se agrega campo activo para tabla cotizacion_servicios_predeterminados
*/
ALTER TABLE `cotizacion_servicios_predeterminados`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `costo`;
/*Se agrega campo de activo para tabla de proceso 
*/
ALTER TABLE `procesos`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `nombre`;
/*se agrega tabla de ruta detalle
*/
CREATE TABLE `seguimiento_10x`.`ruta_detalle`
(
    `id_ruta_detalle` INT(11) NOT NULL AUTO_INCREMENT,
    `nombre`          VARCHAR(250) NOT NULL,
    `descripcion`     VARCHAR(250) NOT NULL,
    `activo`          BIT(1)       NOT NULL DEFAULT b'1',
    PRIMARY KEY (`id_ruta_detalle`)
) ENGINE = InnoDB;
/*Se agrega tabla ruta predeteminada
*/
CREATE TABLE `seguimiento_10x`.`ruta_predeterminada`
(
    `id_ruta_pred` INT          NOT NULL AUTO_INCREMENT,
    `nombre`       VARCHAR(200) NOT NULL,
    `elementos`    VARCHAR(200) NOT NULL,
    `activo`       BIT(1) NULL DEFAULT b'1',
    PRIMARY KEY (`id_ruta_pred`)
) ENGINE = InnoDB;

/*Tabla relacion tabla ruta_predeterminada  con ruta_detalle
*/
CREATE TABLE `seguimiento_10x`.`ruta_predeterminada_detalle_elemento`
(
    `id_ruta_predeterminada_detalle` INT(11) NOT NULL AUTO_INCREMENT,
    `id_ruta_predeterminada`         INT(11) NOT NULL,
    `id_ruta_detalle`                INT(11) NOT NULL,
    PRIMARY KEY (`id_ruta_predeterminada_detalle`)
) ENGINE = InnoDB;
/*Se agrega campo activo a las tablas siguientes */
ALTER TABLE `pla_medicion_comp`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'0' AFTER `compensacion`;

ALTER TABLE `pla_medicion_plancha`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `plancha`;

ALTER TABLE `pla_medicion_sist`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `sistema`;

ALTER TABLE `pla_medicion_alt`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `altura`;

ALTER TABLE `pla_medicion_tra`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER `trama`;
ALTER TABLE `pla_medicion_lin`
    ADD `activo` BIT(1) NOT NULL DEFAULT b'1' AFTER 'lineaje';

/*
    Agrega columnas para almacenar los usuarios que crean, actualizan y eliminan registros
*/
alter table actualizar_reportes
    add created_by int null;

alter table actualizar_reportes
    add updated_by int null;

alter table actualizar_reportes
    add deleted_by int null;

alter table bsc_datos
    add created_by int null;

alter table bsc_datos
    add updated_by int null;

alter table bsc_datos
    add deleted_by int null;

alter table cliente
    add created_by int null;

alter table cliente
    add updated_by int null;

alter table cliente
    add deleted_by int null;

alter table cliente_anilox
    add created_by int null;

alter table cliente_anilox
    add updated_by int null;

alter table cliente_anilox
    add deleted_by int null;

alter table cliente_contacto
    add created_by int null;

alter table cliente_contacto
    add updated_by int null;

alter table cliente_contacto
    add deleted_by int null;

alter table cliente_grupo
    add created_by int null;

alter table cliente_grupo
    add updated_by int null;

alter table cliente_grupo
    add deleted_by int null;

alter table cliente_maquina
    add created_by int null;

alter table cliente_maquina
    add updated_by int null;

alter table cliente_maquina
    add deleted_by int null;

alter table cliente_maquina
    add created_by int null;

alter table cliente_maquina
    add updated_by int null;

alter table cliente_maquina
    add deleted_by int null;

alter table cliente_pais
    add created_by int null;

alter table cliente_pais
    add updated_by int null;

alter table cliente_pais
    add deleted_by int null;

alter table cliente_placa
    add created_by int null;

alter table cliente_placa
    add updated_by int null;

alter table cliente_placa
    add deleted_by int null;

alter table constante_polimero
    add created_by int null;

alter table constante_polimero
    add updated_by int null;

alter table constante_polimero
    add deleted_by int null;

alter table cotizacion_servicios_predeterminados
    add created_by int null;

alter table cotizacion_servicios_predeterminados
    add updated_by int null;

alter table cotizacion_servicios_predeterminados
    add deleted_by int null;

alter table departamentos
    add created_by int null;

alter table departamentos
    add updated_by int null;

alter table departamentos
    add deleted_by int null;

alter table departamentos_tiempo_habil
    add created_by int null;

alter table departamentos_tiempo_habil
    add updated_by int null;

alter table departamentos_tiempo_habil
    add deleted_by int null;

alter table especificacion_colores
    add created_by int null;

alter table especificacion_colores
    add updated_by int null;

alter table especificacion_colores
    add deleted_by int null;

alter table especificacion_general
    add created_by int null;

alter table especificacion_general
    add updated_by int null;

alter table especificacion_general
    add deleted_by int null;

alter table especificacion_guias
    add created_by int null;

alter table especificacion_guias
    add updated_by int null;

alter table especificacion_guias
    add deleted_by int null;

alter table especificacion_matrecgru
    add created_by int null;

alter table especificacion_matrecgru
    add updated_by int null;

alter table especificacion_matrecgru
    add deleted_by int null;

alter table extra
    modify fecha date null;

alter table extra
    add created_at datetime null;

alter table extra
    add updated_at datetime null;

alter table extra
    add deleted_at datetime null;

alter table extra
    add created_by int null;

alter table extra
    add updated_by int null;

alter table extra
    add deleted_by int null;

alter table extra_contra
    add created_at datetime null;

alter table extra_contra
    add updated_at datetime null;

alter table extra_contra
    add deleted_at datetime null;

alter table extra_contra
    add created_by int null;

alter table extra_contra
    add updated_by int null;

alter table extra_contra
    add deleted_by int null;

alter table extra_otro
    add created_by int null;

alter table extra_otro
    add updated_by int null;

alter table extra_otro
    add deleted_by int null;

alter table extra_pedido
    add created_by int null;

alter table extra_pedido
    add updated_by int null;

alter table extra_pedido
    add deleted_by int null;

alter table extra_proy
    add created_by int null;

alter table extra_proy
    add updated_by int null;

alter table extra_proy
    add deleted_by int null;

alter table extra_recibo
    add created_by int null;

alter table extra_recibo
    add updated_by int null;

alter table extra_recibo
    add deleted_by int null;

alter table grupos
    add created_by int null;

alter table grupos
    add updated_by int null;

alter table grupos
    add deleted_by int null;

alter table inventario_bodega_9000
    add created_by int null;

alter table inventario_bodega_9000
    add updated_by int null;

alter table inventario_bodega_9000
    add deleted_by int null;

alter table inventario_equipo
    add created_by int null;

alter table inventario_equipo
    add updated_by int null;

alter table inventario_equipo
    add deleted_by int null;

alter table inventario_lote
    add created_at datetime null;

alter table inventario_lote
    add updated_at datetime null;

alter table inventario_lote
    add deleted_at datetime null;

alter table inventario_lote
    add created_by int null;

alter table inventario_lote
    add updated_by int null;

alter table inventario_lote
    add deleted_by int null;

alter table inventario_material
    add created_by int null;

alter table inventario_material
    add updated_by int null;

alter table inventario_material
    add deleted_by int null;

alter table inventario_material_detalle
    add created_by int null;

alter table inventario_material_detalle
    add updated_by int null;

alter table inventario_material_detalle
    add deleted_by int null;

alter table inventario_material_proveedor
    add created_by int null;

alter table inventario_material_proveedor
    add updated_by int null;

alter table inventario_material_proveedor
    add deleted_by int null;

alter table inventario_proveedor
    add created_by int null;

alter table inventario_proveedor
    add updated_by int null;

alter table inventario_proveedor
    add deleted_by int null;

alter table inventario_requisicion
    add created_at datetime null;

alter table inventario_requisicion
    add updated_at datetime null;

alter table inventario_requisicion
    add deleted_at datetime null;

alter table inventario_requisicion
    add created_by int null;

alter table inventario_requisicion
    add updated_by int null;

alter table inventario_requisicion
    add deleted_by int null;

alter table inventario_tipo
    add created_by int null;

alter table inventario_tipo
    add updated_by int null;

alter table inventario_tipo
    add deleted_by int null;

alter table inventario_tipo_material
    add created_by int null;

alter table inventario_tipo_material
    add updated_by int null;

alter table inventario_tipo_material
    add deleted_by int null;

alter table material_recibido
    add created_by int null;

alter table material_recibido
    add updated_by int null;

alter table material_recibido
    add deleted_by int null;

alter table material_recibido_grupo
    add created_by int null;

alter table material_recibido_grupo
    add updated_by int null;

alter table material_recibido_grupo
    add deleted_by int null;

alter table material_solicitado
    add created_by int null;

alter table material_solicitado
    add updated_by int null;

alter table material_solicitado
    add deleted_by int null;

alter table material_solicitado_grupo
    add created_by int null;

alter table material_solicitado_grupo
    add updated_by int null;

alter table material_solicitado_grupo
    add deleted_by int null;

alter table mc_linea
    add created_by int null;

alter table mc_linea
    add updated_by int null;

alter table mc_linea
    add deleted_by int null;

alter table mc_linea_presupuesto
    add created_by int null;

alter table mc_linea_presupuesto
    add updated_by int null;

alter table mc_linea_presupuesto
    add deleted_by int null;

alter table menu_departamento
    add created_by int null;

alter table menu_departamento
    add updated_by int null;

alter table menu_departamento
    add deleted_by int null;

alter table observacion
    modify fecha_hora datetime null;

alter table observacion
    add created_at datetime null;

alter table observacion
    add updated_at datetime null;

alter table observacion
    add deleted_at datetime null;

alter table observacion
    add created_by int null;

alter table observacion
    add updated_by int null;

alter table observacion
    add deleted_by int null;

alter table pedido_adjuntos
    add created_by int null;

alter table pedido_adjuntos
    add updated_by int null;

alter table pedido_adjuntos
    add deleted_by int null;

alter table pedido
    add created_at datetime null;

alter table pedido
    add updated_at datetime null;

alter table pedido
    add deleted_at datetime null;

alter table pedido
    add created_by int null;

alter table pedido
    add updated_by int null;

alter table pedido
    add deleted_by int null;

alter table pedido_pedido
    add created_by int null;

alter table pedido_pedido
    add updated_by int null;

alter table pedido_pedido
    add deleted_by int null;

alter table pedido_cotizacion
    add created_by int null;

alter table pedido_cotizacion
    add updated_by int null;

alter table pedido_cotizacion
    add deleted_by int null;

alter table pedido_cronograma
    add created_by int null;

alter table pedido_cronograma
    add updated_by int null;

alter table pedido_cronograma
    add deleted_by int null;

alter table pedido_material_solicitud
    add created_by int null;

alter table pedido_material_solicitud
    add updated_by int null;

alter table pedido_material_solicitud
    add deleted_by int null;

alter table pedido_nota_envio
    modify fecha datetime null;

alter table pedido_nota_envio
    add created_at datetime null;

alter table pedido_nota_envio
    add updated_at datetime null;

alter table pedido_nota_envio
    add deleted_at datetime null;

alter table pedido_nota_envio
    add created_by int null;

alter table pedido_nota_envio
    add updated_by int null;

alter table pedido_nota_envio
    add deleted_by int null;

alter table pedido_nota_material
    add created_by int null;

alter table pedido_nota_material
    add updated_by int null;

alter table pedido_nota_material
    add deleted_by int null;

alter table pedido_rechazo
    modify fecha datetime null;

alter table pedido_rechazo
    add created_at datetime null;

alter table pedido_rechazo
    add updated_at datetime null;

alter table pedido_rechazo
    add deleted_at datetime null;

alter table pedido_rechazo
    add created_by int null;

alter table pedido_rechazo
    add updated_by int null;

alter table pedido_rechazo
    add deleted_by int null;

alter table pedido_transito
    add created_by int null;

alter table pedido_transito
    add updated_by int null;

alter table pedido_transito
    add deleted_by int null;

alter table pedido_transito_cantidad
    add created_by int null;

alter table pedido_transito_cantidad
    add updated_by int null;

alter table pedido_transito_cantidad
    add deleted_by int null;

alter table pedido_transito_solicitud
    add created_by int null;

alter table pedido_transito_solicitud
    add updated_by int null;

alter table pedido_transito_solicitud
    add deleted_by int null;

alter table placa_marca
    add created_by int null;

alter table placa_marca
    add updated_by int null;

alter table placa_marca
    add deleted_by int null;

alter table planchas
    add created_by int null;

alter table planchas
    add updated_by int null;

alter table planchas
    add deleted_by int null;

alter table plancha_cantidad
    add created_by int null;

alter table plancha_cantidad
    add updated_by int null;

alter table plancha_cantidad
    add deleted_by int null;

alter table pla_medicion
    add created_by int null;

alter table pla_medicion
    add updated_by int null;

alter table pla_medicion
    add deleted_by int null;

alter table pla_medicion_alt
    add created_by int null;

alter table pla_medicion_alt
    add updated_by int null;

alter table pla_medicion_alt
    add deleted_by int null;

alter table pedido_sap
    add created_at datetime null;

alter table pedido_sap
    add updated_at datetime null;

alter table pedido_sap
    add deleted_at datetime null;

alter table pedido_sap
    add created_by int null;

alter table pedido_sap
    add updated_by int null;

alter table pedido_sap
    add deleted_by int null;

alter table pla_medicion_color
    add created_by int null;

alter table pla_medicion_color
    add updated_by int null;

alter table pla_medicion_color
    add deleted_by int null;

alter table pla_medicion_comp
    add created_by int null;

alter table pla_medicion_comp
    add updated_by int null;

alter table pla_medicion_comp
    add deleted_by int null;

alter table pla_medicion_lin
    add created_by int null;

alter table pla_medicion_lin
    add updated_by int null;

alter table pla_medicion_lin
    add deleted_by int null;

alter table pla_medicion_plancha
    add created_by int null;

alter table pla_medicion_plancha
    add updated_by int null;

alter table pla_medicion_plancha
    add deleted_by int null;

alter table pla_medicion_sist
    add created_by int null;

alter table pla_medicion_sist
    add updated_by int null;

alter table pla_medicion_sist
    add deleted_by int null;

alter table pla_medicion_tra
    add created_by int null;

alter table pla_medicion_tra
    add updated_by int null;

alter table pla_medicion_tra
    add deleted_by int null;

alter table plancha_mensual
    modify fecha date null;

alter table plancha_mensual
    add created_at datetime null;

alter table plancha_mensual
    add updated_at datetime null;

alter table plancha_mensual
    add deleted_at datetime null;

alter table plancha_mensual
    add created_by int null;

alter table plancha_mensual
    add updated_by int null;

alter table plancha_mensual
    add deleted_by int null;

alter table procesos
    add created_by int null;

alter table procesos
    add updated_by int null;

alter table procesos
    add deleted_by int null;

alter table proceso_imagenes
    add created_by int null;

alter table proceso_imagenes
    add updated_by int null;

alter table proceso_imagenes
    add deleted_by int null;

alter table producto
    add created_by int null;

alter table producto
    add updated_by int null;

alter table producto
    add deleted_by int null;

alter table producto_pedido
    add created_by int null;

alter table producto_pedido
    add updated_by int null;

alter table producto_pedido
    add deleted_by int null;

alter table productos_predeterminados
    add created_by int null;

alter table productos_predeterminados
    add updated_by int null;

alter table productos_predeterminados
    add deleted_by int null;

alter table proveedores
    add created_by int null;

alter table proveedores
    add updated_by int null;

alter table proveedores
    add deleted_by int null;

alter table rechazo_razones
    add created_by int null;

alter table rechazo_razones
    add updated_by int null;

alter table rechazo_razones
    add deleted_by int null;

alter table reporte_anual
    add created_by int null;

alter table reporte_anual
    add updated_by int null;

alter table reporte_anual
    add deleted_by int null;

alter table reporte_anual_ventas
    add created_by int null;

alter table reporte_anual_ventas
    add updated_by int null;

alter table reporte_anual_ventas
    add deleted_by int null;

alter table revision_item
    add created_by int null;

alter table revision_item
    add updated_by int null;

alter table revision_item
    add deleted_by int null;

alter table revision_sub_item
    add created_by int null;

alter table revision_sub_item
    add updated_by int null;

alter table revision_sub_item
    add deleted_by int null;

alter table revision_pedido
    add created_by int null;

alter table revision_pedido
    add updated_by int null;

alter table revision_pedido
    add deleted_by int null;

alter table ruta
    add created_by int null;

alter table ruta
    add updated_by int null;

alter table ruta
    add deleted_by int null;

alter table ruta_dpto
    add created_by int null;

alter table ruta_dpto
    add updated_by int null;

alter table ruta_dpto
    add deleted_by int null;

alter table ruta_detalle
    add created_by int null;

alter table ruta_detalle
    add updated_by int null;

alter table ruta_detalle
    add deleted_by int null;

alter table ruta_grupo
    add created_by int null;

alter table ruta_grupo
    add updated_by int null;

alter table ruta_grupo
    add deleted_by int null;

alter table ruta_predeterminada_detalle_elemento
    add created_by int null;

alter table ruta_predeterminada_detalle_elemento
    add updated_by int null;

alter table ruta_predeterminada_detalle_elemento
    add deleted_by int null;

alter table ruta_puesto
    add created_by int null;

alter table ruta_puesto
    add updated_by int null;

alter table ruta_puesto
    add deleted_by int null;

alter table sq_sess_tabla
    add created_by int null;

alter table sq_sess_tabla
    add updated_by int null;

alter table sq_sess_tabla
    add deleted_by int null;

alter table sol_cambio_fecha
    add created_by int null;

alter table sol_cambio_fecha
    add updated_by int null;

alter table sol_cambio_fecha
    add deleted_by int null;

alter table sol_cambio_fecha
    add created_by int null;

alter table sol_cambio_fecha
    add updated_by int null;

alter table sol_cambio_fecha
    add deleted_by int null;

alter table tipo_impresion
    add created_by int null;

alter table tipo_impresion
    add updated_by int null;

alter table tipo_impresion
    add deleted_by int null;

alter table tipo_impd_acabado
    add created_by int null;

alter table tipo_impd_acabado
    add updated_by int null;

alter table tipo_impd_acabado
    add deleted_by int null;

alter table tipo_impd_material
    add created_by int null;

alter table tipo_impd_material
    add updated_by int null;

alter table tipo_impd_material
    add deleted_by int null;

alter table tipo_trabajo
    add created_by int null;

alter table tipo_trabajo
    add updated_by int null;

alter table tipo_trabajo
    add deleted_by int null;

alter table usuario
    add created_by int null;

alter table usuario
    add updated_by int null;

alter table usuario
    add deleted_by int null;

alter table user_conectados
    add created_by int null;

alter table user_conectados
    add updated_by int null;

alter table user_conectados
    add deleted_by int null;

alter table usuario_grupo
    add created_by int null;

alter table usuario_grupo
    add updated_by int null;

alter table usuario_grupo
    add deleted_by int null;

alter table venta_proyeccion
    add created_by int null;

alter table venta_proyeccion
    add updated_by int null;

alter table venta_proyeccion
    add deleted_by int null;

alter table pedido_usuario
    add created_at datetime null;

alter table pedido_usuario
    add updated_at datetime null;

alter table pedido_usuario
    add deleted_at datetime null;

alter table pedido_usuario
    add created_by int null;

alter table pedido_usuario
    add updated_by int null;

alter table pedido_usuario
    add deleted_by int null;



/*
 Agrega llave primaria en tabla cliente_pais
*/
alter table cliente_pais
    add constraint cliente_pais_pk
        primary key (id_pais_cliente);

create unique index cliente_pais_id_pais_cliente_uindex
    on cliente_pais (id_pais_cliente);


/* Existe un error en un catalogo en el que se tiene un 0 como id
    asi que esto se debe arreglar y pasar el id a uno valido, este escrip puede dar error 
    si el id ya esta ocupado
*/
    -- repara el registro 0 si existe--
    UPDATE cliente_pais SET id_pais_cliente = 8 WHERE id_pais_cliente=0;
    -- arreglar la secuencia--
    ALTER TABLE cliente_pais AUTO_INCREMENT = 8;
    -- convertir en autoincremental--
    ALTER TABLE cliente_pais MODIFY COLUMN id_pais_cliente int(11) auto_increment NOT NULL;
/* -----------------------------------------------------------------------------------------*/
alter table cliente_maquina add esConfigurado bit 


CREATE TABLE `central-graphics`.color_impresora (
	id_color INT NOT NULL,
	id_impresora INT NOT NULL
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

/*  
    stock supliers debe de tener valor 0 en desactivado por defecto eso segun el codigo que esta enlinea actualmente
*/
    UPDATE inventario_proveedor SET desactivado=0
/* -----------------------------------------------------------------------------------------*/