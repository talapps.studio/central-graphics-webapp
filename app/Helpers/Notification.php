<?php

namespace App\Helpers;

class Notification
{
    function sendNotification($to,$from, $name, $subject, $message)
    {
        $email = \Config\Services::email();
        $email->setTo($to);
        $email->setFrom($from, $name);
        $email->setSubject($subject);
        $email->setMessage($message);
        return $email->send();
    }
}
