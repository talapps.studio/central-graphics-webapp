<?php

namespace App\Helpers;

class Validadores
{
    public function longitud($value){
        // double sin exponencial y sin comas positivo
        $pattern = "/\\A\\s*[+\\-]?\\d*(\\.\\d+)?\\s*\\Z/";
        return preg_match($pattern, $value) === 1 && (float) $value>0;
    }

    public function cantidadEntera($value){
        // cantidad entera mayor que cero
        return ctype_digit($value) ? 
                    is_int(intval($value)) ? 
                        intval($value)>0 ? TRUE
                        :false 
                    : false
                : false;
    }

    public function montoMonetario($value){
        // positivo, dos decimales

        $pattern = "/^((?:\d{1,3}[,\.]?)+\d*)$/";
        return preg_match($pattern, $value) === 1 && (float) $value>0;
    }
}
