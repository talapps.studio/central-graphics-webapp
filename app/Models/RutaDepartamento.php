<?php

namespace App\Models;

use CodeIgniter\Model;

class RutaDepartamento extends Model
{
    protected $table      = 'ruta_dpto';
    protected $primaryKey = 'id_ruta_dpto';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_ruta_grupo',
        'id_dpto',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}