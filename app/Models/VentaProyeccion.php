<?php

namespace App\Models;

use CodeIgniter\Model;

class VentaProyeccion extends Model
{
    protected $table      = 'venta_proyeccion';
    protected $primaryKey = 'id_proyeccion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_cliente',
        'proyeccion',
        'mes',
        'anho',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}