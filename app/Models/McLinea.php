<?php

namespace App\Models;

use CodeIgniter\Model;

class McLinea extends Model
{
    protected $table      = 'mc_linea';
    protected $primaryKey = 'id_mc_linea';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_padre',
        'codigo',
        'linea',
        'mas_menos',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}