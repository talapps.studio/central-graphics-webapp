<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoTransito extends Model
{
    protected $table      = 'pedido_transito';
    protected $primaryKey = 'id_ped_transito';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'orden',
        'cantidad',
        'cantidad_solicitada',
        'id_inventario_material',
        'tipo',
        'detalle',
        'fecha_ingreso',
        'finalizado',
        'id_grupo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}