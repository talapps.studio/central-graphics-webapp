<?php

namespace App\Models;

use CodeIgniter\Model;

class McLineaPresupuesto extends Model
{
    protected $table      = 'mc_linea_presupuesto';
    protected $primaryKey = 'id_mc_linea_presupuesto';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_mc_linea',
        'anho',
        'mes_01',
        'mes_02',
        'mes_03',
        'mes_04',
        'mes_05',
        'mes_06',
        'mes_07',
        'mes_08',
        'mes_09',
        'mes_10',
        'mes_11',
        'mes_12',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}