<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoTransitoSolicitud extends Model
{
    protected $table      = 'pedido_transito_solicitud';
    protected $primaryKey = 'id_ped_tran_sol';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_inventario_material',
        'fecha',
        'fecha_a',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}