<?php

namespace App\Models;

use CodeIgniter\Model;

class MaterialSolicitadoGrupo extends Model
{
    protected $table      = 'material_solicitado_grupo';
    protected $primaryKey = 'id_material_solicitado_grupo';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_material_solicitado',
        'id_grupo',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}