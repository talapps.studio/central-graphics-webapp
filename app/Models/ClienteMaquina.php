<?php

namespace App\Models;

use CodeIgniter\Model;

class ClienteMaquina extends Model
{
    protected $table = 'cliente_maquina';
    protected $primaryKey = 'id_cliente_maquina';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'maquina',
        'colores',
        'id_cliente',
        'esConfigurado',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}