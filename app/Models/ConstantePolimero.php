<?php

namespace App\Models;

use CodeIgniter\Model;

class ConstantePolimero extends Model
{
    protected $table      = 'constante_polimero';
    protected $primaryKey = 'id_constante_polimero';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'clave_polimero',
        'valor_polimero',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}