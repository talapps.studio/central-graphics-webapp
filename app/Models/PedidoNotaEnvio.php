<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoNotaEnvio extends Model
{
    protected $table      = 'pedido_nota_envio';
    protected $primaryKey = 'id_nota_env';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'correlativo',
        'fecha',
        'att',
        'observacion',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}