<?php
namespace App\Models;

use CodeIgniter\Model;

class CotizacionServiciosPredeterminados extends Model
{
    protected $table        = 'cotizacion_servicios_predeterminados';
    protected $primaryKey   = 'id_servicios_predeterminados';
    protected $useAutoIncrement = true;
    protected $returnType       = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'descripcion',
        'costo',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


}