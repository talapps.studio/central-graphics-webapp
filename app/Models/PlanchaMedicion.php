<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicion extends Model
{
    protected $table      = 'pla_medicion';
    protected $primaryKey = 'id_pla_medicion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_cliente',
        'compensacion',
        'plancha',
        'sistema',
        'altura',
        'trama',
        'linaje',
        'tipo',
        'version',
        'id_pedido',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}