<?php

namespace App\Models;

use CodeIgniter\Model;

class RutaPuesto extends Model
{
    protected $table      = 'ruta_puesto';
    protected $primaryKey = 'id_ruta_puesto';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_ruta',
        'id_dpto',
        'id_usuario',
        'tiempo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}