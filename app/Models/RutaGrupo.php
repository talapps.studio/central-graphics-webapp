<?php

namespace App\Models;

use CodeIgniter\Model;

class RutaGrupo extends Model
{
    protected $table = 'ruta_grupo';
    protected $primaryKey = 'id_ruta_grupo';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_grupo',
        'ruta',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;


}