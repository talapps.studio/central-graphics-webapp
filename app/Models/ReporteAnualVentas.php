<?php

namespace App\Models;

use CodeIgniter\Model;

class ReporteAnualVentas extends Model
{
    protected $table      = 'reporte_anual_ventas';
    protected $primaryKey = 'id_reporte_ventas';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'anho',
        'mes',
        'venta',
        'id_grupo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}