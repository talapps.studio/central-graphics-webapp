<?php

namespace App\Models;

use CodeIgniter\Model;

class ExtraProyecto extends Model
{
    protected $table      = 'extra_proy';
    protected $primaryKey = 'id_dextpro';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_dpto',
        'proyeccion',
        'mes',
        'anho',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}