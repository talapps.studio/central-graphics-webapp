<?php

namespace App\Models;

use CodeIgniter\Model;

class SesionRegistro extends Model
{
    protected $table = 'sq_sess_tabla';
    protected $primaryKey = 'session_id';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'ip_address',
        'user_agent',
        'last_activity',
        'user_data',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}