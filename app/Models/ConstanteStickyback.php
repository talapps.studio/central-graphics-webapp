<?php

namespace App\Models;

use CodeIgniter\Model;

class ConstanteStickyback extends Model
{
    protected $table      = 'constante_stickyback';
    protected $primaryKey = 'id_constante_stickyback';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
   protected $allowedFields = ['stickyback_valor'];
}