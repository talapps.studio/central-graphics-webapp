<?php

namespace App\Models;

use CodeIgniter\Model;

class EspecificacionMatrecgru extends Model
{
    protected $table      = 'especificacion_matrecgru';
    protected $primaryKey = 'id_especificacion_matrecgru';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_material_recibido_grupo',
        'id_pedido',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}