<?php

namespace App\Models;

use CodeIgniter\Model;

class Departamento extends Model
{
    protected $table      = 'departamentos';
    protected $primaryKey = 'id_dpto';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'codigo',
        'departamento',
        'tipo_inv',
        'cant_mensual',
        'iniciales',
        'tiempo',
        'automatico',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}