<?php

namespace App\Models;

use CodeIgniter\Model;

class InfoCilindro extends Model
{
    protected $table      = 'info_cilindro';
    protected $primaryKey = 'id_info_cilindro';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'articulo',
        'detalle',
        'cod',
        'cliente',
        'colores',
        'impresora',
        'ancho_cliente',
        'repe_alto',
        'ancho_impresion',
        'largo_cliente',
        'repe_desarrollo',
        'largo_impresion',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}