<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionComposicion extends Model
{
    protected $table      = 'pla_medicion_comp';
    protected $primaryKey = 'id_pla_medicion_comp';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'compensacion',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}