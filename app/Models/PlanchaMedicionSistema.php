<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionSistema extends Model
{
    protected $table = 'pla_medicion_sist';
    protected $primaryKey = 'id_pla_medicion_sist';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'sistema',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}