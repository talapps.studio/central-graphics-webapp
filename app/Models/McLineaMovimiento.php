<?php

namespace App\Models;

use CodeIgniter\Model;

class McLineaMovimiento extends Model
{
    protected $table      = 'mc_linea_movimiento';
    protected $primaryKey = 'id_mc_linea_movimiento';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_mc_linea',
        'fecha',
        'descripcion',
        'monto',
        'factura',
        'pedido',
        'id_usu_agrega',
        'activo',
        'id_usu_elimina',
        'id_usuario_pago',
        'codigo_pago',
        'tipo_pago',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}