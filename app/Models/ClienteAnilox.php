<?php

namespace App\Models;

use CodeIgniter\Model;

class ClienteAnilox extends Model
{
    protected $table = 'cliente_anilox';
    protected $primaryKey = 'id_cliente_anilox';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'anilox',
        'bcm',
        'cantidad',
        'id_cliente',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

}