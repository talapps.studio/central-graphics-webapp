<?php

namespace App\Models;

use CodeIgniter\Model;

class RazonesRechazo extends Model
{
    protected $table      = 'rechazo_rasones';
    protected $primaryKey = 'id_rechazo_rasones';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'rechazo_razon',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}