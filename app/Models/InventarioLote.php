<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioLote extends Model
{
    protected $table      = 'inventario_lote';
    protected $primaryKey = 'id_inventario_lote';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_inventario_lote',
        'pedido',
        'fecha_ingreso',
        'fecha_fin',
        'unidades',
        'estado',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}