<?php

namespace App\Models;

use CodeIgniter\Model;

class ColorImpresora extends Model
{
    protected $table      = 'color_impresora';
    protected $primaryKey = ['id_color','id_empresora'];
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $allowedFields = ['id_color','id_impresora'];
}