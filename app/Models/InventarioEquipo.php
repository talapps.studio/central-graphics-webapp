<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioEquipo extends Model
{
    protected $table      = 'inventario_equipo';
    protected $primaryKey = 'id_inventario_equipo';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
   protected $allowedFields = [
       'nombre_equipo',
       'id_dpto',
       'id_grupo',
       'created_by',
       'updated_by',
       'deleted_by'
   ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}