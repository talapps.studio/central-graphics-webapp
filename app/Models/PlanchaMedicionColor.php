<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionColor extends Model
{
    protected $table      = 'pla_medicion_color';
    protected $primaryKey = 'id_pla_medicion_color';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pla_medicion',
        'p_5',
        'p_25',
        'p_50',
        'p_75',
        'p_100',
        'color',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}