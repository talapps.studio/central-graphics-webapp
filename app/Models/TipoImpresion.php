<?php

namespace App\Models;

use CodeIgniter\Model;

class TipoImpresion extends Model
{
    protected $table      = 'tipo_impresion';
    protected $primaryKey = 'id_tipo_impresion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'tipo_impresion',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}