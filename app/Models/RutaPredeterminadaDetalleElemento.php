<?php

namespace App\Models;

use CodeIgniter\Model;

class RutaPredeterminadaDetalleElemento extends Model
{
    protected $table = 'ruta_predeterminada_detalle_elemento';
    protected $primaryKey = 'id_ruta_predeterminada_detalle';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_ruta_pred',
        'id_ruta_detalle',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}