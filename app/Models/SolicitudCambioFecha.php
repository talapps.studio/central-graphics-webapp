<?php

namespace App\Models;

use CodeIgniter\Model;

class SolicitudCambioFecha extends Model
{
    protected $table      = 'sol_cambio_fecha';
    protected $primaryKey = 'id_solicitud';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'codigo_cliente',
        'proceso',
        'fecha',
        'solicita',
        'opcion',
        'activo',
        'id_usuario',
        'anho_mes',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}