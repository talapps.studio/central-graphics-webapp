<?php

namespace App\Models;

use CodeIgniter\Model;

class Color extends Model
{
    protected $table      = 'colores';
    protected $primaryKey = 'id_color';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
   protected $allowedFields = ['nombre','activo'];
}