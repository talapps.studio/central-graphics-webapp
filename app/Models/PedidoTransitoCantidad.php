<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoTransitoCantidad extends Model
{
    protected $table      = 'pedido_transito_cantidad';
    protected $primaryKey = 'id_ped_tran_cantidad';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'orden',
        'cant_anterior',
        'cant_ingresar',
        'fecha',
        'id_inventario_material',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}