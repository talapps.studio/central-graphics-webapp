<?php

namespace App\Models;

use CodeIgniter\Model;

class RevisionPedido extends Model
{
    protected $table      = 'revision_pedido';
    protected $primaryKey = 'id_revision_pedido';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'revision',
        'observacion',
        'fecha',
        'id_usuario',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}