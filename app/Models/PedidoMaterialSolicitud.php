<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoMaterialSolicitud extends Model
{
    protected $table      = 'pedido_material_solicitud';
    protected $primaryKey = 'id_solicitud';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_usuario',
        'id_inventario_material',
        'cantidad',
        'tipo',
        'observacion',
        'fecha',
        'activo',
        'deshabilitado',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}