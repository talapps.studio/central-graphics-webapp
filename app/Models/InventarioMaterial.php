<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioMaterial extends Model
{
    protected $table = 'inventario_material';
    protected $primaryKey = 'id_inventario_material';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'id_grupo',
        'codigo_sap',
        'valor',
        'cantidad_unidad',
        'tipo',
        'nombre_material',
        'existencias',
        'id_inventario_equipo',
        'minimo',
        'observacion',
        'numero_individual',
        'numero_cajas',
        'mp_mt',
        'mat_pais',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}