<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoBase extends Model
{
    protected $table      = 'pedido_pedido';
    protected $primaryKey = 'id_pedido_pedido';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_ped_primario',
        'id_ped_secundario',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}