<?php

namespace App\Models;

use CodeIgniter\Model;

class MaterialSolicitado extends Model
{
    protected $table      = 'material_solicitado';
    protected $primaryKey = 'id_material_solicitado';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'material_solicitado',
        'final',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}