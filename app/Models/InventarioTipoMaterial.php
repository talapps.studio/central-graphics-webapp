<?php
namespace App\Models;

use CodeIgniter\Model;

class InventarioTipoMaterial extends Model
{
    protected $table      = 'iventario_tipo_material';
    protected $primaryKey = 'id_inventario_tipo_material';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'abreviatura',
        'tipo_material_nombre',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}