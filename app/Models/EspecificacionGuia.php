<?php

namespace App\Models;

use CodeIgniter\Model;

class EspecificacionGuia extends Model
{
    protected $table      = 'especificacion_guias';
    protected $primaryKey = 'id_especificacion_guias';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_especificacion_general',
        'registro',
        'espectofotometria',
        'corte',
        'color',
        'semaforo',
        'grafikontrol',
        'micropuntos',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}