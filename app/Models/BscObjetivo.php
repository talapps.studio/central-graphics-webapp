<?php

namespace App\Models;

use CodeIgniter\Model;

class BscObjetivo extends Model
{
    protected $table = 'bsc_objetivo';
    protected $primaryKey = 'id_bsc_objetivo';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_padre',
        'id_grupo',
        'objetivo',
        'condicion',
        'indicador',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}