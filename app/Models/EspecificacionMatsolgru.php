<?php

namespace App\Models;

use CodeIgniter\Model;

class EspecificacionMatsolgru extends Model
{
    protected $table      = 'especificacion_matsolgru';
    protected $primaryKey = 'id_especificacion_matsolgru';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_material_solicitado_grupo',
        'id_pedido',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}