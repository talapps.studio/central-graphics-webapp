<?php

namespace App\Models;

use CodeIgniter\Model;

class Plancha extends Model
{
    protected $table      = 'planchas';
    protected $primaryKey = 'id_plancha';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'cod_plancha',
        'grosor',
        'ubicacion',
        'tipo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}