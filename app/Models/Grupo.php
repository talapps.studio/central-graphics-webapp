<?php

namespace App\Models;

use CodeIgniter\Model;

class Grupo extends Model
{
    protected $table      = 'grupos';
    protected $primaryKey = 'id_grupo';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'nombre_grupo',
        'abreviatura',
        'tipo_grupo',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}