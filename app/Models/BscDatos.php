<?php

namespace App\Models;

use CodeIgniter\Model;

class BscDatos extends Model
{
    protected $table = 'bsc_datos';
    protected $primaryKey = 'id_bsc_datos';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_bsc_objetivo',
        'anho',
        'mes',
        'bsc_proyeccion',
        'bsc_real',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

}