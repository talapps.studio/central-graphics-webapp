<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMensual extends Model
{
    protected $table      = 'plancha_mensual';
    protected $primaryKey = 'id_plames';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'cod_plancha',
        'cantidad',
        'fecha',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}