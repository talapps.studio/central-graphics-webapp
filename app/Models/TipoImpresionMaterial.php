<?php

namespace App\Models;

use CodeIgniter\Model;

class TipoImpresionMaterial extends Model
{
    protected $table      = 'tipo_impd_material';
    protected $primaryKey = 'id_tipo_impd_material';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'tipo_impd_material',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}