<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoSap extends Model
{
    protected $table      = 'pedido_sap';
    protected $primaryKey = 'id_pedido_sap';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_cliente',
        'sap',
        'orden',
        'venta',
        'fecha',
        'confirmada',
        'factura',
        'actualizar',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}