<?php

namespace App\Models;

use CodeIgniter\Model;

class ReporteAnual extends Model
{
    protected $table      = 'reporte_anual';
    protected $primaryKey = 'id_rep_cump';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'anho',
        'mes',
        'tped',
        'ttie',
        'tatra',
        'trepro',
        'extras',
        'cafi',
        'ccom',
        'cdiv',
        'csto',
        'total_venta',
        'id_grupo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}