<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionAltura extends Model
{
    protected $table      = 'pla_medicion_alt';
    protected $primaryKey = 'id_pla_medicion_alt';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'altura',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}