<?php

namespace App\Models;

use CodeIgniter\Model;

class Observacion extends Model
{
    protected $table      = 'observacion';
    protected $primaryKey = 'id_observacion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'id_usuario',
        'fecha_hora',
        'observacion',
        'req_aprobacion',
//        'created_by',
//        'updated_by',
//        'deleted_by'
    ];

//    protected $useTimestamps = true;
//    protected $createdField  = 'created_at';
//    protected $updatedField  = 'updated_at';
//    protected $deletedField  = 'deleted_at';
//
//    protected $validationRules    = [];
//    protected $validationMessages = [];
//    protected $skipValidation     = false;


}