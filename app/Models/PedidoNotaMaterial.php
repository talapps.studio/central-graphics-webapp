<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoNotaMaterial extends Model
{
    protected $table      = 'pedido_nota_material';
    protected $primaryKey = 'id_nota_mat';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_nota_env',
        'id_pedido',
        'cantidad',
        'tipo',
        'id_material',
        'otro_mat',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}