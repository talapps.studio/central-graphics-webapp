<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoAdjuntoReproceso extends Model
{
    protected $table = 'pedido_adjunto_reproceso';
    protected $primaryKey = 'id_pedido_adjunto_reproceso';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'img1',
        'img2',
        'img3',
        'img4',
        'img5',
        'img6',
        'antecedentes',
        'observaciones',
        'mejora',
        'adicionales',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}