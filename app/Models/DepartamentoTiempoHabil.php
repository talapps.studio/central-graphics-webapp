<?php

namespace App\Models;

use CodeIgniter\Model;

class DepartamentoTiempoHabil extends Model
{
    protected $table      = 'departamentos_tiempo_habil';
    protected $primaryKey = 'id_departamentos_tiempo_habil';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_grupo',
        'id_dpto',
        'tiempo_habil',
        'hora_inicio',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;


}