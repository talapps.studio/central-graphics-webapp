<?php

namespace App\Models;

use CodeIgniter\Model;

class TipoImpresionAcabado extends Model
{
    protected $table      = 'tipo_impd_acabado';
    protected $primaryKey = 'id_tipo_impd_acabado';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'tipo_impd_acabado',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}