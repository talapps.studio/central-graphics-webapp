<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionPlancha extends Model
{
    protected $table = 'pla_medicion_plancha';
    protected $primaryKey = 'id_pla_medicion_plancha';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'plancha',
        'created_by',
        'updated_by',
        'deleted_by',
        'activo'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}