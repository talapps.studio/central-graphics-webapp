<?php

namespace App\Models;

use CodeIgniter\Model;

class ClienteContacto extends Model
{
    protected $table = 'cliente_contacto';
    protected $primaryKey = 'id_cliente_contacto';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'nombre',
        'cargo',
        'email',
        'tel_oficina',
        'tel_directo',
        'tel_celular',
        'id_cliente',
        'created_by',
        'updated_by',
        'deleted_by'
    ];
    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}