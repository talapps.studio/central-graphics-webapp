<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioBodega extends Model
{
    protected $table      = 'inventario_bodega_9000';
    protected $primaryKey = 'id_inventario_bodega_9000';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_inventario_material',
        'cantidad',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}