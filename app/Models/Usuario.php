<?php

namespace App\Models;

use CodeIgniter\Model;

class Usuario extends Model
{
    protected $table = 'usuario';
    protected $primaryKey = 'id_usuario';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_usuario',
        'usuario',
        'contrasena',
        'nombre',
        'cod_empleado',
        'puesto',
        'email',
        'id_dpto',
        'id_grupo',
        'hora',
        'activo',
        'usu_prog',
        'preferencias',
        'upais',
        'token',
        'expiration_token',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}