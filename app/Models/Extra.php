<?php

namespace App\Models;

use CodeIgniter\Model;

class Extra extends Model
{
    protected $table      = 'extra';
    protected $primaryKey = 'id_extra';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_usuario',
        'inicio',
        'fin',
        'fin_real',
        'fecha',
        'total_h',
        'total_m',
        'id_usu_adm',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}