<?php

namespace App\Models;

use CodeIgniter\Model;

class Cliente extends Model
{
    protected $table = 'cliente';
    protected $primaryKey = 'id_cliente';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'codigo_cliente',
        'nombre',
        'contacto',
        'usuario',
        'contrasena',
        'activo',
        'id_grupo',
        'direccion',
        'nit',
        'web',
        'credito',
        'pais',
        'ruta_imagen',
        'created_by',
        'updated_by',
        'deleted_by',
        'logo'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}