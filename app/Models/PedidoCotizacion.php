<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoCotizacion extends Model
{
    protected $table      = 'pedido_cotizacion';
    protected $primaryKey = 'id_pedido_cotizacion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'id_usuario',
        'cotizacion',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}