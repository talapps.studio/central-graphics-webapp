<?php

namespace App\Models;

use CodeIgniter\Model;

class RutaPredeterminada extends Model
{
    protected $table = 'ruta_predeterminada';
    protected $primaryKey = 'id_ruta_pred';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'nombre',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;
}