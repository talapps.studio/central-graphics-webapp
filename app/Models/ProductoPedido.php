<?php

namespace App\Models;

use CodeIgniter\Model;

class ProductoPedido extends Model
{
    protected $table = 'producto_pedido';
    protected $primaryKey = 'id_prod_ped';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_producto',
        'id_pedido',
        'precio',
        'pulgadas',
        'cantidad',
        'ancho',
        'alto',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}