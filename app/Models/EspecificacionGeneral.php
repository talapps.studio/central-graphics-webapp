<?php

namespace App\Models;

use CodeIgniter\Model;

class EspecificacionGeneral extends Model
{
    protected $table      = 'especificacion_general';
    protected $primaryKey = 'id_especificacion_general';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'unidad_media',
        'ancho_arte',
        'alto_arte',
        'ancho_fotocelda',
        'alto_fotocelda',
        'color_fotocelda',
        'fotocelda_izquierda',
        'fotocelda_derecha',
        'lado_impresion',
        'codb_tipo',
        'codb_num',
        'codb_magni',
        'codb_posicion',
        'codb_bwr',
        'repet_ancho',
        'repet_alto',
        'separ_ancho',
        'separ_alto',
        'id_tipo_impresion',
        'embobinado_cara',
        'embobinado_dorso',
        'material_rec_otro',
        'material_sol_otro',
        'emulsion_negativo',
        'ancho_total',
        'alto_total',
        'maquina',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}