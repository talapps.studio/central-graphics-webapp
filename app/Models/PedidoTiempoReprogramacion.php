<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoTiempoReprogramacion extends Model
{
    protected $table      = 'pedido_tie_repro';
    protected $primaryKey = 'id_ped_tiempo';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_pedido',
        'tiempo_respuesta',
        'tiempo_arte',
        'tiempo_aprobacion',
        'tiempo_efinal',
        'tiempo_venta',
        'tiempo_repro',
        'tiempo_cs',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}