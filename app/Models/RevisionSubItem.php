<?php

namespace App\Models;

use CodeIgniter\Model;

class RevisionSubItem extends Model
{
    protected $table      = 'revision_sub_item';
    protected $primaryKey = 'id_revision_sub_item';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_revision_item',
        'sub_item',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}