<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaMedicionLineaje extends Model
{
    protected $table      = 'pla_medicion_lin';
    protected $primaryKey = 'id_pla_medicion_lin';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'lineaje',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}