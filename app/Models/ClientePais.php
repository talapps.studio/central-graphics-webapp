<?php

namespace App\Models;

use CodeIgniter\Model;

class ClientePais extends Model
{
    protected $table = 'cliente_pais';
    protected $primaryKey = 'id_pais_cliente';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'abreviatura',
        'nombre_pais',
        'activo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

}