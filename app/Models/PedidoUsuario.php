<?php

namespace App\Models;

use CodeIgniter\Model;

class PedidoUsuario extends Model
{
    protected $table      = 'pedido_usuario';
    protected $primaryKey = 'id_peus';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_ruta_dpto',
        'id_pedido',
        'id_usuario',
        'fecha_asignado',
        'fecha_inicio',
        'fecha_fin',
        'tiempo_asignado',
        'estado',
        'pprioridad',
        'tipo',
//        'created_by',
//        'updated_by',
//        'deleted_by'
    ];

//    protected $useTimestamps = true;
//    protected $createdField  = 'created_at';
//    protected $updatedField  = 'updated_at';
//    protected $deletedField  = 'deleted_at';
//
//    protected $validationRules    = [];
//    protected $validationMessages = [];
//    protected $skipValidation     = false;
}