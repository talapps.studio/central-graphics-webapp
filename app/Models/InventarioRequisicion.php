<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioRequisicion extends Model
{
    protected $table      = 'inventario_requisicion';
    protected $primaryKey = 'id_inventario_requisicion';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_inventario_requisicion',
        'id_inventario_lote',
        'numero_requ',
        'fecha_salida',
        'id_usuario',
        'cantidad',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}