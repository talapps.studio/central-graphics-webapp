<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioProveedor extends Model
{
    protected $table      = 'inventario_proveedor';
    protected $primaryKey = 'id_inventario_proveedor';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'proveedor_nombre',
        'usuario',
        'contrasena',
        'id_grupo',
        'desactivado',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}