<?php

namespace App\Models;

use CodeIgniter\Model;

class InventarioMAterialDetalle extends Model
{
    protected $table      = 'inventario_material_detalle';
    protected $primaryKey = 'id_inventario_material_detalle';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'id_inventario_material',
        'id_inventario_proveedor',
        'cod_plancha',
        'numero_individual',
        'numero_total',
        'cod_tipo',
        'tamanho',
        'tipo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}