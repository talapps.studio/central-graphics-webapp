<?php

namespace App\Models;

use CodeIgniter\Model;

class EspecificacionColores extends Model
{
    protected $table      = 'especificacion_colores';
    protected $primaryKey = 'id_especificacion_colores';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'color',
        'angulo',
        'linaje',
        'tono',
        'linea',
        'solicitado',
        'resolucion',
        'anilox',
        'bcm',
        'sticky',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;

}