<?php

namespace App\Models;

use CodeIgniter\Model;

class PlanchaCantidad extends Model
{
    protected $table      = 'plancha_cantidad';
    protected $primaryKey = 'codigo';
    protected $useAutoIncrement = true;
    protected $returnType     = 'array';
    protected $useSoftDeletes = true;

    protected $allowedFields = [
        'cod_tipo',
        'cod_plancha',
        'cantidad',
        'ancho',
        'alto',
        'subtotal',
        'total',
        'dia',
        'mes',
        'year',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}