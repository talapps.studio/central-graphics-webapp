<?php

namespace App\Models;

use CodeIgniter\Model;

class ClienteGrupo extends Model
{
    protected $table = 'cliente_grupo';
    protected $primaryKey = 'id_cliente_grupo';
    protected $useAutoIncrement = true;
    protected $returnType = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = [
        'id_grupo',
        'id_cliente',
        'id_grupo_externo',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $validationRules = [];
    protected $validationMessages = [];
    protected $skipValidation = false;

}