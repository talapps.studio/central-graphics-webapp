<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionPlancha;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementIronerRepository extends BaseRepository
{
    public function entity()
    {
        return new PlanchaMedicionPlancha();
    }

}