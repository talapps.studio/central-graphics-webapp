<?php

namespace App\Repositories;

use App\Models\ConstanteStickyback;

class StickybackRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new ConstanteStickyback();
    }
    
    public function getAllConstants()
    {
        $stickybacks = $this->modelManager->findAll();
        return $stickybacks;
    }

    public function addStickyback($data)
    {
        $this->modelManager->insert($data);
    }
    public function updateStickyback($id, $data)
    {
        $this->modelManager->update($id, $data);
    }
}
