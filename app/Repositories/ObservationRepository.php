<?php
namespace App\Repositories;
use App\Models\Observacion;
use Fluent\Repository\Eloquent\BaseRepository;

class ObservationRepository extends BaseRepository
{
   public function entity()
   {
     return Observacion::class;
   }
   
}