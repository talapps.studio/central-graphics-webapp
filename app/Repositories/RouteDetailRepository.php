<?php

namespace App\Repositories;

use App\Models\RutaDetalle;
use Fluent\Repository\Eloquent\BaseRepository;

class RouteDetailRepository extends BaseRepository
{

    public function entity()
    {
        return RutaDetalle::class;
    }
    public function getAllRouteDetail(){
        $db      = \Config\Database::connect();
        $builder = $db -> table('ruta_detalle');
        $builder-> select('id_ruta_detalle,
                            id_dpto,
                            nombre,
                            descripcion,
                            activo');
        $query = $builder->get();
        $elementos = $query->getResult();
        return $elementos;

    }
    public function getRouteDetail()
    {
        $elementosRuta = array();
        $db      = \Config\Database::connect();
        $builder = $db -> table('ruta_detalle');
        $builder-> select('id_ruta_detalle,
                            id_dpto,
                            nombre,
                            descripcion,
                            activo');
        $query = $builder->get();
        $rutasElementos = $query->getResult();
        foreach ($rutasElementos as $rutaElemento){
            $rutaElementoNew = [
                'id_ruta_detalle' => $rutaElemento->id_ruta_detalle,
                'id_dpto' => $rutaElemento-> id_dpto,
                'nombre' => $rutaElemento->nombre,
                'descripcion' => $rutaElemento->descripcion,
                'activo'=>$rutaElemento->activo,
                'encargados' => self::getUserByDepartament($rutaElemento->id_dpto),
            ];
            array_push($elementosRuta,$rutaElementoNew);

        }
        return $elementosRuta;
         
    }
    public function getUserByDepartament($idDepartamento){
        $db      = \Config\Database::connect();
        $builder =$db->table('usuario as usu');
        $builder->select('usu.id_usuario,
                          usu.id_dpto,
                          usu.usuario,
                          usu.nombre,
                          usu.puesto,
                          usu.usu_prog,
                          usu.activo,
                          dpto.departamento,
                          tiempo');
       $builder->join('departamentos as dpto', 'dpto.id_dpto = usu.id_dpto');
       $builder->where('usu.activo', 's');
       $builder->where('usu.id_grupo', 2);
       $builder->where('usu.usu_prog', "s");
       $builder->where('usu.id_dpto',$idDepartamento);
       $query = $builder->get();
       $encargados = $query->getResult();
       return $encargados;                 
    }
}