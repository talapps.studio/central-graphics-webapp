<?php
 namespace App\Repositories;
 use App\Models\PedidoUsuario;
 use Fluent\Repository\Eloquent\BaseRepository;
 class OrderUserRepository extends BaseRepository
 {
    public function entity()
    {
        return PedidoUsuario::class;
    }
    

 }