<?php

namespace App\Repositories;
use App\Models\Pedido;


class OrderRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new Pedido();
    }
    public function getAssignments()
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('pedido as pe');
        $builder->select('pro.id_proceso, 
                          pe.id_pedido, 
                          estado,
                          proceso,
                          pro.nombre as proceso_nombre, 
                          cli.id_cliente, 
                          fecha_entrega,
                          fecha_asignado, 
                          prioridad, 
                          tiempo_asignado, 
                          codigo_cliente');
        $builder->join('pedido_usuario as pu', 'pu.id_pedido = pe.id_pedido');
        $builder->join('procesos as pro', 'pe.id_proceso = pro.id_proceso');
        $builder->join('usuario as us', 'us.id_usuario = pu.id_usuario');
        $builder->join('cliente as cli', 'pro.id_cliente = cli.id_cliente');
        $builder->where('estado !=', "Terminado");
        $query = $builder->get();
        $pedidos = $query->getResult();
        return $pedidos;
    }

    public function getOrderWorkFlow($id)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('pedido as ped');
        $builder->select('dep.departamento, 
                          usu.usuario, 
                          peus.estado,
                          peus.fecha_asignado, 
                          peus.fecha_fin');
        $builder->join('procesos proc', 'proc.id_proceso = ped.id_proceso');
        $builder->join('pedido_usuario peus', 'ped.id_pedido = peus.id_pedido');
        $builder->join('usuario usu', 'peus.id_usuario = usu.id_usuario');
        $builder->join('departamentos dep', 'dep.id_dpto = usu.id_dpto');
        $builder->join('cliente cli', 'cli.id_cliente = proc.id_cliente');
        $builder->where('ped.id_pedido', $id);
        $builder->where('cli.id_grupo', 2);
        $builder->orderBy('peus.id_peus', 'asc');
        $query = $builder->get();
        $rutas = $query->getResult();
        return $rutas;
    }

    public function getOrderObservations($id)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('observacion obs');
        $builder->select('obs.fecha_hora, 
                          obs.observacion, 
                          usu.usuario');
        $builder->join('usuario usu', 'obs.id_usuario=usu.id_usuario');
        $builder->where('id_pedido', $id);
        $builder->orderBy('obs.fecha_hora', 'desc');
        $query = $builder->get();
        $observations = $query->getResult();
        return $observations;
    }

    public function getOrderHeader($id)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('pedido as ped');
        $builder->select('proc.proceso, 
                          cli.nombre,
                          proc.nombre as nombre_proceso, 
                          codigo_cliente,
                          ped.id_pedido');
        $builder->join('procesos proc', 'proc.id_proceso = ped.id_proceso');
        $builder->join('cliente cli', 'cli.id_cliente = proc.id_cliente');
        $builder->where('ped.id_pedido', $id);
        $builder->where('cli.id_grupo', 2);
        $query = $builder->get();
        $cabecera = $query->getFirstRow();
        return $cabecera;
    }

    public function getOrdersByProcess($id_proceso,$id_cliente){
        
        $db      = \Config\Database::connect();
        $builder = $db->table('pedido as ped');
        $builder->select('proc.proceso, 
                          ped.id_pedido,
                          ped.id_proceso,
                          proc.id_cliente,
                          ped.fecha_entrada,
                          ped.fecha_entrega,
                          ped.fecha_reale,
                          proc.nombre,
                          tt.trabajo, 
                          proc.nombre as nombre_proceso
                          ');
        $builder->join('procesos proc', 'proc.id_proceso = ped.id_proceso');
        $builder->join('tipo_trabajo tt', 'ped.id_tipo_trabajo = tt.id_tipo_trabajo');
        $builder->where('proc.proceso', $id_proceso);
        $builder->where('proc.id_cliente', $id_cliente);
        $builder->orderBy('ped.fecha_entrada');
        $query = $builder->get();
        $pedidos = $query->getResult();
        return $pedidos;

    }
    
    public function getAllOrdersByProcess($id_proceso){
        
        $db      = \Config\Database::connect();
        $builder = $db->table('pedido as ped');
        $builder->select('proc.proceso, 
                          ped.id_pedido,
                          ped.id_proceso,
                          proc.id_cliente,
                          ped.fecha_entrada,
                          ped.fecha_entrega,
                          ped.fecha_reale,
                          proc.nombre,
                          tt.trabajo, 
                          proc.nombre as nombre_proceso
                          ');
        $builder->join('procesos proc', 'proc.id_proceso = ped.id_proceso');
        $builder->join('tipo_trabajo tt', 'ped.id_tipo_trabajo = tt.id_tipo_trabajo');
        $builder->where('proc.proceso', $id_proceso);
        $query = $builder->get();
        $pedidos = $query->getResult();
        return $pedidos;

    }
    public function addNewOrder($data){
        return $this->modelManager->insert($data);

    }
}
