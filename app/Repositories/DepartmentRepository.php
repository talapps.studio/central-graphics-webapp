<?php

namespace App\Repositories;

use App\Models\Departamento;
use Fluent\Repository\Eloquent\BaseRepository;

class DepartmentRepository extends BaseRepository
{

    public function entity()
    {
        return Departamento::class;
    }
    public function getDepartment(){
        $db      = \Config\Database::connect();
        $builder = $db -> table('departamentos');
        $builder-> select('id_dpto,
                            codigo,
                            departamento,
                            tipo_inv,
                            cant_mensual,
                            iniciales,
                            tiempo,
                            automatico,
                            activo');
        $query = $builder->get();
        $departamentos = $query->getResult();
        return $departamentos;
    }
    
}