<?php

namespace App\Repositories;

use App\Models\ProductoCliente;


class ProductCustomerRepository
{
    protected $modelManager;
    function __construct()
    {

        $this->modelManager = new ProductoCliente();
    }
    public function getProductsCustomers()
    {
        //consulta con filtrado
        $db      = \Config\Database::connect();
        $builder = $db->table('producto_cliente');
        $builder->select('pro_cli.id_prod_clie,
                            pro_cli.id_grupo,
                            pro_cli.id_cliente,
                            pro_cli.codigo_cliente,
                            pro_cli.id_producto,
                            pro_cli.precio,
                            pro_cli.cantida,
                            pro_cli.concepto,
                            pro_cli.activo');
        $query = $builder->get();
        $productoClientes = $query->getResult();
        return $productoClientes;
    }

    public function getAll()
    {
        $productoClientes =  $this->modelManager->findAll();
        return $productoClientes;
    }

    public function addProductCustomer($data)
    {
        $this->modelManager->insert($data);
    }
    public function addManyProductCustomer($data)
    {
        $this->modelManager->insertBatch($data);
    }
    public function updateManyProductCustomer($data)
    {
        $this->modelManager->updateBatch($data);
    }
    public function updateProductCustomer($id, $data)
    {
        $this->modelManager->update($id, $data);
    }
}
