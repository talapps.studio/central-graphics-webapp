<?php

namespace App\Repositories;
use App\Models\Cliente;



class CustomerRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new Cliente();
    }
    //Consulta con filtrados
    public function getCustomers($status) 
    {
        $clientesRegistrados = array();
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select('cli.id_cliente, 
                          cli.codigo_cliente, 
                          cli.nombre,
                          cli.nit,
                          cli.direccion,
                          cli.credito,
                          cli.web,
                          cli.pais,
                          cli.activo'
                        );
        $builder->where('cli.activo',$status);
        $builder->orderBy('cli.nombre','asc');
        $query = $builder->get();
        $clientes = $query->getResult();
        foreach ($clientes as $cliente){
            $clienteRegistrado = [
                'id_cliente'=> $cliente->id_cliente,
                'codigo_cliente'=> $cliente->codigo_cliente,
                'nombre'=>$cliente->nombre,
                'nit'=>$cliente->nit,
                'direccion'=>$cliente->direccion,
                'web'=>$cliente->web,
                'credito'=>$cliente->credito,
                'pais'=>$cliente->pais,
                'activo'=>$cliente->activo,
                'contactos' => self::getContactos($cliente->id_cliente),  
                'maquinas'=> self::getMaquinas($cliente->id_cliente),
                'planchas'=> self::getPlanchas($cliente->id_cliente), 
                'aniloxes'=> self::getAniloxes($cliente->id_cliente),
                'productos'=> self::getProductos($cliente->id_cliente),
            ];
            array_push($clientesRegistrados,$clienteRegistrado);
        }
        return $clientesRegistrados;
    }
   //contactos
    public function getContactos($id_cliente){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente_contacto');
        $builder->select('id_cliente_contacto, nombre, cargo, email, tel_oficina, tel_directo, tel_celular, id_cliente');
        $builder->where('id_cliente =',$id_cliente);
        $query = $builder->get();
        $contactos = $query->getResult();
        return $contactos;
    }
    public function getMaquinas($id_cliente){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente_maquina');
        $builder->select('id_cliente_maquina, maquina, colores,id_cliente');
        $builder->where('id_cliente =',$id_cliente);
        $query = $builder->get();
        $maquinas = $query->getResult();
        return $maquinas;
    }
    public function getPlanchas($id_cliente){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente_placa');
        $builder->select('id_cliente_placa, altura, lineaje, marca, id_cliente');
        $builder->where('id_cliente =',$id_cliente);
        $query = $builder->get();
        $planchas = $query->getResult();
        return $planchas;
    }
    public function getAniloxes($id_cliente){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente_anilox');
        $builder->select('id_cliente_anilox, anilox, bcm, cantidad, id_cliente');
        $builder->where('id_cliente =',$id_cliente);
        $query = $builder->get();
        $aniloxes = $query->getResult();
        return $aniloxes;
    }
    public function getProductos($id_cliente){
        $db      = \Config\Database::connect();
        $builder = $db->table('producto_cliente');
        $builder->select('id_prod_clie, precio,concepto,activo, id_producto, id_cliente');
        $builder->where('id_cliente =',$id_cliente);
        $query = $builder->get();
        $productos = $query->getResult();
        return $productos;
    }
    
    //Consulta usando el modelo
    public function getAll(){
        
        $clientes = $this->modelManager->findAll();
        return $clientes;
    }

    //Insertado de datos
    public function addCustomer($data){
        return $this->modelManager->insert($data);
 
    }
    //actualizar datos
    public function updateCustomer($id,$data){
        
        $this->modelManager->update($id,$data);
        
    }

    //Para Listas desplegables
    public function getActiveOrInactiveCustomers($value){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select('cli.id_cliente, 
                          cli.codigo_cliente, 
                          cli.nombre,
                         ');
        $builder->where('cli.activo',$value);
        $query = $builder->get();
        $clientes = $query->getResult();
        return $clientes;
    }

    public function getCustomerById($id){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select('cli.id_cliente, 
                          cli.codigo_cliente, 
                          cli.nombre,
                          cli.nit,
                          cli.direccion,
                          cli.web,
                          cli.credito
                         ');
        $builder->where('cli.id_cliente',$id);
        $query = $builder->get();
        $cliente = $query->getRow();
        return $cliente;
    }
    public function getProductsByCostumers($id){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select('pc.id_producto, pc.precio');
        $builder->join('producto_cliente as pc',' cli.id_cliente = pc.id_cliente');
        $builder->where('cli.id_cliente',$id);
        $query = $builder->get();
        $clientes = $query->getResult();
        return $clientes;
    }
}