<?php
namespace App\Repositories;
use App\Models\RutaPredeterminadaDetalleElemento;
use Fluent\Repository\Eloquent\BaseRepository;

class DefaultRouteDetailRepository extends BaseRepository
{
   public function entity()
   {
     return RutaPredeterminadaDetalleElemento ::class;
   }
   protected $modelManager;
   function __construct()
   {
    $this->modelManager = new RutaPredeterminadaDetalleElemento();
   }
   public function existeByDefaultRouteDetail($idRutaPredeterminada, $idRutaDetalle){

        $db      = \Config\Database::connect();
        $builder = $db -> table('ruta_predeterminada_detalle_elemento as rpd');
        $builder-> select('id_ruta_predeterminada_detalle,
                           id_ruta_pred,
                           id_ruta_detalle');
        $builder->where('id_ruta_pred',$idRutaPredeterminada);
        $builder->where('id_ruta_detalle',$idRutaDetalle);
        $query = $builder->get();
        $elementos = $query->getResult();
        if(sizeof($elementos)>0){
          return true;
        }else {
          return false;
        }

   }
   public function deleteDefaultRouteDetail($idRouteAsociada){
    $db      = \Config\Database::connect();
    $db->transBegin();
    $this->modelManager->delete($idRouteAsociada);
    
    if ($db->transStatus() === FALSE)
    {
            $db->transRollback();
    }
    else
    {
            $db->transCommit();
    }
   }
   public function addNewDefaultRouteDetail($data){
    $this->modelManager->insert($data);
   }
   
}