<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionComposicion;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementCompositionRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function entity()
    {
        return new PlanchaMedicionComposicion();
    }


}