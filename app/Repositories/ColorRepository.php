<?php

namespace App\Repositories;

use App\Models\Color;

class ColorRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new Color();
    }

    public function getAllColors(){
        return $this->modelManager->where('activo', 1)->findAll();
    }

    public function getAllColorByPrinter($idPrinter){
        $db      = \Config\Database::connect();
        $builder = $db->table('color_impresora ci');
        $builder->select('c.nombre, ci.id_color');
        $builder->join('colores c', 'c.id_color = ci.id_color');
        $builder->where('ci.id_impresora',$idPrinter);
        $query = $builder->get();
        $colors = $query->getResult();
        return $colors;
    }
}
