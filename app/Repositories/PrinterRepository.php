<?php

namespace App\Repositories;
use App\Models\ClienteMaquina;
use App\Repositories\ColorRepository;

class PrinterRepository
{
    protected $modelManager;
    protected $colorRepository;

    function __construct(){
        $this->modelManager = new ClienteMaquina();
        $this->colorRepository = new ColorRepository();
    }

    public function getPrinterByCustomer($idCustomer){
        $rows = $this->modelManager->where('id_cliente',$idCustomer)->findAll();
        foreach($rows as $printer){
            $printers[] = array(
                'id_cliente_maquina' => $printer['id_cliente_maquina'],
                'id_cliente' => $printer['id_cliente'],
                'maquina' => $printer['maquina'],
                'colores' => $printer['colores'],
                'esConfigurado' => $printer['esConfigurado'],
                'coloresEstablecidos'=> $this->colorRepository->getAllColorByPrinter($printer['id_cliente_maquina'])
            );
        }
        return $printers;
    }

    public function addPrinter($data){
        return $this->modelManager->insert($data);
    }


}