<?php
namespace App\Repositories;

use App\Models\InventarioTipoMaterial;
class MaterialStockTypeRepository
{
    public function getStocksTypeMaterials()
    {
        $db    = \Config\Database::connect();
        $builder = $db -> table('inventario_tipo_material');
        $builder-> select('id_inventario_tipo_material,
                            abreviatura,
                            tipo_material_nombre');
        $query = $builder->get();
        $tipoMateriales = $query->getResult();
        return $tipoMateriales;  
    }
}