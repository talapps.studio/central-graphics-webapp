<?php
namespace App\Repositories;
use App\Models\Usuario;

class AuthenticationRepository{
    protected $modelUser;

    function __construct()
    {
        $this->modelUser = new Usuario();
    }
    //obteniendo datos del usuario
    public function getUser(){
        $db     =\Config\Database::connect();
        $builder = $db -> table('usuario as us');
        $builder->select('id_usuario,
                          usuario,
                          contrasena,
                          nombre,
                          cod_empleado,
                          puesto,
                          email,
                          id_dpto,
                          id_grupo,
                          hora,
                          activo,
                          usu_prog,
                          preferencias,
                          upais');
        $query = $builder->get();
        $usuarios = $query->getResult();
        return $usuarios;
    }

    public function getUserByUsername($username){
       return $this->modelUser->where('usuario',$username)->first();
    }

    public function setToken($id, $token, $expiration){
        $data = [
            'token' => $token,
            'expiration_token' => $expiration,
        ];
        $this->modelUser->update($id,$data);
    }

    public function validateToken($token){
        $user = $this->modelUser->where('token',$token)->first();
        $now = date_create()->format('Y-m-d H:i:s');
        return ($now <= $user['expiration_token']) ? $user : false;
    }

    public function changePassword($id, $password){
        $options = ['cost'=>10];
        $data = [
            'contrasena' => password_hash($password, PASSWORD_BCRYPT, $options),
            'token' => ''
        ];
        $isValid = password_verify($password, $data['contrasena']);
        return ($isValid) ?  $this->modelUser->update($id,$data) : 0;
    }
}