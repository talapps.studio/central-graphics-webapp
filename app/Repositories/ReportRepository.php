<?php

namespace App\Repositories;


class ReportRepository
{

    public function getComplianceByDates($startDate, $endDate, $customerId) 
    {
        $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $startDate)));
        $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $endDate)));
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select(
                       'cli.id_cliente as id_cliente,
                        cli.codigo_cliente as codigo_cliente,
                        cli.nombre,
                        count(*) as num_pedidos,
                        count(case when pe.fecha_reale > pe.fecha_entrega then 1 end) as pedidos_atrasados,
                        count(case when pe.fecha_reale <= pe.fecha_entrega then 1 end) as pedidos_enTiempo
                        '
                    );
        $builder->join('procesos as pro', 'cli.id_cliente = pro.id_cliente');
        $builder->join('pedido as pe', 'pe.id_proceso = pro.id_proceso');
        $builder->where('cli.activo =', "s");
        $builder->where('pe.fecha_entrega >=', $startDate);
        $builder->where('pe.fecha_entrega <=', $endDate);
        $builder->where('pe.fecha_reale !=', "0000-00-00");
        if($customerId != "-1"){
            $builder->where('pro.id_cliente =', "$customerId");
        }
        $builder->groupBy('cli.id_cliente');
        $builder->orderBy('pro.id_cliente asc');
        $builder->distinct();
        $query = $builder->get();
        $compliance = $query->getResult();
        return $compliance;
    }

    public function getComplianceDetailsByDates($startDate, $endDate, $customerId) 
    {
        $startDate = date('Y-m-d', strtotime(str_replace('/', '-', $startDate)));
        $endDate = date('Y-m-d', strtotime(str_replace('/', '-', $endDate)));
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select(
                       'pro.proceso as proceso,
                        pro.nombre as nombre_proceso,
                        pe.fecha_entrada,
                        pe.id_tipo_trabajo,
                        pe.fecha_reale,
                        pe.fecha_entrega,
                        pe.id_pedido,
                        cli.codigo_cliente,
                        cli.nombre,
                        (case when pe.fecha_reale  > pe.fecha_entrega  then 1 else 0 end) as atrasado
                        '
                    );
        $builder->join('procesos as pro', 'cli.id_cliente = pro.id_cliente');
        $builder->join('pedido as pe', 'pe.id_proceso = pro.id_proceso');
        $builder->where('cli.activo', "s");
        $builder->where('pe.fecha_entrega >=', $startDate);
        $builder->where('pe.fecha_entrega <=', $endDate);
        $builder->where('pe.fecha_reale !=', "0000-00-00");
        if($customerId != "-1"){
            $builder->where('pro.id_cliente =', $customerId);
        }
        $builder->orderBy('pe.fecha_entrega asc');
        $query = $builder->get();
        $compliance = $query->getResult();
        return $compliance;
    }


    public function getFilteredJobPositions($idJobs){
        $db      = \Config\Database::connect();
        $builder = $db->table('departamentos as dep');
        $builder->select('dep.id_dpto,
                          dep.departamento,
                          group_concat(concat_ws("&", usu.id_usuario , usu.nombre)SEPARATOR ";") as empleados
                        ');
        $builder->join('usuario as usu', 'usu.id_dpto = dep.id_dpto');
        $builder->where('dep.activo', "s");
        $builder->where('usu.activo', "s");
        $builder->whereIn('dep.id_dpto', $idJobs);
        $builder->groupBy('dep.id_dpto');
        $builder->orderBy('dep.id_dpto asc');
        $query = $builder->get();
        $jobs = $query->getResult();
        return $jobs;
    }

    //Detalle de trabajos por departamento
    public function getJobsByDepartment($startDate, $endDate){
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as clie');
        $builder->select(' codigo_cliente as codigo_cliente, 
                            proceso as proceso,
                            peus.id_peus as id_pedido_usuario, 
                            proc.nombre as producto, 
                            ped.id_pedido as id_pedido,
                            fecha_entrada, 
                            peus.tiempo_asignado,
                            fecha_entrega, 
                            fecha_reale,
                            id_tipo_trabajo as tipo_trabajo, 
                            peus.id_usuario, 
                            proc.id_proceso,
                            (select case 
                                when ped.fecha_reale > ped.fecha_entrega  then 1 -- Retrasado
                                when peus.estado = "Terminado" and peus.fecha_fin between "2019-02-01 00:00:00" and "2019-02-28 23:59:59" or ped.fecha_reale between "2019-02-01 00:00:00" and "2019-02-28 23:59:59" then 2 -- Terminado
                                when id_tipo_trabajo = 4 then 3 -- reproceso
                                when fecha_reale = "0000-00-00" then 4 -- incompleto
                            end) as estado_pedido
                        ');
        $builder->join('procesos proc', 'clie.id_cliente = proc.id_cliente');
        $builder->join('pedido ped', 'proc.id_proceso = ped.id_proceso');
        $builder->join('pedido_usuario peus', 'ped.id_pedido = peus.id_pedido');
        $builder->join('usuario usu', 'usu.id_usuario = peus.id_usuario');
        $builder->Join('especificacion_matsolgru matsol','ped.id_pedido = matsol.id_pedido','left');
        $builder->Join('material_solicitado_grupo matgru','matsol.id_material_solicitado_grupo = matgru.id_material_solicitado_grupo','left');
        $builder->Join('material_solicitado matsoli','matgru.id_material_solicitado = matsoli.id_material_solicitado','left');
        $builder->where('clie.id_grupo', "2");
        $builder->where('usu.id_dpto', "2");
        $builder->where('usu.activo', "s");
        $builder->where('ped.fecha_reale >=', "2019-02-01");
        $builder->where('ped.fecha_reale <=', "2019-02-28");
        // $builder->orderBy('ped_prioridad asc','fecha_entrega asc','ped.id_pedido asc');
        $builder->distinct();
        $query = $builder->get();
        $jobs = $query->getResult();
        return $jobs;       

    }


}