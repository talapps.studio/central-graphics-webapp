<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionTrama;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementPlotRepository extends BaseRepository
{

    public function entity()
    {
        return new PlanchaMedicionTrama();
    }
}