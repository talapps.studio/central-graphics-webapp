<?php

namespace App\Repositories;

use App\Models\Usuario;
use Fluent\Repository\Eloquent\BaseRepository;
class UserRepository extends BaseRepository
{

    /**
     * @inheritDoc
     */
    public function entity()
    {
        return new Usuario();
    }

    public function findByStatus($status): array
    {
        if ($status == null || $status == '') {
            $data['error'] = 'Invalid status params.';
        } else {
            $model = new Usuario();
            $model->select('usuario.id_usuario, usuario.usuario, usuario.nombre, usuario.cod_empleado, usuario.puesto, usuario.email, usuario.id_dpto, departamentos.departamento, usuario.id_grupo, grupos.nombre_grupo, usuario.activo, usuario.upais, cliente_pais.nombre_pais');
            $model->join('cliente_pais', 'cliente_pais.abreviatura = usuario.upais');
            $model->join('departamentos', 'departamentos.id_dpto = usuario.id_dpto');
            $model->join('grupos', 'grupos.id_grupo = usuario.id_grupo');
            if ($status == "all") {
                $data = $model->findAll();
            } else {
                $data = $model->where('usuario.activo', $status)->findAll();
            }
        }
        return $data;
    }

    public function findAllUserInfo($id) {
        $model = new Usuario();
        $model->select('usuario.id_usuario, usuario.usuario, usuario.nombre, usuario.cod_empleado, usuario.puesto, usuario.email, usuario.id_dpto, departamentos.departamento, usuario.id_grupo, grupos.nombre_grupo, usuario.activo, usuario.upais, cliente_pais.nombre_pais, usuario.preferencias');
        $model->join('cliente_pais', 'cliente_pais.abreviatura = usuario.upais');
        $model->join('departamentos', 'departamentos.id_dpto = usuario.id_dpto');
        $model->join('grupos', 'grupos.id_grupo = usuario.id_grupo');
        $model->where('usuario.id_usuario', $id);
        return $model->first();
    }

    public function uniqueUsername($id, $username, $formType) {
        $model = new Usuario();
        $model->where('usuario', $username);
        $user = $model->first();
        if ($formType == 'new') {
            return ($user == null);
        } else if ($formType == 'edit') {
            if ($user) {
                return ($user['id_usuario'] == $id);
            } else {
                return (true);
            }
        }
    }

    function generateRandomPassword($length)
    {
        $key = "";
        $pattern = "1234567890abcdefghijklmnopqrstuvwxyz$@?#";
        $max = strlen($pattern)-1;
        for($i = 0; $i < $length; $i++){
            $key .= substr($pattern, mt_rand(0,$max), 1);
        }
        return $key;
    }

    function uniqueEmail($id, $email, $formType) {
        $model = new Usuario();
        $model->where('email', $email);
        $user = $model->first();
        if ($formType == 'new') {
            return ($user == null);
        } else if ($formType == 'edit') {
            if ($user) {
                return ($user['id_usuario'] == $id);
            } else {
                return (true);
            }
        }
    }
}