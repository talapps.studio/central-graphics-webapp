<?php
namespace App\Repositories;
use App\Models\InventarioMaterial;

class MaterialRepository
{
    protected $modelManager;
    function __construct()
    {

        $this->modelManager = new InventarioMaterial();
    }

    public function getMaterials()//listado de materiales
    {
        $db    = \Config\Database::connect();
        $builder = $db -> table('inventario_material as im');
        $builder-> select('id_inventario_material,
                            id_grupo,
                            codigo_sap,
                            valor,
                            cantidad_unidad,
                            tipo,
                            nombre_material,
                            existencias,
                            id_inventario_equipo,
                            minimo,
                            observacion,
                            numero_individual,
                            numero_cajas,
                            mp_mt,
                            mat_pais');
        $query = $builder->get();
        $materiales = $query->getResult();
       return $materiales;
    }
    

    public function getAll(){
        $materiales = $this->modelManager->findAll();
        return $materiales;
    }
    public function addMaterial($data){
       return $this->modelManager->insert($data);
    }
   
    public function updateMaterial($id,$data){
        $this->modelManager->update($id,$data);
    }

}