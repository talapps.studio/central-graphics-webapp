<?php
namespace App\Repositories;

use App\Models\CotizacionServiciosPredeterminados;

class ServiceRepository
{
    protected $modelService;
    function __construct()
    {
        $this->modelService = new CotizacionServiciosPredeterminados();
    }

    public function getAll(){
        $services = $this->modelService->findAll();
        return $services;
    }

    public function addService($data){
        $this->modelService->insert($data);
    }
    public function updateService($id,$data){
        $this->modelService->update($id,$data);
    }

}