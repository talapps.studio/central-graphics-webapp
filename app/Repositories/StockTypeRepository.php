<?php
namespace App\Repositories;
use App\Models\InventarioTipo;

class StockTypeRepository
{
    public function getStocksType()
    {
        $db    = \Config\Database::connect();
        $builder = $db -> table('inventario_tipo');
        $builder-> select('id_inventario_tipo,
                            nombre_tipo');
        $query = $builder->get();
        $tipos = $query->getResult();
        return $tipos;
    }
}