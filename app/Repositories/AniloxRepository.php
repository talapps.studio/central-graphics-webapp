<?php

namespace App\Repositories;
use App\Models\ClienteAnilox;


class AniloxRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new ClienteAnilox();
    }
    public function getAnilox()
    {
        //consulta con filtrado
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_anilox as cli_an');
        $builder-> select( 'cli_an.id_cliente_anilox,
                            cli_an.id_cliente,
                            cli_an.anilox,
                            cli_an.bcm,
                            cli_an.cantidad');
        $query = $builder->get();
        $rodillos = $query->getResult();
        return $rodillos;

    }

    public function getAll(){
        $rodillos = $this->modelManager->findAll();
        return $rodillos;
    }

    public function addAnilox($data){
        $this->modelManager->insert($data);

    }

    public function addManyAnilox($data){
        $this->modelManager->insertBatch($data);

    }

    public function updateManyAnilox($data){
        $this->modelManager->updateBatch($data);
    }
    
    public function updateAnilox($id,$data){
        $this->modelManager->update($id,$data);
    }

}