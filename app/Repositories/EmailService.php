<?php

namespace App\Repositories;

class EmailService
{

    public function sendNewUserEmail($pass, $to): array
    {
        $email = \Config\Services::email();
        $email->setFrom('noreply.centralgraphics@gmail.com', 'Central Graphics');
        $email->setTo($to);
        $email->setSubject('Bienvenido a Central Graphics');
        $email->setHeader('X-Mailer', 'PHP/' . phpversion());
        $htmlMessage = view('Email/newUser', ['pass' => $pass]);
        $email->setMessage($htmlMessage);
        $emailSend = $email->send();
        if ($emailSend) {
            $data = [
                'msg' => 'Successful: Email enviado correctamente.'
            ];
        } else {
            $data = [
                'msg' => 'Error: Email No enviado.'
            ];
        }
        return $data;
    }

}