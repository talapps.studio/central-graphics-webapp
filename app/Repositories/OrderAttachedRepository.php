<?php
namespace App\Repositories;
use App\Models\PedidoAdjunto;
use Fluent\Repository\Eloquent\BaseRepository;

class OrderAttachedRepository extends BaseRepository
{
   public function entity()
   {
     return PedidoAdjunto ::class;
   }
   
}