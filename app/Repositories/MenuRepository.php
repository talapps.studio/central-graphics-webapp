<?php
namespace App\Repositories;

use App\Models\Menu;

class MenuRepository
{
    protected $menuManager;
    function __construct()
    {
        $this->menuManager = new Menu();
    }
    public function getMenus()
    {
         //consulta con filtrado
         $db      = \Config\Database::connect();
         $builder = $db -> table('menu');
         $builder-> select( 'id_menu,
                             etiqueta,
                             enlace,
                             id_menu_padre,
                             activo,
                             descripcion,
                             icono');
         $builder->where('id_menu_padre !=','0');              
         $builder->orderBy('id_menu_padre','asc');
         $builder->orderBy('id_menu','asc');
         $query = $builder->get();
         $menus = $query->getResultArray();
         for($i =0; $i < count($menus); $i++){
             $modelo = new Menu();
             $modelo->where('id_menu',$menus[$i]['id_menu_padre']);
             $padre = $modelo->first();
             $menus[$i]['padre'] =$padre['etiqueta'];
         }

         return $menus;
    }
    public function getMenuPadre()
    {
         //consulta con filtrado
         $db      = \Config\Database::connect();
         $builder = $db -> table('menu');
         $builder-> select( 'id_menu,
                             etiqueta,
                             enlace,
                             id_menu_padre,
                             activo,
                             descripcion,
                             icono');
         $builder->where('id_menu_padre', 0);  
         $builder->where('activo','s');  
         $builder->orderBy('id_menu','asc');     
         $query = $builder->get();
         $menus = $query->getResult();
         return $menus;
    }
    public function getSubMenu($idPadre){
        $db      = \Config\Database::connect();
        $builder = $db -> table('menu');
        $builder-> select( 'id_menu,
                            etiqueta,
                            enlace,
                            id_menu_padre,
                            activo,
                            descripcion,
                            icono');
        $builder->where('id_menu_padre',$idPadre);
        $builder->where('activo','s');         
        $builder->orderBy('id_menu','asc');
        $query = $builder->get();
        $submenus = $query->getResult();
        return $submenus;
    }
    public function getMenuDept($idDpto){
        $db      = \Config\Database::connect();
        $builder = $db -> table('menu as me');
        $builder->select('me.id_menu, 
                          etiqueta, 
                          enlace, 
                          id_menu_padre, 
                          activo, 
                          descripcion, 
                          icono,
                          med.id_menu');
        $builder->join('menu_departamento as med','me.id_menu = med.id_menu');
        $builder->where('id_departamento',$idDpto);
        $builder->where('id_menu_padre', 0);
        $builder->where('activo','s');
        $builder->orderBy('id_menu_padre','asc');
        $query = $builder->get();
        $menuDepartamento = $query->getResult();
        return $menuDepartamento;
    }
    
    public function getMenuUser($idUser){
        $db      = \Config\Database::connect();
        $builder = $db -> table('menu as me');
        $builder->select('me.id_menu,
                         etiqueta, 
                         enlace,
                         id_menu_padre, 
                         activo,
                         descripcion,
                         icono,
                         meus.id_usuario');
        $builder->join('menu_usuario as meus','me.id_menu = meus.id_menu');
        $builder->where('id_usuario',$idUser);
        $builder->where('id_menu_padre', 0);
        $builder->where('activo','s');
        $query = $builder->get();
        $menuUser = $query->getResult();
        return $menuUser;
      
       
    }
    public function updateMenu($id,$data){
        $this->menuManager->update($id,$data);
    }
    
}