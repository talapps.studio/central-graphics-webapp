<?php
namespace App\Repositories;
use App\Models\InventarioMaterialProveedor;

class SupplierMaterialStockRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new InventarioMaterialProveedor();
    }
    public function getMaterialSuppliersStock(){
        $db      = \Config\Database::connect();
        $builder = $db->table('inventario_material_proveedor as imp');
        $builder->select('imp.id_inventario_material_proveedor,
                          imp.id_inventario_material,
                          imp.id_inventario_proveedor,
                          im.id_grupo,
                          im.codigo_sap,
                          im.valor,
                          im.cantidad_unidad,
                          im.tipo,
                          im.nombre_material,
                          im.existencias,
                          im.id_inventario_equipo,
                          im.minimo,
                          im.observacion,
                          im.numero_individual,
                          im.numero_cajas,
                          im.mp_mt,
                          im.mat_pais,
                          ip.proveedor_nombre,
                          ip.usuario,
                          ip.contrasena');
        $builder->join('inventario_material as im','im.id_inventario_material = imp.id_inventario_material');
        $builder->join('inventario_proveedor as ip','ip.id_inventario_proveedor = imp.id_inventario_proveedor');
        $query = $builder->get();
        $invMaterialProv = $query->getResult();
        return $invMaterialProv;

    }
    public function addMaterialSupplierStock($data){
        $this->modelManager->insert($data);
    }
    public function updateMaterialSupplierStock($id,$data){
        $this->modelManager->update($id,$data);
    }
    public function getAllMp_Mt($codigo,$proveedor,$equipo,$existencias){

        $db      = \Config\Database::connect();
        $builder = $db->table('inventario_material_proveedor as imp');
        $builder->select('imp.id_inventario_material_proveedor,
                          imp.id_inventario_material,
                          imp.id_inventario_proveedor,
                          im.id_grupo,
                          im.codigo_sap,
                          im.valor,
                          im.cantidad_unidad,
                          im.tipo,
                          im.nombre_material,
                          im.existencias,
                          im.id_inventario_equipo,
                          im.minimo,
                          im.observacion,
                          im.numero_individual,
                          im.numero_cajas,
                          im.mp_mt,
                          im.mat_pais,
                          ip.proveedor_nombre,
                          ip.usuario,
                          ip.contrasena');
        $builder->join('inventario_material as im','im.id_inventario_material = imp.id_inventario_material');
        $builder->join('inventario_proveedor as ip','ip.id_inventario_proveedor = imp.id_inventario_proveedor');
        if ($codigo == 'mp' || $codigo == 'mt') {
           $builder->where('im.mp_mt',$codigo);
        } 
        if ($proveedor != '--') {
           $builder->where('imp.id_inventario_proveedor',$proveedor); 
        }
        if ($equipo != '--'){
            $builder->where('im.id_inventario_equipo',$equipo);
        }
        if ($existencias == 'con') {
            
            $builder->where('im.existencias >', 0);
        }
        if ($existencias == 'sin') {
            $builder->where('im.existencias',0);
        }
        $query = $builder->get();
        $invMaterialProv = $query->getResult();
        return $invMaterialProv;

    }
    
}

