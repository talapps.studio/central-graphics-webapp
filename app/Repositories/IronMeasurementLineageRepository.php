<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionLineaje;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementLineageRepository extends BaseRepository
{

    public function entity()
    {
        return new PlanchaMedicionLineaje();
    }
}