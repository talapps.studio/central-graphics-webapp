<?php
namespace App\Repositories;
use App\Models\Producto;

class ProductRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new Producto();
    }
    public function getProducts($status)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('producto as pro');
        $builder->select('pro.id_producto, 
                          pro.producto, 
                          pro.id_material,
                          pro.activo, 
                         ');
        $builder->where('activo >=', $status);
        $query = $builder->get();
        $productos = $query->getResult();
        return $productos;
    }
    public function getAll(){
        $productos = $this->modeloManager->findAll();
        return $productos;
    }
    public function addProduct($data){
        $this->modelManager->insert($data); 
        
    }
    public function updateProduct($id,$data){
         $this->modelManager->update($id,$data);
    }
}