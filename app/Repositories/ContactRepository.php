<?php

namespace App\Repositories;
use App\Models\ClienteContacto;


class ContactRepository
{
    protected $modelManager;
    function __construct()
    {

        $this->modelManager = new ClienteContacto();
    }
    public function getContacts()
    {
        //consulta con filtrado
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_contacto as cli_cont');
        $builder-> select( 'cli_cont.id_cliente_contacto,
                            cli_cont.nombre,
                            cli_cont.cargo,
                            cli_cont.email,
                            cli_cont.tel_oficina,
                            cli_cont.tel_directo,
                            cli_cont.tel_celular');
        $query = $builder->get();
        $contactos = $query->getResult();
        return $contactos;

    }

    public function getAll(){
        $contactos =  $this->modelManager->findAll();
        return $contactos;
    }

    public function addContact($data){
     $this->modelManager->insert($data); 
     
    }

    public function addManyContacts($data){
        $this->modelManager->insertBatch($data);
    }

    public function updateManyContacts($data){
        $this->modelManager->updateBatch($data);
    }

 
    public function updateContact($id,$data){
        $this->modelManager->update($id,$data);
    }

    /**
     * undocumented function summary
     *
     * Undocumented function long description
     *
     * @param Int $idCliente Representa el id del cliente
     * @return Array  Devuelve los contactos relacionados al cliente
     * @throws None
     **/
    public function getContactsByCustomer($idCliente)
    {
        $contactos =  $this->modelManager->where('id_cliente',$idCliente)->findAll();
        return $contactos;
    }

}
