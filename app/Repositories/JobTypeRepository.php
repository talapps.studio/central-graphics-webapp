<?php

namespace App\Repositories;
use App\Models\TipoTrabajo;


class JobTypeRepository
{
    protected $modelManager;
    function __construct(){
        $this->modelManager = new TipoTrabajo();
    }
    public function getAll(){
        $db      = \Config\Database::connect();
        $builder = $db->table('tipo_trabajo as t');
        $builder->select('t.id_tipo_trabajo, 
                          t.trabajo, 
                          t.activo');
        $query = $builder->get();
        $tiposTrabajo = $query->getResult();
        return $tiposTrabajo;
    }
    public function addJobType($data){
        $this->modelManager->insert($data);
    }
    public function updateJobType($id,$data){
        $this->modelManager->update($id,$data);
    }
}