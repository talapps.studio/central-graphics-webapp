<?php

namespace App\Repositories;


class LoadRepository
{

    public function getDeliveryTable() 
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('procesos as pro');
        $builder->select('pro.proceso as lastCode');
        $builder->orderBy('pro.proceso','desc');
        $builder->limit(1);
        $query = $builder->get();
        $proceso = $query->getRow();
        return $proceso;
    }
}