<?php
 namespace App\Repositories;
 use App\Models\ProductoPedido;
 use Fluent\Repository\Eloquent\BaseRepository;
 class OrderProductRepository extends BaseRepository
 {
    public function entity()
    {
        return ProductoPedido::class;
    }
    

 }