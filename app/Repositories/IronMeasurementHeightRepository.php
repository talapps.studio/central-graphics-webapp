<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionAltura;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementHeightRepository extends BaseRepository
{
    public function entity()
    {
        return new PlanchaMedicionAltura();
    }

}