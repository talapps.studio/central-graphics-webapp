<?php

namespace App\Repositories;

use App\Models\Proceso;

class ProcessRepository
{
    protected $modelProcess;
    function __construct()
    {
        $this->modelProcess = new Proceso();
    }
    
    public function getLastProcessByCustomer($idCustomer)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('procesos as pro');
        $builder->select('max(proceso) as lastCode');
        $builder->orderBy('pro.proceso');
        $builder->limit(1);
        $builder->where('pro.proceso REGEXP "^[0-9]+$"');
        $builder->where('pro.proceso !=', '');
        $builder->where('id_cliente = ', $idCustomer);
        $builder->where('activo =', true);
        $query = $builder->get();
        $proceso = $query->getRow();
        return $proceso;
    }


    public function ProcessByCustomer($idCliente)
    {
        $db     = \Config\Database::connect();
        $builder = $db->table('procesos as proc');
        $builder->select('proc.id_proceso as idProc,
                        cli.id_cliente as idCli, 
                        proc.proceso as proceso,
                        proc.nombre as nombre,
                        proc.activo as activo,
                        cli.codigo_cliente as codCliente,
                        cli.nombre as nomCliente');
        $builder->join('cliente as cli', 'proc.id_cliente = cli.id_cliente');
        $builder->where('cli.id_cliente', $idCliente);
        $query = $builder->get();
        $procesos = $query->getResult();
        return $procesos;
    }

    public function getCustomerAndProcess($value)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('cliente as cli');
        $builder->select('cli.id_cliente, 
                          cli.codigo_cliente, 
                          cli.nombre,
                          count(cli.id_cliente) as total_procesos,
                          logo
                         ');
        $builder->join('procesos as proc', 'proc.id_cliente = cli.id_cliente');
        $builder->where('cli.activo', $value);
        $builder->groupBy('cli.id_cliente');
        $builder->orderBy('total_procesos', 'desc');
        $query = $builder->get();
        $clientes = $query->getResult();
        return $clientes;
    }
    public function getNewOderByProcess($idProceso)
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('procesos as pro');
        $builder->select('pro.id_proceso as id_proceso,
                          pro.proceso as proceso,
                          pro.nombre as nomPro,
                          cli.codigo_cliente as codigo_cliente,
                          cli.nombre as nomCli');
        $builder->join('cliente as cli', 'pro.id_cliente = cli.id_cliente');
        $builder->where('pro.id_proceso', $idProceso);
        $query = $builder->get();
        $procesos = $query->getResult();
        return $procesos;
    }

    public function updateProcess($id, $data)
    {
        $this->modelProcess->update($id, $data);
    }

    public function createProcess($data)
    {
        $this->modelProcess->insert($data);
    }
}
