<?php

namespace App\Repositories;
use App\Models\ColorImpresora;


class ColorImpresoraRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new ColorImpresora();
    }

    public function addManyColors($data){
        $this->modelManager->insertBatch($data);
    }

    public function updateManyColors($data){
        $this->modelManager->updateBatch($data);
    }

    public function updateColor($id,$data){
        $this->modelManager->update($id,$data);
    }

}
