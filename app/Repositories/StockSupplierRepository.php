<?php
namespace App\Repositories;
use App\Models\InventarioProveedor;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class StockSupplierRepository
{
   protected $modelManager;
   function __construct()
   {
      $this->modelManager = new InventarioProveedor();
   }
   public function getAll(){
    $db      = \Config\Database::connect();
    $builder = $db->table('inventario_proveedor');
    $builder->select('id_inventario_proveedor,
                      proveedor_nombre,
                      usuario,
                      contrasena,
                      id_grupo,
                      desactivado');
    $builder->where('desactivado =', "0");
    $query = $builder->get();
    $inventarioProveedor = $query->getResult();
    return $inventarioProveedor; 
   } 

   public function addStockSupplier($data){
        $aleatorio = rand(100, 999);
        $contra = substr($data['proveedor_nombre'], 0, 3);
        $usuario = $contra."-".$aleatorio;
        $aleatorio = rand(1000, 99999);
        $contra .= $aleatorio;
        $encrypted = password_hash($contra, PASSWORD_DEFAULT);
        $user = [
            'desactivado'=> 0,
            'usuario'=>$usuario,
            'contrasena'=>$encrypted,
            'proveedor_nombre'=>$data['proveedor_nombre'],
            'id_grupo'=> 1
        ];
     
        $this->modelManager->insert($user);
   }
   public function updateStockSupplier($id,$data){
      $this->modelManager->update($id,$data);
   }
   
   
}