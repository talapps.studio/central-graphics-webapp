<?php

namespace App\Repositories;

use App\Models\RutaPredeterminada;
use App\Repositories\RouteDetailRepository;
use Fluent\Repository\Eloquent\BaseRepository;

class DefaultRouteRepository extends BaseRepository
{
    private $routeDetailRepository;
    protected $rutaPredeterminada;
    function __construct()
    {
        $this->routeDetailRepository = new RouteDetailRepository();
        $this->rutaPredeterminada = new RutaPredeterminada();
    }

    public function entity()
    {
        return RutaPredeterminada::class;
    }
    public function getAll(){
        $db      = \Config\Database::connect();
        $builder = $db->table('ruta_predeterminada');
        $builder->select('id_ruta_pred, 
                          nombre, 
                          activo');
        $query = $builder->get();
        $rutas = $query->getResult();
        return $rutas; 
    }
    public function getDefaultRoute()
    {   $rutasRegistradas = array();
        $db      = \Config\Database::connect();
        $builder = $db -> table('ruta_predeterminada');
        $builder-> select('id_ruta_pred,
                            nombre,
                            activo');
        $query = $builder->get();
        $rutasPredeterminadas = $query->getResult();
        foreach ($rutasPredeterminadas as $rutaPredeterminada){
            $rutaRegistrada = [
                'id_ruta_pred' => $rutaPredeterminada->id_ruta_pred,
                'nombre' => $rutaPredeterminada->nombre,
                'activo'=>$rutaPredeterminada->activo,
                'elementos' => self::routesByElement($rutaPredeterminada->id_ruta_pred),
            ];
            array_push($rutasRegistradas,$rutaRegistrada);

        }
        return $rutasRegistradas;

         
    }
    public function routesByElement($idRoutep){
        $elementosRuta = array();
        $db      = \Config\Database::connect();
        $builder = $db -> table('ruta_detalle as rd');
        $builder-> select('rd.id_ruta_detalle,
                            nombre,
                            id_dpto,
                            descripcion,
                            activo,
                            id_ruta_predeterminada_detalle');
        $builder->join('ruta_predeterminada_detalle_elemento as rpd','rd.id_ruta_detalle = rpd.id_ruta_detalle');
        $builder->where('rpd.id_ruta_pred',$idRoutep);
        $query = $builder->get();
        $elementos = $query->getResult();
        foreach ($elementos as $rutaElemento){
            $rutaElementoNew = [
                'id_ruta_detalle' => $rutaElemento->id_ruta_detalle,
                'id_dpto' => $rutaElemento-> id_dpto,
                'nombre' => $rutaElemento->nombre,
                'descripcion' => $rutaElemento->descripcion,
                'activo'=>$rutaElemento->activo,
                'encargados' => $this->routeDetailRepository->getUserByDepartament($rutaElemento->id_dpto),
            ];
            array_push($elementosRuta,$rutaElementoNew);

        }
        return $elementosRuta;


    }
    
    public function add($data){
        return $this->rutaPredeterminada->insert($data);
    }

    public function update($id,$data){
        $this->rutaPredeterminada->update($id,$data);
    }

}