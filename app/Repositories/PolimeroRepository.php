<?php

namespace App\Repositories;

use App\Models\ConstantePolimero;

class PolimeroRepository
{
    protected $modelManager;
    function __construct()
    {
        $this->modelManager = new ConstantePolimero();
    }
    public function getAllConstants()
    {
        $polimeros = $this->modelManager->findAll();
        return $polimeros;
    }

    public function addPolimero($data)
    {
        $this->modelManager->insert($data);
    }
    public function updatePolimero($id, $data)
    {
        $this->modelManager->update($id, $data);
    }
}
