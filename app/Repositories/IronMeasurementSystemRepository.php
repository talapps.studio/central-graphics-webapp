<?php

namespace App\Repositories;

use App\Models\PlanchaMedicionSistema;
use Fluent\Repository\Eloquent\BaseRepository;

class IronMeasurementSystemRepository extends BaseRepository
{

    public function entity()
    {
        return new PlanchaMedicionSistema();
    }
}