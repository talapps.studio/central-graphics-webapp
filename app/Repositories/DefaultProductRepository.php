<?php
namespace App\Repositories;
use App\Models\ProductosPredeterminados;
use Fluent\Repository\Eloquent\BaseRepository;

class DefaultProductRepository extends BaseRepository
{
   public function entity()
   {
     return ProductosPredeterminados ::class;
   }
}