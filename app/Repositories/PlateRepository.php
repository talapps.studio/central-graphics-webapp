<?php

namespace App\Repositories;
use App\Models\ClientePlaca;

class PlateRepository
{
    protected $modelManager;
    function __construct()
    {

        $this->modelManager = new ClientePlaca();
    }
    public function getPlates()
    {
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_placa as cli_pla');
        $builder-> select( 'cli_pla.id_cliente_placa,
                            cli_pla.id_cliente,
                            cli_pla.altura,
                            cli_pla.lineaje,
                            cli_pla.marca');
        $query = $builder->get();
        $planchas = $query->getResult();
        return $planchas;

    }
    public function getAll(){
        $planchas =  $this->modelManager->findAll();
        return $planchas;
    }

    public function addPlate($data){
        $this->modelManager->insert($data);
    }

    public function addManyPlates($data){
        $this->modelManager->insertBatch($data);
    }
    public function updateManyPlates($data){
        $this->modelManager->updateBatch($data);
    }


    public function updatePlate($id,$data){
        $this->modelManager->update($id,$data);
    }
    
}