<?php

namespace App\Repositories;


class PlannerRepository
{

    public function getInstallments() //tablero de entregas
    {
        $db      = \Config\Database::connect();
        $fecha = date('Y-m-d', strtotime('+ 4 days', strtotime(date('Y-m-d'))));
        $builder = $db->table('cliente as cli');
        $builder->select('codigo_cliente,
                        proceso, 
                        pro.nombre, 
                        pe.id_pedido, 
                        fecha_entrega,
                       esp.id_material_solicitado_grupo'
                    );
        $builder->join('procesos as pro', 'cli.id_cliente = pro.id_cliente');
        $builder->join('pedido as pe', 'pe.id_proceso = pro.id_proceso');
        $builder->join('especificacion_matsolgru esp', 'pe.id_pedido = esp.id_pedido');
        $builder->where('fecha_reale =', "0000-00-00");
        $builder->where('fecha_entrega <=', $fecha);
        $builder->where('fecha_entrega >=', "2012-01-01");
        $builder->groupBy('pe.id_pedido');
        $builder->orderBy('pe.id_pedido asc','pe.fecha_entraga asc');
        $query = $builder->get();
        $pedidos = $query->getResult();
        return $pedidos;
    }

    
    public function getMaterials($estado) //tablero de entregas
    {
        $db      = \Config\Database::connect();
        $builder = $db->table('material_solicitado as t0');
        $builder->select('material_solicitado, id_material_solicitado_grupo');
        $builder->join('material_solicitado_grupo as t1', 't0.id_material_solicitado = t1.id_material_solicitado');
        $builder->where('activo =', $estado);
        $builder->where('id_grupo =', 2);
        $builder->orderBy('material_solicitado asc');
        $query = $builder->get();
        $pedidos = $query->getResult();
        return $pedidos;
    }
}