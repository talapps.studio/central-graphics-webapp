<?php

namespace App\Repositories;

use App\Models\ClientePais;

class PaisRepository extends \Fluent\Repository\Eloquent\BaseRepository
{

    /**
     * @inheritDoc
     */
    public function entity()
    {
        return ClientePais::class;
    }
}