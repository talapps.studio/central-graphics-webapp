<?php

namespace App\Repositories;

use App\Models\PedidoAdjunto;
use App\Models\RutaDepartamento;
use Fluent\Repository\Eloquent\BaseRepository;

class RutaDepartamentoRepository extends BaseRepository
{

    public function entity()
    {
        return new RutaDepartamento();
    }


}