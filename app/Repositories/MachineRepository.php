<?php

namespace App\Repositories;
use App\Models\ClienteMaquina;

class MachineRepository
{
    protected $modelManager;
    function __construct()
    {

        $this->modelManager = new ClienteMaquina();
    }
    public function getMachines()
    {
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_maquina as cli_maq');
        $builder-> select('cli_maq.id_cliente_maquina,
                            cli_maq.id_cliente,
                            cli_maq.maquina,
                            cli_maq.colores');
        $query = $builder->get();
        $maquinas = $query->getResult();
        return $maquinas;
        
    }
    public function getAll(){
        $maquinas =  $this->modelManager->findAll();
        return $maquinas;
    }

    public function addMachine($data){
        $this->modelManager->insert($data);
        
    }

    public function addManyMachines($data){
        $this->modelManager->insertBatch($data);
    }

    public function updateManyMachines($data){
        $this->modelManager->updateBatch($data);
    }

    public function updateMachine($id,$data){
        $this->modelManager->update($id,$data);
    }
}