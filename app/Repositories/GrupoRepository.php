<?php

namespace App\Repositories;

use App\Models\Grupo;

class GrupoRepository extends \Fluent\Repository\Eloquent\BaseRepository
{

    /**
     * @inheritDoc
     */
    public function entity()
    {
        return Grupo::class;
    }
}