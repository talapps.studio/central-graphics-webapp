<?php

namespace App\Repositories;

use App\Models\PedidoAdjunto;
use Fluent\Repository\Eloquent\BaseRepository;

class PedidoAdjuntoRepository extends BaseRepository
{

    public function entity()
    {
        return new PedidoAdjunto();
    }


}