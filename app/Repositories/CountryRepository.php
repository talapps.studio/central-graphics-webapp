<?php

namespace App\Repositories;
use App\Models\ClientePais;

class CountryRepository
{
    protected $modelManager;
    function __construct(){
        $this->modelManager = new ClientePais();
    }
    public function getCountries()
    {
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_pais as cli_pa');
        $builder-> select('cli_pa.id_pais_cliente,
                            cli_pa.abreviatura,
                            cli_pa.nombre_pais,,
                            cli_pa.activo'
                            );
        $query = $builder->get();
        $paises = $query->getResult();
        return $paises;
        
    }

    public function fineByAbrevitura(){
        $db      = \Config\Database::connect();
        $builder = $db -> table('cliente_pais');
        $builder-> select('abreviatura');
        $query = $builder->get();
        $abreviatura= $query->getResult();
        return $abreviatura;
    }
   Public function addCountry($data){
       $this->modelManager->insert($data);
   }
   public function updateCountry($id,$data){
       $this->modelManager->update($id,$data);
   }
}