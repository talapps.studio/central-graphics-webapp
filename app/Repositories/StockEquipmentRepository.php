<?php
namespace App\Repositories;
use App\Models\InventarioEquipo;

class StockEquipmentRepository
{
   public function getStockEquipment(){
    $db      = \Config\Database::connect();
    $builder = $db->table('inventario_equipo');
    $builder->select('id_inventario_equipo,
                      nombre_equipo,
                      id_dpto,
                      id_grupo');
    $query = $builder->get();
    $inventarioEquipo = $query->getResult();
    return $inventarioEquipo; 
   } 
}