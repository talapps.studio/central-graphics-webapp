<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>

<link rel="stylesheet" href="../bootstrap-template/plugins/content-filter/css/style.css">

<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="card">
            <div class="card-header ">
                <h3 class="mt-0">Procesos por cliente</h3>
                <div class="container">

                </div>
                <hr>
                <div class="row">
                    <label for="">Buscar:</label>
                    <div class="cd-filter-content">
                        <input class="form-control" type="search">
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="cd-main-content">
                        <section class="cd-gallery">
                            <ul>
                                <?php foreach ($clientes as $cliente) : ?>
                                    <li class="mix <?= $cliente->nombre ?>  check1 radio2 option3">
                                        <div class="card-customer card-stats">
                                            <?php $logo= ($cliente->logo!=null && $cliente->logo!='') ? '/files/logos_clientes/'.$cliente->logo:'/app-images/logos/logo-cg.png'; ?>
                                            <img class="card-background-gallery" src="<?= $logo ?>"  alt="Image 2">
                                            <div class="card-body">
                                                <h5 class="card-title text-uppercase text-muted mb-0"><a href="/process/processCustomerList/<?= $cliente->id_cliente ?>"><?= $cliente->codigo_cliente ?>-<?= $cliente->nombre ?></a></h5>
                                                <span class="h6 font-weight-bold mb-0" id="total-procesos"><?= $cliente->total_procesos ?> Procesos</span>
                                            </div>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                            <div class="cd-fail-message">No se encontraron resultados</div>
                        </section> <!-- cd-gallery -->
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script src="../bootstrap-template/plugins/content-filter/js/jquery.mixitup.min.js"></script>
<script src="../bootstrap-template/plugins/content-filter/js/main.js"></script>
<!-- Resource jQuery --
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>