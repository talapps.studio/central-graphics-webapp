<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>
<link href="/material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="/material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="/material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0 text-capitalize">Datos del Pedido</h3>
                    <hr>
                    <div class="row">
                        <input type="hidden" id="idProceso" value="<<?= $idProceso ?>">
                        <input type="hidden" id="idCliente" value="<?= $idCliente ?>" />
                        <div class="col-md-4 col-sm-12">
                            <div class="card  card-stats mb-4 mb-xl-0">
                                <img class="card-background" src="/app-images/logos/logo-cg.png" alt="">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="card-title text-uppercase text-muted mb-0"><?php echo $infoProceso[0]->nomPro; ?></h5>
                                            <span class="h3 font-weight-bold mb-0" id="total-entregas"><?php echo $infoProceso[0]->codigo_cliente; ?>-<?php echo $infoProceso[0]->proceso; ?></span>
                                            <div class="row">
                                                <span class=" h6 font-weight-bold mb-0" id="total-entregas"><?php echo $infoProceso[0]->nomCli; ?></span>
                                            </div>

                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <hr class="dark horizontal my-0">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="card  card-stats mb-4 mb-xl-0">
                                <div class="card-body">
                                    <form id="pedidoNuevo">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group fix-bottom">
                                                <label for="editCodigo" class="control-label">Fecha de ingreso</label>
                                                <strong name="fechaIngProceso" id="fechaIngProceso"><?= date('d/m/Y') ?></strong>
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hr-just">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <label for="fechaEntreProceso" class="col-sm-12 control-label">Fecha entrega:</label>
                                            <div class="form-group">
                                                <div class="">
                                                    <input type="date" class="form-control" name="fechaEntreProceso" id="fechaEntreProceso">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="fechaEntreProceso" class="col-sm-12 control-label">Tipo de trabajo:</label>
                                                <select name="tipoTrabajo" id="tipoTrabajo" class="form-control">
                                                    <option value="" selected>Seleccionar</option>
                                                    <?php foreach ($tiposTrabajo as $tipoTrabajo) : ?>
                                                        <option value="<?= $tipoTrabajo->id_tipo_trabajo ?>"><?= $tipoTrabajo->trabajo ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="" class="col-sm-12 control-label">Miniatura</label>
                                                <div class="">
                                                    <input type="file" class="dropify" data-height="100" name="miniaturaPedido" id="miniaturaPedido">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">

                      
                    </div>
                    <div class="row">
                        <div class="card card-stats mb-4 mb-xl-0">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="" class="col-sm-12 control-label">Aplicar Ruta:</label>
                                            <div class="col-sm-12">
                                                <select name="" id="rutaPedido" class="form-control">
                                                    <option value="0">Seleccionar</option>
                                                    <?php foreach ($rutas as $ruta) : ?>
                                                        <option value="<?= $ruta->id_ruta_pred ?>"><?= $ruta->nombre ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                            <div class="" id="rutaElementos">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <input type="checkbox" name="cotizacionPed" id="cotizacionPed" />
                                        <label for="cotizacionPed">Agregar Cotizaci&Oacute;n</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacion">
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacionArte">
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacionMontaje">
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacionPlancha">
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacionPrueba">
                                    </div>
                                </div>
                                <div class="row">
                                    <div id="mostrarCotizcacionOtros">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="observaciones" id="tiny" cols="20" rows="8"></textarea>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-4">
                                    <button type="button" class="btn btn-lg btn-outline-secondary">Cancelar</button>
                                    <button type="button" id="btn-add-order" class="btn btn-lg btn-outline-success">Guardar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>
<?= $this->section('modal') ?>

<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script src="/material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>
<script src="/material-template/assets/js/plugins/tinymce/js/tinymce/tinymce.min.js"></script>
<script src="/material-template/assets/js/jautocalc.min.js"></script>

<?= $this->include('Scripts/AjaxRequest') ?>
<script>
    var precioArte, precioDesarrollo, precioTexto, precioImpresiones, precioMontajes, precioPlanchas,precioColor, precioFlete,precioDestilado;
    let Artes_Items = [];
    let Montajes_Items = [];
    let Plancha_Items = [];
    $(document).ready(function() {
        tinymce.init({
            selector: "textarea",
            plugins: "link image"
        });

        ajaxRequest("/customer/getCustomerProducts?idCliente=" + $('#idCliente').val(), "GET", null)
            .done(function(response) {
                 precioArte= response.data.filter(art=>parseInt(art.id_producto)===5)[0] !=undefined  ?  response.data.filter(art=>parseInt(art.id_producto)===5)[0].precio : 0;
                 precioDesarrollo= response.data.filter(art=>parseInt(art.id_producto)===8)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===8)[0].precio : 0;
                 precioTexto= response.data.filter(art=>parseInt(art.id_producto)===7)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===7)[0].precio : 0;
                 precioImpresiones= response.data.filter(art=>parseInt(art.id_producto)===23)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===23)[0].precio : 0;
                 precioMontajes= response.data.filter(art=>parseInt(art.id_producto)===28)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===28)[0].precio : 0;
                 precioPlanchas= response.data.filter(art=>parseInt(art.id_producto)===29)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===29)[0].precio : 0
                 precioColor =response.data.filter(art=>parseInt(art.id_producto)===73)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===73)[0].precio : 0;
                 precioFlete =response.data.filter(art=>parseInt(art.id_producto)===54)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===54)[0].precio : 0;
                 precioDestilado =response.data.filter(art=>parseInt(art.id_producto)===103)[0] !=undefined ?  response.data.filter(art=>parseInt(art.id_producto)===103)[0].precio : 0;
                Artes_Items.push({
                    id: 5,
                    nombre: 'Arte Cambio de Formato',
                    precio: precioArte
                });
                Artes_Items.push({
                    id: 8,
                    nombre: 'Arte Desarrollo',
                    precio: precioDesarrollo
                });
                Artes_Items.push({
                    id: 7,
                    nombre: 'Arte Cambio de Textos',
                    precio: precioTexto
                });
                Artes_Items.push({
                    id: 23,
                    nombre: 'Impresi&oacute;n',
                    precio: precioImpresiones
                });

                Montajes_Items.push({
                    id: 28,
                    nombre: 'Montaje Negativos',
                    precio: precioMontajes
                });
                Montajes_Items.push({
                    id: 28,
                    nombre: 'Montaje Negativos',
                    precio: precioMontajes
                });
                Montajes_Items.push({
                    id: 28,
                    nombre: 'Montaje Negativos',
                    precio: precioMontajes
                });
                Montajes_Items.push({
                    id: 28,
                    nombre: 'Montaje Negativos',
                    precio: precioMontajes
                });

                Plancha_Items.push({
                    id: 29,
                    nombre: 'Montaje Planchas',
                    precio: precioPlanchas
                });
                Plancha_Items.push({
                    id: 29,
                    nombre: 'Montaje Planchas',
                    precio: precioPlanchas
                });
                Plancha_Items.push({
                    id: 29,
                    nombre: 'Montaje Planchas',
                    precio: precioPlanchas
                });
                Plancha_Items.push({
                    id: 29,
                    nombre: 'Montaje Planchas',
                    precio: precioPlanchas
                });
                Plancha_Items.push({
                    id: 29,
                    nombre: 'Montaje Planchas',
                    precio: precioPlanchas
                });
            });
        $("#rutaPedido").on('change', function() {
            var idRutaPedido = document.getElementById("rutaPedido");

            if (idRutaPedido.value == 0) {
                $('#rutaElementos').empty();
            } else {
                var Puesto = '';
                $('#rutaElementos').empty();
                ajaxRequest("/defaultRoute/getRouteByElement?idRutaPedido=" + idRutaPedido.value, "GET", null)
                    .done(function(response) {
                        response.data.forEach(rl => {
                            var usuarios = '<option value="0" selected>seleccione</option>';
                            rl.encargados.forEach(dp => {
                                usuarios = usuarios + ' <option value="' + dp.id_usuario + '">' + dp.nombre + '</option>';
                            });
                            Puesto = Puesto + '<div class="row "';
                            Puesto = Puesto + '<div class="flujo_div2">';
                            Puesto = Puesto + '<form id="formularioRuta">';
                            Puesto = Puesto + '<br />';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '<div >';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '<div class="row mt-2">';
                            Puesto = Puesto + '<div class="col-sm-2">';
                            Puesto = Puesto + '<input type="radio" name="selectRuta" id="selectRuta_' + rl.id_ruta_detalle + '_' + rl.id_dpto + '" class="radio_button radio-element formulario-ruta" />';
                           
                            Puesto = Puesto  + '<label class="radio-label" for="selectRuta_' + rl.id_ruta_detalle + '_' + rl.id_dpto + '"><i class="radio-icon fa fa-link"></i><span>'+rl.nombre+'</span></label>'
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '<div class="col-sm-6">';
                            Puesto = Puesto + '<label for="" class="col-sm-12 control-label">Asignar a:</label>'
                            Puesto = Puesto + '<select class="form-control formulario-ruta" id="selectUsuario_' + rl.id_ruta_detalle + '" class="form-control>"';
                            Puesto = Puesto + '<option value="0" selected>Seleccionar</option>';
                            Puesto = Puesto + usuarios;
                            Puesto = Puesto + '</select>';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '<div class="col-sm-2">';
                            Puesto = Puesto + '<label for="" class="col-sm-12 control-label">Hora</label>'
                            Puesto = Puesto + '<input class="form-control formulario-ruta" onkeypress="return validacionSoloNumero(event);" id="selectHora_' + rl.id_ruta_detalle + '">';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '<div class="col-sm-2">';
                            Puesto = Puesto + '<label for="" class="col-sm-12 control-label" >Minutos</label>'
                            Puesto = Puesto + '<input class="form-control" id="selectMin_' + rl.id_ruta_detalle + '" onkeypress="return validacionSoloNumero(event);">';
                            Puesto = Puesto + '</div>';
                            Puesto = Puesto + '</form>';
                            Puesto = Puesto + '</div>';
                        });
                        $('#rutaElementos').append(Puesto);
                       /* $('#formularioRuta').validate();
                        $('.formulario-ruta').each(function() {
                            $(this).rules("add",
                                {
                                    required: true
                                })
                        });*/
                    });



            }



        });

        $("#cotizacionPed").on('click', function() {

            if ($(this).is(":checked")) {
                var cotizacion = '';
                cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless">' +
                    '<tr>' +
                    '<th>Producto</th>' +
                    '<th>Subtotal($)</th>' +
                    '<th>Acciones</th>' +
                    '</tr>' +
                    '<tbody>' +
                    '<tr class="line_items">' +
                    '<td>Arte</td>' +
                    '<td class="number"><input class="form-control col-sm-4" id="total_arte" name="item_total_prueba" value="0.00" readonly="readonly"></input></td>' +
                    '<td class="number"><button type="button" id="coti_agr_arte" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i></button><button type="button" id="coti_agr_arte_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>' +
                    '</tr>' +
                    '<tr class="line_items">' +
                    '<td >Negativo</td>' +
                    '<td class="number"><input class="form-control col-sm-4" id="total_negativo" name="item_total_prueba" value="0.00" readonly="readonly"></input></td>' +
                    '<td class="number"><button type="button" id="coti_agr_negativo" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_negativo_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>' +
                    '</tr>' +
                    '<tr class="line_items">' +
                    '<td>Plancha Fotopol&iacute;mera</td>' +
                    '<td class="number"><input class="form-control col-sm-4" id="total_plancha" name="item_total_prueba" value="0.00" readonly="readonly"></input></td>' +
                    '<td class="number"><button type="button" id="coti_agr_plancha" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_plancha_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>' +
                    '</tr>' +
                    '<tr class="line_items">' +
                    '<td>Prueba de color</td>' +
                    '<td class="number"><input class="form-control col-sm-4" id="total_color" name="item_total_prueba" value="0.00" readonly="readonly"></input></td>' +
                    '<td class="number"><button type="button" id="coti_agr_prueba" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_prueba_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>' +
                    '</tr>' +
                    '<tr class="line_items">' +
                    '<td>Otros</td>' +
                    '<td class="number"><input class="form-control col-sm-4" id="total_otros" name="item_total_prueba" value="0.00" readonly="readonly"></input></td>' +
                    '<td class="number"><button type="button" id="coti_agr_otros" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_otros_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>' +
                    '</tr>' +
                    '<tr>' +
                    '<th>Total($)</th>' +
                    '<th><input class="form-control col-sm-4" name="grand_total_cotizacion" value="" id="grand_total_cotizacion" readonly="readonly" jAutoCalc="SUM({item_total_prueba})" ></input></th>' +
                    '</tr>' +
                    '</tbody>' +
                    '</table></form><div>';
                $('#mostrarCotizcacion').append(cotizacion);

            } else {
                $("#mostrarCotizcacion").empty();
                $('#mostrarCotizcacionArte').empty();
                $('#mostrarCotizcacionMontaje').empty();
                $('#mostrarCotizcacionPlancha').empty();
                $('#mostrarCotizcacionPrueba').empty();
                $('#mostrarCotizcacionOtros').empty();
            }
        });
        $(document).on("click", "#coti_agr_arte", function() {

            document.getElementById("coti_agr_arte").disabled = true;
            var artesdefinidos = "";
            Artes_Items.forEach(artes => {
                artesdefinidos += '<tr class="line_items" >';
                artesdefinidos += '<td>';
                artesdefinidos += '<span>' + artes.nombre + '</span>';
                artesdefinidos += '</td>';
                artesdefinidos += '<td><input type="text" class="form-control" name="pulg_' + artes.id + '" id="pulg_' + artes.id + '" size="6" class="cotizaciones"   onkeypress="return validacionSoloNumero(event);"/></td>';
                artesdefinidos += '<td><input type="text" class="form-control" name="prec_' + artes.id + '" id="prec_' + artes.id + '" size="6" class="cotizaciones" value="'+artes.precio+'" onkeypress="return validacionSoloNumero(event);" readonly="readonly" /></td>';
                artesdefinidos += '<td><input type="text" class="form-control" jAutoCalc="{pulg_' + artes.id + '} * {prec_' + artes.id + '}" name="item_total"  size="7" readonly="readonly" /></td>';
                artesdefinidos += '</tr>';
            })
            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table name="cart" class="table table-condensed table-borderless" id="coti_agr_arte_tbl">' +
                '<tr>' +
                '<th>Detalle Arte</th>' +
                '<th style="width: 100px;">Cantidad</th>' +
                '<th style="width: 100px;">Precio($)</th>' +
                '<th style="width: 100px;">Sub Total($)</th>' +
                '</tr>' + artesdefinidos +
                '<tr>' +
                '<th colspan="3">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text"  size="7" name="grand_total_arte" id="grand_total_arte" jAutoCalc="SUM({item_total})" readonly="readonly"></th>' +
                '</tr>' +
                '</table></form><div>';
            $('#mostrarCotizcacionArte').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_arte_quitar", function() {
            document.getElementById("coti_agr_arte").disabled = false;
            $("#total_arte").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionArte').empty();
        });

        $(document).on("click", "#coti_agr_negativo", function() {
            document.getElementById("coti_agr_negativo").disabled = true;
            var montajesfinidos = "";
            Montajes_Items.forEach(artes => {
                montajesfinidos += '<tr class="line_items">';
                montajesfinidos += '<td>' + artes.nombre + '</td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="cant_39" id="cant_28" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="anch_39" id="cant_28" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="alto_39" id="cant_28" value="" size="7" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="pulg_39" id="cant_28" jAutoCalc="{cant_39} * {anch_39} * {alto_39}" value="" size="6"></td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="prec_39" id="cant_28" value="'+artes.precio+'" size="6" onkeypress="return validacionSoloNumero(event);" readonly="readonly"></td>';
                montajesfinidos += '<td><input class="form-control" type="text" name="item_total_montaje" jAutoCalc="{pulg_39} * {prec_39}" value="" size="7"></td>';
                montajesfinidos += '</tr>';
            })
            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_negativo_tbl">' +
                '<tr>' +
                '<th>Detalle Negativo</th>' +
                '<th style="width: 80px;">Cantidad</th>' +
                '<th style="width: 80px;">Ancho</th>' +
                '<th style="width: 80px;">Alto</th>' +
                '<th style="width: 80px;">Pulgadas</th>' +
                '<th style="width: 80px;">Precio($)</th>' +
                '<th style="width: 80px;">Sub Total($)</th>' +
                '</tr>' + montajesfinidos +

                '<tr>' +
                '<th colspan="6">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_montaje" id="grand_total_montaje" jAutoCalc="SUM({item_total_montaje})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table></form><div>';
            $('#mostrarCotizcacionMontaje').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_negativo_quitar", function() {
            document.getElementById("coti_agr_negativo").disabled = false;
            $("#total_negativo").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionMontaje').empty();

        });

        $(document).on("click", "#coti_agr_plancha", function() {
            document.getElementById("coti_agr_plancha").disabled = true;
            var montajesPlanchas = "";
            Plancha_Items.forEach(artes => {
                montajesPlanchas += '<tr class="line_items">';
                montajesPlanchas += '<td>' + artes.nombre + '</td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="cant_39" id="cant_29" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="anch_39" id="anch_29" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="alto_39" id="alto_29" value="" size="7" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="pulg_39" id="pulg_29" jAutoCalc="{cant_39} * {anch_39} * {alto_39}" value="" size="6"></td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="prec_39" id="prec_29" value="'+artes.precio+'" size="6" onkeypress="return validacionSoloNumero(event);" readonly="readonly"></td>';
                montajesPlanchas += '<td><input class="form-control" type="text" name="item_total_plancha" jAutoCalc="{pulg_39} * {prec_39}" value="" size="7"></td>';
                montajesPlanchas += '</tr>';
            })
            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_plancha_tbl">' +
                '<tr>' +
                '<th>Detalle Plancha Fotopol&iacute;mera</th>' +
                '<th style="width: 80px;">Cantidad</th>' +
                '<th style="width: 80px;">Ancho</th>' +
                '<th style="width: 80px;">Alto</th>' +
                '<th style="width: 80px;">Pulgadas</th>' +
                '<th style="width: 80px;">Precio($)</th>' +
                '<th style="width: 80px;">Sub Total($)</th>' +
                '</tr>' + montajesPlanchas +

                '<tr>' +
                '<th colspan="6">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_plancha" id="grand_total_plancha" jAutoCalc="SUM({item_total_plancha})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table></form><div>';
            $('#mostrarCotizcacionPlancha').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_plancha_quitar", function() {
            document.getElementById("coti_agr_plancha").disabled = false;
            $("#total_plancha").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionPlancha').empty();
        });

        $(document).on("click", "#coti_agr_prueba", function() {
            document.getElementById("coti_agr_prueba").disabled = true;

            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_prueba_tbl">' +
                '<tr>' +
                '<th>Detalle Color</th>' +
                '<th style="width: 100px;">Cantidad</th>' +
                '<th style="width: 100px;">Precio($)</th>' +
                '<th style="width: 100px;">Sub Total($)</th>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Prueba de Color</span>' +
                '</td>' +
                '<td><input type="text" class="form-control" name="pulg_73" id="pulg_73" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input type="text" class="form-control" name="prec_73" id="prec_73" size="6" value="'+precioColor+'" onkeypress="return validacionSoloNumero(event);" readonly="readonly"/></td>' +
                '<td><input type="text" class="form-control" name="item_total_prueba" jAutoCalc="{pulg_73} * {prec_73}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_prueba" id="grand_total_prueba" jAutoCalc="SUM({item_total_prueba})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table></form><div>';
            $('#mostrarCotizcacionPrueba').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_prueba_quitar", function() {
            document.getElementById("coti_agr_prueba").disabled = false;
            $("#total_color").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionPrueba').empty();
        });

        $(document).on("click", "#coti_agr_otros", function() {
            document.getElementById("coti_agr_otros").disabled = true;

            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_otros_tbl">' +
                '<tr>' +
                '<th>Detalle Otros</th>' +
                '<th style="width: 100px;">Cantidad</th>' +
                '<th style="width: 100px;">Precio($)</th>' +
                '<th style="width: 100px;">Sub Total($)</th>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Fletes</span>' +
                '</td>' +
                '<td><input class="form-control" type="text" name="pulg_103" id="pulg_54" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input class="form-control" type="text" name="prec_103" id="prec_54" size="6" value="'+precioFlete+'" onkeypress="return validacionSoloNumero(event);" readonly="readonly" /></td>' +
                '<td><input class="form-control" type="text" name="item_total_otros" jAutoCalc="{pulg_103} * {prec_103}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Destilado de Solvente</span>' +
                '</td>' +
                '<td><input class="form-control" type="text" name="pulg_103" id="pulg_103" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input class="form-control" type="text" name="prec_103" id="prec_103" size="6" value="'+precioDestilado+'" onkeypress="return validacionSoloNumero(event);" readonly="readonly"/></td>' +
                '<td><input class="form-control" type="text" name="item_total_otros" jAutoCalc="{pulg_103} * {prec_103}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_otros" id="grand_total_otros" jAutoCalc="SUM({item_total_otros})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table>';
            $('#mostrarCotizcacionOtros').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_otros_quitar", function() {
            document.getElementById("coti_agr_otros").disabled = false;
            $("#total_otros").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionOtros').empty();
        });

        $('#mostrarCotizcacionArte').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_arte").val(document.getElementById("grand_total_arte").value);

        });
        $('#mostrarCotizcacionMontaje').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_negativo").val(document.getElementById("grand_total_montaje").value);

        });
        $('#mostrarCotizcacionPlancha').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_plancha").val(document.getElementById("grand_total_plancha").value);

        });
        $('#mostrarCotizcacionPrueba').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_color").val(document.getElementById("grand_total_prueba").value);

        });
        $('#mostrarCotizcacionOtros').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_otros").val(document.getElementById("grand_total_otros").value);

        });
        $('#mostrarCotizcacion').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
        });
    });
    $('.dropify').dropify({
        messages: {
            'default': 'Drag and drop a file here or click',
            'replace': 'Drag and drop or click to replace',
            'remove': 'Remove',
            'error': 'Ooops, something wrong happended.'
        }
    });

    $(document).on('click', '#btn-add-order', function() {
        var idUsuario, horas, minutos, idRutaDetalle, departamento, grupo = 6;
        let observaciones;
        let imagenSelecionada;
        let fileImage;
        const productoPedidos = [];
        if($("#pedidoNuevo").valid() && $('#formularioRuta').valid()){
        // miniatura
        imagenSelecionada = document.getElementById('miniaturaPedido');
        fileImage = imagenSelecionada.files[0];
        //ruta

        if ($("input:radio.radio_button:checked").length == 1) {
            $("input:radio.radio_button:checked").each(function(i) {
                idRutaDetalle = $(this).attr('id').split('_')[1];
                departamento = $(this).attr('id').split('_')[2];
                if ((document.getElementById('selectUsuario_' + idRutaDetalle).value) !== "0") {
                    idUsuario = document.getElementById('selectUsuario_' + idRutaDetalle).value;
                    horas = document.getElementById('selectHora_' + idRutaDetalle).value;
                    minutos = document.getElementById('selectMin_' + idRutaDetalle).value;

                    if(horas !="" && parseInt(horas,10) >0 ){
                    //cotizacion
                    if ($("#cotizacionPed").is(":checked")) {
                        if ($("#coti_agr_arte").is(":disabled")) {
                            var table = document.getElementById('coti_agr_arte_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: 1,
                                    alto: 1,
                                    pulgadas: 1,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                        if ($("#coti_agr_negativo").is(":disabled")) {
                            var table = document.getElementById('coti_agr_negativo_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[4].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.ancho = table.rows[r].cells[2].firstChild.value;
                                    productoPedido.alto = table.rows[r].cells[3].firstChild.value;
                                    productoPedido.pulgadas = table.rows[r].cells[4].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[5].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }
                            }
                        }
                        if ($("#coti_agr_plancha").is(":disabled")) {
                            var table = document.getElementById('coti_agr_plancha_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[4].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.ancho = table.rows[r].cells[2].firstChild.value;
                                    productoPedido.alto = table.rows[r].cells[3].firstChild.value;
                                    productoPedido.pulgadas = table.rows[r].cells[4].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[5].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }
                            }
                        }
                        if ($("#coti_agr_prueba").is(":disabled")) {
                            var table = document.getElementById('coti_agr_prueba_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                        if ($("#coti_agr_otros").is(":disabled")) {
                            var table = document.getElementById('coti_agr_otros_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                    }
                    //observaciones
                    observaciones = tinymce.get("tiny").getContent()

                    //pedido y sus elementos
                    var tiempo_usuario = (parseInt(horas, 10) * 60) + parseInt(minutos !="" ? minutos : 0, 10);

                    var formData = new FormData()
                    formData.append('tipoTrabajo', $("#tipoTrabajo").val());
                    formData.append('fechaIngProceso', $(".inputCheckbox").parent().find("strong").text());
                    formData.append('fechaEntreProceso', $("#fechaEntreProceso").val());
                    formData.append('departamento', departamento);
                    formData.append('grupo', grupo);
                    formData.append('idUsuario', idUsuario);
                    formData.append('tiempo_usuario', tiempo_usuario);
                    formData.append('idRutaDetalle', idRutaDetalle);
                    formData.append('productos', JSON.stringify(productoPedidos));
                    formData.append('fileImage', fileImage);
                    formData.append('observaciones', observaciones);
                    formData.append('idProceso',$("#idProceso").val());
                    ajaxRequestOrder("/order/addNewOderByProcess", "POST", formData)
                        .done(function(response) {
                            let respuesta=JSON.parse(response);
                            if(respuesta.status === 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: respuesta.mensaje,
                                    confirmButtonText: "Aceptar",
                                });
                            }else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops... algo salio mal',
                                    text: respuesta.mensaje,
                                    confirmButtonText: "Aceptar",
                                });
                            }
                        }).fail(function(xhr, status, error) {}).always(function() {});
                    }else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'El valor en el campo Hora es incorrecto',
                            confirmButtonText: "Aceptar",
                        });
                        console.log('No se ha seleccionado un usuario');

                    }
                } else {
                    Swal.fire({
                        icon: 'warning',
                        title: 'No se ha seleccionado un usuario',
                        confirmButtonText: "Aceptar",
                    });
                    console.log('No se ha seleccionado un usuario');
                }

            });
        } else {
            Swal.fire({
                icon: 'warning',
                title: 'No se ha seleccionado un elemento ruta',
                confirmButtonText: "Aceptar",
            });
            console.log('No se ha seleccionado un elemento ruta');
        }
        }

    });

    function validacionSoloNumero(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
            && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    function radioSeleccionado() {
        document.querySelectorAll('#selectRuta').forEach(r => r.checked = false);
    }
    //validacion de formularios
    $("#pedidoNuevo").validate({
        rules:{
            fechaEntreProceso:{
                required: true,
            },
            tipoTrabajo:{
                required: true,
            },
            miniaturaPedido:{
                required: true,
            }

        }
    });

</script>
<?= $this->endSection() ?>