<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>

<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />

<?= $this->endSection() ?>

<?= $this->section('content') ?>

<div class="row">
  <div class="col-12">

    <div class="row">
      <div class="col-lg-12 col-md-12 col-12 m-auto">
        <div class="card">
          <div class="card-header">
            <h3 class="mt-0">Crear proceso</h3>
          </div>
          <div class="card-body">
            <div class="bg-gradient-secondary shadow-gray border-radius-lg pt-4 pb-3">
              <div class="multisteps-form__progress">
                <button class="multisteps-form__progress-btn js-active" type="button" title="Product Info">
                  <span>1. Informaci&oacute;n General</span>
                </button>
                <button class="multisteps-form__progress-btn" type="button" title="Media">2. Datos del pedido</button>
                <button class="multisteps-form__progress-btn" type="button" title="Pricing">3. Ruta de trabajo</button>
                <button class="multisteps-form__progress-btn" type="button" title="Socials">4. Observaciones</button>
                <button class="multisteps-form__progress-btn" type="button" title="Socials">5. Hoja de planificaci&oacute;n</button>

              </div>
            </div>
            <form class="multisteps-form__form">
              <!--single form panel-->
              <div class="multisteps-form__panel pt-3 border-radius-xl bg-white js-active" data-animation="FadeIn">
                <h6 class="font-weight-bolder">Informaci&oacute;n General</h5>
                  <div class="multisteps-form__content">
                    <div class="row">
                      <div class="col-12">
                        <div class="input-group input-group-static mb-4 ml-2">
                          <label for="exampleFormControlSelect1" class="ms-0">Cliente</label>
                          <select class="form-control customers-select" id="exampleFormControlSelect1">
                            <option value=""></option>
                            <?php foreach ($clientes as $cliente) : ?>
                              <option codigo="<?= $cliente->codigo_cliente ?>" value="<?= $cliente->id_cliente ?>"><?= $cliente->nombre ?></option>
                            <?php endforeach; ?>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-3">
                        <div class="input-group input-group-static mb-4">
                          <label>C&oacute;digo</label>
                          <input id="codigo-cliente" name="codigo-cliente" type="text" class="form-control" disabled>
                        </div>
                      </div>
                      <div class="col-3">
                        <div class="input-group input-group-static mb-4">
                          <label>Proceso</label>
                          <input id="codigo-proceso" name="codigo-proceso" type="text" class="form-control" disabled>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="input-group input-group-static mb-4">
                          <label>Producto</label>
                          <input id="nombre-producto" name="nombre-producto" type="text" class="form-control">
                        </div>
                      </div>
                    </div>

                    <div class="button-row d-flex mt-4">
                      <button class="btn bg-gradient-dark ms-auto mb-0 js-btn-next" type="button" title="Next">Next</button>
                    </div>
                  </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel pt-3 border-radius-xl bg-white" data-animation="FadeIn">
                <h5 class="font-weight-bolder">Datos del pedido</h5>
                <div class="multisteps-form__content">
                  <div class="row">
                    <div class="col-5">
                      <div class="input-group input-group-static mb-4">
                        <label>Fecha de ingreso</label>
                        <input id="fecha-ingreso" name="fecha-ingreso" type="date" class="form-control" disabled>
                      </div>
                    </div>
                    <div class="col-5">
                      <div class="input-group input-group-static mb-4">
                        <label>Fecha de entrega</label>
                        <input id="fecha-entrega" name="fecha-entrega" type="date" class="form-control">
                      </div>
                    </div>
                    <div class="col-2">
                      <div class="input-group input-group-static mb-0">
                        <label>Miniatura</label>
                        <input type="file" class="dropify" data-height="100" />
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="input-group input-group-static mb-4 ml-2">
                        <label for="tipo-trabajo" class="ms-0">Tipo de trabajo</label>
                        <select class="form-control customers-select" id="tipo-trabajo">
                          <option value=""></option>
                          <?php foreach ($tiposTrabajo as $tipoTrabajo) : ?>
                            <option value="<?= $tipoTrabajo->id_tipo_trabajo ?>"><?= $tipoTrabajo->trabajo ?></option>
                          <?php endforeach; ?>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div class="button-row d-flex mt-4">
                    <button class="btn bg-gradient-light mb-0 js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn bg-gradient-dark ms-auto mb-0 js-btn-next" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel pt-3 border-radius-xl bg-white h-100" data-animation="FadeIn">
                <h5 class="font-weight-bolder">Ruta de trabajo</h5>
                <div class="multisteps-form__content mt-3">



                  <div class="button-row d-flex mt-4 col-12">
                    <button class="btn bg-gradient-light mb-0 js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn bg-gradient-dark ms-auto mb-0 js-btn-next" type="button" title="Next">Next</button>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel pt-3 border-radius-xl bg-white" data-animation="FadeIn">
                <h5 class="font-weight-bolder">Observaciones</h5>
                <div class="multisteps-form__content">
                  <div class="row mt-3 mb-5">
                    <div class="col-12">
                      <div id="editor">

                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="button-row d-flex mt-4">
                      <button class="btn bg-gradient-light mb-0 js-btn-prev" type="button" title="Prev">Prev</button>
                      <button class="btn bg-gradient-dark ms-auto mb-0 js-btn-next" type="button" title="Next">Next</button>
                    </div>
                  </div>
                </div>
              </div>
              <!--single form panel-->
              <div class="multisteps-form__panel pt-3 border-radius-xl bg-white" data-animation="FadeIn">
                <h5 class="font-weight-bolder">Hoja de planificaci&oacute;n</h5>
                <div class="multisteps-form__content">
                  <div class="button-row d-flex mt-0 mt-md-4">
                    <button class="btn bg-gradient-light mb-0 js-btn-prev" type="button" title="Prev">Prev</button>
                    <button class="btn bg-gradient-dark ms-auto mb-0" type="button" title="Send">Send</button>
                  </div>

                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script src="../material-template/assets/js/plugins/multistep-form.js"></script>
<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>
<script src="../material-template/assets/js/plugins/multistep-form.js"></script>
<?= $this->include('Scripts/AjaxRequest') ?>
<script>
  $(document).ready(function() {
    $('.customers-select').select2({
      theme: "material",
      placeholder: "Seleccione un elemento de la lista"
    });

    $('#fecha-ingreso').val(new Date().toDateInputValue());

    $('.dropify').dropify({
      messages: {
        'default': 'Drag and drop a file here or click',
        'replace': 'Drag and drop or click to replace',
        'remove': 'Remove',
        'error': 'Ooops, something wrong happended.'
      }
    });

    if (document.getElementById('editor')) {
      var quill = new Quill('#editor', {
        theme: 'snow'
      });
    }
  });
</script>
<script>
  $(document).on("change", ".customers-select", function() {
    var Cliente = {};
    Cliente.idCliente = $(this).val();

    var codigoCliente = $(".customers-select option:selected").attr('codigo');
    $("#codigo-cliente").val(codigoCliente);

    ajaxRequest('/process/getProcessCode', 'POST', Cliente)
      .done(function(response) {
        var lastValue = parseInt(response.lastCode) + 1
        var code = paddy(lastValue, response.lastCode.length)
        $("#codigo-proceso").val(code);
      });

  });
</script>
<script>
  function paddy(num, padlen, padchar) {
    var pad_char = typeof padchar !== 'undefined' ? padchar : '0';
    var pad = new Array(1 + padlen).join(pad_char);
    return (pad + num).slice(-pad.length);
  }

  Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0, 10);
  });
</script>
<?= $this->endSection() ?>
