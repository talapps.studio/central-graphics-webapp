<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0 text-capitalize">Procesos cliente <?= $cliente->nombre ?></h3>
                    <div class="container">

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-10 col-xs-12">

                        </div>
                        <div class="col-sm-12 col-md-2 ">
                            <input type="hidden" id="idCliente" value="<?= $id ?>" />
                            <button type="button" class="btn btn-block btn btn-outline-info" id="crearProceso"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Proceso</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="processTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Correlativo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Trabajo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Modificar Proceso-->
<div class="modal fade" id="editProcess" tabindex="-1" role="dialog" aria-labelledby="processModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Actualizar Proceso</h4>
            </div>
            <div class="modal-body">
                <form id="editprocessForm" class="form row" role="form">
                    <input type="hidden" id="idProceso">
                    <div class="form-group">
                        <label for="editCodigo" class="col-sm-12 control-label">Codigo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="editCodigo" id="editCodigo" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editProceso" class="col-sm-12 control-label">Proceso</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="editProceso" id="editProceso">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label">Cliente</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="editCliente" id="editCliente" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editProducto" class="col-sm-12 control-label">Producto</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="editProducto" id="editProducto">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-process" class="btn btn-outline-success">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
<!--Agregar Pedido-->
<div class="modal fade" id="newOrders" tabindex="-1" role="dialog" aria-labelledby="processModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Pedidos</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered" id="orderTable">
                    <thead>
                        <tr>
                            <th># Pedido</th>
                            <th>Nombre Proceso</th>
                            <th>Fecha Entrada</th>
                            <th>Fecha Entrega</th>
                            <th>Tipo Trabajo</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-new-order" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>

<!--Agregar Proceso-->
<div class="modal fade" id="newProcess" tabindex="-1" role="dialog" aria-labelledby="processModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Proceso</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-sm-12 control-label">Codigo Cliente:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="codigo-cliente" id="codigo-cliente" readonly value="<?= $cliente->codigo_cliente ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-sm-12 control-label">Codigo Proceso:</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control" name="codigo-proceso" id="codigo-proceso">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <label class="col-sm-12 control-label">Nombre:</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" name="nombre-proceso" id="nombre-proceso">
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-add-process" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
    var dataTable;
    $(document).ready(function() {
        dataTable = $('#processTable').DataTable({
            "language": {
                "url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"
            },
            "bDestroy": true,
            "ajax": {
                "url": "/process/getProcessCustomerById?idCliente=" + $('#idCliente').val(),
                "type": "GET",

            },
            "columns": [{
                    "data": "proceso"
                },
                {
                    "data": "nombre"
                },
                {
                    "data": 'activo',
                    render: function(data, type, row) {
                        var buttons = '<div class="btn-group table-options">';
                        buttons += `<a class="move-on-hover text-info btn-agrePed" href="/process/addOrderByProces/${row.idCli}/${row.idProc}" data-toggle="tooltip" data-placement="top" title="Agregar Pedido"><i class="fa fa-list"></i></a>`;
                        buttons += '<a class="move-on-hover text-success btn-new"  data-toggle="tooltip" data-placement="top" title="Agregar Proceso"><i class="fa fa-info-circle"></i></a>';
                        buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar Proceso"><i class="fa fa-lg fa-pencil"></i></a>';
                        if (data == '1') {
                            buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                        } else if (data == '0') {
                            buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                        }
                        return buttons;
                    }
                }
            ],
        });

        $('#processTable tbody').on('click', '.btn-edit', function() {
            let row = dataTable.row($(this).parents('tr')).data();
            $("#editCodigo").val(row.codCliente).closest('div').addClass('is-filled');
            $("#editProceso").val(row.proceso).closest('div').addClass('is-filled');
            $("#editCliente").val(row.nomCliente).closest('div').addClass('is-filled');
            $("#editProducto").val(row.nombre).closest('div').addClass('is-filled');
            $("#idProceso").val(row.idProc);
            $("#editProcess").modal("show");

        });

    });

    $(document).on('click', '#crearProceso', function() {
        showAlertCorrelative();

    });

    $(document).on('click', '#btn-add-process', function() {
        let proceso = {};
        proceso.id_cliente = <?= $cliente->id_cliente ?>;
        proceso.proceso = $("#codigo-proceso").val();
        proceso.nombre = $("#nombre-proceso").val();

        ajaxRequest("/process/addNewProcess", "POST", proceso)
            .done(function(response) {
                if (response.status === 200) {
                    $('#processTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newProcess").modal("hide");
                } else {
                    Swal.fire({
                        icon: 'error',
                        message: response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newProcess").modal("hide");
                }
            }).fail(function(xhr, status, error) {}).always(function() {
                $("#nombre-proceso").val('');
            });
    });

    $(document).on('click', '#btn-edit-process', function() {
        let editProceso = {};
        editProceso.proceso = $("#editProceso").val();
        editProceso.nombre = $("#editProducto").val();
        editProceso.idProceso = $("#idProceso").val();

        ajaxRequest("/process/editProcessCustomer", "POST", editProceso)
            .done(function(response) {
                if (response.status === 200) {
                    $('#processTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editProcess").modal("hide");
                } else {
                    Swal.fire({
                        icon: 'error',
                        message: response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editProcess").modal("hide");
                }
            }).fail(function(xhr, status, error) {}).always(function() {});
    });
    //boton para desactivar un proceso
    $('#processTable tbody').on('click', '.btn-del', function() {
        let row = dataTable.row($(this).parents('tr')).data();
        var inactiveProceso = {}
        inactiveProceso.idProceso = row.idProc;
        Swal.fire({
            title: '¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, desactivar'
        }).then((result) => {
            if (result.isConfirmed) {
                ajaxRequest("/process/inactiveProcessCustomer", "POST", inactiveProceso)
                    .done(function(response) {
                        if (response.status === 200) {
                            $('#processTable').DataTable().ajax.reload();
                            Swal.fire({
                                icon: 'success',
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                        }
                    }).fail(function(xhr, status, error) {}).always(function() {});
            }
        })
    });
    //boton para activar un proceso
    $('#processTable tbody').on('click', '.btn-act', function() {
        let row = dataTable.row($(this).parents('tr')).data();
        var activeProceso = {}
        activeProceso.idProceso = row.idProc;
        Swal.fire({
            title: '¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, activar'
        }).then((result) => {
            if (result.isConfirmed) {
                ajaxRequest("/process/activeProcessCustomer", "POST", activeProceso)
                    .done(function(response) {
                        if (response.status === 200) {
                            $('#processTable').DataTable().ajax.reload();
                            Swal.fire({
                                icon: 'success',
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                        }
                    }).fail(function(xhr, status, error) {}).always(function() {});
            }
        })
    });
    //metodo para redirigir pagina NewOrder
    $('#processTable tbody').on('click', '.btn-new', function() {
        let row = dataTable.row($(this).parents('tr')).data();
        $('#orderTable').DataTable({
            "language": {
                "url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"
            },
            "bDestroy": true,
            "ajax": {
                "url": "/order/getOrderByProcess?id_proceso=" + row.proceso+"&id_cliente="+<?= $cliente->id_cliente ?>,
                "type": "GET",
            },
            "columns": [{
                    "data": "id_pedido",
                    render: function(data, type, row) {
                        buttons = `<a href="/order/details/${row.id_pedido}" target="_blank">${row.id_pedido}</a>`;
                        return buttons;
                    }
                },
                {
                    "data": "nombre_proceso"
                },
                {
                    "data": "fecha_entrada"
                },
                {
                    "data": "fecha_entrega"
                },
                {
                    "data": "trabajo"
                },

            ],
        });
        $("#newOrders").modal("show");
    });

    $('#processTable tbody').on('click', '.btn-agrePed',function(){
        
    });
    //validacion de formularios
    $(document).ready(function() {
        $("#editprocessForm").validate({
            rules: {
                editProceso: {
                    required: true,
                    number: true
                }
            }
        });
    });


    function showAlertCorrelative() {
        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-danger',
                cancelButton: 'btn btn-secondary'
            },
            buttonsStyling: true
        });
        swalWithBootstrapButtons.fire({
            title: "Generacion del correlativo",
            text: "¿Desea generar de forma automatica un correlativo para el proceso?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Si, Generarlo automaticamente!',
            cancelButtonText: 'No, prefiero crearlo!',
            reverseButtons: true
        }).then((result) => {
            if (result.isConfirmed) {
                ajaxMethod('POST', '/process/getProcessCode', {
                        idCliente: <?= $cliente->id_cliente ?>
                    })
                    .done(function(response) {
                        let level = GetLevel(response.lastCode);
                        let newCode = (level > 0) ? LeadingZeros(parseInt(response.lastCode) + 1, level) : parseInt(response.lastCode) + 1;
                        $("#codigo-proceso").val(newCode);
                        $('#newProcess').modal('show');
                    }).fail(function() {

                    }).always(function() {

                    });
            } else if (result.dismiss === Swal.DismissReason.cancel) {
                $('#newProcess').modal('show');
            }
        });
    }

    function GetLevel(num){
        return (num.match(/^0+/) || [''])[0].length;
        
    }
    function LeadingZeros(num, size) {
        var s = num + "";
        while (s.length <= size) s = "0" + s;
        return s;
    }
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>