<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>
<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h3 class="mt-0">cotizaciones</h3>
                <hr>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="" class="label-control">Cliente</label>
                            <select name="" id="idCliente"  class="form-control customers-select">
                                <option value="0">Seleccione un elemento</option>
                                <?php foreach($clientes as $cliente) :?>
                                <option value="<?=$cliente->id_cliente?>"><?=$cliente->nombre?></option>
                                <?php endforeach ;?>
                            </select>
                        </div> 
                    </div>
                </div>
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="" class="label-control">Procesos por cliente</label>
                            <select name="" id="idProceso" class="form-control customers-client-select">
                            </select>
                        </div>
                    </div>
               </div>
               <div class="row">
                    <div class="form-group">
                        <div class="col-sm-12">
                            <label for="" class="label-control">Tipos de trabajo</label>
                            <select name="tipoTrabajo jobSelect" id="tipoTrabajo" class="form-control">
                                                            <option value="">Seleccionar</option>
                                                            <?php foreach ($tiposTrabajo as $tipoTrabajo) : ?>
                                                                <option value="<?= $tipoTrabajo->id_tipo_trabajo ?>"><?= $tipoTrabajo->trabajo ?></option>
                                                            <?php endforeach; ?>
                            </select>
                        </div> 
                    </div>
                </div>
               <div class="row">
                  <form id="cart">
                    <table class="table table-bordered table-dark" id="mostrarCotizacion" >
                        <thead>
                            <th>Producto</th>
                            <th>Subtotal($)</th>
                            <th>Acciones</th>
                        </thead> 
                        <tbody>
                            <tr class="line_items">
                                <td>Arte</td>
                                <td class="number"><input class="form-control col-sm-4" id="total_arte" name="item_total_cotizacion" value="0.00" readonly="readonly"></input></td>
                                <td class="number"><button type="button" id="coti_agr_arte" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i></button><button type="button" id="coti_agr_arte_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>
                            </tr>
                            <tr class="line_items">
                                <td>Negativo</td>
                                <td class="number"><input class="form-control col-sm-4" id="total_negativo" name="item_total_cotizacion" value="0.00" readonly="readonly"></input></td>
                                <td class="number"><button type="button" id="coti_agr_negativo" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_negativo_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>
                            </tr>
                            <tr class="line_items">
                                <td>Plancha Fotopol&iacute;mera</td>
                                <td class="number"><input class="form-control col-sm-4" id="total_plancha" name="item_total_cotizacion" value="0.00" readonly="readonly"></input></td>
                                <td class="number"><button type="button" id="coti_agr_plancha" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_plancha_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>
                            </tr>
                            <tr class="line_items">
                                <td>Prueba de color</td>
                                <td class="number"><input class="form-control col-sm-4" id="total_color" name="item_total_cotizacion" value="0.00" readonly="readonly"></input></td>
                                <td class="number"><button type="button" id="coti_agr_prueba" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_prueba_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>
                            </tr>
                            <tr class="line_items">
                                <td>Otros</td>
                                <td class="number"><input class="form-control col-sm-4" id="total_otros" name="item_total_cotizacion" value="0.00" readonly="readonly"></input></td>
                                <td class="number"><button type="button" id="coti_agr_otros" class="btn btn-outline-info btn-md">Agregar<i class="fa fa-lg fa-plus"></i><button type="button" id="coti_agr_otros_quitar" class="btn btn-outline-primary btn-md">Quitar<i class="fa fa-lg fa-arrow-left"></i></button></td>
                            </tr>
                            <tr>
                                <th>Total($)</th>
                                <th><input class="form-control col-sm-4" name="grand_total_cotizacion" value="" id="grand_total_cotizacion" readonly="readonly" jAutoCalc="SUM({item_total_cotizacion})" ></input></th>
                            </tr>
                        </tbody>
                    </table>
                  </form>
                  <div class="row">
                    <div id="mostrarCotizcacionArte">
                    </div>
                  </div>
                  <div class="row">
                    <div id="mostrarCotizcacionMontaje">
                    </div>
                  </div>
                  <div class="row">
                    <div id="mostrarCotizcacionPlancha">
                    </div>
                  </div>
                  <div class="row">
                    <div id="mostrarCotizcacionPrueba">
                    </div>
                  </div>
                  <div class="row">
                    <div id="mostrarCotizcacionOtros"></div>
                  </div>
                  <div class="row">
                        <div class="col-sm-4">
                            <button type="button" class="btn btn-lg btn-outline-secondary">Cancelar</button>
                            <button type="button" id="btn-add-quotation" class="btn btn-lg btn-outline-success">Guardar</button>
                        </div>
                    </div>
               </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>

<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>
<script src="/material-template/assets/js/jautocalc.min.js"></script>
<?= $this->include('Scripts/AjaxRequest') ?>
<script>
    $(document).ready(function(){
        function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
        $('.customers-select').select2();
        let Artes_Items = [];
        let Montajes_Items =[];
        let Plancha_Items = [];

        Artes_Items.push({id:5,nombre:'Arte Cambio de Formato'});
        Artes_Items.push({id:8,nombre:'Arte Desarrollo'});
        Artes_Items.push({id:7,nombre:'Arte Cambio de Textos'});
        Artes_Items.push({id:23,nombre:'Impresi&oacute;n'});

        Montajes_Items.push({id:28,nombre:'Montaje Negativos'});
        Montajes_Items.push({id:28,nombre:'Montaje Negativos'});
        Montajes_Items.push({id:28,nombre:'Montaje Negativos'});
        Montajes_Items.push({id:28,nombre:'Montaje Negativos'});

        Plancha_Items.push({id:29,nombre:'Montaje Planchas'});
        Plancha_Items.push({id:29,nombre:'Montaje Planchas'});
        Plancha_Items.push({id:29,nombre:'Montaje Planchas'});
        Plancha_Items.push({id:29,nombre:'Montaje Planchas'});

        $(document).on("click","#coti_agr_arte",function(){
            document.getElementById("coti_agr_arte").disabled = true;
            var artesdefinidos = "";
            Artes_Items.forEach(artes =>{
                artesdefinidos+='<tr class="line_items">';
                artesdefinidos+='<td>';
                artesdefinidos+='<span>'+artes.nombre+'</span>';
                artesdefinidos+='</td>';
                artesdefinidos+='<td><input type="text" class="form-control" name="pulg_'+artes.id+'" id="pulg_'+artes.id+'" size="6" class="cotizaciones"  onkeypress="return validacionSoloNumero(event);"/></td>';
                artesdefinidos+='<td><input type="text" class="form-control" name="prec_'+artes.id+'" id="prec_'+artes.id+'" size="6" class="cotizaciones" onkeypress="return validacionSoloNumero(event);" /></td>';
                artesdefinidos+='<td><input type="text" class="form-control" jAutoCalc="{pulg_'+artes.id+'} * {prec_'+artes.id+'}" name="item_total"  size="7" readonly="readonly" /></td>';
                artesdefinidos+='</tr>';
            })
            var cotizacion ='';
            cotizacion= '<div class="row"><form id="cart"><table class="table table-bordered table-dark" id="coti_agr_arte_tbl">'+
                '<tr>'+
                    '<th>Detalle Arte</th>'+
                    '<th style="width: 100px;">Cantidad</th>'+
                    '<th style="width: 100px;">Precio($)</th>'+
                    '<th style="width: 100px;">Sub Total($)</th>'+  
                '</tr>'+artesdefinidos +
                '<tr>'+
                    '<th colspan="3">Total($)</th>'+
                    '<th style="width: 100px;"><input class="form-control" type="text"  size="7" name="grand_total_arte" id="grand_total_arte" jAutoCalc="SUM({item_total})" readonly="readonly"></th>'+
                '</tr>'+
                '</table></form><div>';
            $('#mostrarCotizcacionArte').append(cotizacion);
        });
        $(document).on("click", "#coti_agr_arte_quitar", function() {
            document.getElementById("coti_agr_arte").disabled = false;
            $("#total_arte").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionArte').empty();
        });
        $(document).on("click","#coti_agr_negativo",function(){
            document.getElementById("coti_agr_negativo").disabled = true;
            var montajesfinidos="";
            Montajes_Items.forEach(artes=>{
                montajesfinidos+='<tr class="line_items">';
                montajesfinidos+='<td>'+artes.nombre+'</td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="cant_39" id="cant_28" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="anch_39" id="cant_28" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="alto_39" id="cant_28" value="" size="7" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="pulg_39" id="cant_28" jAutoCalc="{cant_39} * {anch_39} * {alto_39}" value="" size="6"></td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="prec_39" id="cant_28" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesfinidos+='<td><input class="form-control" type="text" name="item_total_montaje" jAutoCalc="{pulg_39} * {prec_39}" value="" size="7"></td>';
                montajesfinidos+='</tr>';
            })
            var cotizacion ='';
            cotizacion= '<div><form id="cart"><table class="table table-bordered table-dark" id="coti_agr_negativo_tbl">'+
                '<tr>'+
                    '<th>Detalle Negativo</th>'+
                    '<th style="width: 80px;">Cantidad</th>'+
                    '<th style="width: 80px;">Ancho</th>'+
                    '<th style="width: 80px;">Alto</th>'+
                    '<th style="width: 80px;">Pulgadas</th>'+
                    '<th style="width: 80px;">Precio($)</th>'+
                    '<th style="width: 80px;">Sub Total($)</th>'+
                '</tr>'+montajesfinidos+
                '<tr>'+
                    '<th colspan="6">Total($)</th>'+
                    '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_montaje" id="grand_total_montaje" jAutoCalc="SUM({item_total_montaje})" size="7" readonly="readonly"></th>'+
                '</tr>'+
            '</table></form><div>';
            $('#mostrarCotizcacionMontaje').append(cotizacion);
        });
        $(document).on("click","#coti_agr_negativo_quitar",function(){
            document.getElementById("coti_agr_negativo").disabled = false;
            $("#total_negativo").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionMontaje').empty();  
        });
        $(document).on("click","#coti_agr_plancha", function(){
            document.getElementById("coti_agr_plancha").disabled = true;
            var montajesPlanchas="";
            Plancha_Items.forEach(artes=>{
                montajesPlanchas+='<tr class="line_items">';
                montajesPlanchas+='<td>'+artes.nombre+'</td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="cant_39" id="cant_29" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="anch_39" id="anch_29" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="alto_39" id="alto_29" value="" size="7" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="pulg_39" id="pulg_29" jAutoCalc="{cant_39} * {anch_39} * {alto_39}" value="" size="6"></td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="prec_39" id="prec_29" value="" size="6" onkeypress="return validacionSoloNumero(event);"></td>';
                montajesPlanchas+='<td><input class="form-control" type="text" name="item_total_plancha" jAutoCalc="{pulg_39} * {prec_39}" value="" size="7"></td>';
                montajesPlanchas+='</tr>';   
            })
            var cotizacion='';
            cotizacion=  '<div><form id="cart"><table class="table table-bordered table-dark" id="coti_agr_plancha_tbl">'+
                '<tr>'+
                    '<th>Detalle Plancha Fotopol&iacute;mera</th>'+
                    '<th style="width: 80px;">Cantidad</th>'+
                    '<th style="width: 80px;">Ancho</th>'+
                    '<th style="width: 80px;">Alto</th>'+
                    '<th style="width: 80px;">Pulgadas</th>'+
                    '<th style="width: 80px;">Precio($)</th>'+
                    '<th style="width: 80px;">Sub Total($)</th>'+
                '</tr>'+montajesPlanchas +
                '<tr>'+
                    '<th colspan="6">Total($)</th>'+
                    '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_plancha" id="grand_total_plancha" jAutoCalc="SUM({item_total_plancha})" size="7" readonly="readonly"></th>'+
                    '</tr>'+
                '</table></form><div>';
            $('#mostrarCotizcacionPlancha').append(cotizacion);
        });
        $(document).on("click", "#coti_agr_plancha_quitar", function() {
            document.getElementById("coti_agr_plancha").disabled = false;
            $("#total_plancha").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionPlancha').empty();
        });
        $(document).on("click", "#coti_agr_prueba", function() {
            document.getElementById("coti_agr_prueba").disabled = true;

            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_prueba_tbl">' +
                '<tr>' +
                '<th>Detalle Color</th>' +
                '<th style="width: 100px;">Cantidad</th>' +
                '<th style="width: 100px;">Precio($)</th>' +
                '<th style="width: 100px;">Sub Total($)</th>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Prueba de Color</span>' +
                '</td>' +
                '<td><input type="text" class="form-control" name="pulg_73" id="pulg_73" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input type="text" class="form-control" name="prec_73" id="prec_73" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input type="text" class="form-control" name="item_total_prueba" jAutoCalc="{pulg_73} * {prec_73}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_prueba" id="grand_total_prueba" jAutoCalc="SUM({item_total_prueba})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table></form><div>';
            $('#mostrarCotizcacionPrueba').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_prueba_quitar", function() {
            document.getElementById("coti_agr_prueba").disabled = false;
            $("#total_color").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionPrueba').empty();
        });
        $(document).on("click", "#coti_agr_otros", function() {
            document.getElementById("coti_agr_otros").disabled = true;

            var cotizacion = '';
            cotizacion = '<div><form id="cart"><table class="table table-condensed table-borderless" id="coti_agr_otros_tbl">' +
                '<tr>' +
                '<th>Detalle Otros</th>' +
                '<th style="width: 100px;">Cantidad</th>' +
                '<th style="width: 100px;">Precio($)</th>' +
                '<th style="width: 100px;">Sub Total($)</th>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Fletes</span>' +
                '</td>' +
                '<td><input class="form-control" type="text" name="pulg_103" id="pulg_54" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input class="form-control" type="text" name="prec_103" id="prec_54" size="6" value="" onkeypress="return validacionSoloNumero(event);" /></td>' +
                '<td><input class="form-control" type="text" name="item_total_otros" jAutoCalc="{pulg_103} * {prec_103}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr class="line_items">' +
                '<td>' +
                '<span>Destilado de Solvente</span>' +
                '</td>' +
                '<td><input class="form-control" type="text" name="pulg_103" id="pulg_103" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input class="form-control" type="text" name="prec_103" id="prec_103" size="6" value="" onkeypress="return validacionSoloNumero(event);"/></td>' +
                '<td><input class="form-control" type="text" name="item_total_otros" jAutoCalc="{pulg_103} * {prec_103}" size="7" readonly="readonly" value="" /></td>' +
                '</tr>' +
                '<tr>' +
                '<th colspan="3">Total($)</th>' +
                '<th style="width: 100px;"><input class="form-control" type="text" name="grand_total_otros" id="grand_total_otros" jAutoCalc="SUM({item_total_otros})" size="7" readonly="readonly"></th>' +
                '</tr>' +
                '</table>';
            $('#mostrarCotizcacionOtros').append(cotizacion);

        });
        $(document).on("click", "#coti_agr_otros_quitar", function() {
            document.getElementById("coti_agr_otros").disabled = false;
            $("#total_otros").val(parseFloat("0.00").toFixed(2));
            $('#mostrarCotizcacionOtros').empty();
        });
        $('#mostrarCotizcacionArte').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_arte").val(document.getElementById("grand_total_arte").value);

        });
        $('#mostrarCotizcacionMontaje').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_negativo").val(document.getElementById("grand_total_montaje").value);

        });
        $('#mostrarCotizcacionPlancha').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_plancha").val(document.getElementById("grand_total_plancha").value);

        });
        $('#mostrarCotizcacionPrueba').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_color").val(document.getElementById("grand_total_prueba").value);

        });
        $('#mostrarCotizcacionOtros').on('click', 'div', function() {
            function autoCalcSetup() {
                $('form#cart').jAutoCalc('destroy');
                $('form#cart tr.line_items').jAutoCalc({
                    keyEventsFire: true,
                    decimalPlaces: 2,
                    emptyAsZero: true
                });
                $('form#cart').jAutoCalc({
                    decimalPlaces: 2
                });
            }
            autoCalcSetup();
            $("#total_otros").val(document.getElementById("grand_total_otros").value);

        });
       
    });
    
    function validacionSoloNumero(evt) {
        var code = (evt.which) ? evt.which : evt.keyCode;
        if (code == 8) {
            return true;
        } else if (code >= 48 && code <= 57) {
            return true;
        } else {
            return false;
        }
    }

    $("#idCliente").on('change', function() {
        //obtengo el id del select

        var idCliente = document.getElementById("idCliente");
        var selectRef = $(".customers-client-select");
   
                ajaxRequest("/quotation/getQuotationProcessByCustomer?idCliente=" + idCliente.value, "GET", null)
                    .done(function(response) {
                        var procesosdeclientes = '<option value="0" selected>seleccione</option>';
                        response.data.forEach(pc => {
                            procesosdeclientes = procesosdeclientes + ' <option value="' + pc.idProc + '">' + pc.nombre + '</option>';
                        });
                    selectRef.html(procesosdeclientes);
                });

    });
    $(document).on('click', '#btn-add-quotation', function() {

       

        let pedido = {};
        const productoPedidos = [];
                         if ($("#coti_agr_arte").is(":disabled")) {
                            var table = document.getElementById('coti_agr_arte_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: 1,
                                    alto: 1,
                                    pulgadas: 1,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                        if ($("#coti_agr_negativo").is(":disabled")) {
                            var table = document.getElementById('coti_agr_negativo_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[4].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.ancho = table.rows[r].cells[2].firstChild.value;
                                    productoPedido.alto = table.rows[r].cells[3].firstChild.value;
                                    productoPedido.pulgadas = table.rows[r].cells[4].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[5].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }
                            }
                        }
                        if ($("#coti_agr_plancha").is(":disabled")) {
                            var table = document.getElementById('coti_agr_plancha_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[4].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.ancho = table.rows[r].cells[2].firstChild.value;
                                    productoPedido.alto = table.rows[r].cells[3].firstChild.value;
                                    productoPedido.pulgadas = table.rows[r].cells[4].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[5].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }
                            }
                        }
                        if ($("#coti_agr_prueba").is(":disabled")) {
                            var table = document.getElementById('coti_agr_prueba_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                        if ($("#coti_agr_otros").is(":disabled")) {
                            var table = document.getElementById('coti_agr_otros_tbl');
                            for (var r = 1, n = table.rows.length - 1; r < n; r++) {
                                let productoPedido = {
                                    id: null,
                                    cantidad: null,
                                    ancho: null,
                                    alto: null,
                                    pulgadas: null,
                                    precio: null
                                }
                                if (table.rows[r].cells[3].firstChild.value != '0.00') {
                                    productoPedido.id = table.rows[r].cells[1].firstChild.id.split('_')[1];
                                    productoPedido.cantidad = table.rows[r].cells[1].firstChild.value;
                                    productoPedido.precio = table.rows[r].cells[2].firstChild.value;
                                    productoPedidos.push(productoPedido);
                                }

                            }
                        }
                       
                        pedido.tipoTrabajo = $("#tipoTrabajo").val();
                        pedido.idProceso = $("#idProceso").val();
                        pedido.productos = productoPedidos;
                  
                    if (productoPedidos.length > 0) {
                        ajaxRequest("/quotation/addNewQuotation", "POST", pedido)
                        .done(function(response) {
                            if (response.status === 200) {
                                Swal.fire({
                                    icon: 'success',
                                    title: response.mensaje,
                                    confirmButtonText: "Aceptar",
                                });
                            }
                        }).fail(function(xhr, status, error) {}).always(function() {});
                    } else{
                        Swal.fire({
                            icon: 'warning',
                            title: 'debe seleccionar por lo menos un producto',
                            confirmButtonText: "Aceptar",
                        });
                    }

                   
       

    });
</script>
<?= $this->endSection() ?>