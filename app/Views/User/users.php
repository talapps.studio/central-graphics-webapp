<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/material-template/assets/css/plugins/country-select-js/css/countrySelect.min.css'); ?>">

<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="row">
        <div class="col-12">
            <div class="panel">
                <div class="card-header">
                    <h3 class="mt-0">Administración de usuarios</h3>
                    <hr>
                    <div class="row">
                        <div class="col-lg-10 col-md-6 col-sm-8">
                            <select id="user-status-select" class="form-select form-select-lg mb-3" aria-label="Usuario filtro">
                                <option disabled>Seleccione una opción</option>
                                <option value="all" selected>Mostrar Todos</option>
                                <option value="s">Usuarios activos</option>
                                <option value="n">Usuarios desabilitados</option>
                            </select>
                        </div>
                        <div class="col-lg-2 col-md-6 col-sm-4">
                            <button id="new-btn" type="button" class="btn btn-outline-info btn-block" data-bs-toggle="modal" data-bs-target="#modal-new-usuario"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar usuario</button>
                        </div>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="table-responsive p-0">
                        <table id="mainTable" class="table align-items-center table-responsive thead-dark" style="width:100%">
                            <thead>
                            <tr class="table-secondary">
                                <th class="text-uppercase font-weight-bolder opacity-7">Usuario</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Nombre completo</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Puesto</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Código empleado</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Email</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">País</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Departamento</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Grupo</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>
    <div class="modal fade" id="modal-new-usuario" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Nuevo usuario</h4>
                </div>
                <div class="modal-body">
                    <form id="save-form" class="form row" role="form">
                        <div class="form-group has-feedback">
                            <label for="new-usuario-pais" class="col-sm-12 control-label">Oficina</label>
                            <div class="col-sm-12">
                                <input class="form-select form-select-lg mb-3" type="text" id="country" readonly/>
                                <input type="hidden" class="CRUD-element" name="pais" id="new-usuario-pais"/>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-nombre" class="col-sm-12 control-label">Nombre completo</label>
                            <div class="col-sm-12">
                                <input type="text" name="nombre" id="new-usuario-nombre" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-usuario" class="col-sm-12 control-label">Nombre de usuario</label>
                            <div class="col-sm-12">
                                <input type="text" name="usuario" id="new-usuario-usuario" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-email" class="col-sm-12 control-label" >Email</label>
                            <div class="col-sm-12">
                                <input type="email" name="email" id="new-usuario-email" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-puesto" class="col-sm-12 control-label">Puesto</label>
                            <div class="col-sm-12">
                                <input type="text" name="puesto" id="new-usuario-puesto" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-codigo" class="col-sm-12 control-label">Código empleado</label>
                            <div class="col-sm-12">
                                <input type="text" name="codigo" id="new-usuario-codigo" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-programable" class="col-sm-12 control-label">Puesto programable</label>
                            <div class="col-sm-12">
                                <input type="checkbox" value="" id="new-usuario-programable" checked>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-activo" class="col-sm-12 control-label">Habilitar usuario</label>
                            <div class="col-sm-12">
                                <input type="checkbox" value="" id="new-usuario-activo" checked>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="new-usuario-departamento" class="col-sm-12 control-label">Departamento</label>
                            <div class="col-sm-12">
                                <select name="departamento" id="new-usuario-departamento" class="form-select mb-3 CRUD-element" aria-label=".form-select-lg example">
                                    <option selected disabled>Seleccione una opción</option>
                                    <?php foreach ($departamentos as $departamento):?>
                                        <option value="<?= $departamento['id_dpto'] ?>"><?= $departamento['departamento'] ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="new-usuario-grupo" class="col-sm-12 control-label">Grupo</label>
                            <div class="col-sm-12">
                                <select name="grupo" id="new-usuario-grupo" class="form-select mb-3 CRUD-element" aria-label=".form-select-lg example">
                                    <option selected disabled>Seleccione una opción</option>
                                    <?php foreach ($grupos as $grupo):?>
                                        <option value="<?= $grupo['id_grupo'] ?>"><?= $grupo['nombre_grupo'] ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="modal-new-btn" type="button" class="btn btn-outline-success">Guardar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-edit-usuario" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Actualizar usuario</h4>
                </div>
                <div class="modal-body">
                    <form id="edit-form" class="form row" role="form">
                        <div class="form-group has-feedback">
                            <label for="edit-usuario-pais" class="col-sm-12 control-label">Oficina</label>
                            <div class="col-sm-12">
                                <input class="form-select form-select-lg mb-3" type="text" id="country-edit" readonly/>
                                <input type="hidden" class="CRUD-element" name="pais" id="edit-usuario-pais"/>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-nombre" class="col-sm-12 control-label">Nombre completo</label>
                            <div class="col-sm-12">
                                <input type="text" name="nombre" id="edit-usuario-nombre" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-usuario" class="col-sm-12 control-label">Nombre de usuario</label>
                            <div class="col-sm-12">
                                <input type="text" name="usuario" id="edit-usuario-usuario" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-email" class="col-sm-12 control-label" >Email</label>
                            <div class="col-sm-12">
                                <input type="email" name="email" id="edit-usuario-email" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-puesto" class="col-sm-12 control-label">Puesto</label>
                            <div class="col-sm-12">
                                <input type="text" name="puesto" id="edit-usuario-puesto" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-codigo" class="col-sm-12 control-label">Código empleado</label>
                            <div class="col-sm-12">
                                <input type="text" name="codigo" id="edit-usuario-codigo" class="form-control CRUD-element">
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-programable" class="col-sm-12 control-label">Puesto programable</label>
                            <div class="col-sm-12">
                                <input type="checkbox" value="" id="edit-usuario-programable" checked>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-activo" class="col-sm-12 control-label">Habilitar usuario</label>
                            <div class="col-sm-12">
                                <input type="checkbox" value="" id="edit-usuario-activo" checked>
                            </div>
                        </div>

                        <div class="form-group has-feedback">
                            <label for="edit-usuario-departamento" class="col-sm-12 control-label">Departamento</label>
                            <div class="col-sm-12">
                                <select name="departamento" id="edit-usuario-departamento" class="form-select mb-3 CRUD-element" aria-label=".form-select-lg example">
                                    <option selected disabled>Seleccione una opción</option>
                                    <?php foreach ($departamentos as $departamento):?>
                                        <option value="<?= $departamento['id_dpto'] ?>"><?= $departamento['departamento'] ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group has-feedback">
                            <label for="edit-usuario-grupo" class="col-sm-12 control-label">Grupo</label>
                            <div class="col-sm-12">
                                <select name="grupo" id="edit-usuario-grupo" class="form-select mb-3 CRUD-element" aria-label=".form-select-lg example">
                                    <option selected disabled>Seleccione una opción</option>
                                    <?php foreach ($grupos as $grupo):?>
                                        <option value="<?= $grupo['id_grupo'] ?>"><?= $grupo['nombre_grupo'] ?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                    <button id="modal-edit-btn" type="button" class="btn btn-outline-success">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-info-usuario" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel">Información de usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="info-usuario-usuario">Nombre de usuario</label>
                        <input type="text" readonly class="form-control" id="info-usuario-usuario">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-nombre">Nombre completo</label>
                        <input type="text" readonly class="form-control" id="info-usuario-nombre">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-puesto">Puesto</label>
                        <input type="text" readonly class="form-control" id="info-usuario-puesto">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-codigo">Cóodigo</label>
                        <input type="text" readonly class="form-control" id="info-usuario-codigo">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-email">Correo Electrónico</label>
                        <input type="text" readonly class="form-control" id="info-usuario-email">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-departamento">Departamento</label>
                        <input type="text" readonly class="form-control" id="info-usuario-departamento">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-grupo">Grupo</label>
                        <input type="text" readonly class="form-control" id="info-usuario-grupo">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-pais"">País</label>
                        <input type="text" readonly class="form-control" id="info-usuario-pais">
                    </div>
                    <div class="form-group">
                        <label for="info-usuario-preferencia">Preferencias</label>
                        <div class="panel">
                            <div class="panel-body">
                                <ul class="list-group" id="info-usuario-preferencia"></ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this-> section('scripts')?>
    <script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/country-select-js/countrySelect.min.js') ?>"></script>
    <script>
        datatableData['status'] = 'all';
        let apiUri = '/userResourceController/';
        let saveForm = $('#save-form');
        let editForm = $('#edit-form');
        let CRUDModel = {
            id_usuario: null,
            usuario: null,
            nombre: null,
            codigo: null,
            puesto: null,
            email: null,
            id_dpto: true,
            id_grupo: null,
            departamento: null,
            grupo: null,
            hora: false,
            activo: null,
            usu_prog: null,
            preferencias: null,
            pais: null
        };

        $("#country").countrySelect({
            defaultCountry: "sv",
            onlyCountries: [
                <?php foreach ($counties as $country): ?>
                    "<?= $country['abreviatura'] ?>",
                <?php endforeach; ?>
            ]
        });

        $("#country-edit").countrySelect({
            defaultCountry: "sv",
            onlyCountries: [
                <?php foreach ($counties as $country): ?>
                "<?= $country['abreviatura'] ?>",
                <?php endforeach; ?>
            ]
        });

        const tableColumns = [
            { "data": "usuario"},
            { "data": "nombre" },
            { "data": "puesto" },
            { "data": "cod_empleado" },
            { "data": "email" },
            { "data": "nombre_pais" },
            { "data": "departamento" },
            { "data": "nombre_grupo" },
            null
        ];
        
        const tableReqData = () => {
            return {'status': $('#user-status-select').val()}
        }

        const tableColumnsDefs = `
                <div class="btn-group table-options">
                    <a class="move-on-hover text-info info-btn" data-toggle="tooltip" data-placement="top" title="Información"><i class="fa fa-info-circle"></i></a>
                    <a class="move-on-hover text-warning px-3 edit-btn" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil"></i></a>
                    <a class="move-on-hover text-danger delete-btn" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o"></i></a>
                </div>
                `;

        initMainDatatable('GET', apiUri, tableReqData(), tableColumns, tableColumnsDefs);

        let userSelect = document.getElementById('user-status-select');

        userSelect.addEventListener('change', function (e) {
            datatableData['status'] = $('#user-status-select').val();
            mainDatatable.ajax.reload();
        });

        document.getElementById("modal-new-btn").addEventListener("click", function () {
            CRUDModel.usuario = document.getElementById("new-usuario-usuario").value;
            CRUDModel.activo = document.getElementById("new-usuario-activo").checked;
            CRUDModel.usu_prog = document.getElementById("new-usuario-programable").checked;
            CRUDModel.pais = $("#country").countrySelect("getSelectedCountryData").iso2;
            if(saveForm.valid()){
                ajaxRequest('POST', apiUri, CRUDModel).always(function() {
                    resetCRUDModel();
                    clearValidation(saveForm);
                });
            }
        });

        document.getElementById("modal-edit-btn").addEventListener("click", function () {
            CRUDModel.usuario = document.getElementById("edit-usuario-usuario").value;
            CRUDModel.activo = document.getElementById("edit-usuario-activo").checked;
            CRUDModel.usu_prog = document.getElementById("edit-usuario-programable").checked;
            CRUDModel.pais = $("#country-edit").countrySelect("getSelectedCountryData").iso2;
            if(editForm.valid()){
                ajaxRequest('PUT', apiUri + CRUDModel.id_usuario, CRUDModel).always(function() {
                    resetCRUDModel();
                    clearValidation(editForm);
                });
            }
        });

        document.getElementById("new-btn").addEventListener("click", function () {
            $('#modal-new-usuario').modal('show');
        });

        $('#mainTable tbody').on('click', '.edit-btn', function () {
            let dataTable = mainDatatable.row( $(this).parents('tr') ).data();
            getRequest(apiUri + dataTable['id_usuario']).done(function (data) {
                setCRUDModel(data.id_usuario, data.usuario, data.nombre, data.cod_empleado, data.puesto, data.email, data.id_dpto, data.id_grupo, data.hora, data.activo, data.usu_prog, data.upais, data.preferencias);
                $('#modal-edit-usuario').modal('show');
            });
        });

        $('#mainTable tbody').on('click', '.delete-btn', function () {
            let dataTable = mainDatatable.row( $(this).parents().parents('tr') ).data();
            setCRUDModel(dataTable['id_usuario'], null, null, null, null, null, null, null, null, null, null, 'sv', null);
            showDeleteAlert('Eliminar usuario', apiUri + dataTable['id_usuario']);
        });

        $('#mainTable tbody').on('click', '.info-btn', function () {
            let dataTable = mainDatatable.row( $(this).parents().parents('tr') ).data();
            getRequest(apiUri + dataTable['id_usuario']).done(function (data) {
                setInfoModal(data);
                $("#modal-info-usuario").modal("show");
            });
        });

        function setCRUDModel(id_usuario, usuario, nombre, cod_empleado, puesto, email, id_dpto, id_grupo, hora, activo, usu_prog, upais, preferencias) {
            CRUDModel.id_usuario = id_usuario;
            CRUDModel.usuario = usuario;
            CRUDModel.nombre = nombre;
            CRUDModel.cod_empleado = cod_empleado;
            CRUDModel.puesto = puesto;
            CRUDModel.email = email;
            CRUDModel.departamento = id_dpto;
            CRUDModel.grupo = id_grupo;
            CRUDModel.hora = hora;
            CRUDModel.activo = activo;
            CRUDModel.usu_prog = usu_prog;
            CRUDModel.preferencias = preferencias;
            CRUDModel.upais = upais;
            setCRUDInput(id_usuario, usuario, nombre, cod_empleado, puesto, email, id_dpto, id_grupo, hora, activo, usu_prog, upais, preferencias);
        }

        function resetCRUDModel() {
            setCRUDModel(null, null, null, null, null, null, false, null, null, null, null, null, null);
        }

        function setCRUDInput(id_usuario, usuario, nombre, cod_empleado, puesto, email, id_dpto, id_grupo, hora, activo, usu_prog, upais, preferencias) {
            document.getElementById("edit-usuario-nombre").value = nombre;
            document.getElementById("edit-usuario-usuario").value = usuario;
            document.getElementById("edit-usuario-codigo").value = cod_empleado;
            document.getElementById("edit-usuario-puesto").value = puesto;
            document.getElementById("edit-usuario-email").value = email;
            changeSelectedOption(document.getElementById("edit-usuario-departamento"), id_dpto);
            changeSelectedOption(document.getElementById("edit-usuario-grupo"), id_grupo);
            document.getElementById("edit-usuario-activo").checked = activo;
            document.getElementById("edit-usuario-programable").checked = usu_prog;
            $('#country-edit').countrySelect("selectCountry", upais);
        }

        function setInfoModal(model) {
            document.getElementById("info-usuario-usuario").value = validEmptyField(model.usuario);
            document.getElementById("info-usuario-nombre").value = validEmptyField(model.nombre);
            document.getElementById("info-usuario-codigo").value = validEmptyField(model.cod_empleado);
            document.getElementById("info-usuario-puesto").value = validEmptyField(model.puesto);
            document.getElementById("info-usuario-email").value = validEmptyField(model.email);
            document.getElementById("info-usuario-departamento").value = validEmptyField(model.departamento);
            document.getElementById("info-usuario-grupo").value = validEmptyField(model.nombre_grupo);
            document.getElementById("info-usuario-pais").value = validEmptyField(model.nombre_pais);
            document.getElementById("info-usuario-preferencia").innerHTML = buildJsonList(model.preferencias);

        }

        function buildJsonList(json) {
            if (json === null || json === undefined || json === "") {
                return "<li>No hay preferencias</li>";
            }
            json = JSON.parse(json);
            let list = `<li>Usuarios: ${json.usuarios}</li>`;
            return list;
        }

        const usuarioFormValidationRulesNew = {
            rules: {
                nombre: {
                    required: true,
                    maxlength: 200,
                },
                usuario: {
                    required: true,
                    maxlength: 30,
                    remote: {
                        url: contextPath + '/UserResourceController/' + 'isUniqueUsername' + '/new',
                        type: 'get',
                        data: {
                            id: function() {
                                return CRUDModel.id_usuario;
                            },
                            user: function() {
                                return CRUDModel.usuario;
                            }
                        }
                    }
                },
                puesto: {
                    required: true,
                    maxlength: 30
                },
                codigo: {
                    required: true,
                    maxlength: 20
                },
                email: {
                    required: true,
                    maxlength: 150,
                    email: true,
                    remote: {
                        url: contextPath + '/UserResourceController/' + 'isUniqueEmail' + '/new',
                        type: 'get',
                        data: {
                            id: function() {
                                return CRUDModel.id_usuario;
                            },
                            user: function() {
                                return CRUDModel.email;
                            }
                        }
                    }
                },
                departamento: {
                    required: true
                },
                grupo: {
                    required: true
                }
            }
        };

        const usuarioFormValidationRulesEdit = {
            rules: {
                nombre: {
                    required: true,
                    maxlength: 200,
                },
                usuario: {
                    required: true,
                    maxlength: 30,
                    remote: {
                        url: contextPath + '/UserResourceController/' + 'isUniqueUsername' + '/edit',
                        type: 'get',
                        data: {
                            id: function() {
                                return CRUDModel.id_usuario;
                            },
                            user: function() {
                                return CRUDModel.usuario;
                            }
                        }
                    }
                },
                puesto: {
                    required: true,
                    maxlength: 30
                },
                codigo: {
                    required: true,
                    maxlength: 20
                },
                email: {
                    required: true,
                    maxlength: 150,
                    email: true,
                    remote: {
                        url: contextPath + '/UserResourceController/' + 'isUniqueEmail' + '/edit',
                        type: 'get',
                        data: {
                            id: function() {
                                return CRUDModel.id_usuario;
                            },
                            user: function() {
                                return CRUDModel.email;
                            }
                        }
                    }
                },
                departamento: {
                    required: true
                },
                grupo: {
                    required: true
                }
            }
        };

        saveForm.validate(usuarioFormValidationRulesNew);
        editForm.validate(usuarioFormValidationRulesEdit);

        document.getElementById('new-usuario-nombre').addEventListener('input', function () {
            document.getElementById("new-usuario-usuario").value = suggestUsername();
        });

        function suggestUsername() {
            let Name = document.getElementById("new-usuario-nombre").value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
            if (Name.length > 0){
                let names = Name.split(" ");
                let suggest = '';
                if (names.length === 1){
                    suggest = names[0].toLowerCase();
                } else if (names.length === 2) {
                    suggest = names[0].toLowerCase() + "." + names[1].toLowerCase();
                } else if (names.length > 2) {
                    suggest = names[0].toLowerCase();
                    for (let i = 1; i < names.length; i++) {
                        suggest = suggest + '.' + names[i].toLowerCase().charAt(0);
                    }
                }
                checkUsername(suggest)
                return suggest;
            } else {
                return "";
            }
        }

        function checkUsername(username) {
            ajaxGET('/UserResourceController/isUniqueUsername/new?user=' + username, null).done(function (data) {
                if (!data) {
                    document.getElementById("new-usuario-usuario").value = username + makeid(6);
                }
            });
        }
    </script>

<?= $this->endSection() ?>