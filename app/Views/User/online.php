<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('/material-template/assets/css/plugins/country-select-js/css/countrySelect.min.css'); ?>">

<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="row">
        <div class="col-12">
            <?php foreach ($grupos as $grupo):?>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Usuarios online | <?= $grupo['nombre_grupo'] ?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive p-0">
                            <table  class="table align-items-center table-responsive thead-dark" style="width:100%">
                                <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Usuario</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre completo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Puesto</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Código empleado</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Email</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">País</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Ultima fecha ingreso</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($departamentos as $departamento):?>
                                    <tr>
                                        <td colspan="7"><?= $departamento['departamento'] ?></td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
<?= $this->endSection() ?>

<?= $this-> section('scripts')?>
    <script>

    </script>

<?= $this->endSection() ?>