<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <h3 class="mt-0">Administraci&oacute;n de medicion sistema </h3>
                    <div  class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarSystem" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Nuevo</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="ironMeasurementSystemTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                            <tr class="table-secondary">
                                <th class="text-uppercase font-weight-bolder opacity-7">Identificador</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Sistema</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar Modal-->
<div class="modal fade" id="newIronMeasurementSystem" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Nueva sistema</h4>
            </div>
            <div class="modal-body">
                <form id="saveIronMeasurementSystem" class="form row" role="form">
                    <div class="form-group">
                        <label for="newNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newNombre" id="newNombre">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-element" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar Modal-->
<div class="modal fade" id="editIronMeasurementSystem" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Actualizar sistema</h4>
            </div>
            <div class="modal-body">
                <form id="updateIronMeasurementSystem" class="form row" role="form">
                    <input type="hidden" id="idPlaMedicionSystem">
                    <div class="form-group">
                        <label for="editNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editNombre" id="editNombre">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-route" class="btn btn-outline-success">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
    var dataTable;
    (function($){
        dataTable = $('#ironMeasurementSystemTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "processing": true,
            "ajax":{
                "url":"/ironMeasurementSystem/getAllIronMeasurementSystem",
                "type":"GET",
            },
            "columns":[
                {"data":"id_pla_medicion_sist"},
                {"data":"sistema"},
                {"data":'activo', render: function(data, type, row){
                        var buttons = '<div class="btn-group table-options">';
                        buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                        if(data == '1'){
                            buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                        }else if(data == '0'){
                            buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                        }
                        return buttons;
                    }
                }
            ],
        });
    })(jQuery);
    //guardar
    $(document).on('click','#agregarSystem', function(){
        $("#newIronMeasurementSystem").modal("show");
    });
    $(document).on('click','#btn-save-element', function(){
        let newIronMeasurement = {};
        newIronMeasurement.sistema = $('#newNombre').val();
        if($("#saveIronMeasurementSystem").valid()){
            ajaxRequest("/ironMeasurementSystem/addNewIronMeasurementSystem","POST", newIronMeasurement)
                .done(function(response){
                    if(response.status === 200){
                        Swal.fire({
                            icon: 'success',
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                        $('#ironMeasurementSystemTable').DataTable().ajax.reload();
                        $("#newIronMeasurementSystem").modal("hide");
                    }else {
                        Swal.fire({
                            icon: 'error',
                            message:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //editar
    $('#ironMeasurementSystemTable tbody').on('click','.btn-edit', function(){
       /* let route = '/ironMeasurementSystem/';

        getRequest(route + row['id_pla_medicion_sist']).done(function(){

        });*/
        let row = dataTable.row($(this).parents('tr')).data();
        $("#editNombre").val(row.sistema).closest('div').addClass('is-filled');
        $("#idPlaMedicionSystem").val(row.id_pla_medicion_sist).closest('div').addClass('is-filled');
        $("#editIronMeasurementSystem").modal("show");
    });
    $(document).on('click','#btn-edit-route', function(){
        let editIronMeasurement = {};
        editIronMeasurement.id =$("#idPlaMedicionSystem").val();
        editIronMeasurement.sistemaEdit = $('#editNombre').val();
        if($("#updateIronMeasurementSystem").valid()){
            ajaxRequest("/ironMeasurementSystem/editIronMeasurementSystem","POST", editIronMeasurement)
                .done(function(response){
                    if(response.status === 200){
                        Swal.fire({
                            icon: 'success',
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                        $('#ironMeasurementSystemTable').DataTable().ajax.reload();
                        $("#editIronMeasurementSystem").modal("hide");
                    }else {
                        Swal.fire({
                            icon: 'error',
                            text:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                }).fail(function (xhr, status, error) {}).always(function () {});

        }
    });
    //desactivar
    $('#ironMeasurementSystemTable tbody').on('click','.btn-del',function(){
        let row = dataTable.row($(this).parents('tr')).data();
        let editIronMeasurement = {};
        editIronMeasurement.id =row.id_pla_medicion_sist;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
            if(result.isConfirmed){
                ajaxRequest("/ironMeasurementSystem/inactiveIronMeasurementSystem","POST",null)
                    .done(function(response){
                        if(response.status === 200){
                            Swal.fire({
                                icon: 'success',
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                            $('#ironMeasurementSystemTable').DataTable().ajax.reload();
                        }else{
                            Swal.fire({
                                icon: 'error',
                                text:response.error,
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                        }

                    }).fail(function (xhr, status, error) {}).always(function () {});
            }
        })
    });
</script>
<script>
    $(document).ready(function(){
        //funcion para resetear campos del modal
        $('#newIronMeasurementSystem').on('hide.bs.modal', function (){
            clearColorValidation('#saveIronMeasurementSystem');
        });
        $('#editIronMeasurementSystem').on('hide.bs.modal', function (){
            clearColorValidation('#updateIronMeasurementSystem');
        });
        //validacion de formularios
        $("#saveIronMeasurementSystem").validate({
            rules:{
                newNombre:{
                    required: true,
                }
            }
        });
        $("#updateIronMeasurementSystem").validate({
            rules:{
                editNombre:{
                    required: true,
                }
            }
        });
    });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>
