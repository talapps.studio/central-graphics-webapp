<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <h3 class="mt-0">Administraci&oacute;n de medición plancha </h3>
                    <div  class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarIroner" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Nuevo</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="ironMeasurementIronerTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                            <tr class="table-secondary">
                                <th class="text-uppercase font-weight-bolder opacity-7">Identificador</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Plancha</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar Modal-->
<div class="modal fade" id="newIronMeasurementIroner" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Nueva plancha</h4>
            </div>
            <div class="modal-body">
                <form id="saveIronMeasurementIroner" class="form row" role="form">
                    <div class="form-group">
                        <label for="newNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" required class="form-control clear-element" name="newNombre" id="newNombre">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-element" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar Modal-->
<div class="modal fade" id="editIronMeasurementIroner" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Actualizar plancha</h4>
            </div>
            <div class="modal-body">
                <form id="updateIronMeasurementIroner" class="form row" role="form">
                    <input type="hidden" id="idPlaMedicionIroner">
                    <div class="form-group">
                        <label for="editNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editNombre" id="editNombre">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-route" class="btn btn-outline-success">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
    var dataTable;
    (function($){
        dataTable = $('#ironMeasurementIronerTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "processing": true,
            "ajax":{
                "url":"/ironMeasurementIroner/getAllIronMeasurementIroner",
                "type":"GET",
            },
            "columns":[
                {"data":"id_pla_medicion_plancha"},
                {"data":"plancha"},
                {"data":'activo', render: function(data, type, row){
                        var buttons = '<div class="btn-group table-options">';
                        buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                        if(data == '1'){
                            buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                        }else if(data == '0'){
                            buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                        }
                        return buttons;
                    }
                }
            ],
        });
    })(jQuery);
    //guardar
    $(document).on('click','#agregarIroner', function(){
        $("#newIronMeasurementIroner").modal("show");
    });
    $(document).on('click','#btn-save-element', function(){
        let newIronMeasurement = {};
        newIronMeasurement.plancha = $('#newNombre').val();
        if($("#saveIronMeasurementIroner").valid()){
            ajaxRequest("/ironMeasurementIroner/addNewIronMeasurementIroner","POST", newIronMeasurement)
                .done(function(response){
                    if(response.status === 200){
                        Swal.fire({
                            icon: 'success',
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                        $('#ironMeasurementIronerTable').DataTable().ajax.reload();
                        $("#newIronMeasurementIroner").modal("hide");
                    }else {
                        Swal.fire({
                            icon: 'error',
                            message:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //editar
    $('#ironMeasurementIronerTable tbody').on('click','.btn-edit', function(){
       /* let route = '/ironMeasurementIroner/';

        getRequest(route + row['id_pla_medicion_plancha']).done(function(){

        });*/
        let row = dataTable.row($(this).parents('tr')).data();
        $("#editNombre").val(row.plancha).closest('div').addClass('is-filled');
        $("#idPlaMedicionIroner").val(row.id_pla_medicion_plancha).closest('div').addClass('is-filled');
        $("#editIronMeasurementIroner").modal("show");
    });
    $(document).on('click','#btn-edit-route', function(){
        let editIronMeasurement = {};
        editIronMeasurement.id = $("#idPlaMedicionIroner").val();
        editIronMeasurement.planchaEdit = $('#editNombre').val();
        if($("#updateIronMeasurementIroner").valid()){
            ajaxRequest("/ironMeasurementIroner/editIronMeasurementIroner","POST", editIronMeasurement)
                .done(function(response){
                    if(response.status === 200){
                        Swal.fire({
                            icon: 'success',
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                        $('#ironMeasurementIronerTable').DataTable().ajax.reload();
                        $("#editIronMeasurementIroner").modal("hide");
                    }else {
                        Swal.fire({
                            icon: 'error',
                            text:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                }).fail(function (xhr, status, error) {}).always(function () {});

        }
    });
    //desactivar
    $('#ironMeasurementIronerTable tbody').on('click','.btn-del',function(){
        let row = dataTable.row($(this).parents('tr')).data();
        let editIronMeasurement = {};
        editIronMeasurement.id = +row.id_pla_medicion_plancha;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
            if(result.isConfirmed){
                ajaxRequest("/ironMeasurementIroner/inactiveIronMeasurementIroner","POST",editIronMeasurement)
                    .done(function(response){
                        if(response.status === 200){
                            Swal.fire({
                                icon: 'success',
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                            $('#ironMeasurementIronerTable').DataTable().ajax.reload();
                        }else{
                            Swal.fire({
                                icon: 'error',
                                text:response.error,
                                title: response.mensaje,
                                confirmButtonText: "Aceptar",
                            });
                        }

                    }).fail(function (xhr, status, error) {}).always(function () {});
            }
        })
    });
</script>
<script>
    $(document).ready(function(){
        //funcion para resetear campos del modal
        $('#newIronMeasurementIroner').on('hide.bs.modal', function (){
            clearColorValidation('#saveIronMeasurementIroner');
        });
        $('#editIronMeasurementIroner').on('hide.bs.modal', function (){
            clearColorValidation('#updateIronMeasurementIroner');
        });
        //validacion de formularios
        $("#newIronMeasurementIroner").validate({
            rules:{
                newNombre:{
                    required: true,
                }
            }
        });
        $("#updateIronMeasurementIroner").validate({
            rules:{
                editNombre:{
                    required: true,
                }
            }
        });
    });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>
