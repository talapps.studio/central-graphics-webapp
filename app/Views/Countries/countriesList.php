<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>
<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<link href="../bootstrap-template/assets/css/bd-wizard.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">  
      <div class="card">
          <div class="card-header">
            <div class="d-lg-flex">
              <div class="col-10">
                <h3 class="mb-0">Administraci&oacute;n de Pa&iacute;ses</h3>
              </div>
              <div class="row">
                <div class="ms-auto my-auto mt-lg-0 mt-4">
                  <div class="ms-auto my-auto">
                    <button type="button" class="btn btn-outline-info pull-right" id="agregar-country"><span class="material-icons">add_circle_outline</span>&nbsp; Agregar Pa&iacute;s</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <hr>
          <div class="card-body">         
            <div class="table-responsive p-0">
              <table class="table align-items-center table-responsive" id="table-country" style="width:100%">
                <thead>
                  <tr class="table-secondary">
                    <th class="text-uppercase font-weight-bolder opacity-7">Codigo</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Abreviatura</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                  </tr>
                </thead>
              <tbody>
             
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>        
  </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agrepar pais Modal -->
<div class="modal fade" id="countryModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title font-weight-normal" id="countryModalLable" >Crear Pa&iacute;s</h4>
            </div>
            <div class="modal-body">
              <form class="form row" role="form" id="form-countries">
                <div class="form-group">
                    <label for="nomCountry" class="col-sm-12 control-label">Nombre</label>
                    <div class="col-sm-12">
                      <input type="text" class="form-control clear-element" name="nomCountry" id="nombre-country">
                    </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-12 control-label">Abreviatura</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control clear-element" id="abreviatura-country" >  
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary"  data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-country" action-type="save" class="btn btn-outline-success">Guardar</button> 
            </div>
        </div>
    </div>
</div>
<!--fin modal-->
<!--Editar pais modal-->
<div class="modal fade" id="countryEditModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h5 class="modal-title" id="countryModalLable" id="tituloModal">Editar Pa&iacute;s</h5>
            </div>
            <div class="modal-body">
              <form id="editform-countries"class="form row" role="form">
                <input type="hidden" id="idCountry">
                <div class="form-group">
                  <label for="editNomCountry" class="col-sm-12 control-label">Nombre</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control clear-element" name="editNomCountry" id="edit-nom-country">
                  </div>
                </div>
                <div class="form-group">
                  <label for="editAbreCountry" class="col-sm-12 control-label">Abreviatura</label>
                  <div class="col-sm-12">
                    <input type="text" class="form-control clear-element" name="editAbreCountry" id="edit-abrev-country">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-edit-country" action-type="save" class="btn btn-outline-success">Guardar</button> 
            </div>
        </div>
    </div>
</div>
<!--fin modal-->

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>
<script src="../bootstrap-template/assets/js/jquery.steps.min.js"></script>
<script src="../bootstrap-template/assets/js/bd-wizard.js"></script>
<script src="../bootstrap-template/assets/js/jquery-repeater.js"></script>

<script>
  var dataTable;
  (function ($){
      "use strict";
      dataTable = $('#table-country').DataTable({
        "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
        "bDestroy": true,
        "ajax": {
                "url": "/country/getAllContries",
                "type": "GET",
            },
        "columns": [
                { "data": "id_pais_cliente" },
                { "data": "nombre_pais" },
                { "data": "abreviatura" },
                {"data": 'activo', render: function (data, type, row) {
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                   if(data == '1'){
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                   } else if(data == '0'){
                    buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                   } 
                    return buttons;
                    }
                }
            ],
      });
  })(jQuery);
  $('#table-country tbody').on('click','.btn-edit',function(){
    let country = '/country/getAllContries/';
    let row = dataTable.row($(this).parents('tr')).data();
    getRequest(country + row['id_pais_cliente']).done(function(data){
      $("#edit-nom-country").val(row.nombre_pais).closest('div').addClass('is-filled');
      $("#edit-abrev-country").val(row.abreviatura).closest('div').addClass('is-filled');
      $("#countryEditModal").modal("show");
      $("#idCountry").val(row.id_pais_cliente);
    
    });
  });
</script>
<script>
//boton modal guardar info pais
$(document).on('click','#btn-save-country',function(){
  let Pais = {};
  Pais.nombre =$('#nombre-country').val();
  Pais.abreviatura =$('#abreviatura-country').val();
  if($("#form-countries").valid()){
    ajaxRequest("/country/addNewCountry","POST", Pais)
    .done(function(response){
      if(response.status === 200){
        $('#table-country').DataTable().ajax.reload();
        console.log(response.mensaje);
        Swal.fire({
             icon: 'success',
             title: response.mensaje,
             confirmButtonText: "Aceptar",
        });
        $("#countryModal").modal("hide");
      }
    }).fail(function (xhr, status, error) {}).always(function () {});
   
  }

});
//boton para guardar actualizaciones
$(document).on('click','#btn-edit-country',function(){
  let editPais = {};
  editPais.nombre= $("#edit-nom-country").val();
  editPais.abreviatura= $("#edit-abrev-country").val();
  editPais.idCountry= $("#idCountry").val();
  if($("#editform-countries").valid()){
    ajaxRequest("/country/editCountry","POST", editPais)
    .done(function (response){
      if(response.status === 200){
        $('#table-country').DataTable().ajax.reload();
        console.log(response.mensaje);
        Swal.fire({
             icon: 'success',
             title: response.mensaje,
             confirmButtonText: "Aceptar",
        });
      }
      $("#countryEditModal").modal("hide");
    }).fail(function (xhr, status, error) {}).always(function () {});
  }
});
//boton para mostrar modal crear pais
 $(document).on('click','#agregar-country',function(){
    $("#countryModal").modal("show");
 });
 //funcion para resetera campos del modal
$('#countryModal').on('hide.bs.modal', function(){

 clearColorValidation('#form-countries');
 
  
});
$('#countryEditModal').on('hide.bs.modal', function (){
    clearColorValidation('#editform-countries');
});
//boton para desactiar paises
$('#table-country').on('click','.btn-del', function(){
  let row = dataTable.row($(this).parents('tr')).data();
  var deactiveCountry = {}
  deactiveCountry.idCountry = row.id_pais_cliente;
    Swal.fire({
      title:'¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, desactivar'
    }).then((result)=>{
    if(result.isConfirmed){
      ajaxRequest("/country/inactiveCountry","POST",deactiveCountry)
      .done(function(response){
        if(response.status === 200){
          $('#table-country').DataTable().ajax.reload();
          Swal.fire({
            icon: 'success',
            title: response.mensaje,
            confirmButtonText: "Aceptar",
          });  
        }        
      }).fail(function (xhr, status, error) {}).always(function () {});  
    }
  })
});
//boton para activar paises
$('#table-country').on('click','.btn-act', function(){
  let row = dataTable.row($(this).parents('tr')).data();
  var activeCountry = {}
  activeCountry.idCountry = row.id_pais_cliente;
    Swal.fire({
      title:'¿Está seguro?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí, activar'
    }).then((result)=>{
    if(result.isConfirmed){
      ajaxRequest("/country/activeCountry","POST",activeCountry)
      .done(function(response){
        if(response.status === 200){
          $('#table-country').DataTable().ajax.reload();
          Swal.fire({
            icon: 'success',
            title: response.mensaje,
            confirmButtonText: "Aceptar",
          });  
        }        
      }).fail(function (xhr, status, error) {}).always(function () {});  
    }
  })
});
//validacion de formulario
$(document).ready(function(){
  $("#form-countries").validate({
    rules:{
      nomCountry:{
        required: true
      }
    }
  });
  $("#editform-countries").validate({
    rules:{
      editNomCountry:{
        required: true
      }
    }
  });
});

</script>

<?= $this->include('Scripts/AjaxRequest') ?>

<?= $this->endSection() ?>
