<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">  
      <div class="panel panel-default">
          <div class="panel-heading">
            <h3  class="mt-0">Listado de Proveedores</h3>
            <div class="row">
                <div class="ms-auto my-auto mt-lg-0 mt-4">
                  <div class="ms-auto my-auto">
                    <button type="button" id="crear-proveedor" class="btn btn-outline-info pull-right"><span class="material-icons">add_circle_outline</span>&nbsp; Agregar Proveedor</button>
                  </div>
                </div>
            </div>
          </div>

          <div class="card-body">         
            <div class="table-responsive">
              <table class="table align-items-center table-responsive" id="table-suppliers" style="width:100%">
                <thead>
                  <tr class="table-secondary">
                    <th class="text-uppercase font-weight-bolder opacity-7">Usuario</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Proveedor</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                  </tr>
                </thead>
              <tbody>
             
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>        
  </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>
<!--agregar proveedor-->
<div class="modal fade" id="invProvModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="inventarioProveedorModalLable" id="titutoModal">Agregar Proveedor</h4>
      </div>
      <div class="modal-body">
        <form id="agregarProv" class="form row" role="form">
          <div class="form-group">
            <label for="nombreProveedor" class="col-sm-12 control-label">Nombre</label>
            <div class="col-sm-12">
              <input type="text" id="nombreProveedor" name="nombreProveedor" class="form-control clear-element"/>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn-save-proveedor" action-type="save" class="btn btn-outline-success">Guardar</button> 
      </div>
    </div>
  </div>
</div>
<!--fin modal-->
<!--editar Proveedor-->
<div class="modal fade" id="editInvProvModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
        <h4 class="modal-title" id="inventarioProveedorModalLable" id="titutoModal">Editar Proveedor</h4>
      </div>
      <div class="modal-body">
        <form id="formInvProv" class="form row" role="form">
          <input type="hidden" id="idInvProv">
            <div class="form-group">
              <label for="nomEditProv" class="col-sm-12 control-label">Nombre</label>
              <div class="col-sm-12">
              <input type="text" id="nomEditProv" name="nomEditProv" class="form-control clear-element">
              </div>
            </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btn-edit-proveedor" action-type="save" class="btn btn-outline-success">Guardar</button> 
      </div>
    </div>
  </div>
</div>
<!--fin modal-->

<?= $this->endSection() ?>

<?= $this-> section('scripts')?>

<script>
   var dataTable;
    (function ($) {
        "use strict";
        dataTable = $('#table-suppliers').DataTable({
            "ordering": true,
            "order": [[ 2, 'asc' ]],
            "bAutoWidth": false,
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ajax": {
                "url": "/stockSupplier/getAllStockSuppliers",
                "type": "GET",
            },
            "columns": [
                { "data": "usuario" },
                { "data": "proveedor_nombre" },
                {"data": '', render: function (data, type, row) {
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                    return buttons;
                    }
                }
            ],
            
        });
      $('#table-suppliers tbody').on('click','.btn-edit',function(){
        let supplier = '/stockSupplier/getAllStockSuppliers/1';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(supplier + row['id_inventario_proveedor']).done(function(data){
          $("#nomEditProv").val(row.proveedor_nombre).closest('div').addClass('is-filled');
          $("#editInvProvModal").modal("show");
          $("#idInvProv").val(row.id_inventario_proveedor);
        }); 
      });
       
    })(jQuery);
</script>
<script>
 //funcion para resetear campos del modal
 $('#invProvModal').on('hide.bs.modal', function (){
  clearColorValidation('#agregarProv');
});
$('#editInvProvModal').on('hide.bs.modal', function (){
  clearColorValidation('#formInvProv');
}); 
  //boton para agregar un nuevo proveedor
$(document).on('click','#btn-save-proveedor',function(){
  let InvProv = {};
  InvProv.nombreProv = $("#nombreProveedor").val();

  if($("#agregarProv").valid()){
    ajaxRequest("/stockSupplier/addNewStockSupplier/","POST", InvProv)
    .done(function (response){
      if(response.status === 200){
        $('#table-suppliers').DataTable().ajax.reload();
          console.log(response.mensaje);
          Swal.fire({
            icon: 'success',
            title: response.mensaje,
            confirmButtonText: "Aceptar",
          });
        $("#invProvModal").modal("hide");
      }
    }).fail(function (xhr, status, error) {}).always(function () {}); 
  }
});
$(document).on('click','#btn-edit-proveedor',function(){
  let editIvnProv = {};
  editIvnProv.nombreProv = $("#nomEditProv").val();
  editIvnProv.idInvProv = $("#idInvProv").val();
  if($("#formInvProv").valid()){
    ajaxRequest("/stockSupplier/editStockSupplier/","POST", editIvnProv)
    .done(function(response){
      if(response.status === 200){
        $('#table-suppliers').DataTable().ajax.reload();
        console.log(response.mensaje);
          Swal.fire({
            icon: 'success',
            title: response.mensaje,
            confirmButtonText: "Aceptar",
          });
        $("#editInvProvModal").modal("hide");
      }
    }).fail(function (xhr, status, error) {}).always(function () {});
  }
});
$('#table-suppliers tbody').on('click','.btn-del', function(){
  let row = dataTable.row($(this).parents('tr')).data();
  var delInvProv= {}
  delInvProv.idInvProv = row.id_inventario_proveedor;
  Swal.fire({
         title:'¿Está seguro?',
         icon: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         //title: response.mensaje,
         confirmButtonText: 'Sí, desactivar'
      }).then((result)=>{
        if(result.isConfirmed){
      ajaxRequest("/stockSupplier/deleteMaterial","POST",delInvProv)
      .done(function(response){
     
      //$("#table-suppliers tr:tg(0)").remove();
        if(response.status === 200){
              $('#table-suppliers').DataTable().ajax.reload();
              Swal.fire(
              'Proveedor Desactivado',
              'success',
              )
            }
     
        }).fail(function (xhr, status, error) {}).always(function () {});
           
        }
      })
});
</script>
<script>
  //boton para agregar un nuevo proveedor
  $(document).on('click','#crear-proveedor',function(){
    $("#invProvModal").modal("show");

 });
</script>
<script>
  $(document).ready(function(){
    $("#agregarProv").validate({
      rules:{
        nombreProveedor:{
          required: true,
          minlength: 3
        }
      }
    });
    $("#formInvProv").validate({
      rules:{
        nomEditProv:{
          required: true,
          minlength: 3
        }
      }
    });
  });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>
