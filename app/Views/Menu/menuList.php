<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="d-lg-flex">
                        <div class="col-10">
                            <h3 class="mt-0">Menu de Usuario</h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-reponsive p-0">
                        <table class="table align-items-center table-responsive" id="table_menu" style="width:100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Enlace</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Descripcion</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Modal editar Menu-->
<div class="modal fade" id="editarMenu" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="idEditMenu">Editar Menu</h4>
            </div>
            <div class="modal-body">
                <form class="form row" role="form" id="editMenu">
                    <input type="hidden" id="idMenu">
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label">Etiqueta</label>
                        <div class="col-sm-12">
                            <input type="text" name="editMenusEtiqueta" id="editMenuEtiqueta"class="form-control clear-element">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label">Icono</label>
                        <div class="col-sm-12">
                            <input type="text" name="editMenuIcono" id="editMenuIcono" class="form-control clear-element">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label">Menu</label>
                        <div class="col-sm-12">
                            <select class="form-control clear-element" name="editMenuPadre" id="editMenuPadre">
                                <option value="0">Principal</option>    
                                <?php foreach ($menus as $menu): ?>
                                <option value="<?= $menu->id_menu?>"><?= $menu->etiqueta ?></option>
                               <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                        <textarea class="form-control" id="editMenuDescripcion" aria-label="With textarea"></textarea>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-outline-success" id="btn-edit-menu" >Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--fin modal editar-->
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<script>
    var dataTable;
    (function ($){
        "use strict";
        dataTable = $('#table_menu').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ordering": false,
            "ajax": {
                "url": "/menu/getAllMenu",
                "type": "GET",
            },
            "columns":[
                { "data": "etiqueta" },
                { "data": "enlace" },
                { "data": "descripcion" },

                {"data": 'activo', render: function (data, type, row) {
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                if(data == 's'){
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                 }else if(data == 'n'){
                    buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>'
                 }
                    return buttons;
                    }
                }
            ],
            "rowGroup":{
                "startRender": function(rows, group){
                    return group;
                },
                
                "dataSrc":'padre'
            },
        });
    })(jQuery);
    $('#table_menu tbody').on('click','.btn-edit', function(){
        let menu = '/menu/getAllMenu/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(menu + row['id_menu']).done(function(data){
            $("#editMenuEtiqueta").val(row.etiqueta).closest('div').addClass('is-filled');
            $("#editMenuPadre").val(row.id_menu_padre).closest('div').addClass('is-filled');
            $("#editMenuDescripcion").val(row.descripcion).closest('div').addClass('is-filled');
            $("#editMenuIcono").val(row.icono).closest('div').addClass('is-filled');
            $("#editarMenu").modal("show");
            $("#idMenu").val(row.id_menu);
        });
    });
</script>
<script>
 $('#editarMenu').on('hide.bs.modal', function (){
    clearColorValidation('#editMenu');
 });
  $(document).on('click','#btn-edit-menu', function(){
     let editMenu = {}
     editMenu.etiqueta= $("#editMenuEtiqueta").val();
     editMenu.menuPadre= $("#editMenuPadre").val();
     editMenu.descripcion= $("#editMenuDescripcion").val();
     editMenu.icono= $("#editMenuIcono").val();
     editMenu.idMenu= $("#idMenu").val();
     if($("#editMenu").valid()){
        ajaxRequest("/menu/editMenu","POST", editMenu)
        .done(function (response){
            if(response.status === 200){
            $('#table_menu').DataTable().ajax.reload();
                console.log(response.mensaje);
                    Swal.fire({
                    icon:'success',
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                    }); 
                $("#editarMenu").modal("hide");
            }
    }).fail(function (xhr, status, error) {}).always(function () {}); 
     }
  });
  $('#table_menu tbody').on('click','.btn-del',function(){
    let row = dataTable.row($(this).parents('tr')).data();
      var InactivarMenu = {}
      InactivarMenu.idMenu = row.id_menu;
      Swal.fire({
        title:'¿Está seguro?',
         icon: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         //title: response.mensaje,
         confirmButtonText: 'Sí, desactivar'
      }).then((result)=>{
          if(result.isConfirmed){
            ajaxRequest("/menu/inactiveMenu","POST",InactivarMenu)
            .done(function(response){
                $('#table_menu').DataTable().ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: "Menu inactivado exitosamente",
                    confirmButtonText: "Aceptar",
                });
            }).fail(function(xhr, status, error) {}).always(function() {});
          }
      })   
  });
  $('#table_menu tbody').on('click','.btn-act', function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var activarMenu = {}
    activarMenu.idMenu = row.id_menu;
    Swal.fire({
        title:'¿Está seguro?',
         icon: 'warning',
         showCancelButton: true,
         confirmButtonColor: '#3085d6',
         cancelButtonColor: '#d33',
         //title: response.mensaje,
         confirmButtonText: 'Sí,activar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/menu/activeMenu","POST",activarMenu)
            .done(function(response){
                $('#table_menu').DataTable().ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: "Menu activado exitosamente",
                    confirmButtonText: "Aceptar",
                });
            }).fail(function(xhr, status, error) {}).always(function() {});
        }
    })
  });
  //validacion de formularios
  $(document).ready(function(){
    $("#editMenu").validate({
        rules:{
            editMenusEtiqueta:{
                required: true,
            },
            editMenuIcono:{
                required: true,
            },
            editMenuPadre:{
                required: true,
            }
        }
    });
  });
</script>


<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>