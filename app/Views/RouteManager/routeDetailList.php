<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <h3 class="mt-0">Administraci&oacute;n Elementos de Ruta </h3>
                    <div  class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarElemento" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Elemento</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="routeTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">nombre</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Descripcion</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar elemento Modal-->
<div class="modal fade" id="newElement" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Nuevo Elemento de Ruta</h4>
            </div>
            <div class="modal-body">
                <form id="saveElement" class="form row" role="form">
                    <div class="form-group">
                        <label for="newNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newNombre" id="newNombre">
                        </div>
                    </div>
                    <div class="form-goup">
                        <label for="addDepartment" class="col-sm-12 control-label">Departamento</label>
                        <div class="col-sm-12">
                            <select name="" id="newRutaDepartamento" class="form-control">
                                <option value="0">Seleccionar</option>
                                <?php foreach($departamentos as $departamento) :?>
                                    <option value="<?=$departamento->id_dpto?>"><?=$departamento->departamento?></option>
                                <?php endforeach ;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newDescripcion" id="newDescripcion">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-element" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar elemento Modal-->
<div class="modal fade" id="editarElement" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Actualizar Elemento de Ruta</h4>
            </div>
            <div class="modal-body">
                <form id="editElement" class="form row" role="form">
                <input type="hidden" id="idElement">
                    <div class="form-group">
                        <label for="editNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editNombre" id="editNombre">
                        </div>
                    </div>
                    <div class="form-goup">
                        <label for="editRutaDepartmento" class="col-sm-12 control-label">Departamento</label>
                        <div class="col-sm-12">
                            <select name="editRutaDepartmento" id="editRutaDepartmento" class="form-control">
                                <option value="0">Seleccionar</option>
                                <?php foreach($departamentos as $departamento) :?>
                                    <option value="<?=$departamento->id_dpto?>"><?=$departamento->departamento?></option>
                                <?php endforeach ;?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editDescripcion" id="editDescripcion">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-element" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
     var dataTable;
    (function($){
        dataTable = $('#routeTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "processing": true,
            "ajax":{
                "url":"/routeDetail/getAllRouteDetail",
                "type":"GET",
            },
            "columns":[
                {"data":"nombre"},
                {"data":"descripcion"},
                {"data":'activo', render: function(data, type, row){
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                  if(data == '1'){
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                  }else if(data == '0'){
                    buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                  }
                    return buttons;    
                    }
                }
            ],
        }); 
    })(jQuery);
    $('#routeTable tbody').on('click','.btn-edit', function(){
        let element = '/getAllRouteDetail/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(element + row['id_ruta_detalle']).done(function(data){
            $("#editNombre").val(row.nombre).closest('div').addClass('is-filled');
            $("#editDescripcion").val(row.descripcion).closest('div').addClass('is-filled');
            $("#editRutaDepartmento").val(row.id_dpto).closest('div').addClass('is-filled');
            $("#idElement").val(row.id_ruta_detalle);
            $("#editarElement").modal("show");
           
        });
    });
</script>
<script>
    //boton para mostar modal agregar Elemento
    $(document).on('click','#agregarElemento', function(){
        $("#newElement").modal("show");
    });
    //funcion para resetear campos del modal
    $('#newElement').on('hide.bs.modal', function (){
       clearColorValidation('#saveElement');  
    });
    $('#editarElement').on('hide.bs.modal', function (){
       clearColorValidation('#editElement');  
    });
    //boton para guardar informacion de Elementos de una ruta
    $(document).on('click','#btn-save-element', function(){
        let newElement = {};
        newElement.nombre = $('#newNombre').val();
        newElement.descripcion = $('#newDescripcion').val();
        newElement.departamento = $('#newRutaDepartamento').val();  
       if($("#saveElement").valid()){
            ajaxRequest("/routeDetail/addNewRouteDetail","POST", newElement)
            .done(function(response){
                if(response.status === 200){
                    $('#routeTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newElement").modal("hide");
                }else {
                    Swal.fire({
                        icon: 'error',
                        text:response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newElement").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //boton para actualizar informacion de Elementos de una ruta
    $(document).on('click','#btn-edit-element', function(){
        let editElement = {};
        editElement.nombre = $("#editNombre").val();
        editElement.descripcion = $("#editDescripcion").val();
        editElement.departamento =$("#editRutaDepartmento").val();
        editElement.idElement = $("#idElement").val();
        if($("#editElement").valid()){
            ajaxRequest("/routeDetail/editRouteDetail","POST", editElement)
            .done(function(response){
                if(response.status === 200){
                    $('#routeTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editarElement").modal("hide");
                }else {
                    Swal.fire({
                        icon: 'error',
                        message:response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editarElement").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});   
        }
    });
    //boton para desactivar elemento de una ruta
    $('#routeTable tbody').on('click','.btn-del', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var inactiveRoute ={}
        inactiveRoute.idElement = row.id_ruta_detalle;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
           if(result.isConfirmed){
                ajaxRequest("/routeDetail/inactiveRouteDetail","POST",inactiveRoute)
                .done(function(response){
                 if(response.status === 200){
                        $('#routeTable').DataTable().ajax.reload();
                        Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                        });  
                    }else {
                        Swal.fire({
                            icon: 'error',
                            message:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                
                }).fail(function (xhr, status, error) {}).always(function () {});  
            }
        })
    });
    //boton para activar elemento de una ruta
    $('#routeTable tbody').on('click','.btn-act', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var activeRoute ={}
        activeRoute.idElement = row.id_ruta_detalle;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, activar'
        }).then((result)=>{
           if(result.isConfirmed){
                ajaxRequest("/routeDetail/activeRouteDetail","POST",activeRoute)
                .done(function(response){
                 if(response.status === 200){
                        $('#routeTable').DataTable().ajax.reload();
                        Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                        });  
                    }else {
                        Swal.fire({
                            icon: 'error',
                            message:response.error,
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                
                }).fail(function (xhr, status, error) {}).always(function () {});  
            }
        })
    });
    //validacion de formularios
    $(document).ready(function(){
        $("#saveElement").validate({
            rules:{
                newNombre:{
                    required: true
                },
            }
        });
        $("#editElement").validate({
            rules:{
                editNombre:{
                    required: true
                },
            }
        });
    });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>