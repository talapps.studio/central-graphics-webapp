<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <h3 class="mt-0">Administraci&oacute;n de Ruta </h3>
                    <div  class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarRuta" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Ruta</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="routesTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">nombre</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Elementos</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar ruta Modal-->
<div class="modal fade" id="newRoute" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Nueva Ruta</h4>
            </div>
            <div class="modal-body">
                <form id="saveRoute" class="form row" role="form">
                    <div class="form-group">
                        <label for="newNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newNombre" id="newNombre">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label">Elementos</label>
                        <div clas="col-sm-12">
                            <select class="form-control clear-element" name="newElemento" id="newElemento" multiple="multiple">
                                <?php foreach ($elementos as $elemento): ?>
                                <option value="<?= $elemento->id_ruta_detalle?>"><?=$elemento->nombre?></option>   
                                <?php endforeach; ?>        
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-element" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar ruta Modal-->
<div class="modal fade" id="editarRoute" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Actualizar Ruta</h4>
            </div>
            <div class="modal-body">
                <form id="editRoute" class="form row" role="form">
                    <input type="hidden" id="idRoutePred">
                    <div class="form-group">
                        <label for="editNombre" class="col-sm-12 control-label">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editNombre" id="editNombre">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-12 control-label clear-element">Elementos</label>
                        <div clas="col-sm-12">
                             <select class="col-sm-12 form-control js-example-basic-single ruta-select-list-edit" name="editElement" id="editElement" multiple="multiple">
                             </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-route" class="btn btn-outline-success">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
  
    //$('.js-example-basic-single').select2();
    
    var dataTable;
    (function($){
        dataTable = $('#routesTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "processing": true,
            "ajax":{
                "url":"/defaultRoute/getAllDefaultRoute",
                "type":"GET",
            },
            "columns":[
                {"data":"nombre"},
                {"data": "elementos", render:  function ( data , row ) {
                   var output='';
                    $.each(data, function(index,item) {
                    output+='<span class="badge badge-primary">'+data[index].nombre+'</span>';
                    });
                    return output;
                 }
                },
                {"data":'activo', render: function(data, type, row){
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                    if(data == '1'){
                        buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                    }else if(data == '0'){
                        buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                    }
                    return buttons;    
                    }
                }
            ],
        }); 
    })(jQuery); 
    $('#routesTable tbody').on('click','.btn-edit', function(){
        let route = '/defaultRoute/getAllDefaultRoute/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(route + row['id_ruta_pred']).done(function(data){
            $("#editNombre").val(row.nombre).closest('div').addClass('is-filled');
            $("#idRoutePred").val(row.id_ruta_pred).closest('div').addClass('is-filled');
            let elementosRuta = [];
            elementosRuta = row['elementos'];
            var selectRef = $(".ruta-select-list-edit");
            var url = "/defaultRoute/defaulRouteListDetail";
            var result = getRequest(url);

            result.done(function (data) {
                var optionsEdit = "";
                   $.each(data.elementos, function (key, item) {
                    if(elementosRuta.filter(rd=>rd.id_ruta_detalle===item.id_ruta_detalle)[0] !=undefined){
                        optionsEdit += "<option  value='" + item.id_ruta_detalle + "' selected>" + item.nombre + "</option>";
                    }else{
                        optionsEdit += "<option  value='" + item.id_ruta_detalle + "'>" + item.nombre + "</option>";
                    }
                   
                    });
                    selectRef.html(optionsEdit);
                    $("#editarRoute").modal("show");
            });
        });
    });
</script>
<script>
    //boton para mostrar modal agregar Ruta
    $(document).on('click','#agregarRuta', function(){
        $("#newRoute").modal("show");
    });
   
    //funcion para resetear campos del modal
    $('#newRoute').on('hide.bs.modal', function (){
       clearColorValidation('#saveRoute');
    });
    $('#editarRoute').on('hide.bs.modal', function (){
        clearColorValidation('#editRoute');
    });
    //boton para guardar informacion 
    $(document).on('click','#btn-save-element', function(){
        let newRoute = {};
        const selectedElement=[];
        $("#newElemento :selected").map(function(i, el) {
            selectedElement.push({id:$(el).val()}); 
        });
       
        newRoute.nombre = $('#newNombre').val();
        newRoute.elementos = selectedElement;
        if($("#saveRoute").valid()){
            ajaxRequest("/defaultRoute/addNewDefaultRoute","POST", newRoute)
            .done(function(response){
                if(response.status === 200){
                    $('#routesTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newRoute").modal("hide");
                }else {
                    Swal.fire({
                        icon: 'error',
                        message:response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newRoute").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //boton para actualizar infomacion de rutas
    $(document).on('click','#btn-edit-route', function(){
        let editRoute = {};
        var id=0;
        const selectedElement=[];
        $("#editElement :selected").map(function(i, el) {
            selectedElement.push({id:$(el).val()}); 
            id= $(el).val();
        });
       
        editRoute.nombre = $('#editNombre').val();
        editRoute.elementos = selectedElement;
        editRoute.idRutaPredeterminada = $("#idRoutePred").val();
        if($("#editRoute").valid()){

            ajaxReqMessageDefault("/defaultRoute/editDefaultRoute/"+id,"POST", editRoute)
            .done(function(response){
                if(response.status === 200){
                    $('#routesTable').DataTable().ajax.reload();
                    $("#editarRoute").modal("hide");
                }else {
                    Swal.fire({
                        icon: 'error',
                        text:response.error,
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editarRoute").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});
        
        }
    });
    //boton para desactivar ruta
    $('#routesTable tbody').on('click','.btn-del',function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var delDefaultRoute = {}
        delDefaultRoute.idDefaultRoute = row.id_ruta_pred;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            //title: response.mensaje,
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
            if(result.isConfirmed){
      ajaxRequest("/defaultRoute/inactiveDefaultRoute","POST",delDefaultRoute)
      .done(function(response){
     
        //$("#routesTable tr:tg(0)").remove();
            if(response.status === 200){
                $('#routesTable').DataTable().ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                }); 
            }else{
                Swal.fire({
                    icon: 'error',
                    text:response.error,
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                });
            }
        
            }).fail(function (xhr, status, error) {}).always(function () {});
            
            }
        })
    });
    //boton para activar ruta
    $('#routesTable tbody').on('click','.btn-act',function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var actDefaultRoute = {}
        actDefaultRoute.idDefaultRoute = row.id_ruta_pred;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí, activar'
        }).then((result)=>{
            if(result.isConfirmed){
      ajaxRequest("/defaultRoute/activeDefaultRoute","POST",actDefaultRoute)
      .done(function(response){
     
        //$("#routesTable tr:tg(0)").remove();
            if(response.status === 200){
                $('#routesTable').DataTable().ajax.reload();
                Swal.fire({
                    icon: 'success',
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                }); 
            }else{
                Swal.fire({
                    icon: 'error',
                    text:response.error,
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                });
            }
        
            }).fail(function (xhr, status, error) {}).always(function () {});
            
            }
        })
    });
    //validacion de formularios
    $(document).ready(function(){
        $("#saveRoute").validate({
            rules:{
                newNombre:{
                    required: true,
                },
                newElemento:{
                    required: true,
                }
            }
            
        });
        $("#editRoute").validate({
            rules:{
                editNombre:{
                    required: true,
                },
                editElement:{
                    required: true,
                }
            }
            
        });
    });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>