<?= $this->extend('Layout/base') ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <div class="d-lg-flex">
                        <div class="col-10">
                            <h3 class="mt-0">Administraci&oacute;n de departamentos</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="ms-auto my-auto mt-lg-0 mt-2">
                            <div class="ms-auto my-auto">
                                <button type="button" class="btn btn-outline-info pull-right" id="agregarDpto"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Departamento</button>
                            </div>
                        </div>
                    </div> 
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="table-department" class="table align-items-center table-responsive" style="width:100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Codigo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Departamento</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Tipo de inventario</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Catidad Mensual</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Iniciales</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar departamentos Modal-->
<div class="modal fade" id="new-dptoModal" tabindex="-1" role="dialog" aria-labelledby="departmentModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Nuevo Departamento</h4>
            </div>
            <div class="modal-body">
                <form id="saveDpto" class="form row" role="form">
                    <div class="form-group">
                        <label for="newCodigo" class="col-sm-12 control-label">Codigo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newCodigo" id="newCodigo">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newDpto"class="col-sm-12 control-label">Departamento</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="newDpto" name="newDpto">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newTipoInv" class="col-sm-12 control-label">Tipo de inventario</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="newTipoInv" name="newTipoInv">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newCantMensual" class="col-sm-12 control-label">Cantidad mensual</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="newCantMensual" name="newCantMensual">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newIniciales" class="col-sm-12 control-label">Iniciales</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="newIniciales" name="newIniciales">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-department" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar departamentos Modal-->
<div class="modal fade" id="edit-dptoModal" tabindex="-1" role="dialog" aria-labelledby="maquinaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Actualizar Departamento</h4>
            </div>
            <div class="modal-body">
                <form id="editDpto" class="form row" role="form">
                    <input type="hidden" id="idDepartment">
                    <div class="form-group">
                        <label for="editCodigo" class="col-sm-12 control-label">Codigo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="editCodigo" name="editCodigo" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editDepartamento" class="col-sm-12 control-label">Departamento</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="editDepartamento" name="editDepartamento">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editTipoInv" class="col-sm-12 control-label">Tipo de Inventario</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="editTipoInv" name="editTipoInv">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editCantMensual" class="col-sm-12 control-label">Cantidad Mensual</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="editCantMensual" name="editCantMensual">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editIniciales" class="col-sm-12 control-label">Iniciales</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" id="editIniciales" name="editIniciales">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button id="btn-edit-department" type="button" class="btn btn-outline-success">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this-> section('scripts')?>
<script>
    var dataTable;
    (function ($){
        "use strict";
        dataTable = $('#table-department').DataTable({
           "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ajax":{
                "url":"/department/getAllDepartments",
                "type":"GET",
            },
            "columns":[
                {"data":"codigo"},
                {"data":"departamento"},
                {"data":"tipo_inv"},
                {"data":"cant_mensual"},
                {"data":"iniciales"},
                {"data": 'activo', render: function (data, type, row) {
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                   if(data == 's'){
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                   }else if(data == 'n'){
                    buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                   }
                    return buttons;
                    }
                }
            ],
        });
    })(jQuery);
    $('#table-department tbody').on('click','.btn-edit',function(){
        let department = '/department/getAllDepartments/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(department + row['id_dpto']).done(function(data){
            $("#editCodigo").val(row.codigo).closest('div').addClass('is-filled');
            $("#editDepartamento").val(row.departamento).closest('div').addClass('is-filled');
            $("#editTipoInv").val(row.tipo_inv).closest('div').addClass('is-filled');
            $("#editCantMensual").val(row.cant_mensual).closest('div').addClass('is-filled');
            $("#editIniciales").val(row.iniciales).closest('div').addClass('is-filled');
            $("#edit-dptoModal").modal("show");
            $("#idDepartment").val(row.id_dpto);
        }); 
    });
</script>
<script>
    //boton modal agregar departamentos
    $(document).on('click','#agregarDpto',function(){
        $("#new-dptoModal").modal("show");
    });
    //funcion para resetear campos del modal
    $('#new-dptoModal').on('hide.bs.modal', function (){
       clearColorValidation('#saveDpto');
    });
    $('#edit-dptoModal').on('hide.bs.modal', function (){
       clearColorValidation('#editDpto');
    });
    //boton para guardar informacion de departamentos
    $(document).on('click','#btn-save-department',function(){
        let Departamento = {};
        Departamento.codigo = $('#newCodigo').val();
        Departamento.nomDpto = $('#newDpto').val();
        Departamento.tipoInv = $('#newTipoInv').val();
        Departamento.cantMensual = $('#newCantMensual').val();
        Departamento.iniciales = $('#newIniciales').val();

        if($("#saveDpto").valid()){
            ajaxRequest("/department/addNewDepartment","POST", Departamento) //{errorFunc:errorFunc} para definir una funcion especial en caso de error
            .done(function(response){
                if(response.status === 200){
                    $('#table-department').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#new-dptoModal").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});
        }
        
    });
    //boton para actualizar departamento
    $(document).on('click','#btn-edit-department',function(){
        let editDpto = {};
        editDpto.codigo = $("#editCodigo").val();
        editDpto.nomDpto = $("#editDepartamento").val();
        editDpto.tipoInv = $("#editTipoInv").val();
        editDpto.cantMensual = $("#editCantMensual").val();
        editDpto.iniciales = $("#editIniciales").val();
        editDpto.idDepartment = $("#idDepartment").val();
        if($("#editDpto").valid()){
            ajaxRequest("/department/editDepartment","POST", editDpto, function(data){
                Swal.fire({
                    icon: 'error',
                    text: data.mensaje,
                    title: 'Error',
                    confirmButtonText: "Aceptar",
                });
            })
            .done(function(response){
                if(response.status === 200){
                    $('#table-department').DataTable().ajax.reload();
                    console.log(response.mensaje);
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });  
                    $("#edit-dptoModal").modal("hide");
                }
                
            }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //boton para desactivar un departamento
    $('#table-department tbody').on('click','.btn-del', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var inactivarDpto = {}
        inactivarDpto.idDepartment = row.id_dpto;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            //title: response.mensaje,
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
            if(result.isConfirmed){
                ajaxRequest("/department/inactiveDepto","POST",inactivarDpto)
                .done(function(response){
                    if(response.status === 200){
                        $('#table-department').DataTable().ajax.reload();
                        Swal.fire({
                        icon: 'success',
                       title: response.mensaje,
                        confirmButtonText: "Aceptar",
                     });
                    }
                }).fail(function(xhr, status, error) {}).always(function() {});
            }
        })
    });
//boton para activar un departamento
    $('#table-department tbody').on('click','.btn-act', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var activarDpto = {}
        activarDpto.idDepartment = row.id_dpto;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            //title: response.mensaje,
            confirmButtonText: 'Sí,activar'
        }).then((result)=>{
            if(result.isConfirmed){
                ajaxRequest("/department/activeDepto","POST",activarDpto)
                .done(function(response){
                    if(response.status === 200){
                        $('#table-department').DataTable().ajax.reload();
                        Swal.fire({
                        icon: 'success',
                       title: response.mensaje,
                        confirmButtonText: "Aceptar",
                     });
                    }
                }).fail(function(xhr, status, error) {}).always(function() {});
            }
        })
    });
    //validacion de formulario
    $(document).ready(function(){
        $("#saveDpto").validate({
            rules:{
                newCodigo: {
                    required: true,
                    minlength: 4,
                },
                newDpto: {
                    required: true,
                    minlength: 5,
                },
                newTipoInv: {
                    required: true,
                },
                newCantMensual: {
                    required: true,
                },
                newIniciales: {
                    required: true,
                }
            }
        });
        $("#editDpto").validate({
            rules: {
                editCodigo: {
                    required: true,
                },
                editDepartamento: {
                    required: true,
                },
                editTipoInv: {
                    required: true,
                },
                editCantMensual: {
                    required: true,
                },
                editIniciales: {
                    required: true,
                }
            }
        });
    });
</script>
<?= $this->include('Scripts/AjaxRequest') ?>

<?= $this->endSection() ?>
