<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>
<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<link href="../bootstrap-template/assets/css/bd-wizard.css" rel="stylesheet" />
<link href="/material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<?= $this->endSection() ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0">Administración de clientes</h3>
                    <div class="container">

                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-10 col-xs-12">
                            <select id="status_id" class="form-select form-select-lg mb-4" aria-label="Usuario filtro">
                                <option selected disabled>Seleccione una opción</option>
                                <option value="s">Activos</option>
                                <option value="n">Inactivos</option>
                            </select>
                        </div>
                        <div class="col-sm-12 col-md-2 ">
                            <button type="button" class="btn btn-block btn btn-outline-info" id="idCrearCliente">
                                <span class="material-icons">add_circle_outline</span>&nbsp;Agregar Cliente
                            </button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-responsive" id="table-customers">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Código</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Días Crédito</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">País</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>
<!-- Modal -->

<div class="modal fade" id="clienteModal" typeAction="0" tabindex="-1" role="dialog" aria-labelledby="clienteModalLabel" aria-hidden="true">
    <div class="modal-dialog" style="width: 90%;height: 90%">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="clienteModalLabel"><span id="tituloModal">Crear Cliente</span></h4>
            </div>
            <div class="modal-body ">
                <!-- Wizard New -->
                <div class="container ">
                    <form id="asistente_cliente" enctype="multipart/form-data">
                        <div id="wizard">
                            <h3>Informaci&oacute;n Cliente</h3>
                            <section>
                                <div class="card" id="step-one">
                                    <div class="card-header">
                                        <h5 class="bd-wizard-step-title">Informaci&oacute;n Cliente</h5>
                                    </div>
                                    <div class="card-body">
                                        <div id="content-cliente" class="multisteps-form__content">
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12" style="margin-bottom: 1rem !important;">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label for="pais" class="col-form-label">Oficina:</label>
                                                        <select class="form-select form-select-lg mb-3" id="pais" style="margin-left:8px; border:solid 1px #ececec; width: 95%; height:40px;">
                                                            <?php foreach ($paises as $pais) : ?>
                                                                <option value="<?= $pais->abreviatura ?>"><?= $pais->nombre_pais ?></option>
                                                            <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8 col-sm-12" style="margin-bottom: 1rem !important;">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label for="file_logo" class="col-form-label">Logo:</label>
                                                        <input type="file" id="file_logo" name="file_logo" class="dropify" data-height="70" accept="image/*">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12" style="margin-bottom: 1rem !important;">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="codigo">C&oacute;digo Cliente:</label>
                                                        <div class="col-12">
                                                            <input id="codigo" toSend="true" name="codigo" type="text" class="form-control" class="required" required />
                                                            <input id="idCliente" name="idCliente" type="hidden" disabled />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="nit">NIT:</label>
                                                        <div class="col-12">
                                                            <input id="nit" toSend="true" name="nit" type="text" class="form-control" require>
                                                            <input type="hidden" id="hdd-nit" disabled />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="empresa">Empresa:</label>
                                                        <div class="col-12">
                                                            <input id="empresa" toSend="true" name="empresa" type="text" class="form-control" require>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-4 col-sm-12" style="margin-bottom: 1rem !important;">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="credito">D&iacute;as Cr&eacute;dito:</label>
                                                        <div class="col-12">
                                                            <input id="credito" toSend="true" name="credito" type="text" class="form-control" require>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="direccion">Dirección:</label>
                                                        <div class="col-12">
                                                            <input id="direccion" toSend="true" name="direccion" type="text" class="form-control" require>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="web">Web:</label>
                                                        <div class="col-12">
                                                            <input id="web" toSend="true" name="web" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h3>Informaci&oacute;n Contacto</h3>
                            <section class="repeater">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 class="bd-wizard-step-title">Informaci&oacute;n Contacto
                                            <button data-repeater-create type="button" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                                        </h5>
                                    </div>
                                    <div class="card-body">
                                        <div data-repeater-list="contacts">
                                            <div class="row" data-repeater-item>
                                                <div class="col-md-10 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="contacto-0">Contacto:</label>
                                                                <div class="col-12">
                                                                    <input name="nombre-contacto" type="text" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="cargo-0">Cargo:</label>
                                                                <div class="col-12">
                                                                    <input name="cargo-contacto"  type="text" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="email-0">E-mail:</label>
                                                                <div class="col-12">
                                                                    <input name="email-contacto"  type="email" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="telOficina-0">Tel-Oficina:</label>
                                                                <div class="col-12">
                                                                    <input name="telOficina-contacto"  type="text" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="telDirecto-0">Tel-Directo:</label>
                                                                <div class="col-12">
                                                                    <input name="telDirecto-contacto"  type="text" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-2">
                                                            <div class="input-group input-group-outline my-3 row">
                                                                <label class="col-form-label" for="telCel-0">Tel-Celular:</label>
                                                                <div class="col-12">
                                                                    <input name="telCelular-contacto"  type="text" class="form-control" required />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <hr>
                                                    <button data-repeater-delete type="button" class="btn btn-outline-danger btn-lg"><i class="fa fa-trash"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </section>
                            <!-- <h3>Maquinas Impresoras</h3>
                            <section class="repeater">
                                <h5 class="bd-wizard-step-title">Maquinas Impresoras
                                    <button data-repeater-create type="button" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                                </h5>
                                <div id="maquinas-content" data-repeater-list="printers">
                                    <div class="row" data-repeater-item>

                                        <div class="col-md-10 col-sm-12">
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="maquina-0">Maquina:</label>
                                                        <div class="col-12">
                                                            <input name="nombre-maquina[]" id="maquina-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="colores-0">Colores:</label>
                                                        <div class="col-12">
                                                            <input name="colores-maquina[]" id="colores-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <hr>
                                            <button data-repeater-delete type="button" class="btn btn-outline-danger btn-lg"><i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section> -->
                            
                            <!-- <h3>Configuraci&oacute;n de Planchas</h3>
                            <section class="repeater">
                                <h5 class="bd-wizard-step-title">Configuraci&oacute;n de Planchas
                                    <button data-repeater-create type="button" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                                </h5>
                                <div id="planchas-content" data-repeater-list="plates">
                                    <div class="row" data-repeater-item>
                                        <div class="col-md-10 col-sm-12">
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="altura-0">Altura:</label>
                                                        <div class="col-12">
                                                            <input name="altura-plancha[]" id="altura-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="lineaje-0">Lineaje:</label>
                                                        <div class="col-12">
                                                            <input name="lineaje-plancha[]" id="lineaje-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="marca-0">Marca:</label>
                                                        <div class="col-12">
                                                            <input name="marca-plancha[]" id="marca-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <hr>
                                            <button data-repeater-delete type="button" class="btn btn-outline-danger btn-lg"><i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                            <h3>Listado de Anilox</h3>
                            <section class="repeater">
                                <h5 class="bd-wizard-step-title">Listado de Anilox
                                    <button data-repeater-create type="button" class="btn btn-outline-success"><i class="fa fa-plus"></i></button>
                                </h5>
                                <div id="anilox-content" data-repeater-list="aniloxes">
                                    <div class="row" data-repeater-item>
                                        <div class="col-md-10 col-sm-12">
                                            <div class="row">

                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="liAnilox-0">Lineaje:</label>
                                                        <div class="col-12">
                                                            <input name="lineaje-anilox[]" id="liAnilox-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="bcmAnilox-0">BCM:</label>
                                                        <div class="col-12">
                                                            <input name="bcm-anilox[]" id="bcmAnilox-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label" for="cantidadAnilox-0">Cantidad:</label>
                                                        <div class="col-12">
                                                            <input name="cantidad-anilox[]" id="cantidadAnilox-0" type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 col-12">
                                            <hr>
                                            <button data-repeater-delete type="button" class="btn btn-outline-danger btn-lg"><i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </div>


                                </div>
                            </section> -->
                            <h3>Listado de Productos</h3>
                            <section>
                                <h5 class="bd-wizard-step-title">Listado de Productos</h5>
                                <div id="producto-content" class="multisteps-form__content">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <table class="table table-condensed table-borderless" id="productos_cliente">
                                                <tr>
                                                    <th class="col-md-2">Descripci&oacute;n</th>
                                                    <th class="col-md-2">Asignar</th>
                                                    <th class="col-md-2">Precio</th>
                                                    <th class="col-md-2">Unidad de medida</th>

                                                </tr>
                                                <?php foreach ($productos as $producto) : ?>
                                                    <tr>
                                                        <td><?= $producto->producto ?></td>
                                                        <td><input name="asignarProducto[]" type="checkbox" id="producto-activo-<?= $producto->id_producto ?>"></td>
                                                        <td><input name="precioProducto[]" type="text" class="col-md-6 form-control"></td>
                                                        <td><input name="unidadProducto[]" type="text" class="col-md-6 form-control"></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>

<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>
<script src="../bootstrap-template/assets/js/jquery.steps.min.js"></script>
<script src="../bootstrap-template/assets/js/jquery-repeater.js"></script>
<script src="/material-template/assets/js/plugins/dropify/js/dropify.min.js"></script>

<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->include('Scripts/Customers/CustomerCreate') ?>
<?= $this->include('Scripts/Customers/CustomerEdit') ?>

<?= $this->endSection() ?>