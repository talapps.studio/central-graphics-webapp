<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="/material-template/assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="/material-template/assets/img/favicon.ico">
  <title>
    <?= isset($titulo) ? $titulo." | " : "" ?>Central Graphic's
  </title>
  <?= $this->include('Layout/styles') ?>
  <?= $this->renderSection('styles') ?>
</head>

  <body class="wrapper">
    <?= $this->include('Layout/loader') ?>
    <?= $this->include('Layout/aside') ?>
    <div class="main-panel" id="main-body">
      <?= $this->include('Layout/navbar') ?>
      <div class="content">
        <div class="container-fluid">
          <?= $this->renderSection('content') ?>
        </div>
      </div>
      <?= $this->include('Layout/footer') ?>
    </div>
    <?= $this->renderSection('modal') ?>
  </body>
  <?= $this->include('Layout/scripts') ?>


</html>