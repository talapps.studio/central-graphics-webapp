<footer class="footer">
    <div class="container-fluid">
        <p class="copyright pull-right" style="letter-spacing: 0.3rem">
            &copy; <script>
                document.write(new Date().getFullYear())
            </script> Hecho con <i class="fa fa-heart"></i> en El Salvador
        </p>
    </div>
</footer>