<script type="text/javascript" src="<?= base_url('/bootstrap-template/assets/js/jquery.3.2.1.min.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('/bootstrap-template/assets/js/bootstrap.min.js')?>" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/core/popper.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/perfect-scrollbar.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/dragula/dragula.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/jquerymask/jquery.mask.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/datatables/datatables.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/smooth-scrollbar.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/choices.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/dropzone.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/quill.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/sweetalert/sweetalert.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/jkanban/jkanban.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/material-dashboard.js?v=3.0.2') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/toastr/toastr.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/jquery-validation/jquery.validate.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/jquery-validation/messages_es.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/plugins/app.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/material-template/assets/js/core/menus.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/bootstrap-template/assets/js/bootstrap-notify.js')?>"></script>
<script type="text/javascript" src="<?= base_url('/bootstrap-template/assets/js/light-bootstrap-dashboard.js?v=1.4.0')?>"></script>
<?= $this->renderSection('scripts') ?>