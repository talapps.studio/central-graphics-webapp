<div
        class="sidebar sidebarbackground"
        id="sidenav-main"
        data-color="black"
        data-image="<?php base_url('/bootstrap-template/assets/img/sidebar-5.jpg') ?>">
    <div class="sidebar-wrapper">
        <div class="logo d-flex">
            <a href="<?= base_url('/')?>" class="simple-text">
                <h5 class="simple-text menu-main-title" style='font-family: "Helvetica Neue", Helvetica, Arial, sans-serif'>Central Graphic's</h5>
                <i id="current-menu-icon" class=""></i>
            </a>
        </div>
        <ul class="nav" id="submenus-navbar"></ul>
    </div>
</div>
