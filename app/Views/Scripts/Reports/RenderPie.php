<script>
    function renderGraphicPie(elementId,dataValues) {
        var chartDom = document.getElementById(elementId);
        var myChart = echarts.init(chartDom);
        var option;

        option = {
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
                orient: 'vertical',
                left: 'left'
            },
            toolbox: {
                show: true,
                orient: 'vertical',
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    dataView: {
                        readOnly: false
                    },
                    magicType: {
                        type: ['line', 'bar']
                    },
                    saveAsImage: {}
                }
            },
            series: [{
                name: 'Trabajos desarrollados',
                type: 'pie',
                radius: '50%',
                data: dataValues,
                emphasis: {
                    itemStyle: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                }
            }]
        };

        option && myChart.setOption(option);

    }
</script>