<!-- Script que inicializa el plugin de datarange picker -->
<script type="text/javascript">
    $(function() {

        $('#customer-list').on('change',function(){
            var dateRange = $("#reportrange").text().split('-');
            var start = moment(dateRange[0].trim(), ['DD/MM/YYYY','DD/MM/YYYY']);
            var end = moment(dateRange[1].trim(), ['DD/MM/YYYY','DD/MM/YYYY']);;
            makeReport(start, end)
        });


        var start = moment().startOf('month');
        var end = moment().endOf('month');

        function makeReport(start, end) {
            $('#reportrange span').html(start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
            var Params = {};
            Params.dateRange = $("#reportrange").text().trim();
            Params.customerId = $("#customer-list option:selected").val();
            RenderGraphics(Params);
        }


        $('#reportrange').daterangepicker({
            "locale": {
                "format": "MM/DD/YYYY",
                "separator": " - ",
                "applyLabel": "Aplicar",
                "cancelLabel": "Cancelar",
                "fromLabel": "Desde",
                "toLabel": "Hasta",
                "customRangeLabel": "Personalizado",
                "weekLabel": "W",
                "daysOfWeek": [
                    "D",
                    "L",
                    "M",
                    "MX",
                    "J",
                    "V",
                    "S"
                ],
                "monthNames": [
                    "Enero",
                    "Febrero",
                    "Marzo",
                    "Abril",
                    "Mayo",
                    "Junio",
                    "Julio",
                    "Agosto",
                    "Septiembre",
                    "Octubre",
                    "Noviembre",
                    "Diciembre"
                ],
                "PrimerDia": 1
            },
            startDate: start,
            endDate: end,
            changeYear: true,
            yearRange: "1930:2010",
            ranges: {
                'Hoy': [moment(), moment()],
                'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Ultimos 7 Dias': [moment().subtract(6, 'days'), moment()],
                'Ultimos 30 Dias': [moment().subtract(29, 'days'), moment()],
                'Este Mes': [moment().startOf('month'), moment().endOf('month')],
                'Ultimo Mes': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, makeReport);
        makeReport(start, end);
    });
</script>