<script>
    function renderGraphicDoughnut(elementId, dataValues) {
        var chartDom = document.getElementById(elementId);
        var myChart = echarts.init(chartDom);
        var option;

        option = {
            tooltip: {
                trigger: 'item'
            },
            legend: {
                top: '0%',
                left: '0%',
                orient: 'vertical'
            },
            series: [{
                name: 'Access From',
                type: 'pie',
                radius: ['50%', '80%'],
                center: ['65%', '50%'],
                avoidLabelOverlap: false,
                label: {
                    show: false,
                    position: 'center'
                },
                emphasis: {
                    label: {
                        show: true,
                        fontSize: '30',
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: false
                },
                data: dataValues
            }]
        };

        option && myChart.setOption(option);
    }
</script>
