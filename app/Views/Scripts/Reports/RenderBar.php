<script>
    function renderGraphicBar(elementId, dataNames, dataValues) {
        var chartDom = document.getElementById(elementId);
        var myChart = echarts.init(chartDom);
        var option;

        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                },
            },
            toolbox: {
                show: true,
                orient: 'vertical',
                feature: {
                    dataZoom: {
                        yAxisIndex: 'none'
                    },
                    dataView: {
                        readOnly: false
                    },
                    magicType: {
                        type: ['line', 'bar']
                    },
                    saveAsImage: {}
                }
            },
            xAxis: {
                type: 'value'

            },
            yAxis: {
                type: 'category',
                data: dataNames
            },
            series: [{
                data: dataValues,
                type: 'bar',
                label: {
                    show: true,
                    position: 'right'
                },
            }]
        };

        option && myChart.setOption(option);

    }
</script>