<script>
    //variables globales
    let form = $("#asistente_cliente");
    var dataTable;

    //Carga de plugins
    $(document).ready(function() {
        initWizard();
        initRepeater();
        initDropify();
        initCustomers("s");
    });

    function initWizard() {
        form.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
        });
        form.children("div").steps({
            headerTag: "h3",
            bodyTag: "section",
            transitionEffect: "none",
            stepsOrientation: "vertical",
            titleTemplate: '<span class="number">#index#</span>',
            enableAllSteps: "true",
            labels: {
                finish: 'Guardar',
                previous: 'Anterior',
                next: 'Siguiente'
            },
            onFinished: function(event, currentIndex) {
                let data = $('.repeater').repeaterVal();
                let inputs = getInputs("step-one");
                let Cliente = {};

                //Primer Paso
                Cliente.file = $("#file_logo")[0].files[0]; //imagen
                Cliente.pais = $("#pais").val();
                for (let i = 0; i < inputs.length; i++) { //Campos de formulario
                    var input = inputs[i];
                    if (input.getAttribute('toSend') == "true") {
                        var nameInput = input.getAttribute('id');
                        var valueInput = input.value;
                        Cliente[nameInput] = valueInput;
                    }
                }

                //Segundo Paso
                Cliente.contactos = data.contacts;
                //Tercer Paso
                Cliente.maquinas = data.printers;
                //Cuarto Paso
                Cliente.planchas = data.plates;
                //quinto paso
                Cliente.aniloxes = data.aniloxes;
                //paso seis
                var Productos = [];
                var codigoProductos = document.getElementsByName('idProducto[]');
                var precioProductos = document.getElementsByName('precioProducto[]');
                var unidadProductos = document.getElementsByName('unidadProducto[]');


                for (let index = 0; index < codigoProductos.length; index++) {
                    var ClienteProducto = {};
                    var a = codigoProductos[index];
                    var b = precioProductos[index];
                    var c = unidadProductos[index];
                    var e = document.getElementById("producto-activo-" + a.value).checked ? "s" : "n";
                    ClienteProducto.idClienteProducto = a.id.split('-')[1];
                    ClienteProducto.CodigoProducto = a.value;
                    ClienteProducto.PrecioProducto = b.value;
                    ClienteProducto.UnidadProducto = c.value;
                    ClienteProducto.AsignarProducto = e;

                    if (ClienteProducto.AsignarProducto === "s") {
                        Productos.push(ClienteProducto);
                    }

                }
                Cliente.productos = Productos;
                Cliente.aniloxes = JSON.stringify(Cliente.aniloxes);
                Cliente.contactos = JSON.stringify(Cliente.contactos);
                Cliente.planchas = JSON.stringify(Cliente.planchas);
                Cliente.maquinas = JSON.stringify(Cliente.maquinas);
                Cliente.productos = JSON.stringify(Cliente.productos);

                ajaxRequest('/customer/addNewCustomer', 'POST', Cliente)
                    .done(function(response) {
                        $('#table-customers').DataTable().ajax.reload();
                        $("#clienteModal").modal("hide");
                        console.log(response);
                        if (response.toString().includes("error")) {
                            Swal.fire({
                                icon: 'warning',
                                title: "Cliente agregado, pero archivo de logo no se guardó",
                                confirmButtonText: "Aceptar",
                            });
                        } else {
                            Swal.fire({
                                icon: 'success',
                                title: "Cliente agregado exitosamente",
                                confirmButtonText: "Aceptar",
                            });
                        }
                    }).fail(function() {
                        Swal.fire({
                            icon: 'error',
                            title: "Ocurrió un error al guardar, contacte a soporte técnico.",
                            confirmButtonText: "Aceptar",
                        });
                        console.error(response.responseJSON);
                    }).always(function() {
                        
                    });

            },
            onStepChanging: function(e, currentIndex, newIndex) {
                form.validate().settings.ignore = ":disabled,:hidden";
                return form.valid();
            },
            onFinishing: function(event, currentIndex) {
                form.validate().settings.ignore = ":disabled,:disabled";
                if (!form.valid()) {
                    Swal.fire({
                        icon: 'error',
                        title: "Hay elementos en este formulario que requieren su atencion",
                        confirmButtonText: "Aceptar",
                    });
                }
                return form.valid();
            },

        });
    }

    function initRepeater() {
        repeater = $('.repeater').repeater({
            initEmpty: false,
            show: function() {
                $(this).show();
            },
            hide: function(deleteElement) {
                Swal.fire({
                    title: '¿Seguro de que quiere eliminar este elemento?',
                    showCancelButton: true,
                    confirmButtonText: `Eliminar`,
                    cancelButtonText: `Cancelar`,
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).slideUp(deleteElement);
                    }
                });
            },
            isFirstItemUndeletable: true
        });
    }

    function initDropify() {
        $('.dropify').dropify({
            messages: {
                'default': 'Arrastre un archivo o haga click',
                'replace': 'Arrastre y suelte o haga click para sustituir',
                'remove': 'Quitar',
                'error': 'Ocurrió un error.'
            }
        });
    }

    function getInputs(target) {
        let step = document.getElementById(target);
        let inputs = step.getElementsByTagName("input");
        return inputs
    }

    function initCustomers(customerStatus) {
        dataTable = $('#table-customers').DataTable({
            "ordering": true,
            "order": [
                [1, 'asc']
            ],
            "bAutoWidth": false,
            "language": {
                "url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"
            },
            "bDestroy": true,
            "ajax": {
                "url": "/customer/getAllCustomers?activeCustomer=" + customerStatus,
                "type": "GET",
            },
            "columnDefs": [{
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "data": null,
                "width": "10%",
                "className": "text-center",
            }],
            "columns": [{
                    "data": "codigo_cliente"
                },
                {
                    "data": "nombre"
                },
                {
                    "data": "credito"
                },
                {
                    "data": "pais"
                },
                {
                    "data": '',
                    render: function(data, type, row) {
                        var buttons = '<div class="btn-group table-options">';
                        buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                        buttons += '<a href="/printer/printerCustomer/'+row.id_cliente+'" class="move-on-hover text-info btn-print" data-toggle="tooltip" data-placemnet="top" title="Impresoras"><i class="fa fa-lg fa-print"></i></a>';
                        if (row.activo == 's') {
                            buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                        } 
                        if (row.activo  == 'n') {
                            buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                        }
                        return buttons;
                    }
                }
            ],
        });
    }

    $(document).on('change', '#status_id', function() {
        var estado = $(this).val();
        initCustomers(estado);
    });

    $(document).on('click', "#idCrearCliente", function() {
        $("#clienteModal").modal("show");
    });
</script>