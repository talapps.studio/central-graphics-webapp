<script>
    $(document).on('click', '.btn-edit', function() {
        
        let row = dataTable.row($(this).parents('tr')).data();
        let Cliente = {};
        Cliente.id_cliente= row.id_cliente;
        ajaxRequest('/customer/getCustomerData', 'POST', Cliente).done(function(data) {
           //Asignacion de datos del cliente
           $("#codigo").val(data.Cliente.General.codigo_cliente);
           $("#nit").val(data.Cliente.General.nit);
           $("#empresa").val(data.Cliente.General.nombre);
           $("#credito").val(data.Cliente.General.credito);
           $("#direccion").val(data.Cliente.General.direccion);
           $("#web").val(data.Cliente.General.web);

           //Asignacion de contactos
           Contacts = [];
           data.Cliente.Contactos.map((element,index)=>{
                Contacto = {
                    "nombre-contacto": element.nombre,
                    "cargo-contacto": element.cargo,
                    "email-contacto": element.email,
                    "telOficina-contacto": element.tel_oficina,
                    "telDirecto-contacto": element.tel_directo,
                    "telCelular-contacto": element.tel_celular
                }
                Contacts.push(Contacto);
           });
           repeater.setList(Contacts);

           //Mostramos el modal
           $("#clienteModal").modal("show");
        });
    });

   
</script>