<script>    
    function ajaxRequest(url, type, data) {
            return $.ajax({
                url: url,
                type: type,
                dataType:"json",
                data: data,
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded"
                },
            });
    }
    function ajaxRequestOrder(url, type, data) {
        return $.ajax({
            url: url,
            type: type,
            processData: false,
            contentType: false,
            data: data
        });
    }

    function ajaxReqMessageDefault(url, type, data, 
        errorFunc={
            errorFunc:function(data, status, error){
                Swal.fire({
                    icon: 'error',
                    text: error,
                    title: 'ERROR ',
                    confirmButtonText: "Aceptar",
                });
                console.log('Error por defecto')
            }
        },
        successFunc={
            successFunc:function(response){
                if(response.status === 200){
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                }
            }
        }
    ){
        return $.ajax({
            url: url,
            type: type,
            dataType:"json",
            data: data,
            headers: {
                "Content-Type": "application/x-www-form-urlencoded"
            },
        }).done(function(response){
            successFunc.successFunc(response);
        })
        .fail(
            function (data, status, error) {
                errorFunc.errorFunc(data, status, error)
            }
        );
    }
</script>
