<?= $this->extend('Layout/base') ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card my-4">
                <div class="card-header pb-0">
                    <div class="row">
                    </div>
                </div>
                <div class="card-body pt-2">
                    <div class="row">
                        <div class="col-md-10 mx-auto">
                            <div class="accordion" id="accordionRental">
                                <div class="accordion-item mb-3">
                                    <h5 class="accordion-header" id="headingOne">
                                        <button class="accordion-button border-bottom font-weight-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                            Seccion 1
                                            <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                            <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        </button>
                                    </h5>
                                    <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionRental">
                                        <div class="accordion-body text-sm opacity-8">
                                            We’re not always in the position that we want to be at. We’re constantly growing. We’re constantly making mistakes. We’re constantly trying to express ourselves and actualize our dreams. If you have the opportunity to play this game
                                            of life you need to appreciate every moment. A lot of people don’t appreciate the moment until it’s passed.
                                        </div>
                                    </div>
                                </div>
                                <div class="accordion-item mb-3">
                                    <h5 class="accordion-header" id="headingTwo">
                                        <button class="accordion-button border-bottom font-weight-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Seccion 2
                                            <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                            <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        </button>
                                    </h5>
                                    <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionRental">
                                        <div class="row mt-3">
                                            <div class="col-6">
                                                <div class="input-group input-group-static">
                                                    <label>Comienzo</label>
                                                    <input class="form-control form-control-sm datetimepicker" type="text" data-input>
                                                </div>
                                                <div class="input-group input-group-static">
                                                    <label>Finalizacion</label>
                                                    <input class="form-control form-control-sm datetimepicker" type="text" data-input>
                                                </div>
                                                <div class="input-group input-group-static">
                                                    <label>Pais</label>
                                                    <select class="form-control form-control-sm" name="" id="">
                                                        <option value="">Opcion 1</option>
                                                        <option value="">Opcion 2</option>
                                                        <option value="">Opcion 3</option>
                                                        <option value="">Opcion 4</option>
                                                    </select>
                                                </div>
                                                <div class="input-group input-group-static">
                                                    <label>Area</label>
                                                    <select class="form-control form-control-sm" name="" id="">
                                                        <option value="">Opcion 1</option>
                                                        <option value="">Opcion 2</option>
                                                        <option value="">Opcion 3</option>
                                                        <option value="">Opcion 4</option>
                                                    </select>
                                                </div>
                                                <div class="input-group input-group-static">
                                                    <label>Cliente</label>
                                                    <select class="form-control form-control-sm" name="" id="">
                                                        <option value="">Opcion 1</option>
                                                        <option value="">Opcion 2</option>
                                                        <option value="">Opcion 3</option>
                                                        <option value="">Opcion 4</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="trabajo" id="trabajo1" class="pull-right" value="finalizado">
                                                            </span>
                                                            <label for="trabajo1" class="pull-right form-control form-control-sm">Terminados</label>
                                                        </div>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="trabajo" id="trabajo1" class="pull-right" value="finalizado">
                                                            </span>
                                                            <label for="trabajo1" class="pull-right form-control form-control-sm">Inconclusos</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-6">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="trabajo" id="trabajo1" class="pull-right" value="finalizado">
                                                            </span>
                                                            <label for="trabajo1" class="pull-right form-control form-control-sm">Atrasados</label>
                                                        </div>
                                                        <div class="input-group">
                                                            <span class="input-group-addon">
                                                                <input type="radio" name="trabajo" id="trabajo1" class="pull-right" value="finalizado">
                                                            </span>
                                                            <label for="trabajo1" class="pull-right form-control form-control-sm">Reproceso</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="input-group input-group-static">
                                                        <label>Material</label>
                                                        <select class="form-control form-control-sm" name="" id="">
                                                            <option value="">Opcion 1</option>
                                                            <option value="">Opcion 2</option>
                                                            <option value="">Opcion 3</option>
                                                            <option value="">Opcion 4</option>
                                                        </select>
                                                    </div>
                                                </div>



                                            </div>
                                        </div>
                                        <div class="d-flex justify-content-end mt-4">
                                            <button type="button" name="button" class="btn bg-gradient-dark m-0 ms-2">Reporte</button>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div>
<?= $this->endSection() ?>