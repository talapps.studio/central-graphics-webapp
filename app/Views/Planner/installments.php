<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
  <?php foreach ($materiales as $material) : ?>
    <?php foreach ($grupos as $grupo) : ?>
      <?php if ($grupo == $material->id_material_solicitado_grupo) : ?>
        <div class="row">
          <div class="col-12">
            <div class="card my-4">
              <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-gray shadow-gray border-radius-lg pt-4 pb-3">
                  <h6 class="text-white text-capitalize ps-3"><?= $material->material_solicitado ?></h6>
                </div>
              </div>
              <div class="card-body px-0 pb-2">
                <div class="table p-0 table-xs">
                  <table class="table align-items-center mb-0 table-bordered">
                    <thead class="bg-gradient-gray ">
                      <tr>
                        <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Proceso</th>
                        <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Producto</th>
                        <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Entrega</th>
                        <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Ruta</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($trabajos as $trabajo) : ?>
                        <?php if ($trabajo->id_material_solicitado_grupo == $material->id_material_solicitado_grupo) : ?>
                          <tr>
                            <td class="text-uppercase bg-gradient-gray text-white"><?= $trabajo->codigo_cliente ?>-<?= $trabajo->proceso ?></td>
                            <td class="align-middle"><?= $trabajo->nombre ?></td>
                            <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($trabajo->fecha_entrega)) ?></td>
                            <td class="align-middle text-center"></td>
                          </tr>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endif; ?>
    <?php endforeach; ?>
  <?php endforeach; ?>
</div>
<?= $this->endSection() ?>