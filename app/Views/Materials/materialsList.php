<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="d-lg-flex">
                    <div class="col-10">
                         <h3 class="mt-0">Existencia de Materiales</h3>
                    </div>
                    <div class="row">
                        <div class="ms-auto my-auto mt-lg-0 mt-4">
                            <div class="ms-auto my-auto">
                                <button type="button" class="btn btn-outline-info pull-right" id="agregar-material"><span class="material-icons">add_circle_outline</span>&nbsp; Agregar Material</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="">Materiales</label>
                            <select class="form-select form-select-lg filtroMateriales" name="" id="id_codigo" style="width:100%">
                                <option value="todos">C&oacute;digos Comp.</option>
                                <option value="mp">Mat. Prima</option>
                                <option value="mt">Materiales</option>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="">Todos los proveedores</label>
                            <select class="form-select form-select-lg filtroMateriales" name="" id="id_proveedor" style="width:100%">
                                <option value="--">Todos los proveedores</option>
                                <?php foreach ($proveedores as $proveedor): ?>
                                <option value="<?= $proveedor->id_inventario_proveedor?>"><?= $proveedor->proveedor_nombre?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="">Arte</label>
                            <select class="form-select form-select-lg filtroMateriales" name="" id="id_equipo" style="width:100%">
                                <option value="--">Equipo/Area</option>
                                <?php foreach ($areas as $area): ?>
                                <option value="<?= $area->id_inventario_equipo?>"><?= $area->nombre_equipo?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <label for="">existencias</label>
                            <select class="form-select form-select-lg filtroMateriales" name="" id="id_existencias" style="width:100%">
                                <option value="todos">Existencias</option>
                                <option value="con">Con Existencias</option>
                                <option value="sin">Sin Existencias</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="col-lg-12 mt-2">
        <div class="card">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-0">Listado de materiales</h6>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive p-0">
                    <table id="table-materials" class="table align-items-center table-responsive" style="width:100%">
                        <thead>
                            <tr class="table-secondary">
                                <th class="text-uppercase font-weight-bolder opacity-7" >C&oacute;digo</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Material</th>
                                <th class="text-uppercase font-weight-bolder opacity-7" >Pa&iacute;s</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Cantidad</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Total</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">UMB</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Valor</th>
                                <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--agregar material modal -->
<div class="modal fade" id="materialModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="materialModalLable" id="tituloModal">Crear Material</h4>
            </div>
            <div class="modal-body">
                <form id="formMaterial" class="form row" role="form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label  for="materialCodigo" class="col-sm-12 control-label">Codigo*</label>
                                <div class="col-sm-12">
                                    <input type="text" id="materialCodigo" name="materialCodigo" class="form-control clear-element">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  for="" class="col-sm-12  control-label">Cantidad/Unidad*</label>
                                <div class="col-sm-12">
                                    <input type="text" id="materialCanUni" name="materialCanUni" class="form-control clear-element">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12  control-label">Tipo</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="material-tipo">
                                        <option value="">--</option>
                                        <?php foreach ($tipos as $tipo): ?>
                                        <option value="<?= $tipo->nombre_tipo?>"><?= $tipo->nombre_tipo ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12  control-label">Tipo de Material</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="material-tipoMaterial">
                                        <?php foreach ($materiales as $material): ?>
                                        <option value="<?= $material->abreviatura?>"><?=$material->tipo_material_nombre?></option>   
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  for="materialCanUni" class="col-sm-12 control-label">Pulgadas por Placas</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matPulgadasPlacas" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label  for="" class="col-sm-12 control-label">País</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="material-pais">
                                        <?php foreach ($paises as $pais): ?>
                                        <option value="<?= $pais->abreviatura?>"><?=$pais->nombre_pais?></option>
                                        <?php endforeach; ?>
                                    </select>  
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="materialValUni" class="col-sm-12 control-label">Valor/Unidad*</label>
                                <div class="col-sm-12">
                                    <input type="text" id="materialValUni" name="materialValUni" class="form-control clear-element">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="" class="col-sm-12 control-label">Proveedor</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="material-proveedor">
                                        <option value="">Proveedor Principal</option>
                                        <?php foreach ($proveedores as $proveedor): ?>
                                        <option value="<?= $proveedor->id_inventario_proveedor?>"><?= $proveedor->proveedor_nombre?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Area</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="material-area">
                                        <option value="">Seleccionar</option>
                                        <?php foreach ($areas as $area): ?>
                                            <option value="<?= $area->id_inventario_equipo?>"><?= $area->nombre_equipo?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Placas por caja</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matPlacasCaja" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="materialNombre" class="col-sm-12 control-label">Nombre*</label>
                                <div class="col-sm-12">
                                    <input type="text" id="materialNombre" name="materialNombre" class="form-control clear-element">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="materialObservaciones" class="col-sm-12  control-label">Observaciones*</label>
                                <div class="col-sm-12">
                                <input type="text" id="materialObservaciones" name="materialObservaciones" class="form-control clear-element">
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-material" action-type="save" class="btn btn-outline-success">Guardar</button> 
            </div>
        </div>
    </div>
</div>
<!--fin Modal-->

<!--editar Modal-->
<div class="modal fade" id="materialEditModal" tabindex="-1" aria-labelledby="maquinaEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="materialEditModalLable" id="tituloModal">Editar Material</h4>
            </div>
            <div class="modal-body">
                <form id="formEditMaterial" class="form row" role="form">
                    <input type="hidden" id="idMaterial">
                    <input type="hidden" id="idInventarioMatProv">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label  for="matEditCodigo" class="col-sm-12 control-label">Codigo</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matEditCodigo" class="form-control" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  for="matEditCanUni" class="col-sm-12 control-label">Cantidad/Unidad</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matEditCanUni" name="matEditCanUni" class="form-control clear-element" required />
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-12 control-label">Tipo</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="mat-edit-tipo">
                                        <option value="">Tipo</option>
                                        <?php foreach ($tipos as $tipo): ?>
                                        <option value="<?= $tipo->nombre_tipo?>"><?= $tipo->nombre_tipo ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-12 control-label">Tipo de Material</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="mat-edit-tipoMaterial">
                                        <option value="" class="form-label">Tipo Material</option>
                                        <?php foreach ($materiales as $material): ?>
                                        <option value="<?= $material->abreviatura?>"><?=$material->tipo_material_nombre?></option>   
                                        <?php endforeach; ?>
                                    </select> 
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Pulgadas por Placas</label>
                                <div class="col-sm-12">
                                <input type="text" id="mat-edit-pul-placas" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Pais</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="mat-edit-pais">
                                        <?php foreach ($paises as $pais): ?>
                                        <option value="<?= $pais->abreviatura?>"><?=$pais->nombre_pais?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="matEditValUni" class="col-sm-12 control-label">Valor/Unidad</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matEditValUni" name="matEditValUni" class="form-control clear-element">
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-12 control-label">Proveedor</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="mat-edit-proveedor">
                                        <option value="">Proveedor Principal</option>
                                        <?php foreach ($proveedores as $proveedor): ?>
                                        <option value="<?= $proveedor->id_inventario_proveedor?>"><?= $proveedor->proveedor_nombre?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-12 control-label">Área</label>
                                <div class="col-sm-12">
                                    <select class="form-control" id="mate-edit-area">
                                        <option value="">Area</option>
                                        <?php foreach ($areas as $area): ?>
                                            <option value="<?= $area->id_inventario_equipo?>"><?= $area->nombre_equipo?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for=""class="col-sm-12 control-label">Placas por caja</label>
                                <div class="col-sm-12">
                                    <input type="text" id="mat-edit-placas-caja" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label  for="matEditNombre" class="col-sm-12 control-label">Nombre</label>
                                <div class="col-sm-12">
                                    <input type="text" id="matEditNombre" name="matEditNombre"class="form-control clear-element" required/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  for="matEditObservaciones" class="col-sm-12 control-label">Observaciones</label>
                                <div class="col-sm-12">
                                <input type="text" id="matEditObservaciones" name="matEditObservaciones" class="form-control clear-element" required/> 
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-edit-material"  class="btn btn-outline-success">Guardar Cambios</button> 
            </div>
        </div>
    </div>
</div>
<!--fin Modal-->
<!--Modal Agregar Lote-->
<div class="modal fade" id="ejemploModal" tabindex="-1" aria-labelledby="maquinaEditModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="materialEditModalLable" id="tituloModal">Ejemplo</h4>
            </div>
            <div class="modal-body">
               <form id="agregaLote" class="form row" role="form">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label">Pedido</label>
                            <div class="col-sm-12">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="" class="col-sm-12 control-label">Cantidad</label>
                            <div class="col-sm-12">
                            <input type="text" class="form-control">
                            </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-lote"  class="btn btn-outline-success">Guardar Cambios</button> 
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>

<script>
//agregando estilo a los select
 //$('.filtroMateriales').select2();
 
//tabla de listado de materiales
var dataTable;
$(document).ready(function (){
    $(document).on('change','.filtroMateriales',function(){
        dataTable.ajax.reload();

    });

});
fillMaterials();
"use strict";
function fillMaterials(){
    dataTable = $('#table-materials').DataTable({
            "ordering": true,
            "order": [[ 1, 'desc' ]],
            "bAutoWidth": false,
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ajax": {
                 "url": "/material/getAllCodMaterials",
                 "type": "POST",
                 dataSrc:'',
                 data() {
                    return {
                        'codigoMaterial':$('#id_codigo').val(),
                        'proveedor':$('#id_proveedor').val(),
                        'equipo':$('#id_equipo').val(),
                        'existencias':$('#id_existencias').val()
                  };
                }
            },
            "columns": [{ 
                    "data": "codigo_sap", render: function (data, type, row) {
                    var buttons = '';
                     buttons += '<a class="link" onclick="mostrarLote(this)" href="javascript:void(0)" type="button">'+ data +'</a>';
                    
                    return buttons;
                    }
                },
                {
                     "data": "nombre_material" 
                },
                {   
                    "data": "mat_pais"
                },
                {   
                    "data": "cantidad_unidad"
                },
                {   
                    "data": "", render:function( data, type, row, meta ){
                        return Number.parseFloat(row.cantidad_unidad * row.existencias).toFixed(2); ;
                    }

                },
                {   
                    "data": "tipo"
                },
                {   
                    "data": "valor", render:function( data, type, row, meta ){
                        return  '$'+Number.parseFloat(data).toFixed(2);
                    }

                },
                {"data": 'activo', render: function (data, type, row) {
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                    if(data == 1){
                        buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                    }else if(data == 0){
                        buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                    }
                    return buttons;
                    }
                }
               
            ],
            
        });
}
$('#table-materials tbody').on('click','.btn-edit',function(){
            let material = '/material/getAllCodMaterials/';
            let row = dataTable.row($(this).parents('tr')).data();
            getRequest(material + row['id_inventario_material_proveedor']).done(function(data){
                $("#matEditCodigo").val(row.codigo_sap).closest('div').addClass('is-filled');
                $("#matEditCanUni").val(row.cantidad_unidad).closest('div').addClass('is-filled');
                $("#mat-edit-tipo").val(row.tipo).closest('div').addClass('is-filled');
                $("#mat-edit-tipoMaterial").val(row.mp_mt).closest('div').addClass('is-filled');
                $("#mat-edit-pul-placas").val(row.numero_individual).closest('div').addClass('is-filled');
                $("#mat-edit-pais").val(row.mat_pais).closest('div').addClass('is-filled');
                $("#matEditValUni").val(row.valor).closest('div').addClass('is-filled');
                $("#mat-edit-proveedor").val(row.id_inventario_proveedor).closest('div').addClass('is-filled');
                $("#mate-edit-area").val(row.id_inventario_equipo).closest('div').addClass('is-filled');
                $("#mat-edit-placas-caja").val(row.numero_cajas).closest('div').addClass('is-filled');
                $("#matEditNombre").val(row.nombre_material).closest('div').addClass('is-filled');
                $("#matEditObservaciones").val(row.observacion).closest('div').addClass('is-filled');
                $("#materialEditModal").modal("show");
                $("#idMaterial").val(row.id_inventario_material);
                $("#idInventarioMatProv").val(row.id_inventario_material_proveedor);
            });  
        });

</script>
<script>
$(document).on('click','#btn-save-material',function(){
    //var action = $(this).attr("action-type");
    let Material = {};
    Material.codigo = $("#materialCodigo").val();
    Material.cantUni = $("#materialCanUni").val();
    Material.tipo = $("#material-tipo").val();
    Material.materiales = $("#material-tipoMaterial").val();
    Material.numIndi = $("#matPulgadasPlacas").val();
    Material.pais = $("#material-pais").val();
    Material.valorUni = $("#materialValUni").val();
    Material.proveedor = $("#material-proveedor").val();
    Material.area = $("#material-area").val();
    Material.numCajas = $("#matPlacasCaja").val();
    Material.nombre = $("#materialNombre").val();
    Material.observacion = $("#materialObservaciones").val();
    
    if($("#formMaterial").valid()){
        ajaxRequest("/material/addNewMaterial","POST", Material)
        .done(function (response) {
           if(response.status === 200){
                $('#table-materials').DataTable().ajax.reload();
                console.log(response.mensaje);
                Swal.fire({
                    icon: 'success',
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                });
             $("#materialModal").modal("hide");
           }   
        }).fail(function (xhr, status, error) {}).always(function () {});  
    }
});

$(document).on('click','#btn-edit-material',function(){
    //var action = $(this).attr("action-type");
    let editMaterial = {};
    editMaterial.codigo = $("#matEditCodigo").val();
    editMaterial.cantUni = $("#matEditCanUni").val();
    editMaterial.tipo = $("#mat-edit-tipo").val();
    editMaterial.materiales = $("#mat-edit-tipoMaterial").val();
    editMaterial.numIndi = $("#mat-edit-pul-placas").val();
    editMaterial.pais = $("#mat-edit-pais").val();
    editMaterial.valorUni = $("#matEditValUni").val();
    editMaterial.proveedor = $("#mat-edit-proveedor").val();
    editMaterial.area = $("#mate-edit-area").val();
    editMaterial.numCajas = $("#mat-edit-placas-caja").val();
    editMaterial.nombre = $("#matEditNombre").val();
    editMaterial.observacion = $("#matEditObservaciones").val();
    editMaterial.idMaterial = $("#idMaterial").val();
    editMaterial.idInvenarioMatProv = $("#idInventarioMatProv").val();

    if( $("#formEditMaterial").valid()){
        ajaxRequest("/material/editMaterial","POST", editMaterial)
        .done(function (response) {
           if(response.status === 200){
                $('#table-materials').DataTable().ajax.reload();
                console.log(response.mensaje);
                Swal.fire({
                    icon: 'success',
                    title: response.mensaje,
                    confirmButtonText: "Aceptar",
                });
             $("#materialEditModal").modal("hide");
           }   
        }).fail(function (xhr, status, error) {}).always(function () {});  
    }
});
//boton para activar material
$('#table-materials tbody').on('click','.btn-del', function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var inactivarMat = {}
    inactivarMat.idMaterial = row.id_inventario_material;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, desactivar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/material/inactiveMaterial","POST",inactivarMat)
            .done(function(response){
                if(response.status === 200){
                    $('#table-materials').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                }
            }).fail(function(xhr, status, error) {}).always(function() {});
        }
    })
});
//boton para desactivar material
$('#table-materials tbody').on('click','.btn-act', function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var activarMat = {}
    activarMat.idMaterial = row.id_inventario_material;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí,activar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/material/activeMaterial","POST",activarMat)
            .done(function(response){
                if(response.status === 200){
                    $('#table-materials').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                }
            }).fail(function(xhr, status, error) {}).always(function() {});
        }
    })
});
function mostrarLote(data){
    $('#ejemploModal').modal('show')
}
 //funcion para resetear campos del modal
 $('#materialModal').on('hide.bs.modal', function (){
    
    clearColorValidation('#formMaterial');
   

 });
 $('#materialEditModal').on('hide.bs.modal', function (){
    clearColorValidation('#formEditMaterial');
 });
</script>
<script>
//boton crear
 $(document).on('click','#agregar-material',function(){
    $("#materialModal").modal("show");
    
 });
</script>
<script>
  //validar formulario de material
 $(document).ready(function(){
      $("#formMaterial").validate({
         rules:{
            materialCodigo:{
                required: true,
                minlength:3
            },
            materialCanUni:{
                required:true,
                number: true,
                min: 18
            },
            materialValUni:{
                required:true,
                number: true,
                min:18
            },
            matPulgadasPlacas:{
                number:true,
                min : 11
            },
            matPlacasCaja:{
                number:true,
                min:3
            },
            materialNombre:{
                required:true,
                minlength: 5
            },
            materialObservaciones:{
                required:true,
            }
         }
         
      });
      $("#formEditMaterial").validate({
        rules:{
           
            matEditCanUni:{
                required:true,
                number: true,
                min: 18
            },
            matEditValUni:{
                required:true,
                number: true,
                min:18
            },
            matEditNombre:{
                required:true,
                minlength: 5
            },
            matEditObservaciones:{
                required:true,
            }
         }

     });
  });
  
</script>

<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>