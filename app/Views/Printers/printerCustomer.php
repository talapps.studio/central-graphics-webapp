<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>

<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">

<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-lg-flex">
                        <div class="col-10">
                            <h3 class="mb-0">Impresoras del cliente <?= $Cliente->nombre ?></h3>
                            <input type="hidden" id="idCliente" value="<?= $Cliente->id_cliente ?>" />
                        </div>
                        <div class="row">
                            <div class="ms-auto my-auto mt-lg-0 mt-4">
                                <div class="ms-auto my-auto">

                                    <button type="button" class="btn btn-outline-info pull-right" id="agregar-impresora"><span class="material-icons">add_circle_outline</span>&nbsp; Agregar Impresora</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-responsive" id="table-printers" style="width:100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre Impresora</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7"># Colores</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Colores Precargados</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>
<!--Agrepar impresora Modal -->
<div class="modal fade" id="ImpresoraModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title font-weight-normal" id="ImpresoraModalLable">Crear Maquina Impresosa</h4>
            </div>
            <div class="modal-body">
                <form class="form row" role="form" id="form-countries">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="checkbox-inline">
                                    <input id="color-configuration" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Si" data-off="No" type="checkbox"> Configurar Colores
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="printer-name" class="col-sm-12 control-label">Nombre</label>
                            <input type="text" class="form-control clear-element" name="printer-name" id="printer-name">
                        </div>
                    </div>
                    <div id="optional-number-colors" class="col-md-12">
                        <div class="form-group">
                            <label for="printer-colors-number" class="col-sm-12 control-label"># Colores</label>
                            <input type="text" class="form-control clear-element" name="printer-colors-number" id="printer-colors-number">
                        </div>
                    </div>

                    <div class="row" id="optional-config-colors" style="display: none;">
                        <div class="col-md-12">
                            <span>Arrastre los colores que desea configurar del panel izquiero hacia el derecho</span>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group sortable-content" id="color-list-main">
                                <?php foreach ($Colores as $color) : ?>
                                    <div class="list-group-item" id="<?= $color['id_color'] ?>"><?= $color['nombre'] ?></div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="list-group sortable-content" id="color-list-slave">

                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-printer" action-type="save" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<?= $this->include('Scripts/AjaxRequest') ?>
<script>
    $(document).ready(function() {
        initPrinters();
        initSortableList(document.getElementById("color-list-main"));
        initSortableList(document.getElementById("color-list-slave"));
    });

    function initPrinters() {
        dataTable = $('#table-printers').DataTable({
            "ordering": true,
            "order": [
                [1, 'asc']
            ],
            "bAutoWidth": false,
            "language": {
                "url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"
            },
            "bDestroy": true,
            "ajax": {
                "url": "/printer/getAllPrintersByCustomer?idCustomer=" + $("#idCliente").val(),
                "type": "GET",
            },
            "columnDefs": [{
                "targets": -1,
                "searchable": false,
                "orderable": false,
                "data": null,
                "width": "10%",
                "className": "text-center",
            }],
            "columns": [{
                    "data": "maquina"
                },
                {
                    "data": "colores"
                },
                {
                    "data": "",
                    render: function(data, type, row) {
                        var output = '';
                        $.each(row.coloresEstablecidos, function(index, item) {
                            output += '<span class="badge badge-primary">' + row.coloresEstablecidos[index].nombre + '</span>';
                        });
                        return output;
                    }
                },
                {
                    "data": '',
                    render: function(data, type, row) {
                        var buttons = '<div class="btn-group table-options">';
                        buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                        return buttons;
                    }
                }
            ],
        });
    }

    function initSortableList(list) {
        Sortable.create(list, {
            group: 'colors',
            animation: 100
        });
    }

    $(document).on('click', '#agregar-impresora', function() {
        $("#ImpresoraModal").modal("show")
    });

    $('#color-configuration').change(function() {
        if ($(this).prop('checked')) {
            $("#optional-number-colors").hide()
            $("#optional-config-colors").show()
        } else {
            $("#optional-number-colors").show()
            $("#optional-config-colors").hide()
        }
    });

    $(document).on("click", "#btn-save-printer", function() {
        Impresora = {}
        let nombre = $("#printer-name").val();
        let esConfigurado = $('#color-configuration').prop('checked');
        let elementos = [...document.querySelectorAll('#color-list-slave > div')].map(element => {
            var color = {}
            color.id = element.id;
            color.color = element.innerText;
            return color
        });
        let numeroColores = (esConfigurado) ? elementos.length : $("#printer-colors-number").val();
        Impresora.cliente = $("#idCliente").val();
        Impresora.nombre = nombre;
        Impresora.esConfigurado = esConfigurado;
        Impresora.numeroColores = numeroColores;
        Impresora.colores = JSON.stringify(elementos);
        ajaxRequest('/printer/addPrinterForCustomer', 'POST', Impresora).done(function(response) {

        });
    });
</script>
<?= $this->endSection() ?>