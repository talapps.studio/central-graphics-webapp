<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <h3 class="mt-0">Administracion de Productos</h3>
                    <div  class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarProducto" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Producto</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="productTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Descripcion</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar productos Determinados Modal-->
<div class="modal fade" id="newProduct" tabindex="-1" role="dialog" aria-labelledby="productModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Producto Nuevo</h4>    
            </div>
            <div class="modal-body">
                <form id="saveProduct" class="form row" role="form">
                    <div class="form-group">
                        <label for="newDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newDescripcion" id="newDescripcion">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-product" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar Productos determinados Modal-->
<div class="modal fade" id="editarProductos" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Actualizar Producto</h4>    
            </div>
            <div class="modal-body">
                <form id="editProduct" class="form row" role="form">
                    <input type="hidden" id="idProductPre">
                    <div class="form-group">
                        <label for="editDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editDescripcion" id="editDescripcion">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-product" class="btn btn-outline-success">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
    var dataTable;
    (function($){
        "use strict";
        dataTable = $('#productTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "ajax":{
                "url":"/defaultProduct/getAllDefaultProducts",
                "type":"GET",
            },
            "columns":[
                {"data":"descripcion"},
                {"data":'activo', render: function(data, type, row){
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                   if(data == '1'){
                    buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                   } else if(data == '0'){
                    buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                   } 
                    return buttons;    
                    }
                }
            ],
        }); 
    })(jQuery);
    $('#productTable tbody').on('click','.btn-edit', function(){
        let service = '/defaultProduct/getAllDefaultProducts/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(service + row['id_servicios_predeterminados']).done(function(data){
            $("#editDescripcion").val(row.descripcion).closest('div').addClass('is-filled');
            $("#editarProductos").modal("show");
            $("#idProductPre").val(row.id_productos_predeterminados);
        });
    });
</script>
<script>
    //boton para mostrar modal agregar productos
    $(document).on('click','#agregarProducto', function(){
        $("#newProduct").modal("show");
    });
    //funcion para resetear campos del modal agregar producto
    $('#newProduct').on('hide.bs.modal', function (){
       clearColorValidation('#saveProduct');
      
    });
    //funcion para resetear campos del modal editar productos
    $('#editarProductos').on('hide.bs.modal', function (){
        clearColorValidation('#editProduct');
    });
    //boton para guardar informacion de Servicios predeterminados
    $(document).on('click','#btn-save-product', function(){
        let newProduct = {};
        newProduct.descripcion = $('#newDescripcion').val();
       if($("#saveProduct").valid()){
            ajaxRequest("/defaultProduct/addNewDefaultProduct","POST", newProduct)
            .done(function(response){
                if(response.status === 200){
                    $('#productTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#newProduct").modal("hide");
                    clearColorValidation('#saveProduct');
                }  
            }).fail(function (xhr, status, error) {}).always(function () {});
        }
    });
    //boton para actualizar informacion de Productos Predeterminados
    $(document).on('click','#btn-edit-product', function(){
        let editProduct = {};
        editProduct.descripcion = $("#editDescripcion").val();
        editProduct.idProductPre = $("#idProductPre").val();
        if($("#editProduct").valid()){
            ajaxRequest("/defaultProduct/editDefaultProduct","POST", editProduct)
            .done(function(response){
                $('#productTable').DataTable().ajax.reload();
                if(response.status === 200){
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });
                    $("#editarProductos").modal("hide");
                }
            }).fail(function (xhr, status, error) {}).always(function () {});   
        }
    });
    //boton para desactivar productos predeterminados
    $('#productTable tbody').on('click','.btn-del', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var deleteProduct = {}
        deleteProduct.idProductPre = row.id_productos_predeterminados;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            //title: response.mensaje,
            confirmButtonText: 'Sí, desactivar'
        }).then((result)=>{
           if(result.isConfirmed){
                ajaxRequest("/defaultProduct/inactiveProduct","POST",deleteProduct)
                .done(function(response){
                 //$("#table-suppliers tr:tg(0)").remove();
                 if(response.status === 200){
                        $('#productTable').DataTable().ajax.reload();
                        Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                        });  
                    }
                
                }).fail(function (xhr, status, error) {}).always(function () {});  
            }
        })
    });
    //boton para activar productos predeterminados
    $('#productTable tbody').on('click','.btn-act', function(){
        let row = dataTable.row($(this).parents('tr')).data();
        var activeProduct = {}
        activeProduct.idProductPre = row.id_productos_predeterminados;
        Swal.fire({
            title:'¿Está seguro?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sí,activar'
        }).then((result)=>{
            if(result.isConfirmed){
                ajaxRequest("/defaultProduct/activeProduct","POST",activeProduct)
                .done(function(response){
                    if(response.status === 200){
                        $('#productTable').DataTable().ajax.reload();
                        Swal.fire({
                            icon: 'success',
                            title: response.mensaje,
                            confirmButtonText: "Aceptar",
                        });
                    }
                });
            }
        });
    });
    //validacion de formularios
    $(document).ready(function(){
    $("#saveProduct").validate({
        rules:{
            newDescripcion:{
                required: true,
            },
            newCosto:{
                required: true,
                number: true
            }
        }
    });
    $("#editProduct").validate({
        rules:{
            editDescripcion:{
                required: true,
            },
            editCosto:{
                required: true,
                number: true 
            }
        }
    });
});
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>