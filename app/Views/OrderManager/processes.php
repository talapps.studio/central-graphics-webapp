<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>

<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0">Listado de procesos por cliente</h3>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-responsive" id="table-customers" style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-uppercase font-weight-bolder opacity-7">Código</th>
                                <th class="text-uppercase font-weight-bolder opacity-7 ">Nombre de cliente</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>

<?= $this-> section('scripts')?>

    <script>
        let dataTable = new DataTable('#table-customers', {
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ajax": {
                "url": "/RestProcess/getAllCustomers",
                "type": "GET",
                dataSrc: ''
            },
            "columns": [
                { "data": "codigo_cliente" },
                { "data": "nombre" },
                {"data": '',
                    render: function(data, type, row) {
                        return `<a class="btn btn-info btn-sm" href="/OrderManager/customerProcess/${row.id_cliente}">Seleccionar cliente</a>`;
                    }

                }
            ],
        });
    </script>

<?= $this->endSection() ?>