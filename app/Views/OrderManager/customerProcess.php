<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>


<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-lg-flex">
                        <div>
                            <h3 class="mt-0">Procesos de cliente: <?= $customer['nombre'] ?></h3>
                        </div>
                        <div class="ms-auto my-auto mt-lg-0 mt-4">
                            <div class="ms-auto my-auto">
                                <div class="btn-group">
                                    <a class="btn btn-primary bg-gradient-primary" href="<?php echo site_url(['OrderManager/'. 'newProcess', $customer['id_cliente']]) ?>" id="idCrearCliente">Crear proceso</a>
                                    <a class="btn btn-secondary bg-gradient-secondary" href="<?php echo site_url('/OrderManager/processes') ?>">Regresar</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-responsive" id="table-process" style="width:100%">
                            <thead>
                            <tr>
                                <th class="text-center text-uppercase font-weight-bolder opacity-7">Correlativo</th>
                                <th class="text-uppercase font-weight-bolder opacity-7 ">Nombre de proceso</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($processes as $process) : ?>
                            <tr>
                                <td class="text-center"><?= $process['proceso'] ?></td>
                                <td><?= $process['nombre'] ?></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>

<?= $this-> section('scripts')?>

    <script>
        let dataTable = new DataTable('#table-process', {
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "columns": [
                { "data": "proceso" },
                { "data": "nombre" }
            ],

        });
    </script>

<?= $this->endSection() ?>