<?= $this->extend('Layout/base') ?>

<?= $this->section('styles') ?>

<?= $this->endSection() ?>


<?= $this->section('content') ?>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0"><span class="material-icons md-48">data_saver_on</span>&nbsp;Ingreso de pedidos</h3>
                </div>
                <div class="card-body">
                    <div class="accordion" id="accordionExample">
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingOne">
                                <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <span class="material-icons md-48">description</span> &nbsp;- Datos Generales
                                </button>
                            </h4>
                            <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <form id="form-billing-profile">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Código Cliente</label>
                                                    <div class="col-4">
                                                        <input type="text" id="codigoCliente" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Proceso</label>
                                                    <div class="col-12">
                                                        <input type="text" id="nitCliente" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Cliente</label>
                                                    <div class="col-12">
                                                        <input type="text" id="direccionCliente" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Producto</label>
                                                    <div class="col-12">
                                                        <input type="text" id="empresaCliente" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Fecha de ingreso</label>
                                                    <div class="col-6">
                                                        <input type="date" id="diasCreditoCliente" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Fecha de entrega</label>
                                                    <div class="col-6">
                                                        <input type="date" id="webCliente" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Miniatura</label>
                                                    <div class="col-12">
                                                        <div>
                                                            <input class="form-control form-control-lg" id="formFileLg" type="file">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    <span class="material-icons md-48">rule</span> &nbsp;- Especificaciones
                                </button>
                            </h4>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <form id="form-billing-profile">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Tipo de trabajo:</label>
                                                    <div class="col-6 mx-2">
                                                        <select class="form-select-lg" aria-label="Default select example">
                                                            <option selected>Nuevo</option>
                                                            <option value="1">Cambios</option>
                                                            <option value="2">Mejora</option>
                                                            <option value="3">Reproceso</option>
                                                            <option value="4">Repetición Exacta</option>
                                                            <option value="5">Otro</option>
                                                            <option value="6">Muestra</option>
                                                            <option value="7">Orden</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Tipo de impresión:</label>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" name="flexRadioDefault" id="flexRadioDefault2" checked>
                                                        <label class="form-check-label" for="flexRadioDefault2">
                                                            Flexografía Embobinado
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Cara</label>
                                                    <div class="col-12 mx-2">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                            <label class="form-check-label" for="inlineRadio1">1</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                            <label class="form-check-label" for="inlineRadio2">2</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                                            <label class="form-check-label" for="inlineRadio3">3</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                                            <label class="form-check-label" for="inlineRadio4">4</label>
                                                        </div>
                                                        <img rel="embobinados" type="image/jpg" width="70%" src="/app-images/img/embobinados.jpg">
                                                        <br>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option5">
                                                            <label class="form-check-label" for="inlineRadio5">5</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio6" value="option6">
                                                            <label class="form-check-label" for="inlineRadio6">6</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio7" value="option7">
                                                            <label class="form-check-label" for="inlineRadio7">7</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio8" value="option8">
                                                            <label class="form-check-label" for="inlineRadio8">8</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Dorso</label>
                                                    <div class="col-12 mx-2">
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">
                                                            <label class="form-check-label" for="inlineRadio1">1</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                            <label class="form-check-label" for="inlineRadio2">2</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3">
                                                            <label class="form-check-label" for="inlineRadio3">3</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option4">
                                                            <label class="form-check-label" for="inlineRadio4">4</label>
                                                        </div>
                                                        <img rel="embobinados" type="image/jpg" width="70%" src="/app-images/img/embobinados.jpg">
                                                        <br>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option5">
                                                            <label class="form-check-label" for="inlineRadio5">5</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio6" value="option6">
                                                            <label class="form-check-label" for="inlineRadio6">6</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio7" value="option7">
                                                            <label class="form-check-label" for="inlineRadio7">7</label>
                                                        </div>
                                                        &emsp;&emsp;
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio8" value="option8">
                                                            <label class="form-check-label" for="inlineRadio8">8</label>
                                                        </div>
                                                    </div>
                                            </div>
                                        </div>
                                            <div class="col-12">
                                                <label class="col-form-label">Material Solicitado: </label>
                                                <div class="input-group input-group-outline">
                                                    <div class="custom-control custom-checkbox form-check-inline mx-2"  >
                                                        <input type="checkbox" class="custom-control-input" id="Check1">
                                                        <label class="custom-control-label" for="inLineCheck1">Arte Digital</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox form-check-inline mx-2">
                                                        <input type="checkbox" class="custom-control-input" id="Check2">
                                                        <label class="custom-control-label" for="inLineCheck2">Destilado de Solvente</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox form-check-inline mx-2">
                                                        <input type="checkbox" class="custom-control-input" id="Check3">
                                                        <label class="custom-control-label" for="inLineCheck3">Negativo</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox form-check-inline mx-2">
                                                        <input type="checkbox" class="custom-control-input" id="Check4">
                                                        <label class="custom-control-label" for="inLineCheck4">Planchas Fotopolímeras</label>
                                                    </div>
                                                    <div class="custom-control custom-checkbox form-check-inline mx-2">
                                                        <input type="checkbox" class="custom-control-input" id="Check5">
                                                        <label class="custom-control-label" for="inLineCheck5">Prueba Digital</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    <span class="material-icons md-48">looks</span> &nbsp;- Datos del Arte
                                </button>
                            </h4>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="input-group input-group-outline my-3 row">
                                                <label class="col-form-label">Unidad de medida:</label>
                                                <div class="col-12 mx-2">
                                                    <select class="form-select-lg" aria-label="Default select example">
   n                                                     <option selected>mm&nbsp;</option>
                                                        <option value="1">in&nbsp;</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <label for="colFormLabel" class="col-sm-6 col-form-label">Alto del arte:</label>
                                                <div class="col-sm-6">
                                                    <input type="number" min="0" class="form-control" id="colFormLabel" placeholder="Alto">
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <label for="colFormLabel" class="col-sm-6 col-form-label">Ancho del arte:</label>
                                                <div class="col-sm-6">
                                                    <input type="number" min="0" class="form-control" id="colFormLabel" placeholder="Ancho">
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <label for="colFormLabel" class="col-sm-6 col-form-label">Ancho de fotocelda:</label>
                                                <div class="col-sm-6">
                                                    <input type="number" min="0" class="form-control" id="colFormLabel" placeholder="Ancho">
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <label for="colFormLabel" class="col-sm-6 col-form-label">Alto de fotocelda:</label>
                                                <div class="col-sm-6">
                                                    <input type="number" min="0" class="form-control" id="colFormLabel" placeholder="Alto">
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <label for="colFormLabel" class="col-sm-6 col-form-label">Color de fotocelda:</label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="Color">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-8">
                                            <div class="input-group input-group-outline my-3 row">
                                                <div class="col-4">
                                                    <h6>Impresión</h6><hr>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Cara</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">Dorso</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <h6>Emulsión del negativo</h6><hr>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio1" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio1">Cara</label>
                                                    </div>
                                                    <div class="custom-control custom-radio">
                                                        <input type="radio" id="customRadio2" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label" for="customRadio2">Dorso</label>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <h6>Código de barra</h6><hr>
                                                    <div class="custom-control custom-switch">
                                                        <input type="checkbox" class="custom-control-input" id="switchCodigoBarra">
                                                        <label class="custom-control-label" for="switchCodigoBarra">Aplicar</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="input-group input-group-outline my-3 row">
                                                <div class="col-sm-6 my-2 boxCodigoBarraInput">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="Tipo">
                                                </div>
                                                <div class="col-sm-6 my-2 boxCodigoBarraInput">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="Número">
                                                </div>
                                                <div class="col-sm-6 my-2 boxCodigoBarraInput">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="Magnificación">
                                                </div>
                                                <div class="col-sm-6 my-2 boxCodigoBarraInput">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="Posición">
                                                </div>
                                                <div class="col-sm-6 my-2 boxCodigoBarraInput">
                                                    <input type="text" class="form-control" id="colFormLabel" placeholder="BWR">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                    <span class="material-icons md-48">square_foot</span> &nbsp;- Datos del Montaje
                                </button>
                            </h4>
                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label">Repeticiones en ancho</label>
                                                        <div class="col-12">
                                                            <input type="number" id="nitCliente" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label">Separación en ancho</label>
                                                        <div class="col-12">
                                                            <input type="number" id="nitCliente" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label">Repeticiones en alto</label>
                                                        <div class="col-12">
                                                            <input type="number" id="nitCliente" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="input-group input-group-outline my-3 row">
                                                        <label class="col-form-label">Separación en alto</label>
                                                        <div class="col-12">
                                                            <input type="number" id="nitCliente" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        <label class="form-check-label" for="inlineCheckbox1">Guías de montaje</label>
                                                    </div>
                                                    <div class="form-check form-check-inline">
                                                        <input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
                                                        <label class="form-check-label" for="inlineCheckbox2">Distorsión</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card mt-2">
                                        <div class="card-body">
                                            <table class="table">
                                                <thead class="thead-dark">
                                                <tr>
                                                    <th scope="col" class="text-center">N°</th>
                                                    <th scope="col">S</th>
                                                    <th scope="col">T</th>
                                                    <th scope="col">L</th>
                                                    <th scope="col">Ángulo</th>
                                                    <th scope="col">Linaje</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <th scope="row" class="text-center">1</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">2</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">3</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">4</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">5</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">6</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">7</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">8</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">9</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th scope="row" class="text-center">10</th>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="form-check form-check-inline">
                                                            <input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="input-group input-group-outline">
                                                            <input type="text" class="form-control" id="formGroupExampleInput">
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    <span class="material-icons md-48">route</span> &nbsp;- Aplicar Ruta
                                </button>
                            </h4>
                            <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <form id="form-billing-profile">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="input-group input-group-outline my-3 row">
                                                    <label class="col-form-label">Aplicar ruta:</label>
                                                    <div class="col-6 mx-2">
                                                        <select class="form-select-lg form-select" aria-label="Default select example">
                                                            <option selected disabled>Seleccione una opción</option>
                                                            <option value="1">Default</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingSix">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    <span class="material-icons md-48">calculate</span> &nbsp;- Cotización
                                </button>
                            </h4>
                            <div id="collapseSix" class="accordion-collapse collapse" aria-labelledby="headingSix" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <div class="d-flex align-items-start">
                                        <div class="nav flex-column nav-pills me-3" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <button class="nav-link active" id="v-pills-home-tab" data-bs-toggle="pill" data-bs-target="#v-pills-home" type="button" role="tab" aria-controls="v-pills-home" aria-selected="true"><span class="material-icons">interests</span>&nbsp;Arte</button>
                                            <button class="nav-link" id="v-pills-profile-tab" data-bs-toggle="pill" data-bs-target="#v-pills-profile" type="button" role="tab" aria-controls="v-pills-profile" aria-selected="false"><span class="material-icons">local_movies</span>&nbsp;Negativos</button>
                                            <button class="nav-link" id="v-pills-messages-tab" data-bs-toggle="pill" data-bs-target="#v-pills-messages" type="button" role="tab" aria-controls="v-pills-messages" aria-selected="false"><span class="material-icons">window</span>&nbsp;Planchas fotopolimeras</button>
                                            <button class="nav-link" id="v-pills-settings-tab" data-bs-toggle="pill" data-bs-target="#v-pills-settings" type="button" role="tab" aria-controls="v-pills-settings" aria-selected="false"><span class="material-icons">auto_mode</span>&nbsp;Otros</button>
                                        </div>
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                                <div class="d-grid gap-2">
                                                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newArtModal" type="button">Agregar nuevo detalle</button>
                                                </div>
                                                <table class="table table-responsive table-sm text-center" id="artTable">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">DETALLE DE ARTE</th>
                                                        <th scope="col">CANTIDAD</th>
                                                        <th scope="col">PRECIO</th>
                                                        <th scope="col">SUB-TOTAL</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody><tr><td id="arteTableNotFoundMessage" class="table-info" colspan="5">Sin detalles agregados...</td></tr></tbody>
                                                    <tfoot>
                                                    <tr><td colspan="4">TOTAL</td><td id="artTableFootTotal">0</td></tr>
                                                    </tfoot>
                                                </table>
                                                <div class="d-grid gap-2">
                                                    <button id="artReportExportBtn" type="button" class="btn btn-info"><span class="material-icons">picture_as_pdf</span>&nbsp;Generar cotización</button>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                                <div class="d-grid gap-2">
                                                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newNegModal" type="button">Agregar nuevo detalle</button>
                                                </div>
                                                <table class="table table-responsive table-sm text-center" id="negTable">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">DETALLE MONTAJE</th>
                                                        <th scope="col">ANCHO</th>
                                                        <th scope="col">ALTO</th>
                                                        <th scope="col">CANTIDAD</th>
                                                        <th scope="col">PRECIO</th>
                                                        <th scope="col">SUB-TOTAL</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody><tr><td id="negTableNotFoundMessage" class="table-info" colspan="7">Sin detalles agregados...</td></tr></tbody>
                                                    <tfoot>
                                                    <tr><td colspan="6">TOTAL</td><td id="negTableFootTotal">0</td></tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                                <div class="d-grid gap-2">
                                                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newPhotoModal" type="button">Agregar nuevo detalle</button>
                                                </div>
                                                <table class="table table-responsive table-sm text-center" id="photoTable">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">DETALLE</th>
                                                        <th scope="col">CANTIDAD</th>
                                                        <th scope="col">PRECIO</th>
                                                        <th scope="col">SUB-TOTAL</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody><tr><td id="photoTableNotFoundMessage" class="table-info" colspan="5">Sin detalles agregados...</td></tr></tbody>
                                                    <tfoot>
                                                    <tr><td colspan="4">TOTAL</td><td id="photoTableFootTotal">0</td></tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                                                <div class="d-grid gap-2">
                                                    <button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#newOthersModal" type="button">Agregar nuevo detalle</button>
                                                </div>
                                                <table class="table table-responsive table-sm text-center" id="otherTable">
                                                    <thead>
                                                    <tr>
                                                        <th scope="col">DETALLE</th>
                                                        <th scope="col">MONTO</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody><tr><td id="otherTableNotFoundMessage" class="table-info" colspan="3">Sin detalles agregados...</td></tr></tbody>
                                                    <tfoot>
                                                    <tr><td colspan="2">TOTAL</td><td id="otherTableFootTotal">0</td></tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h4 class="accordion-header" id="headingSeven">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">
                                    <span class="material-icons md-48">live_help</span> &nbsp;- Observaciones
                                </button>
                            </h4>
                            <div id="collapseSeven" class="accordion-collapse collapse" aria-labelledby="headingSeven" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                    <label for="exampleFormControlTextarea1" class="form-label">Observación</label>
                                    <div class="input-group input-group-outline my-3 row">
                                        <div class="mb-3">
                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="4"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
    <div class="modal fade" id="newArtModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Agregar arte</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="input-group input-group-outline my-3 row">
                        <label class="col-form-label" for="newArtDetailsInput">Detalle del arte</label>
                        <div class="col-12">
                            <input type="text" minlength="1" maxlength="90" id="newArtDetailsInput" class="form-control">
                        </div>
                    </div>
                    <div class="input-group input-group-outline my-3 row">
                        <label class="col-form-label" for="newArtAmountInput">Cantidad</label>
                        <div class="col-12">
                            <input type="number" min="0" id="newArtAmountInput" class="form-control">
                        </div>
                    </div>
                    <div class="input-group input-group-outline my-3 row">
                        <label class="col-form-label" for="newArtPriceInput">Precio</label>
                        <div class="col-12">
                            <input type="number" min="0" id="newArtPriceInput" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Cancelar</button>
                    <button type="button" class="btn btn-primary" id="addNewArtBtn">Agregar</button>
                </div>
            </div>
        </div>
    </div>

<?= $this->endSection() ?>

<?= $this-> section('scripts')?>

    <script>
        let artReportExportBtn = document.getElementById('artReportExportBtn');
        let toggleCodigoBarra = document.getElementById('switchCodigoBarra');
        let boxCodigoBarraInput = document.getElementsByClassName('boxCodigoBarraInput');
        let addNewArtBtn = document.getElementById('addNewArtBtn');
        let artTable = document.getElementById('artTable');
        let artItems= [];

        artReportExportBtn.style.display = 'none';

        for (let element of boxCodigoBarraInput) {
            element.style.display = 'none';
        }

        toggleCodigoBarra.addEventListener('change', function() {
            if (this.checked) {
                for (let element of boxCodigoBarraInput) {
                    element.style.display = 'block';
                }
            } else {
                for (let element of boxCodigoBarraInput) {
                    element.style.display = 'none';
                    element.getElementsByTagName('input')[0].value = '';
                }
            }
        });

        addNewArtBtn.addEventListener('click', function () {
            let name = document.getElementById('newArtDetailsInput');
            let amount = document.getElementById('newArtAmountInput');
            let price = document.getElementById('newArtPriceInput');
            artItems.push({
                'id': uuidv4(),
                'name': name.value,
                'amount': amount.value,
                'price': price.value,
                'total': Math.round(amount.value * price.value * 100) /100
            });
            fillArtTable(artItems);
            name.value = '';
            amount.value = '';
            price.value = '';
        });

        function fillArtTable(items) {
            let newRow = artTable.insertRow(1);
            let newCell0 = newRow.insertCell(0);
            let newCell1 = newRow.insertCell(1);
            let newCell2 = newRow.insertCell(2);
            let newCell3 = newRow.insertCell(3);
            let newCell4 = newRow.insertCell(4);
            let newText0 = document.createTextNode(items[artItems.length - 1]['name']);
            let newText1 = document.createTextNode(items[artItems.length - 1]['amount']);
            let newText2 = document.createTextNode(items[artItems.length - 1]['price']);
            let newText3 = document.createTextNode(items[artItems.length - 1]['total']);

            let newBtn = document.createElement('button');
            newBtn.setAttribute('type', 'button');
            newBtn.setAttribute('value', 'Remover');
            newBtn.innerText = 'Remover';
            newBtn.setAttribute('class', 'btn btn-danger');
            newBtn.setAttribute('onclick', 'removeBtn(this)');
            newBtn.setAttribute('name', items[artItems.length - 1]['id']);

            newCell0.appendChild(newText0);
            newCell1.appendChild(newText1);
            newCell2.appendChild(newText2);
            newCell3.appendChild(newText3);
            newCell4.appendChild(newBtn);

            recalculateTotal(items);
            bootstrap.Modal.getOrCreateInstance(document.getElementById('newArtModal')).hide();
        }

        function removeBtn(item) {
            recalculateTotal(item.getAttribute('name'));
            item.parentNode.parentNode.remove();
        }

        function recalculateTotal(item) {
            let footTotal = 0;
            if (item !== undefined && item !== null) {
                artItems.forEach(x => {
                   if (x['id'] === item)
                       removeItemOnce(artItems, x);
                });
            }
            artItems.forEach(x => {
                footTotal = footTotal + x['total'];
            });
            toggleNofItemFoundTableRow(document.getElementById('arteTableNotFoundMessage'), artItems, artReportExportBtn);
            document.getElementById('artTableFootTotal').innerText = Math.round(footTotal * 100) / 100;
        }

        function toggleNofItemFoundTableRow(element, list, reportBtn) {
            if (list.length === 0) {
                reportBtn.style.display = 'none';
                element.style.display = 'block';
            } else {
                reportBtn.style.display = 'block';
                element.style.display = 'none';
            }

        }

        function uuidv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }

        function removeItemOnce(arr, value) {
            var index = arr.indexOf(value);
            if (index > -1) {
                arr.splice(index, 1);
            }
            return arr;
        }

    </script>

<?= $this->endSection() ?>