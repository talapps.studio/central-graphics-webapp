<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<style>
    .celda_iconos{

    }
    .centrar_img{
        text-align-last: center;
    }
    .titulocelda{
        background-color: #f8f8f8;
    }

    .celda{
        margin: 0px;
        padding: 0px;
        border: 2px;
    }
    .margen_punteado {
        margin-top: 30px;
        border: dotted;
        border-width: 1px;
    }
    .titulo_cuadro {
        border: dotted;
        border-width: 1px;
    }
    .dato_cuadricula {
        font-size: xxx-large;
        font-weight: bold;
        margin-top: 35%;
    }
    .texto_cuadricula{
        font-size: large;
    }
</style>

<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card ">
                <div class="card-header" style="padding: 15px 20px 15px 20px;">
                    <div class="row" style="background-color: #2e2e2e;color: white;text-align: -webkit-center;">
                        <h2>
                            Hoja de Planificaci&oacute;n
                        </h2>
                    </div>
                    <div class="row">
                        <h4 style="background-color: #eaeaea;">
                            13 de agostp 2019
                        </h4>
                    </div>

                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3 celda_iconos centrar_img">
                                    <p class="titulocelda">ARTE</p>
                                    <i class="fa fa-5x fa-pencil iconoceda"></i>
                                </div>
                                <div class="col-md-3 celda_iconos centrar_img">
                                    <p class="titulocelda">ARTE</p>
                                    <i class="fa fa-5x fa-pencil iconoceda"></i>
                                </div>
                                <div class="col-md-3 celda_iconos centrar_img">
                                    <p class="titulocelda">ARTE</p>
                                    <i class="fa fa-5x fa-pencil iconoceda"></i>
                                </div>
                                <div class="col-md-3 celda_iconos centrar_img">
                                    <p class="titulocelda">ARTE</p>
                                    <i class="fa fa-5x fa-pencil iconoceda"></i>
                                </div>
                            </div>
                            <div class="row centrar_img">
                                <div class="col-md-6 celda">
                                    <div class="celda margen_punteado">MAQUINA IMPRESORA</div><div class="celda"> COMEXI</div>
                                </div>
                                <div class="col-md-6 celda">
                                    <div class="celda margen_punteado">LADO DE IMPRESION</div><div class="celda"> DORSO</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 centrar_img" style="background-color: #eaeaea;">
                            <img src="http://www.charliechaplin.com/images/film/poster/0000/0002/big_gold_rush_dance_of_the_rolls.jpg" style="max-width: 100%;max-height: 234px;">
                        </div>
                    </div>
                    <div class="row" style="margin-top: 22px;">
                        <div class="row centrar_img">
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>

                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            <div class="col-md-3 celda titulo_cuadro">
                                <div class="col-md-12 texto_cuadricula">Medidas</div>
                                <div class="col-md-12 dato_cuadricula">98.75%</div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row" style="margin-top: 22px;">
                        <style type="text/css">
                            .tg  {border-collapse:collapse;border-spacing:0;margin:0px auto;}
                            .tg td{border-color:rgb(23, 23, 23);border-style:dotted;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                              overflow:hidden;padding:10px 5px;word-break:normal;}
                            .tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
                              font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
                            .tg .tg-lboi{border-color:inherit;text-align:left;vertical-align:middle}
                            .tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
                            .tg .tg-0lax{text-align:left;vertical-align:top}
                            .fila_impar{
                                background-color: #f8f8f8;
                            }
                        </style>
                        <table class="tg">
                            <thead>
                              <tr style="border-bottom-style: groove;">
                                <th class="tg-0pky"></th>
                                <th class="tg-0pky">COLOR</th>
                                <th class="tg-0pky">ANG.</th>
                                <th class="tg-0pky">ANILLOX</th>
                                <th class="tg-0pky">BCM</th>
                                <th class="tg-0pky">LINEAJE</th>
                                <th class="tg-0pky">OBSERVACIONES</th>
                              </tr>
                            </thead>
                            <tbody id="tbody">
                              <tr>
                                <td class="tg-0pky">1</td>
                                <td class="tg-0pky">NEGRO</td>
                                <td class="tg-0pky">45.00</td>
                                <td class="tg-0pky">900</td>
                                <td class="tg-0pky">2.2</td>
                                <td class="tg-0pky">160</td>
                                <td class="tg-0pky">BARCODE: 7 23232323</td>
                              </tr>
                              <tr>
                                <td class="tg-0pky">2</td>
                                <td class="tg-0pky">Cyan</td>
                                <td class="tg-0pky">45.00</td>
                                <td class="tg-0pky">900</td>
                                <td class="tg-0pky">2.4</td>
                                <td class="tg-0pky">160</td>
                                <td class="tg-lboi" rowspan="3"></td>
                              </tr>
                              <tr>
                                <td class="tg-0lax">3</td>
                                <td class="tg-0pky"></td>
                                <td class="tg-0lax"></td>
                                <td class="tg-0lax"></td>
                                <td class="tg-0lax"></td>
                                <td class="tg-0lax"></td>
                              </tr>
                              <tr>
                                <td class="tg-0pky">4</td>
                                <td class="tg-0pky"></td>
                                <td class="tg-0lax"></td>
                                <td class="tg-0pky"></td>
                                <td class="tg-0pky"></td>
                                <td class="tg-0pky"></td>
                              </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script>
  
        var filas = document.getElementById('tbody').getElementsByTagName('tr');
        var arr = Array.prototype.slice.call( filas );
        arr.forEach((element, index) => {
            if( (index+2) % 2 !=0 ){
                element.classList.add('fila_impar')
            }
        });
        
</script>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<?= $this->endSection() ?>