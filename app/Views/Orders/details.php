<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="card  card-stats mb-4 mb-xl-0">
                <img class="card-background" src="/app-images/logos/logo-cg.png" alt="">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0"><?= $cabecera->nombre_proceso ?></h5>
                            <span class="h3 font-weight-bold mb-0" id="total-entregas"><?= $cabecera->codigo_cliente ?>-<?= $cabecera->proceso ?></span>
                            <div class="row">
                                <span class=" h6 font-weight-bold mb-0" id="total-entregas"><?= $cabecera->nombre ?></span>
                            </div>
                            <div class="row">
                                <span class="text-uppercase font-weight-bold mb-0 text-muted" id="total-entregas"># Pedido: <?= $cabecera->id_pedido ?></span>
                            </div>
                        </div>
                        <div class="col-auto">
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <h5 class="card-title text-uppercase text-muted mb-0">Historial</h5>
                    </div>
                    <table class="table table-bordered table-dark">
                        <thead>
                            <th>Departamento</th>
                            <th>Usuario</th>
                            <th>Estado</th>
                            <th>Fecha Asignado</th>
                            <th>Fecha Terminado</th>
                        </thead>
                        <tbody>
                            <?php foreach ($rutas as $ruta) : ?>
                                <tr>
                                    <td class="text-uppercase bg-gradient-gray text-white"><?= $ruta->departamento ?></td>
                                    <td class="align-middle"><?= $ruta->usuario ?></td>
                                    <td class="align-middle"><?= $ruta->estado ?></td>
                                    <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($ruta->fecha_asignado)) ?></td>
                                    <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($ruta->fecha_fin)) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
                <div class="row">
                    <h5 class="card-title text-uppercase text-muted mb-0">Observaciones</h5>
                </div>
                <table class="table table-bordered table-dark">
                    <thead>
                        <th>Fecha </th>
                        <th>Usuario</th>
                        <th>Observacion</th>

                    </thead>
                    <tbody>
                        <?php foreach ($observaciones as $observacion) : ?>
                            <tr>
                                <td class="text-uppercase bg-gradient-gray text-white"><?= $observacion->usuario ?></td>
                                <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($observacion->fecha_hora)) ?></td>
                                <td class="align-middle"><?= $observacion->observacion ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card card-stats mb-4 mb-xl-0">
            <div class="card-body">
                <div class="row">
                    <h5 class="card-title text-uppercase text-muted mb-0">Consumos</h5>
                </div>
                <table class="table table-bordered table-dark">
                    <thead>
                        <th>Material </th>
                        <th>Descripcion</th>
                        <th>Cantidad</th>
                        <th>Reproceso</th>

                    </thead>
                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<?= $this->endSection() ?>