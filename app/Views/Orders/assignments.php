<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">
      <div class="card my-4">

        <div class="card-body px-0 pb-2">
          <div class="row">
            <h5 class="card-title text-uppercase text-muted mb-0">Trabajos en carga</h5>
          </div>
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0 table-bordered">
              <thead class="bg-gradient-gray ">
                <tr>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Proceso</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Nombre Producto</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Fecha de Asignacion</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Fecha de Entrega</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Tiempo Estimado</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Ruta</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($pedidos as $pedido) : ?>
                  <?php if ($pedido->estado == 'Agregado') : ?>
                    <tr>
                      <td class="text-uppercase bg-gradient-gray text-white">
                        <a href="/order/details/<?= $pedido->id_pedido ?>"><?= $pedido->codigo_cliente ?>-<?= $pedido->proceso ?></a>
                      </td>
                      <td class="align-middle "><?= $pedido->proceso_nombre ?></td>
                      <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($pedido->fecha_asignado)) ?></td>
                      <td class="align-middle text-center"><?= ('0000-00-00' != $pedido->fecha_entrega) ? date('d-m-Y', strtotime($pedido->fecha_entrega)) : '0000-00-00' ?></td>
                      <td class="align-middle text-center"><?= $pedido->tiempo_asignado ?></td>
                      <td class="align-middle text-center"></td>
                    </tr>
                  <?php endif; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-12">
      <div class="card my-4">
        <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
          <div class="bg-gradient-gray shadow-gray border-radius-lg pt-4 pb-3">
            <h6 class="text-white text-capitalize ps-3"></h6>
          </div>
        </div>
        <div class="card-body px-0 pb-2">
          <div class="row">
            <h5 class="card-title text-uppercase text-muted mb-0">Trabajos en ruta</h5>
          </div>
          <div class="table-responsive p-0">
            <table class="table align-items-center mb-0 table-bordered">
              <thead class="bg-gradient-gray ">
                <tr>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Proceso</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7 ps-2">Nombre Producto</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Fecha de Asignacion</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Fecha de Entrega</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Tiempo Estimado</th>
                  <th class="text-white text-center text-uppercase text-secondary font-weight-bolder opacity-7">Ruta</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($pedidos as $pedido) : ?>
                  <?php if ($pedido->estado != 'Agregado') : ?>
                    <tr>
                      <td class="text-uppercase bg-gradient-gray text-white">
                        <a href="/order/details/<?= $pedido->id_pedido ?>"><?= $pedido->codigo_cliente ?>-<?= $pedido->proceso ?></a>
                      </td>
                      <td class="align-middle "><?= $pedido->proceso_nombre ?></td>
                      <td class="align-middle text-center"><?= date(('d-m-Y H:i:s'), strtotime($pedido->fecha_asignado)) ?></td>
                      <td class="align-middle text-center"><?= ('0000-00-00' != $pedido->fecha_entrega) ? date('d-m-Y', strtotime($pedido->fecha_entrega)) : '0000-00-00' ?></td>
                      <td class="align-middle text-center"><?= $pedido->tiempo_asignado ?></td>
                      <td class="align-middle text-center"></td>
                    </tr>
                  <?php endif; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?= $this->endSection() ?>