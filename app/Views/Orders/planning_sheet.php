<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header ">
                    <h3 class="mt-0 text-capitalize">Hoja de Planificaci&oacute;n</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="card  card-stats mb-4 mb-xl-0">
                                <img class="card-background" src="/app-images/logos/logo-cg.png" alt="">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col">
                                            <h5 class="card-title text-uppercase text-muted mb-0"><?= $cabecera->nombre_proceso ?></h5>
                                            <span class="h3 font-weight-bold mb-0" id="total-entregas"><?= $cabecera->codigo_cliente ?>-<?= $cabecera->proceso ?></span>
                                            <div class="row">
                                                <span class=" h6 font-weight-bold mb-0" id="total-entregas"><?= $cabecera->nombre ?></span>
                                            </div>
                                            <div class="row">
                                                <span class="text-uppercase font-weight-bold mb-0 text-muted" id="total-entregas"># Pedido: <?= $cabecera->id_pedido ?></span>
                                            </div>
                                        </div>
                                        <div class="col-auto">
                                        </div>
                                    </div>
                                    <hr class="dark horizontal my-0">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <div class="card  card-stats mb-4 mb-xl-0">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group fix-bottom">
                                                <label for="editCodigo" class="control-label">Tipo Impresi&oacute;n</label>
                                                <input type="radio" class="form-control-sm" checked> Flexograf&iacute;a
                                            </div>
                                        </div>
                                    </div>
                                    <hr class="hr-just">
                                    <div class="row">
                                        <label for="editCodigo" class="control-label">Material Solicitado</label>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Arte
                                            </div>
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Destilado Solvente
                                            </div>
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Negativo
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Planchas
                                            </div>
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Positivos
                                            </div>
                                            <div class="form-group fix-bottom">
                                                <input type="checkbox" class="form-control-sm"> Prueba Digital
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-md-5 col-sm-12">
                            <table class="table table-bordered planificacion-colores">
                                <thead>
                                    <th>Colores</th>
                                    <th>S</th>
                                    <th>&Aacute;ngulo</th>
                                    <th>Lineaje</th>
                                    <th>Anilox</th>
                                    <th>BCM</th>
                                    <th>Sticky</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3 col-sm-12">
                            <div class="row">
                                <div class="col-12">
                                    <table class="table table-bordered table-condensed planificacion-impresion">
                                        <thead>
                                            <th>Informaci&oacute;n Impresi&oacute;n</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div class="form-group fix-bottom">
                                                        <label for="editCodigo" class="col-sm-12 control-label">M&aacute;quina Impresora</label>
                                                        <select name="maquina-impresora" id="maquina-impresora" class="form-control form-control-sm custom-sm"></select>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="form-group fix-bottom">
                                                        <label for="editCodigo" class="col-sm-12 control-label">Lado a imprimir</label>
                                                        <input type="text" class="form-control form-control-sm custom-sm">
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-12">
                                    <table class="table table-bordered table-condensed planificacion-impresion">
                                        <thead>
                                            <th colspan="2">Embobinado Arte</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Cara:</td>
                                                <td>
                                                    <input type="text" class="form-control form-control-sm custom-sm">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Dorso:</td>
                                                <td>
                                                    <input type="text" class="form-control form-control-sm custom-sm">
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-12">
                            <table class="table table-bordered table-condensed planificacion-repeticiones">
                                <thead>
                                    <th colspan="4">Repeticiones (mm)</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Ancho</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                        <td>De</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Circunferencia</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                        <td>De</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-condensed planificacion-repeticiones">
                                <thead>
                                    <th colspan="4">Medidas Totales (mm)</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Ancho</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                        <td>Circunferencia</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Exceso (in)</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                        <td>in2</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-condensed planificacion-repeticiones">
                                <thead>
                                    <th colspan="2">Montaje Segmentado (Opcional)</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Desface</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <table class="table table-bordered table-condensed planificacion-repeticiones">
                                <thead>
                                    <th colspan="4">Fotocelda </th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Tamaño</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <div class="row">
                                                    <div class="col-5">
                                                        <input type="text" class="form-control form-control-sm custom-sm">
                                                    </div> x
                                                    <div class="col-5">
                                                        <input type="text" class="form-control form-control-sm custom-sm">
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>Color</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>C&oacute;digo de Barras</td>
                                        <td colspan="3">
                                            <input type="text" class="form-control form-control-sm custom-sm">
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <table class="table table-bordered table-condensed planificacion-distorsion">
                                <thead>
                                    <th colspan="6">Distorsi&oacute;n</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Altura</td>
                                        <td>
                                            <select name="planificacion-polimeros" id="planificacion-polimeros" class="form-control form-control-sm custom-sm">
                                                <?php foreach ($polimeros as $polimero) : ?>
                                                    <option value="<?= $polimero['id_constante_polimero'] ?>"><?= $polimero['clave_polimero'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td>StickyBack</td>
                                        <td>
                                            <select name="planificacion-sticky" id="planificacion-sticky" class="form-control form-control-sm custom-sm">
                                                <?php foreach ($stickybacks as $stickyback) : ?>
                                                    <option value="<?= $stickyback['id_constante_stickyback'] ?>"><?= $stickyback['stickyback_valor'] ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </td>
                                        <td>Distorsi&oacute;n</td>
                                        <td>
                                            <div class="form-group fix-bottom">
                                                <input type="text" class="form-control form-control-sm custom-sm">
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
<?= $this->endSection() ?>

<?= $this->section('modal') ?>

<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<?= $this->endSection() ?>