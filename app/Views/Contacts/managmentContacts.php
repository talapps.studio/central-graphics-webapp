<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid py-4">
  <div class="row">
    <div class="col-12">  
      <div class="card">
          <div class="card-header">
            <div class="d-lg-flex">
               <div class="col-10">
                  <h3 class="mb-0">Administraci&oacute;n de Contactos</h3>
                </div>
                <hr>
                <div class="row">
                  <div class="ms-auto my-auto mt-lg-0 mt-2">
                    <div class="ms-auto my-auto">
                      <a type="button" class="btn btn-lg btn btn-outline-info" id="agregarContacto"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Contacto</a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          <div class="card-body">         
            <div class="table-responsive p-0">
              <table class="table align-items-center table-responsive" id="table-contacts" style="width:100%">
                <thead>
                  <tr class="table-secondary">
                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Cargo</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">E-mail</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Tel-Oficina</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Tel-Directo</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Tel-Celular</th>
                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                  </tr>
                </thead>
              <tbody>
             
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>        
  </div>
</div>
 <!-- Modal -->
 <div class="modal fade" id="contactoModal" tabindex="-1" aria-labelledby="contactoModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="contactoModalLabel"> <span id="tituloModal">Crear Contacto</span></h5>
            <button type="button" class="btn-close text-dark" data-bs-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="form-billing-profile">
             <input type="hidden" id="idContacto">
              <div class="row">
                <div class="col-md-6">
                  <div class="input-group input-group-outline my-3">
                      <label class="col-form-label">Nombre Contacto:</label>
                      <div class="col-12">
                        <input type="text" id="nombreContacto" class="form-control">
                      </div>
                  </div>
                  <div class="input-group input-group-outline my-3">
                      <label class="col-form-label">Cargo</label>
                      <div class="col-12">
                        <input type="text" id="cargoContacto" class="form-control">
                      </div>
                  </div>
                      <div class="input-group input-group-outline my-3">
                      <label class="col-form-label">E-mail</label>
                      <div class="col-12">
                        <input type="text" id="emailContacto" class="form-control">
                      </div>
                  </div>
                </div>
                <div class="col-md-6">
                    <div class="input-group input-group-outline my-3">
                         <label class="col-form-label">Tel-Oficina</label>
                         <div class="col-12">
                          <input type="text" id="telOficinaContacto" class="form-control">
                         </div> 
                    </div>
                    <div class="input-group input-group-outline my-3">
                        <label class="col-form-label">Tel-Directo</label>
                        <div class="col-12">
                          <input type="text" id="telDirectoContacto" class="form-control">
                        </div>
                    </div>
                    <div class="input-group input-group-outline my-3">
                        <label class="col-form-label">Tel-Celular</label>
                        <div class="col-12">
                          <input type="text" id="telCelularContacto" class="form-control">
                        </div>
                    </div>
               </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn bg-gradient-secondary" data-bs-dismiss="modal">Cerrar</button>
            <button type="button" id="btn-save-contact" action-type="save" class="btn bg-gradient-primary">Guardar</button>
          </div>
        </div>
    </div>

<?= $this->endSection() ?>
<?= $this-> section('scripts')?>
<script>
   var dataTable;
    (function ($) {
        "use strict";
        dataTable = $('#table-contacts').DataTable({
            "language": {"url": "../material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "bDestroy": true,
            "ajax": {
                "url": "/contact/getAllContacts",
                "type": "GET",
            },
            "columns": [
                { "data": "nombre" },
                { "data": "cargo" },
                { "data": "email" },
                { "data": "tel_oficina" },
                { "data": "tel_directo" },
                { "data": "tel_celular" },
                {"data": '', render: function (data, type, row) {
                    var buttons = '<div class="btn-group">';
                    buttons += '<a class="move-on-hover text-warning px-3 btn-edit" title="Editar"><i class="fa fa-pencil-alt"></i></a>';
                    buttons += '<a class="move-on-hover text-danger btn-del" title="Eliminar"><i class="fa fa-trash-alt"></i></a>';
                   // buttons += '<a class="btn btn-icon btn-2 btn-primary" ><span class="btn-inner--icon"><i class="material-icons">lightbulb</i></span></button>';
                    return buttons;
                    }
                }
            ],
            
        });
        //boton actualizar
        $('#table-contacts tbody').on('click', '.btn-edit', function() {
          let row = dataTable.row($(this).parents('tr')).data();
          $("#btn-save-contact").attr("action-type", "edit");
          $("#nombreContacto").val(row.nombre).closest('div').addClass('is-filled');
          $("#cargoContacto").val(row.cargo).closest('div').addClass('is-filled');
          $("#emailContacto").val(row.email).closest('div').addClass('is-filled');
          $("#telOficinaContacto").val(row.tel_oficina).closest('div').addClass('is-filled');
          $("#telDirectoContacto").val(row.tel_directo).closest('div').addClass('is-filled');
          $("#telCelularContacto").val(row.tel_celular).closest('div').addClass('is-filled');
          $("#contactoModal").modal("show");
          $("#tituloModal").empty().text('Editar Contacto');
          $("#idContacto").val(row.id_cliente_contacto);
        });
        $('#table-contacts tbody').on('click', '.btn-del', function() {
          let row = dataTable.row($(this).parents('tr')).data();
          $("#btn-save-contact").attr("action-type", "del");
          $("#idContacto").val(row.id_cliente_contacto);
          $("#contactoModal").modal("show");
        });
    })(jQuery);
</script>
<script>    
$(document).on('click','#btn-save-contact',function(){
    var action = $(this).attr("action-type");
    var Contact = {};
    Contact.nombre = $("#nombreContacto").val();
    Contact.cargo = $("#cargoContacto").val();
    Contact.email = $("#emailContacto").val();
    Contact.telOficina = $("#telOficinaContacto").val();
    Contact.telDirecto = $("#telDirectoContacto").val();
    Contact.telCelular = $("#telCelularContacto").val();
    $("#tituloModal").empty().text('Crear Contacto');

    switch(action){
      case "save":
      ajaxRequest("/contact/addNewContact","POST",Contact)
        .done(function (response) {
            $('#table-contacts').DataTable().ajax.reload();
            $("#contactoModal").modal("hide");
            
        }).fail(function (xhr, status, error) {}).always(function () {});
        break;
        case "edit":
          Contact.idContacto = $("#idContacto").val();
          ajaxRequest("/contact/editContact", "POST", Contact)
            .done(function (response) {
                $('#table-contacts').DataTable().ajax.reload();
                $("#contactoModal").modal("hide");
                
                $("#btn-save-contact").attr("action-type","save");
            }).fail(function (xhr, status, error) {}).always(function() {});;
        break;
        case "del":
        Contact.idContacto = $("#idContacto").val();
        ajaxRequest("/contact/deleteContact", "POST", Customer)
          .done(function(response) {
            $('#table-contacts').DataTable().ajax.reload();
            $("#contactoModal").modal("hide");
            $("#btn-save-contact").attr("action-type", "save");
          }).fail(function(xhr, status, error) {}).always(function() {});;
        break; 
    } 
    
});
</script>
<script>
  $(document).on('click','#agregarContacto',function(){
    limpiarFormulario();
    $("#contactoModal").modal("show");
  });
  function limpiarFormulario() {
    document.getElementById("form-billing-profile").reset();
  }
</script>
<?= $this->include('Scripts/AjaxRequest') ?>



<?= $this->endSection() ?>
