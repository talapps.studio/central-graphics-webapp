<!--
=========================================================
* Material Dashboard 2 - v3.0.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard
* Copyright 2021 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://www.creative-tim.com/license)
* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
-->
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="apple-touch-icon" sizes="76x76" href="../material-template/assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../material-template/assets/img/favicon.png">
    <title>
        Central Graphic's
    </title>
    <!--     Fonts and icons     -->
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700" />
    <!-- Nucleo Icons -->
    <link href="/material-template/assets/css/nucleo-icons.css" rel="stylesheet" />
    <link href="/material-template/assets/css/nucleo-svg.css" rel="stylesheet" />
    <!-- Font Awesome Icons -->
    <script src="https://kit.fontawesome.com/42d5adcbca.js" crossorigin="anonymous"></script>
    <!-- Material Icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <!-- CSS Files -->
    <link id="pagestyle" href="/material-template/assets/css/material-dashboard.css?v=3.0.0" rel="stylesheet" />
</head>

<body class="bg-gray-200">
    <div class="container position-sticky z-index-sticky top-0">
        <div class="row">
            <div class="col-12">

            </div>
        </div>
    </div>
    <main class="main-content  mt-0">
        <div class="page-header align-items-start min-vh-100" style="background-image: url('/app-images/backgrounds/custom-background-3.jpg');">
            <span class="mask bg-gradient-dark opacity-1"></span>
            <div class="container my-auto">

                <div class="card z-index-0 fadeIn3 fadeInBottom">
                    <div class="row">
                    <div class="col-lg-12">
                            <?php if (session()->getFlashdata('msg')) : ?>
                                <div class="alert alert-warning">
                                    <?= session()->getFlashdata('msg') ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="col-lg-6 mt-3">
                            <div class="text-center">
                                <img class="w-60" src="/app-images/logos/logo-cg.png" alt="">
                            </div>
                        </div>
                        
                        <div class="col-lg-6 mt-3">
                            <h2>Olvido su contraseña?</h2>
                            <h6>Por favor ingrese el nombre de usuario con el que ingresa a la plataforma</h6>
                            <a href="/login">Regresar al login</a>
                            <form role="form" class="text-start" action="<?php echo base_url(); ?>/Authentication/SendEmailForReset" method="POST">
                                <div class="row">
                                    <div class="col-md-11">
                                        <div class="input-group input-group-outline my-3">
                                            <label for="username" class="form-label">nombre de usuario</label>
                                            <input type="text" name="email" id="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="text-center">
                                            <button type="submit" id="iniciar_sesion" class="btn btn-lg bg-gradient-primary w-100 my-4 mb-2">Reestablecer contraseña</button>
                                        </div>
                                    </div>
                            </form>
                            
                        </div>

                    </div>
                </div>
            </div>
        </div>
        </div>
        <footer class="footer position-absolute bottom-2 py-2 w-100">
            <div class="container">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-12 col-md-6 my-auto">
                        <div class="copyright text-center text-sm text-white text-lg-start">
                            © <script>
                                document.write(new Date().getFullYear())
                            </script>,
                            Hecho con <i class="fa fa-heart" aria-hidden="true"></i> en El Salvador
                            <!-- <a href="https://www.creative-tim.com" class="font-weight-bold text-white" target="_blank">Creative Tim</a>
                for a better web. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        </div>
    </main>

    <!--   Core JS Files   -->
    <script src="/material-template/assets/js/core/popper.min.js"></script>
    <script src="/material-template/assets/js/core/bootstrap.min.js"></script>
    <script src="/material-template/assets/js/plugins/perfect-scrollbar.min.js"></script>
    <script src="/material-template/assets/js/plugins/smooth-scrollbar.min.js"></script>
    <script src="/material-template/assets/js/material-dashboard.min.js?v=3.0.0"></script>
</body>

</html>