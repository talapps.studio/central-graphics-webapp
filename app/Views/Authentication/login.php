<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="<?= base_url('material-template/assets/img/apple-icon.png') ?>" rel="apple-touch-icon" sizes="76x76" />
    <link href="<?= base_url('material-template/assets/img/favicon.ico') ?>" rel="icon" />
    <link href="<?= base_url('bootstrap-template/assets/css/plugins/font-awesome/css/font-awesome.min.css') ?>"
          rel="stylesheet"/>
    <title>Central Graphic's</title>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900|Roboto+Slab:400,700"/>
    <link href="<?= base_url('material-template/assets/css/nucleo-icons.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('material-template/assets/css/nucleo-svg.css') ?>" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons+Round" rel="stylesheet">
    <link id="pagestyle" href="<?= base_url('material-template/assets/css/material-dashboard.css?v=3.0.0') ?>" rel="stylesheet" />
    <style>
        .form-control {
            border-radius: 0;
            padding-left: 10px;
        }
    </style>
</head>

<body class="bg-gray-200">
<div class="container position-sticky z-index-sticky top-0">
    <div class="row">
        <div class="col-12">

        </div>
    </div>
</div>
<main class="main-content  mt-0">
    <div class="page-header align-items-start min-vh-100"
         style="background-image: url('/app-images/backgrounds/custom-background-3.jpg');">
        <span class="mask bg-gradient-dark opacity-1"></span>
        <div class="container my-auto">
            <div class="row">
                <div class="col-lg-4 col-md-8 col-12 mx-auto">
                    <div class="card z-index-0 fadeIn3 fadeInBottom">
                        <div class="card-header p-0 position-relative  mx-3 ">
                            <div class="border-radius-lg py-3 pe-1">
                                <div class="mt-3 text-center">
                                    <img class="w-70" src="<?php echo base_url(); ?>/app-images/logos/logo-cg.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <?php if (session()->getFlashdata('msg')): ?>
                                <div class="alert alert-warning">
                                    <?= session()->getFlashdata('msg') ?>
                                </div>
                            <?php endif; ?>
                            <form role="form" class="text-start"
                                  action="<?php echo base_url(); ?>/Authentication/userAuth" method="POST">

                                <div class="form-group my-3">
                                    <label for="username" class="form-label">Usuario</label>
                                    <input type="text" name="username" id="username" class="form-control" placeholder="Usuario">
                                </div>
                                <div class="form-group mb-3">
                                    <label for="password" class="form-label">Contraseña</label>
                                    <input type="password" name="password" id="password"
                                           class="form-control text-black" placeholder="Contraseña">
                                </div>
                                <div class="text-center">
                                    <i class="info"></i>
                                    <a class="btn-link" href="<?php echo base_url(); ?>/Authentication/forgotPassword">0lvide
                                        mi contraseña</a>
                                </div>

                                <div class="text-center">
                                    <button type="submit" id="iniciar_sesion"
                                            class="btn btn-lg bg-gradient-primary w-100 my-4 mb-2"><i class="fa fa-sign-in" aria-hidden="true"></i>
                                        Ingresar
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer class="footer position-absolute bottom-2 py-2 w-100">
            <div class="container">
                <div class="row align-items-center justify-content-lg-between">
                    <div class="col-12 col-md-6 my-auto">
                        <div class="copyright text-center text-sm text-white text-lg-start">
                            ©
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            ,
                            Hecho con <i class="fa fa-heart" aria-hidden="true"></i> en El Salvador
                            <!-- <a href="https://www.creative-tim.com" class="font-weight-bold text-white" target="_blank">Creative Tim</a>
                            for a better web. -->
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</main>

<!--   Core JS Files   -->
<script type="text/javascript" src="<?= base_url('material-template/assets/js/core/popper.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('material-template/assets/js/core/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('material-template/assets/js/plugins/perfect-scrollbar.min.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('material-template/assets/js/plugins/smooth-scrollbar.min.js') ?>"></script>
<script>
    var win = navigator.platform.indexOf('Win') > -1;
    if (win && document.querySelector('#sidenav-scrollbar')) {
        var options = {
            damping: '0.5'
        }
        Scrollbar.init(document.querySelector('#sidenav-scrollbar'), options);
    }
</script>

<!-- Github buttons -->
<script async defer src="https://buttons.github.io/buttons.js"></script>
<!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
<script type="text/javascript" src="<?= base_url('material-template/assets/js/plugins/smooth-scrollbar.min.js') ?>"></script>
</body>
</html>