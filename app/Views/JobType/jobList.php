<?= $this->extend('Layout/base') ?>
<?= $this->section('content') ?>
<div class="container-fluid py-4">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                    <div class="d-lg-flex">
                        <div class="col10">
                            <h3 class="mb-0">Administraci&oacute;n de Tipo de trabajos</h3>
                        </div>
                        <div class="row">
                            <div class="ms-auto my-auto mt-lg-0 mt-4">
                                <div class="ms-auto my-auto">
                                    <button type="button" class="btn btn-outline-info pull-right" id="agregar-tipoTrabajo"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar tipo de trabajo</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table class="table align-items-center table-responsive" id="table-jobsType" style="width:100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Codigo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Nombre</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agrepar tipo de trabajo Modal -->
<div class="modal fade" id="jobTypeModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="jobTypeModalLable" id="tituloModal">Crear tipo de trabajo</h4>
            </div>
            <div class="modal-body">
            <form id="jobTypeform" class="form row" role="form">
                <div class="form-group">
                  <label for="newJobType" class="col-sm-12 control-label">Nombre</label>
                  <div class="col-sm-12">
                  <input type="text" class="form-control clear-element" id="newJobType" name="newJobType">
                  </div>
                </div>
              </form>
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-save-job" action-type="save" class="btn btn-outline-success">Guardar</button> 
            </div>
        </div>
    </div>
</div>
<!--fin modal-->
<!--Editar tipo de trabajo Moda -->
<div class="modal fade" id="editjobTypeModal" tabindex="-1" aria-labelledby="maquinaModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="jobTypeModalLable" id="tituloModal">Editar tipo de trabajo</h4>
            </div>
            <div class="modal-body">
              <form id="editJobTypeform" class="form row" role="form">
                <input type="hidden" id="idJobType">
                    <div class="form-group">
                        <label for="editJobType" class="col-sm-12 control-label clear-element">Nombre</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="editJobType" name="editJobType">
                        </div>
                    </div>
              </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" id="btn-edit-job" action-type="save" class="btn btn-outline-success">Guardar</button> 
            </div>
        </div>
    </div>
</div>
<!--fin modal-->
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script>
var dataTable
(function ($){
    "use strict";
    dataTable = $('#table-jobsType').DataTable({
        "language":{"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
        "bDestroy":true,
        "ajax": {
            "url":"/jobType/getAllJobType",
            "type":"GET",
        },
        "columns":[
            {"data":"id_tipo_trabajo"},
            {"data":"trabajo"},
            {"data":'activo', render: function(data, type, row){
                var buttons = '<div class="btn-group table-options">';
                buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
               if(data == 's'){
                buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
               }else if(data == 'n'){
                buttons += '<a class="move-on-hover text-info btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
               }
                return buttons;
              }
            }
        ],
    });
})(jQuery);
$('#table-jobsType tbody').on('click','.btn-edit',function(){
    let job = '/getAllJobType/';
    let row = dataTable.row($(this).parents('tr')).data();
    getRequest(job + row['id_tipo_trabajo']).done(function(data){
        $("#editJobType").val(row.trabajo).closest('div').addClass('is-filled');
        $("#editjobTypeModal").modal("show");
        $("#idJobType").val(row.id_tipo_trabajo);
    });
  });
</script>
<script>
//boton para agregar un tipo de trabajo
$(document).on('click','#agregar-tipoTrabajo',function(){
    $("#jobTypeModal").modal("show");
});
$('#jobTypeModal').on('hide.bs.modal', function (){
    clearColorValidation('#jobTypeform');
});
$('#editjobTypeModal').on('hide.bs.modal', function (){
    clearColorValidation('#editJobTypeform');
});
//boton para guardar informacion de tipo de trabajo
$(document).on('click','#btn-save-job',function(){
  let Trabajo = {};
  Trabajo.nombre=$("#newJobType").val();
  if($("#jobTypeform").valid()){
    ajaxRequest("/jobType/addNewJobType","POST",Trabajo)
    .done(function(response){
      if(response.status === 200){
        $('#table-jobsType').DataTable().ajax.reload();
        console.log(response.mensaje);
        Swal.fire({
             icon: 'success',
             title: response.mensaje,
             confirmButtonText: "Aceptar",
        });
        $("#jobTypeModal").modal("hide");
      }
    }).fail(function (xhr, status, error) {}).always(function () {});
  } 
});
$(document).on('click','#btn-edit-job',function(){
  let editTrabajo = {};
  editTrabajo.nombre= $("#editJobType").val();
  editTrabajo.idJobType= $("#idJobType").val();
  if($("#editJobTypeform").valid()){
    ajaxRequest("/jobType/editJobType","POST", editTrabajo)
    .done(function(response){
      if(response.status === 200){
        $('#table-jobsType').DataTable().ajax.reload();
        console.log(response.mensaje);
        Swal.fire({
             icon: 'success',
             title: response.mensaje,
             confirmButtonText: "Aceptar",
        });
        $("#editjobTypeModal").modal("hide");
      }
    }).fail(function (xhr, status, error) {}).always(function () {});
  }
});
//boton para desactivar tipo de trabajo
$('#table-jobsType').on('click','.btn-del', function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var inactivarTrabajo = {}
    inactivarTrabajo.idJobType = row.id_tipo_trabajo;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        //title: response.mensaje,
        confirmButtonText: 'Sí, desactivar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/jobType/inactiveJob","POST",inactivarTrabajo)
            .done(function(response){
                if(response.status === 200){
                    $('#table-jobsType').DataTable().ajax.reload();
                    Swal.fire({
                    icon: 'success',    
                    title: response.mensaje,  
                    confirmButtonText: "Aceptar",
                     });
                }
            }).fail(function(xhr, status, error) {}).always(function() {});
        }
    })
});
//boton para activar tipo de trabajo
$('#table-jobsType').on('click','.btn-act', function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var activarTrabajo = {}
    activarTrabajo.idJobType = row.id_tipo_trabajo;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        //title: response.mensaje,
        confirmButtonText: 'Sí, activar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/jobType/activeJob","POST",activarTrabajo)
            .done(function(response){
                if(response.status === 200){
                    $('#table-jobsType').DataTable().ajax.reload();
                    Swal.fire({
                    icon: 'success',    
                    title: response.mensaje,                   
                    confirmButtonText: "Aceptar",
                     });
                }
            }).fail(function(xhr, status, error) {}).always(function() {});
        }
    })
});
//Validacion de formulario
$(document).ready(function(){
    $("#jobTypeform").validate({
        rules: {
            newJobType:{
                required: true,
            }
        }
    });
    $("#editJobTypeform").validate({
        rules: {
            editJobType:{
                required: true,
            }
        }
    });
});
</script>


<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>