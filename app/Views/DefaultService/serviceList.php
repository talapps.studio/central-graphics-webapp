<?= $this->extend('Layout/base') ?>

<?= $this->section('content') ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="panel panel-default">
                <div class="card-header">
                   <h3 class="mt-0">Administración de servicios</h3>
                   <div class="row">
                        <div class="col-lg-12 col-md-6 col-sm-8">
                            <button id="agregarServicio" type="button" class="btn btn-outline-info pull-right" data-toggle="modal"><span class="material-icons">add_circle_outline</span>&nbsp;Agregar Servicio</button>
                        </div>
                   </div> 
                </div>
                <div class="card-body">
                    <div class="table-responsive p-0">
                        <table id="serviceTable" class="table align-items-center table-responsive" style="width: 100%">
                            <thead>
                                <tr class="table-secondary">
                                    <th class="text-uppercase font-weight-bolder opacity-7">Descripcion</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Costo</th>
                                    <th class="text-uppercase font-weight-bolder opacity-7">Opciones</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!--Agregar Servicios Modal -->
<div class="modal fade" id="newService" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Nuevo Servicio</h4>    
            </div>
            <div class="modal-body">
                <form id="saveService" class="form row" role="form">
                    <div class="form-group">
                        <label for="newDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newDescripcion" id="newDescripcion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="newCosto" class="col-sm-12 control-label">Costo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="newCosto" id="newCosto">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-save-service" class="btn btn-outline-success">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!--Editar Servicios Modal-->
<div class="modal fade" id="editarServicios" tabindex="-1" role="dialog" aria-labelledby="serviceModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> 
                <h4 class="modal-title" id="exampleModalLabel">Editar Servicio</h4>    
            </div>
            <div class="modal-body">
                <form id="editService" class="form row" role="form">
                    <input type="hidden" id="idService">
                    <div class="form-group">
                        <label for="editDescripcion" class="col-sm-12 control-label">Descripcion</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editDescripcion" id="editDescripcion">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="editCosto" class="col-sm-12 control-label">Costo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control clear-element" name="editCosto" id="editCosto">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="btn-edit-service" class="btn btn-outline-success">Guardar Cambios</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script>
    var dataTable;
    (function ($){
        "use strict";
        dataTable = $('#serviceTable').DataTable({
            "language": {"url": "/material-template/assets/js/plugins/datatables/i18n/es-ES.json"},
            "dDestroy": true,
            "ajax":{
                "url":"/service/getAllServices",
                "type":"GET",
            },
            "columns":[
                {"data":"descripcion"},
                {"data":"costo", render:function( data, type, row, meta ){
                        return  '$'+Number.parseFloat(data).toFixed(2);
                    }
                },
                {"data":'activo', render: function(data, type, row){
                    var buttons = '<div class="btn-group table-options">';
                    buttons += '<a class="move-on-hover text-warning btn-edit" data-toggle="tooltip" data-placemnet="top" title="Editar"><i class="fa fa-lg fa-pencil"></i></a>';
                    if(data == '1'){
                        buttons += '<a class="move-on-hover text-danger px-3 btn-del" data-toggle="tooltip" data-placemnet="top" title="Desactivar"><i class="fa fa-lg fa-trash"></i></a>';
                    }else if(data == '0'){
                        buttons += '<a class="move-on-hover text-info px-3 btn-act" data-toggle="tooltip" data-placemnet="top" title="Activar"><i class="fa fa-lg fa-plus"></i></a>';
                    }
                    return buttons;    
                    }
                }
            ],
        });
    })(jQuery);
    $('#serviceTable tbody').on('click','.btn-edit', function(){
        let service = '/service/getAllServices/';
        let row = dataTable.row($(this).parents('tr')).data();
        getRequest(service + row['id_servicios_predeterminados']).done(function(data){
            $("#editDescripcion").val(row.descripcion).closest('div').addClass('is-filled');
            $("#editCosto").val(row.costo).closest('div').addClass('is-filled');
            $("#editarServicios").modal("show");
            $("#idService").val(row.id_servicios_predeterminados);
        });
    });
</script>
<script>
    //boton para mostrar modal agregar servicios
    $(document).on('click','#agregarServicio', function(){
        $("#newService").modal("show");
    });
     //funcion para resetear campos del modal
    $('#newService').on('hide.bs.modal', function (){
       clearColorValidation('#saveService');
    });
    $('#editarServicios').on('hide.bs.modal', function (){
       clearColorValidation('#editService');
    });
//boton para guardar informacion de Servicios predeterminados
$(document).on('click','#btn-save-service', function(){
    let newService = {};
    newService.descripcion = $('#newDescripcion').val();
    newService.costo = $('#newCosto').val();
    if($("#saveService").valid()){
        ajaxReqMessageDefault("/service/addNewService","POST", newService)
        .done(function(response){
            if(response.status === 200){
                $('#serviceTable').DataTable().ajax.reload();
                $("#newService").modal("hide");
            }
        });
    }
});
//boton para actualizar informacion de Servicios Predeterminados
$(document).on('click','#btn-edit-service', function(){
    let editService = {};
    editService.descripcion = $("#editDescripcion").val();
    editService.costo = $("#editCosto").val();
    editService.idService = $("#idService").val();

    if($("#editService").valid()){
        ajaxReqMessageDefault("/service/editService","POST", editService)
        .done(function(response){
            if(response.status === 200){
                $('#serviceTable').DataTable().ajax.reload();
                $("#editarServicios").modal("hide");
            }
        });   
    }
});
//funcion para resetear campos del modal
$('#newService').on('hide.bs.modal', function (){
    document.getElementById("saveService").reset();
});
//boton para desactivar servicios predeterminados
$('#serviceTable tbody').on('click','.btn-del',function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var inactiveServicio = {}
    inactiveServicio.idService = row.id_servicios_predeterminados;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, desactivar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/service/inactiveService","POST",inactiveServicio)
            .done(function(response){
                if(response.status === 200){
                    $('#serviceTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });  
                }
                
            }).fail(function (xhr, status, error) {}).always(function () {});  
        }
    })
});
//boton para activar servicios predetermindados
$('#serviceTable tbody').on('click','.btn-act',function(){
    let row = dataTable.row($(this).parents('tr')).data();
    var activeServicio = {}
    activeServicio.idService = row.id_servicios_predeterminados;
    Swal.fire({
        title:'¿Está seguro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sí, activar'
    }).then((result)=>{
        if(result.isConfirmed){
            ajaxRequest("/service/activeService","POST",activeServicio)
            .done(function(response){
                if(response.status === 200){
                    $('#serviceTable').DataTable().ajax.reload();
                    Swal.fire({
                        icon: 'success',
                        title: response.mensaje,
                        confirmButtonText: "Aceptar",
                    });  
                }
                
            }).fail(function (xhr, status, error) {}).always(function () {});  
        }
    })
});
//validacion de formulario 
$(document).ready(function(){
    $("#saveService").validate({
        rules:{
            newDescripcion:{
                required: true,
            },
            newCosto:{
                required: true,
                number: true
            }
        }
    });
    $("#editService").validate({
        rules:{
            editDescripcion:{
                required: true,
            },
            editCosto:{
                required: true,
                number: true 
            }
        }
    });
});
</script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->endSection() ?>