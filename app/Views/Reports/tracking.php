<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>
<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
    <div class="col-lg-12">
        <div class="card h-100">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-0">Filtros</h6>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-12">
                        <label for="reportrange">Rango de fechas</label>
                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <label for="customer-list">Cliente</label>
                        <select id="customer-list" class="form-control customers-select">
                            <option value="-1">Todos</option>
                            <?php foreach ($customers as $cliente) : ?>
                                <option codigo="<?= $cliente->codigo_cliente ?>" value="<?= $cliente->id_cliente ?>"><?= $cliente->codigo_cliente ?> - <?= $cliente->nombre ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-2">

    </div>
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <?php $count = 1; ?>
                    <?php foreach ($departments as $departamento) : ?>
                        <?php if ($count == 1) : ?>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active text-secondary text-bold" id="element<?= $count ?>-tab" data-bs-toggle="tab" data-bs-target="#element<?= $count ?>" type="button" role="tab" aria-controls="element<?php $count ?>" aria-selected="true"><?= $departamento->departamento ?></button>
                            </li>
                        <?php else : ?>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link text-secondary text-bold" id="element<?= $count ?>-tab" data-bs-toggle="tab" data-bs-target="#element<?= $count ?>" type="button" role="tab" aria-controls="element<?php $count ?>" aria-selected="true"><?= $departamento->departamento ?></button>
                            </li>
                        <?php endif ?>
                        <?php $count++ ?>
                    <?php endforeach; ?>
                </ul>
                <div class="tab-content mt-4" id="myTabContent">

                    <?php $count = 1; ?>
                    <?php foreach ($departments as $departamento) : ?>
                        <?php if ($count == 1) : ?>
                            <div class="tab-pane fade show active" id="element<?= $count ?>" role="tabpanel" aria-labelledby="element<?php $count ?>-tab">
                                <div class="row">
                                    <div class="col-lg-5 col-md-12 mt-4 mt-lg-0">
                                        <div class="card h-100">
                                            <div class="card-header pb-0 p-3">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="mb-0">Operadores <?= $departamento->departamento ?></h6>
                                                    <button type="button" class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto" data-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Seleccione un empleado para visualizar sus datos">
                                                        <i class="material-icons text-sm">priority_high</i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body p-3">
                                                <div class="row">
                                                    <div class="col-lg-7 col-12 text-center">

                                                        <div id="chart-doughnut1" class="chart-canvas mt-1" style="height: 200px; width: 350px;" width="350" height="250"></div>

                                                    </div>
                                                    <div class="col-lg-5 col-12">
                                                        <div class="">
                                                            <table class="table align-items-center mb-0">
                                                                <tbody>
                                                                    <?php foreach (explode(';', $departamento->empleados) as $empleado) : ?>
                                                                        <?php $datos = explode('&', $empleado) ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div>
                                                                                        <img src="../../assets/img/small-logos/logo-xd.svg" class="avatar avatar-sm me-2" alt="logo_xd">
                                                                                    </div>
                                                                                    <div class="d-flex flex-column justify-content-center">
                                                                                        <h6 class="mb-0 text-sm text-uppercase"><?= $datos[1] ?></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <!-- <td class="align-middle text-center text-sm">
                                                                            <span class="text-xs font-weight-bold"> 25% </span>
                                                                        </td> -->
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-12 mt-4 mt-lg-0">
                                        <div class="card h-100">
                                            <div class="card-header pb-0 p-3">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="mb-0">Operadores <?= $departamento->departamento ?></h6>
                                                    <button type="button" class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto" data-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Seleccione un empleado para visualizar sus datos">
                                                        <i class="material-icons text-sm">priority_high</i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body p-3">
                                                <div class="row">
                                                    <div class="col-lg-5 col-12 text-center">
                                                        <div class="chart mt-5">
                                                            <canvas id="chart-doughnut" class="chart-canvas" style="display: block; box-sizing: border-box; height: 200px; width: 177.6px;" width="222" height="250"></canvas>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-7 col-12">
                                                        <div class="table-responsive">
                                                            <table class="table align-items-center mb-0">
                                                                <tbody>
                                                                    <?php foreach (explode(';', $departamento->empleados) as $empleado) : ?>
                                                                        <?php $datos = explode('&', $empleado) ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div>
                                                                                        <img src="../../assets/img/small-logos/logo-xd.svg" class="avatar avatar-sm me-2" alt="logo_xd">
                                                                                    </div>
                                                                                    <div class="d-flex flex-column justify-content-center">
                                                                                        <h6 class="mb-0 text-sm text-uppercase"><?= $datos[1] ?></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <!-- <td class="align-middle text-center text-sm">
                                                                            <span class="text-xs font-weight-bold"> 25% </span>
                                                                        </td> -->
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        <?php else : ?>
                            <div class="tab-pane fade" id="element<?= $count ?>" role="tabpanel" aria-labelledby="element<?php $count ?>-tab">
                                <div class="row">
                                    <div class="col-lg-5 col-md-12 mt-4 mt-lg-0">
                                        <div class="card h-100">
                                            <div class="card-header pb-0 p-3">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="mb-0">Operadores <?= $departamento->departamento ?></h6>
                                                    <button type="button" class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto" data-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Seleccione un empleado para visualizar sus datos">
                                                        <i class="material-icons text-sm">priority_high</i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body p-3">
                                                <div class="row">
                                                    <div class="col-lg-5 col-12 text-center">
                                                        <div class="chart mt-5">
                                                            <canvas id="chart-doughnut" class="chart-canvas" style="display: block; box-sizing: border-box; height: 200px; width: 177.6px;" width="222" height="250"></canvas>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-7 col-12">
                                                        <div class="table-responsive">
                                                            <table class="table align-items-center mb-0">
                                                                <tbody>
                                                                    <?php foreach (explode(';', $departamento->empleados) as $empleado) : ?>
                                                                        <?php $datos = explode('&', $empleado) ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div>
                                                                                        <img src="../../assets/img/small-logos/logo-xd.svg" class="avatar avatar-sm me-2" alt="logo_xd">
                                                                                    </div>
                                                                                    <div class="d-flex flex-column justify-content-center">
                                                                                        <h6 class="mb-0 text-sm text-uppercase"><?= $datos[1] ?></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <!-- <td class="align-middle text-center text-sm">
                                                                            <span class="text-xs font-weight-bold"> 25% </span>
                                                                        </td> -->
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-7 col-md-12 mt-4 mt-lg-0">
                                        <div class="card h-100">
                                            <div class="card-header pb-0 p-3">
                                                <div class="d-flex align-items-center">
                                                    <h6 class="mb-0">Operadores <?= $departamento->departamento ?></h6>
                                                    <button type="button" class="btn btn-icon-only btn-rounded btn-outline-secondary mb-0 ms-2 btn-sm d-flex align-items-center justify-content-center ms-auto" data-toggle="tooltip" data-bs-placement="bottom" title="" data-bs-original-title="Seleccione un empleado para visualizar sus datos">
                                                        <i class="material-icons text-sm">priority_high</i>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="card-body p-3">
                                                <div class="row">
                                                    <div class="col-lg-5 col-12 text-center">
                                                        <div class="chart mt-5">
                                                            <canvas id="chart-doughnut" class="chart-canvas" style="display: block; box-sizing: border-box; height: 200px; width: 177.6px;" width="222" height="250"></canvas>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-7 col-12">
                                                        <div class="table-responsive">
                                                            <table class="table align-items-center mb-0">
                                                                <tbody>
                                                                    <?php foreach (explode(';', $departamento->empleados) as $empleado) : ?>
                                                                        <?php $datos = explode('&', $empleado) ?>
                                                                        <tr>
                                                                            <td>
                                                                                <div class="d-flex px-2 py-1">
                                                                                    <div>
                                                                                        <img src="../../assets/img/small-logos/logo-xd.svg" class="avatar avatar-sm me-2" alt="logo_xd">
                                                                                    </div>
                                                                                    <div class="d-flex flex-column justify-content-center">
                                                                                        <h6 class="mb-0 text-sm text-uppercase"><?= $datos[1] ?></h6>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                            <!-- <td class="align-middle text-center text-sm">
                                                                            <span class="text-xs font-weight-bold"> 25% </span>
                                                                        </td> -->
                                                                        </tr>
                                                                    <?php endforeach; ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                </div>
                            </div>
                        <?php endif ?>
                        <?php $count++ ?>
                    <?php endforeach; ?>
                </div>

                <!-- <h5 class="font-weight-bolder">Puestos de trabajo</h5>
                <div class="accordion-1">
                    <div class="">
                        <div class="accordion" id="accordionRental">
                            <div class="accordion-item mb-3">
                                <h5 class="accordion-header" id="headingOne">
                                    <button class="accordion-button border-bottom font-weight-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        How do I order?
                                        <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                    </button>
                                </h5>
                                <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionRental" style="">
                                    <div class="accordion-body text-sm opacity-8">
                                        We’re not always in the position that we want to be at. We’re constantly growing. We’re constantly making mistakes. We’re constantly trying to express ourselves and actualize our dreams. If you have the opportunity to play this game
                                        of life you need to appreciate every moment. A lot of people don’t appreciate the moment until it’s passed.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mb-3">
                                <h5 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button border-bottom font-weight-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        How can i make the payment?
                                        <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                    </button>
                                </h5>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionRental">
                                    <div class="accordion-body text-sm opacity-8">
                                        It really matters and then like it really doesn’t matter. What matters is the people who are sparked by it. And the people who are like offended by it, it doesn’t matter. Because it's about motivating the doers. Because I’m here to follow my dreams and inspire other people to follow their dreams, too.
                                        <br>
                                        We’re not always in the position that we want to be at. We’re constantly growing. We’re constantly making mistakes. We’re constantly trying to express ourselves and actualize our dreams. If you have the opportunity to play this game of life you need to appreciate every moment. A lot of people don’t appreciate the moment until it’s passed.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mb-3">
                                <h5 class="accordion-header" id="headingThree">
                                    <button class="accordion-button border-bottom font-weight-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        How much time does it take to receive the order?
                                        <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                    </button>
                                </h5>
                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionRental">
                                    <div class="accordion-body text-sm opacity-8">
                                        The time is now for it to be okay to be great. People in this world shun people for being great. For being a bright color. For standing out. But the time is now to be okay to be the greatest you. Would you believe in what you believe in, if you were the only one who believed it?
                                        If everything I did failed - which it doesn't, it actually succeeds - just the fact that I'm willing to fail is an inspiration. People are so scared to lose that they don't even try. Like, one thing people can't say is that I'm not trying, and I'm not trying my hardest, and I'm not trying to do the best way I know how.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mb-3">
                                <h5 class="accordion-header" id="headingFour">
                                    <button class="accordion-button border-bottom font-weight-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                        Can I resell the products?
                                        <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                    </button>
                                </h5>
                                <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionRental">
                                    <div class="accordion-body text-sm opacity-8">
                                        I always felt like I could do anything. That’s the main thing people are controlled by! Thoughts- their perception of themselves! They're slowed down by their perception of themselves. If you're taught you can’t do anything, you won’t do anything. I was taught I could do everything.
                                        <br><br>
                                        If everything I did failed - which it doesn't, it actually succeeds - just the fact that I'm willing to fail is an inspiration. People are so scared to lose that they don't even try. Like, one thing people can't say is that I'm not trying, and I'm not trying my hardest, and I'm not trying to do the best way I know how.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item mb-3">
                                <h5 class="accordion-header" id="headingFifth">
                                    <button class="accordion-button border-bottom font-weight-bold" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFifth" aria-expanded="false" aria-controls="collapseFifth">
                                        Where do I find the shipping details?
                                        <i class="collapse-close fa fa-plus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                        <i class="collapse-open fa fa-minus text-xs pt-1 position-absolute end-0 me-3" aria-hidden="true"></i>
                                    </button>
                                </h5>
                                <div id="collapseFifth" class="accordion-collapse collapse" aria-labelledby="headingFifth" data-bs-parent="#accordionRental">
                                    <div class="accordion-body text-sm opacity-8">
                                        There’s nothing I really wanted to do in life that I wasn’t able to get good at. That’s my skill. I’m not really specifically talented at anything except for the ability to learn. That’s what I do. That’s what I’m here for. Don’t be afraid to be wrong because you can’t learn anything from a compliment.
                                        I always felt like I could do anything. That’s the main thing people are controlled by! Thoughts- their perception of themselves! They're slowed down by their perception of themselves. If you're taught you can’t do anything, you won’t do anything. I was taught I could do everything.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div> -->
            </div>
        </div>
    </div>


</div>
<?= $this->endSection() ?>

<?= $this->section('scripts') ?>
<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript" src="../material-template/assets/js/plugins/moment/moment.min.js"></script>
<script type="text/javascript" src="../material-template/assets/js/plugins/daterange/daterange.min.js"></script>
<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->include('Scripts/Reports/RenderDoughnut') ?>
<?= $this->include('Scripts/Reports/DateRange') ?>
<script>
    $(document).ready(function() {
        $("#customer-list").select2();
    });

    function RenderGraphics(Params) {
        ajaxRequest('/reports/getJobByDepartmentData', 'POST', Params)
            .done(function(response) {
                fillGraphics(response.jobs);
            });
    }

    function fillGraphics(data) {
        var dataValuesDoughnut = [];
        var totalTerminados = 0;
        var totalAtrasados = 0;
        var totalInconclusos = 0;
        var totalReprocesos = 0;
        $.map(data, function(obj) {
            switch (obj.estado_pedido) {
                case '1':
                    totalAtrasados++;
                    break;
                case '2':
                    totalTerminados++;
                    break;
                case '3':
                    totalReprocesos++;
                    break;
                case '4':
                    totalInconclusos++;
                    break;
            }
        });
        dataValuesDoughnut.push({
            value: totalTerminados,
            name: "Terminados"
        }, {
            value: totalAtrasados,
            name: "Atrasados"
        }, {
            value: totalReprocesos,
            name: "Reprocesos"
        }, {
            value: totalInconclusos,
            name: "Inconclusos"
        });
        renderGraphicDoughnut('chart-doughnut1', dataValuesDoughnut);
    }
</script>
<?= $this->endSection() ?>
