<?= $this->extend('Layout/base') ?>
<?= $this->section('styles') ?>
<link href="../material-template/assets/css/plugins/select2/select2.min.css" rel="stylesheet" />
<link href="../material-template/assets/css/plugins/select2/select2.material.css" rel="stylesheet" />
<link href="../material-template/assets/js/plugins/dropify/css/dropify.min.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
<div class="row">
<div class="col-lg-12">
        <div class="card h-100">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-0">Filtros</h6>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="row">
                    <div class="col-12">
                        <label for="reportrange">Rango de fechas</label>
                        <div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
                            <i class="fa fa-calendar"></i>&nbsp;
                            <span></span> <i class="fa fa-caret-down"></i>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    <div class="col-12">
                        <label for="customer-list">Cliente</label>
                        <select id="customer-list" class="form-control customers-select">
                            <option value="-1">Todos</option>
                            <?php foreach ($customers as $cliente) : ?>
                                <option codigo="<?= $cliente->codigo_cliente ?>" value="<?= $cliente->id_cliente ?>"><?= $cliente->codigo_cliente ?> - <?= $cliente->nombre ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row mt-4">
        <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Total de entregas</h5>
                            <span class="h2 font-weight-bold mb-0" id="total-entregas">0</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-lg icon-shape bg-gradient-dark shadow-dark shadow text-center border-radius-xl mt-n4 position-absolute">
                                <i class="material-icons opacity-10">assessment</i>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer text-center p-0">
                        <a class="mb-0 btn text-dark btn-link source-option" query="">
                            <span class="material-icons">
                                visibility
                            </span> Ver Entregas
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-sm-0 mt-4">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Entregas a tiempo</h5>
                            <span class="h2 font-weight-bold mb-0" id="entregas-puntuales">0</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-lg icon-shape bg-gradient-success shadow-success  shadow text-center border-radius-xl mt-n4 position-absolute">
                                <i class="material-icons opacity-10">check</i>
                            </div>
                        </div>
                    </div>
                    <hr class="dark horizontal my-0">
                    <div class="card-footer text-center p-0">
                        <a class="mb-0 btn text-dark btn-link source-option" query="0">
                            <span class="material-icons">
                                visibility
                            </span> Ver Entregas
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-lg-0 mt-4">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Entregas atrasadas</h5>
                            <span class="h2 font-weight-bold mb-0" id="entregas-atrasadas">0</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-lg icon-shape bg-gradient-warning shadow-warning  text-center border-radius-xl mt-n4 position-absolute">
                                <i class="material-icons opacity-10">highlight_off</i>
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal my-0 dark">
                    <div class="card-footer text-center p-0">
                        <a class="mb-0 btn text-dark btn-link source-option" query="1">
                            <span class="material-icons">
                                visibility
                            </span> Ver Entregas
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-6 mt-lg-0 mt-4">
            <div class="card card-stats mb-4 mb-xl-0">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title text-uppercase text-muted mb-0">Reprocesos</h5>
                            <span class="h2 font-weight-bold mb-0" id="reprocesos">0</span>
                        </div>
                        <div class="col-auto">
                            <div class="icon icon-lg icon-shape bg-gradient-primary shadow-primary text-center border-radius-xl mt-n4 position-absolute">
                                <i class="material-icons opacity-10">next_week</i>
                            </div>
                        </div>
                    </div>
                    <hr class="horizontal my-0 dark">
                    <div class="card-footer text-center p-0">
                        <a class="mb-0 btn text-dark btn-link source-option" query="2">
                            <span class="material-icons">
                                visibility
                            </span> Ver Entregas
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-lg-8 mt-2">
        <div class="card h-100">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-0">Productividad por cliente</h6>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="chart">
                    <div id="customer-compliance-bar" style="height: 450px;">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 mt-2">
        <div class="card h-100">
            <div class="card-header pb-0 p-3">
                <div class="d-flex justify-content-between">
                    <h6 class="mb-0">Trabajos Desarrollados</h6>
                </div>
            </div>
            <div class="card-body p-3">
                <div class="chart">
                    <div id="customer-compliance-pie" style="height: 450px;">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->endSection() ?>
<?= $this->section('modal') ?>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title font-weight-normal" id="exampleModalLabel">Total de entregas</h5>
                <button type="button" class="close text-dark" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 col-12">
                        <table class="table table-bordered table-responsive" style="width: 100%;" id="data-compliance">
                            <thead>
                                <tr>
                                    <th>Proceso</th>
                                    <th>Nombre</th>
                                    <th>Fecha Ingreso</th>
                                    <th>Fecha Estimada</th>
                                    <th>Fecha Real</th>
                                    <th>Atrasado</th>
                                </tr>
                            </thead>
                            <tbody id="tabla-cumplimiento">

                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('scripts') ?>
<script src="../material-template/assets/js/plugins/select2/select2.min.js"></script>
<script src="../material-template/assets/js/plugins/echarts/echarts.min.js"></script>
<script type="text/javascript" src="../material-template/assets/js/plugins/moment/moment.min.js"></script>
<script type="text/javascript" src="../material-template/assets/js/plugins/daterange/daterange.min.js"></script>

<?= $this->include('Scripts/AjaxRequest') ?>
<?= $this->include('Scripts/Reports/DateRange') ?>
<?= $this->include('Scripts/Reports/RenderBar') ?>
<?= $this->include('Scripts/Reports/RenderPie') ?>
<script>
    $(document).ready(function() {
        if (document.getElementById('customer-list')) {
           $("#customer-list").select2();
           
        }
    });

    var complianceTable;

    function RenderGraphics(Params) {
        ajaxRequest('/reports/getComplianceData', 'POST', Params)
            .done(function(response) {
                fillGraphics(response.compliance);
                fillTable(response.complianceDetails);
            });
    }

    function fillGraphics(data) {
        var dataNamesBar = [];
        var dataValuesBar = [];
        var dataValuesPie = [];
        var totalPuntuales = 0;
        var totalRestrados = 0;
        $.map(data, function(obj) {
            dataNamesBar.push(obj.codigo_cliente);
            dataValuesBar.push({
                value: ((obj.pedidos_enTiempo / obj.num_pedidos) * 100).toFixed(2),
                itemStyle: {
                    color: getRandomColor()
                }
            });
            totalPuntuales += parseInt(obj.pedidos_enTiempo);
            totalRestrados += parseInt(obj.pedidos_atrasados);
        });
        dataValuesPie.push({
            value: totalPuntuales,
            name: "Puntuales"
        });
        dataValuesPie.push({
            value: totalRestrados,
            name: "Atrasados"
        });
        $("#entregas-puntuales").empty().text(totalPuntuales);
        $("#entregas-atrasadas").empty().text(totalRestrados);
        $("#total-entregas").empty().text(totalPuntuales + totalRestrados);
        renderGraphicBar('customer-compliance-bar', dataNamesBar, dataValuesBar);
        renderGraphicPie('customer-compliance-pie', dataValuesPie);
    }

    function fillTable(data) {
        var content = "";
        $.map(data, function(obj) {
            content += "<tr>";
            content += "<td>" + obj.proceso + "</td>";
            content += "<td>" + obj.nombre_poceso + "</td>";
            content += "<td>" + obj.fecha_entrada + "</td>";
            content += "<td>" + obj.fecha_entrega + "</td>";
            content += "<td>" + obj.fecha_reale + "</td>";
            content += "</tr>";
        });
        $("#tabla-cumplimiento").empty().append(content);
        complianceTable = $('#data-compliance').DataTable({
            "bDestroy": true,
            "data": data,
            "columns": [{
                    "data": "proceso", render: function (data, type, row) {
                    buttons = `<a href="/order/details/${row.id_pedido}" target="_blank">${row.codigo_cliente}${row.proceso}</a>`;
                    return buttons;
                    }
                },
                {
                    "data": "nombre_proceso"
                },
                {
                    "data": "fecha_entrada"
                },
                {
                    "data": "fecha_entrega"
                },
                {
                    "data": "fecha_reale"
                },
                {
                    "data": "atrasado"
                },
            ],
            "columnDefs": [{
                "targets": [5],
                "visible": false
            }]
        });
    }

    $(document).on('click', '.source-option', function() {
        var filter = $(this).attr('query');
        complianceTable
            .columns(5)
            .search(filter)
            .draw();
        $("#exampleModal").modal("show");
    });

    function getRandomColor() {
        var letters = '0123456789ABCDEF'.split('');
        var color = '#';
        for (var i = 0; i < 6; i++) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }
</script>
<?= $this->endSection() ?>