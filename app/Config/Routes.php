<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index',['filter' => 'authGuard']);
$routes->get('/login','Authentication::login');
$routes->get('/logout','Authentication::logout');
//$routes->get('/anilox/aniloxes', 'Anilox::aniloxes',['filter' => 'authGuard']);
//$routes->get('/contact/contacts', 'Contact::contacts',['filter' => 'authGuard']);
//$routes->get('/country/countriesList', 'Country::countriesList',['filter' => 'authGuard']);
//$routes->get('/customer/customers', 'Customer::customers',['filter' => 'authGuard']);
//$routes->get('/loads/deliveryBoard', 'Loads::deliveryBoard',['filter' => 'authGuard']);
//$routes->get('/machine/machines', 'Machine::machines',['filter' => 'authGuard']);
//$routes->get('/material/materialsList', 'Material::materialsList',['filter' => 'authGuard']);
//$routes->get('/order/assignments', 'Order::assignments',['filter' => 'authGuard']);
//$routes->get('/orderManager/processes', 'OrderManager::processes',['filter' => 'authGuard']);
//$routes->get('/planner/jobs', 'Planner::jobs',['filter' => 'authGuard']);
//$routes->get('/planner/installments', 'Planner::installments',['filter' => 'authGuard']);
//$routes->get('/plate/plates', 'Plate::plates',['filter' => 'authGuard']);
$routes->put('products/1', 'Product::feature');

$routes->get('defaultRoute/editDefaultRoute/(:rutaId)', 'DefaultRoute::DefaultRouteById/$1');


/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
