<?php

namespace App\Controllers;
use App\Repositories\LoadRepository;

class Loads extends BaseController
{
    protected $loadRepository;
    function __construct()
    {
        $this->loadRepository = new LoadRepository();
    }
    
    public function deliveryBoard() //tablero de entregas
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('Loads/deliveryBoard');
    }

    
}
