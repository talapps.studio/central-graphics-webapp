<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class UserResourceController extends ResourceController
{
    use ResponseTrait;

    /**
     * @var \App\Repositories\UserRepository
     */

    private $repository;
    private $mailService;

    public function __construct()
    {
        $this->repository = new \App\Repositories\UserRepository();
        $this->mailService = new \App\Repositories\EmailService();
    }

    public function index()
    {
        $status = $this->request->getVar('status');
        $data = $this->repository->findByStatus($status);
        return $this->respond($data);
    }

    public function show($id = null)
    {
        return $this->respond($this->repository->findAllUserInfo($id));
    }

    public function create()
    {
        $session = session();
        try {
            $randomPassword = $this->repository->generateRandomPassword(8);
            $encryptedPassword = password_hash($randomPassword, PASSWORD_DEFAULT);
            $data = [
                'usuario' => $this->request->getVar('usuario'),
                'contrasena' => $encryptedPassword,
                'nombre' => $this->request->getVar('nombre'),
                'cod_empleado' => $this->request->getVar('codigo'),
                'puesto' => $this->request->getVar('puesto'),
                'email' => $this->request->getVar('email'),
                'id_dpto' => $this->request->getVar('departamento'),
                'id_grupo' => $this->request->getVar('grupo'),
                'hora' => $this->request->getVar('hora'),
                'activo' => $this->request->getVar('activo') ? 's' : 'n',
                'usu_prog' => $this->request->getVar('usu_prog') ? 's' : 'n',
                'preferencias' => '',
                'upais' => $this->request->getVar('pais'),
                'created_by' => $session->get('id_usuario'),
            ];
            $this->repository->create($data);
            $response = [
                'status' => 201,
                'error' => null,
                'messages' => [
                    'success' => 'Usuario creada correctamente.'
                ]
            ];
            $this->mailService->sendNewUserEmail($randomPassword, $data['email']);
            return $this->respondCreated($response);
        } catch (\Exception $e) {
            return $this->failServerError('Error al crear usuario.', 500);
        }
    }

    public function update($id = null)
    {
        $session = session();
        try {
            $data = [
                'usuario' => $this->request->getVar('usuario'),
                'nombre' => $this->request->getVar('nombre'),
                'cod_empleado' => $this->request->getVar('codigo'),
                'puesto' => $this->request->getVar('puesto'),
                'email' => $this->request->getVar('email'),
                'id_dpto' => $this->request->getVar('departamento'),
                'id_grupo' => $this->request->getVar('grupo'),
                'hora' => $this->request->getVar('hora'),
                'activo' => $this->request->getVar('activo') ? 's' : 'n',
                'usu_prog' => $this->request->getVar('usu_prog') ? 's' : 'n',
                'upais' => $this->request->getVar('pais'),
                'updated_by' => $session->get('id_usuario'),
            ];
            $this->repository->update($data, $id);
            $response = [
                'status' => 200,
                'error' => null,
                'messages' => [
                    'success' => 'Usuario actualizado correctamente.'
                ]
            ];
            return $this->respondCreated($response);
        } catch (\Exception $e) {
            return $this->failServerError('Error al actualizar usuario.', 500);
        }
    }

    public function delete($id = null)
    {
        try {
            $session = session();
            if ($this->repository->find($id)) {
                $data = [
                    'deleted_by' => $session->get('id_usuario')
                ];
                $this->repository->update($data, $id);
                $this->repository->destroy($id);
                $response = [
                    'status' => 200,
                    'error' => null,
                    'messages' => [
                        'success' => 'Usuario eliminado correctamente.'
                    ]
                ];
                return $this->respondCreated($response);
            } else {
                return $this->failNotFound('Usuario no encontrado.', 404);
            }
        } catch (\Exception $e) {
            return $this->failServerError('Error al eliminar usuario.', 500);
        }
    }

    public function isUniqueUsername($formType = null)
    {
        $id = $this->request->getVar('id');
        $username = $this->request->getVar('user');
        return $this->respond($this->repository->uniqueUsername($id, $username, $formType));
    }

    public function isUniqueEmail($formType = null)
    {
        $id = $this->request->getVar('id');
        $email = $this->request->getVar('email');
        return $this->respond($this->repository->uniqueEmail($id, $email, $formType));
    }

}