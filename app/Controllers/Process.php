<?php

namespace App\Controllers;

use App\Repositories\CustomerRepository;
use App\Repositories\DefaultRouteRepository;
use App\Repositories\JobTypeRepository;
use App\Repositories\ProcessRepository;
use App\Repositories\OrderRepository;

class Process extends BaseController
{
    protected $customerRepository;
    protected $jobTypeRepository;
    protected $proccessRepository;
    protected $defaultRouteRepository;
    protected $orderRepository;
    function __construct()
    {
        $this->customerRepository = new CustomerRepository();
        $this->jobTypeRepository = new JobTypeRepository();
        $this->proccessRepository = new ProcessRepository();
        $this->defaultRouteRepository = new DefaultRouteRepository();
        $this->orderRepository = new OrderRepository();
    }

    public function create()
    {
        /* s = activo
        *  n = inactivo
        */
        $data['clientes'] = $this->customerRepository->getActiveOrInactiveCustomers('s');
        $data['tiposTrabajo'] = $this->jobTypeRepository->getAll();
        $session = session();
        if (!$session->get('isLoggedIn')) {
            return redirect()->route('login');
        }
        return view('Process/create', $data);
    }

    public function list()
    {
        $data['clientes'] = $this->proccessRepository->getCustomerAndProcess('s');
        $session = session();
        if (!$session->get('isLoggedIn')) {
            return redirect()->route('login');
        }
        $data['titulo'] = "Listado de procesos";
        return view('Process/list', $data);
    }

    //Retorna un JSON con el ultimo correlativo de procesos de un cliente
    public function getProcessCode()
    {
        $id_cliente = $this->request->getVar('idCliente');
        $result = $this->proccessRepository->getLastProcessByCustomer($id_cliente);
        header('Content-Type: application/json');
        echo json_encode($result);
    }

    //Retorna un JSON con los procesos de un determinado cliente
    public function getProcessByCustomer()
    {
        $id_cliente = $this->request->getVar('idCliente');
        $result = $this->proccessRepository->getProcessByCustomer(); //En texto por funcion no construida
        echo json_encode($result);
    }

    public function processCustomerList($id)
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $idCustomer = $id;
        $data['cliente'] = $this->customerRepository->getCustomerById($idCustomer);
        $data['id'] = $id;
        return view('Process/listByCustomer', $data);
    }

    public function getProcessCustomerById()
    {
        $id_cliente = $this->request->getVar('idCliente');

        $data['data'] = $this->proccessRepository->ProcessByCustomer($id_cliente);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function editProcessCustomer()
    {
        try {
            $id = $this->request->getVar('idProceso');
            $data = [
                'proceso' => $this->request->getVar('proceso'),
                'nombre' => $this->request->getVar('nombre')
            ];
            $this->proccessRepository->updateProcess($id, $data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Proceso actualizado correctamente'
            ];
            return json_encode($respuesta);
        } catch (\Exception $e) {
            $respuesta = [
                'status' => 500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizado'
            ];
            return json_encode($respuesta);
        }
    }
    public function addOrderByProces($idCliente,$idProceso)
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $data['tiposTrabajo'] = $this->jobTypeRepository->getAll();
        $data['rutas'] = $this->defaultRouteRepository->getAll();
        $data['infoProceso'] = $this->proccessRepository->getNewOderByProcess($idProceso);
        $data['idProceso'] = $idProceso;
        $data['idCliente'] = $idCliente;
        
        return view('Process/newOrder', $data);
    }
    public function getAllUserByDepartament(){
        
        $data['data'] = $this->orderRepository->getUserByDepartament($this->request->getVar('idDepartamento'));
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function inactiveProcessCustomer()
    {
        $id = $this->request->getVar('idProceso');
        $data = [
            'activo' => 0
        ];
        $data = $this->proccessRepository->updateProcess($id, $data);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Proceso inactivado correctamente'
        ];
        return json_encode($respuesta);
    }
    public function activeProcessCustomer()
    {
        $id = $this->request->getVar('idProceso');
        $data = [
            'activo' => 1
        ];
        $data = $this->proccessRepository->updateProcess($id, $data);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Proceso activado correctamente'
        ];
        return json_encode($respuesta);
    }

    public function addNewProcess()
    {
        try {
            $data = [
                'id_cliente' => $this->request->getVar('id_cliente'),
                'proceso' => $this->request->getVar('proceso'),
                'nombre' => $this->request->getVar('nombre'),
                'activo' => 1
            ];
            $this->proccessRepository->createProcess($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Proceso agregado correctamente',
            ];
            return json_encode($respuesta);
        } catch (\Exception $e) {
            $respuesta = [
                'status' => 500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
            return json_encode($respuesta);
        }
    }
}
