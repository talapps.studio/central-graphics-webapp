<?php
namespace App\Controllers;
use App\Repositories\DefaultRouteRepository;
use App\Repositories\RouteDetailRepository;
use App\Repositories\DefaultRouteDetailRepository;

class DefaultRoute extends BaseController
{
    protected $defaultRouteRepository;
    protected $routeDetailRepository;
    protected $defaultRouteDetailRepository;
    function __construct()
    {
        $this->defaultRouteRepository = new DefaultRouteRepository();
        $this->routeDetailRepository = new RouteDetailRepository();
        $this->defaultRouteDetailRespository = new DefaultRouteDetailRepository();
    }
    
    public function defaultRouteList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $data['elementos'] = $this->routeDetailRepository->getAllRouteDetail();
      return view('RouteManager/defaultRouteList',$data);  
    }
    public function defaulRouteListDetail(){
        $data['elementos'] = $this->routeDetailRepository->getRouteDetail();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function getAllDefaultRoute()
    {
        $data['data'] = $this->defaultRouteRepository->getDefaultRoute();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewDefaultRoute(){
        try{
            $ruta = [
                'nombre' => $this->request->getVar('nombre'),
            ];
            $elementosRutas = $this->request->getVar('elementos');
            
            $idRuta = $this->defaultRouteRepository->add($ruta);

            foreach ($elementosRutas as $elementoRuta){
                $defaultRouteDetail = [
                    'id_ruta_pred' =>$idRuta,
                    'id_ruta_detalle' => $elementoRuta['id'],
                ];
                $this->defaultRouteDetailRespository->addNewDefaultRouteDetail($defaultRouteDetail);
        
            }
            $respuesta = [
                'status'=> 200, 
                'mensaje'=> 'Ruta agregada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error ',
            ];
            return json_encode($respuesta);
           
        }
        
    }
    public function editDefaultRoute($rutaId){
        try{
            
            $idRutaPredeterminada = $rutaId;
            $rutasAsociadas = $this->defaultRouteRepository->routesByElement($idRutaPredeterminada);
            $ruta = [
                'nombre' => $this->request->getVar('nombre'),
            ];
         
            $this->defaultRouteRepository->update($idRutaPredeterminada, $ruta);
            $elementosRutas = $this->request->getVar('elementos');
            $arrayRutaAsociada = json_decode(json_encode($rutasAsociadas), true);
            if(count($arrayRutaAsociada)>0){
                foreach ($arrayRutaAsociada as $rutaAsociada){
                    $this->defaultRouteDetailRespository->deleteDefaultRouteDetail($rutaAsociada['id_ruta_predeterminada_detalle']);
               }
            }
            foreach ($elementosRutas as $elementoRuta){
                $defaultRouteDetail = [
                    'id_ruta_pred' =>$idRutaPredeterminada,
                    'id_ruta_detalle' => $elementoRuta['id'],
                ];
                $this->defaultRouteDetailRespository->addNewDefaultRouteDetail($defaultRouteDetail); 
            }
            $respuesta = [
                'status'=> 200, 
                'mensaje'=> 'Ruta actualizada correctamente',
            ];
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al actualizar Ruta: '.' '.$e->getMessage(),
            ];
        }
        
        return json_encode($respuesta);
    }
    public function deleteDefaultRoute(){
        try{
            $id = $this->request->getVar('idDefaultRoute');
            $rp = $this->defaultRouteRepository->delete($id);
            $respuesta = [
                'status'=>200,
                'mensaje'=>'Ruta fue desactivado exitosamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al desactivar Ruta: '.' '.$e->getMessage(),
            ];
            return json_encode($respuesta);
        }
        
    }
    public function inactiveDefaultRoute(){
        try{
            $idDefaultRoute = $this->request->getVar('idDefaultRoute');
            $defaultRoute = [
                'activo'=> 0
            ];
            $defaultRoute = $this->defaultRouteRepository->update($defaultRoute,$idDefaultRoute);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Ruta desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al desactivar Ruta: '.' '.$e->getMessage(),
            ];
            return json_encode($respuesta);
        }
    }
    public function activeDefaultRoute(){
        try
        {
            $idDefaultRoute = $this->request->getVar('idDefaultRoute');
            $defaultRoute = [
                'activo'=> 1
            ];
            $defaultRoute = $this->defaultRouteRepository->update($defaultRoute,$idDefaultRoute);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Ruta activada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al desactivar Ruta: '.' '.$e->getMessage(),
            ];
            return json_encode($respuesta);
        }
    }
    public function getRouteByElement(){
        $idRouteElem = $this->request->getVar('idRutaPedido');
        $data['data'] =$this->defaultRouteRepository->routesByElement($idRouteElem);
        header('Content-Type: application/json');
        echo json_encode($data);
    }
}