<?php
namespace App\Controllers;

use App\Repositories\RouteDetailRepository;
use App\Repositories\DepartmentRepository;

class RouteDetail extends BaseController
{
    protected $routeDetailRepository;
    protected $departmentRepository;
    function __construct()
    {
        $this->routeDetailRepository = new RouteDetailRepository();
        $this->departmentRepository = new DepartmentRepository;
    }
    public function routeDetailList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $data['departamentos'] = $this->departmentRepository->getDepartment();
        return view('RouteManager/routeDetailList',$data);
    }
    public function getAllRouteDetail()
    {
        $data['data'] = $this->routeDetailRepository->getRouteDetail();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewRouteDetail()
    { 
         try{
        $data = [
            'nombre' => $this->request->getVar('nombre'),
            'descripcion' => $this->request->getVar('descripcion'),
            'id_dpto' => $this->request->getVar('departamento')
        ];
        $this->routeDetailRepository->create($data);

        $respuesta = [
            'status' => 200,
            'mensaje' => 'Elemento de ruta agregado correctamente',
        ];
        return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al ingresar elemento'
            ];
            return json_encode($respuesta);
           
        }
        
    }
    public function editRouteDetail(){
        try{
        $id = $this->request->getVar('idElement');
        $data = [
            'nombre' => $this->request->getVar('nombre'),
            'descripcion' => $this->request->getVar('descripcion'),
            'id_dpto' => $this->request->getVar('departamento')
        ];
        $this->routeDetailRepository->update($data, $id);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Elemento de ruta actualizado correctamente',
        ];
        return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizar elemento'
            ];
            return json_encode($respuesta);
           
        }
        
    }
    public function inactiveRouteDetail()
    {
        try{
            $idRoute = $this->request->getVar('idElement');
            $route = [
                'activo' => 0
            ];
            $route = $this->routeDetailRepository->update($route,$idRoute);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Elemento desactivado correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);
           
        }
        
    }
    public function activeRouteDetail()
    {
        try{
            $idRoute = $this->request->getVar('idElement');
            $route = [
                'activo' => 1
            ];
            $route = $this->routeDetailRepository->update($route,$idRoute);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Elemento activado correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al activar elemento'
            ];
            return json_encode($respuesta);
           
        }
    }
}