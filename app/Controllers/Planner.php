<?php

namespace App\Controllers;
use App\Repositories\PlannerRepository;

class Planner extends BaseController
{
    
    public function jobs() //puestos de trabajo
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('Planner/jobs');
    }

    public function installments() //tablero de entregas
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $repository = new PlannerRepository();
        $data['materiales'] = $repository->getMaterials('s'); //materiales
        $trabajos = $repository->getInstallments(); //entregas
        $data['trabajos'] = $trabajos;
        $data['grupos'] = array_unique(array_column($trabajos, 'id_material_solicitado_grupo'));
        return view('Planner/installments',$data);
    }
}
