<?php
namespace App\Controllers;

use App\Repositories\DefaultProductRepository;

class DefaultProduct extends BaseController
{
    protected $defaultProductRepository;
    function __construct()
    {
        $this->defaultProductRepository = new DefaultProductRepository();
    }
    public function defaultProductList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('DefaultProduct/productList');
    }
    public function getAllDefaultProducts()
    {
        $data['data'] = $this->defaultProductRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewDefaultProduct()
    {
        $data = [
            'descripcion' => $this->request->getVar('descripcion'),
        ];
        $this->defaultProductRepository->create($data);

        $respuesta = [
            'status' => 200,
            'mensaje' => 'Producto agregado correctamente',
        ];
        return json_encode($respuesta);
    }

    

    public function editDefaultProduct(){
        $id = $this->request->getVar('idProductPre');
        $data = [
            'descripcion' => $this->request->getVar('descripcion'),
        ];
        $this->defaultProductRepository->update($data, $id);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Producto actualizado correctamente',
        ];
        return json_encode($respuesta);
    }
    public function inactiveProduct()
    {
        $idPro = $this->request->getVar('idProductPre');
        $prod = [
            'activo' => 0
        ];
        $prod = $this->defaultProductRepository->update($prod,$idPro);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Producto desactivado correctamente'
        ];
        return json_encode($respuesta);
    }
    public function activeProduct()
    {
        $idPro = $this->request->getVar('idProductPre');
        $prod = [
            'activo' => 1
        ];
        $prod = $this->defaultProductRepository->update($prod, $idPro);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Producto activado correctamente'
        ];
        return json_encode($respuesta);
    }
}