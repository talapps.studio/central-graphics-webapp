<?php
namespace App\Controllers;
use App\Repositories\MaterialRepository;
use App\Repositories\StockSupplierRepository;
use App\Repositories\MaterialStockTypeRepository;
use App\Repositories\StockTypeRepository;
use App\Repositories\CountryRepository;
use App\Repositories\StockEquipmentRepository;
use App\Repositories\SupplierMaterialStockRepository;



class Material extends BaseController 
{
    protected $materialRepository;
    protected $stockSupplierRepository;
    protected $materialStockTypeRepository;
    protected $stockTypeRepository;
    protected $countryRepository;
    protected $stockEquipmentRepository;
    protected $supplierMaterialStockRepository;


    function __construct()
    {
        $this-> materialRepository = new MaterialRepository();
        $this-> stockSupplierRepository = new StockSupplierRepository();
        $this-> materialStockTypeRepository = new materialStockTypeRepository();
        $this-> stockTypeRepository = new StockTypeRepository();
        $this-> countryRepository = new CountryRepository();
        $this-> stockEquipmentRepository = new StockEquipmentRepository();
        $this-> supplierMaterialStockRepository = new SupplierMaterialStockRepository();

    }

    public function materialsList()
    {
        $data['proveedores'] = $this->stockSupplierRepository ->getAll();
        $data['tipos'] = $this->stockTypeRepository->getStocksType();
        $data['materiales'] = $this->materialStockTypeRepository->getStocksTypeMaterials();
        $data['paises']= $this->countryRepository->getCountries();
        $data['areas']= $this->stockEquipmentRepository->getStockEquipment();

        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //Mostrar listado de materiales
        return view ('Materials/materialsList', $data);
    }
    public function getAllMaterials(){
        $data['data'] = $this->supplierMaterialStockRepository->getMaterialSuppliersStock();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewMaterial(){
        $material = [
            'codigo_sap' => $this->request->getVar('codigo'),
            'cantidad_unidad' => $this->request->getVar('cantUni'),
            'tipo' => $this->request->getVar('tipo'),
            'mp_mt' => $this->request->getVar('materiales'),
            'mat_pais' => $this->request->getVar('pais'),
            'valor' => $this->request->getVar('valorUni'),
            'id_inventario_equipo' => $this->request->getVar('area'),
            'numero_cajas' => $this->request->getVar('numCajas'),
            'numero_individual' => $this->request->getVar('numIndi'),
            'nombre_material' => $this->request->getVar('nombre'),
            'observacion' => $this->request->getVar('observacion')

        ];
        $idMaterial = $this->materialRepository->addMaterial($material);
        $inventariosMaterialesProv = $this->request->getVar('proveedor');
        $supplierMaterial = [
            'id_inventario_material'=>$idMaterial,
            'id_inventario_proveedor'=>$inventariosMaterialesProv
          ];
        $this->supplierMaterialStockRepository->addMaterialSupplierStock($supplierMaterial);
        
       
        $respuesta = [
            'status'=> 200, 
            'mensaje'=> 'Material agregado correctamente',
            
        ];
        return json_encode($respuesta);
    }
    public function editMaterial(){
        $id = $this->request->getVar('idMaterial');
        $idMatProv = $this->request->getVar('idInvenarioMatProv');
        $data = [
            'codigo_sap' => $this->request->getVar('codigo'),
            'cantidad_unidad' => $this->request->getVar('cantUni'),
            'tipo' => $this->request->getVar('tipo'),
            'mp_mt' => $this->request->getVar('materiales'),
            'numero_individual' => $this->request->getVar('numIndi'),
            'mat_pais' => $this->request->getVar('pais'),
            'valor' => $this->request->getVar('valorUni'),
            'id_inventario_equipo' => $this->request->getVar('area'),
            'numero_cajas' => $this->request->getVar('numCajas'),
            'nombre_material' => $this->request->getVar('nombre'),
            'observacion' => $this->request->getVar('observacion')
        ];
        $this->materialRepository->updateMaterial($id,$data);
        $inventariosMaterialesProv = $this->request->getVar('proveedor');

        $supplierMaterial = [

            'id_inventario_material' => $id,
            'id_inventario_proveedor' =>$inventariosMaterialesProv
         ];
        $this->supplierMaterialStockRepository->updateMaterialSupplierStock($idMatProv,$supplierMaterial);
        $respuesta = [
            'status'=> 200, 
            'mensaje'=> 'Material actualizado correctamente',
            
        ];
        return json_encode($respuesta);
    }
    public function getAllCodMaterials(){
        $codigo = $this->request->getVar('codigoMaterial');
        $proveedor = $this->request->getVar('proveedor');
        $equipo = $this->request->getVar('equipo');
        $existencias = $this->request->getVar('existencias');

    
          $data = $this->supplierMaterialStockRepository->getAllMp_Mt($codigo,$proveedor,$equipo,$existencias);

       return json_encode($data);
    }
    public function inactiveMaterial()
    {
        $idMat = $this->request->getVar('idMaterial');
        $material = [
            'activo' => 0
        ];
        $material = $this->materialRepository->updateMaterial($idMat,$material);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Material desactivado correctamente'
        ];
        return json_encode($respuesta);
    }
    public function activeMaterial()
    {
        $idMat = $this->request->getVar('idMaterial');
        $material = [
            'activo' => 1
        ];
        $material = $this->materialRepository->updateMaterial($idMat,$material);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Material activado correctamente'
        ];
        return json_encode($respuesta);
    }
}


