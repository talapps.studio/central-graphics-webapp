<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementIronerRepository;

class IronMeasurementIroner extends BaseController
{

    private $ironMeasurementIronerRepository;

    public function __construct()
    {
        $this->ironMeasurementIronerRepository = new IronMeasurementIronerRepository();
    }

    public function ironMeasurementIronerList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('IronMeasurement/ironMeasurementIronerList');
    }
    public function getAllIronMeasurementIroner()
    {
        $data['data'] = $this->ironMeasurementIronerRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewIronMeasurementIroner()
    {
        $data = [
            'plancha' => $this->request->getVar('plancha'),
            'activo' => 1
        ];
        try{
            $this->ironMeasurementIronerRepository->create($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion plancha agregada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
            return json_encode($respuesta);

        }

    }
    public function editIronMeasurementIroner(){
        $data = [
            'plancha' => $this->request->getVar('planchaEdit')
        ];
        try{
            $this->ironMeasurementIronerRepository->update($data, $this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion plancha actualizada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizar'
            ];
            return json_encode($respuesta);

        }

    }
    public function inactiveIronMeasurementIroner()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $route = $this->ironMeasurementIronerRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion plancha desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }
}