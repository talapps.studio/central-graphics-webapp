<?php

namespace App\Controllers;

use App\Repositories\CustomerRepository;
use App\Repositories\CountryRepository;
use App\Repositories\ContactRepository;
use App\Repositories\MachineRepository;
use App\Repositories\PlateRepository;
use App\Repositories\AniloxRepository;
use App\Repositories\ProductRepository;
use App\Repositories\ProductCustomerRepository;
use App\Repositories\DefaultProductRepository;


class Customer extends BaseController
{

    protected $countryRepository;
    protected $customerRepository;
    protected $contactRepository;
    protected $machineRepository;
    protected $plateRepository;
    protected $aniloxRepository;
    protected $productRepository;
    protected $productCustomerRepository;
    protected $defaultProductRepository;


    function __construct()
    {
        $this->countryRepository = new CountryRepository();
        $this->customerRepository = new CustomerRepository();
        $this->productRepository = new ProductRepository();
        $this->contactRepository = new ContactRepository();
        $this->machineRepository = new MachineRepository();
        $this->plateRepository = new PlateRepository();
        $this->aniloxRepository = new AniloxRepository();
        $this->productCustomerRepository = new ProductCustomerRepository();
        $this->defaultProductRepository = new DefaultProductRepository();
    }

    public function customers()
    {
        $data['paises'] = $this->countryRepository->getCountries();
        $data['productos'] = $this->productRepository->getProducts('s');
        /**
         * 1 Activo
         * 0 Inactivo
         */
        $data['productosPredeterminados'] = $this->defaultProductRepository->findWhere(['activo' => 1])->get();


        //validacion de session
        $session = session();
        if (!$session->get('isLoggedIn')) {
            return redirect()->route('login');
        }
        //mostrar la lista de clientes
        return view('Customers/managmentCustomers', $data);
    }

    public function getAllCustomers()
    {
        $esActivo = $this->request->getVar('activeCustomer');

        $data['data'] = $this->customerRepository->getCustomers($esActivo);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function addNewCustomer()
    {
        //Archivo de Imagen
        $file_logo = $this->request->getFile('file');

        //Informacion General del cliente
        $cliente = [
            'pais' => $this->request->getVar('pais'),
            'codigo_cliente' => $this->request->getVar('codigo'),
            'nit' => $this->request->getVar('nit'),
            'credito' => $this->request->getVar('credito'),
            'direccion' => $this->request->getVar('direccion'),
            'web' => $this->request->getVar('web'),
            'nombre' => $this->request->getVar('empresa'),
        ];

        //Guardado del cliente
        $cliente = $this->customerRepository->addCustomer($cliente);

        //Informacion de los contactos
        $contactos = json_decode($this->request->getVar('contactos'));
        foreach ($contactos as $contacto) {
            $contacts[] = array(
                'nombre' => $contacto->{"nombre-contacto"},
                'cargo' => $contacto->{"cargo-contacto"},
                'email' => $contacto->{"email-contacto"},
                'tel_oficina' => $contacto->{"telOficina-contacto"},
                'tel_directo' => $contacto->{"telDirecto-contacto"},
                'tel_celular' => $contacto->{"telCelular-contacto"},
                'id_cliente' => $cliente,
            );
        }

        //Informacion de maquinas
        $maquinas = json_decode($this->request->getVar('maquinas'));
        foreach ($maquinas as $maquina) {
            $machines[] = array(
                'maquina' => $maquina->{"nombre-maquina"},
                'colores' => $maquina->{"colores-maquina"},
                'id_cliente' => $cliente,
            );
        }

        //Informacion de planchas
        $planchas = json_decode($this->request->getVar('planchas'));
        foreach ($planchas as $plancha) {
            $plates[] = array(
                'altura' => $plancha->{"altura-plancha"},
                'lineaje' => $plancha->{"lineaje-plancha"},
                'marca' => $plancha->{"marca-plancha"},
                'id_cliente' => $cliente,
            );
        }

        //Informacion de Anilox
        $aniloxes = json_decode($this->request->getVar('aniloxes'));
        foreach ($aniloxes as $anilox) {
            $rodillos[] = array(
                'anilox' => $anilox->{"lineaje-anilox"},
                'bcm' => $anilox->{"bcm-anilox"},
                'cantidad' => $anilox->{"cantidad-anilox"},
                'id_cliente' => $cliente,
            );
        }

        //Informacion de productos
        $productos = json_decode($this->request->getVar('productos'));
        foreach ($productos as $producto) {
            $products[] = array(
                'id_grupo' => 1,
                'id_cliente' => $cliente,
                'codigo_cliente' => $this->request->getVar('codigo'),
                'id_producto' => $producto->CodigoProducto,
                'precio' => $producto->PrecioProducto,
                'cantidad' => 1,
                'concepto' => $producto->UnidadProducto,
                'activo' => $producto->AsignarProducto,
                'id_prod_clie' => $producto->idClienteProducto,
            );
        }

        $this->contactRepository->addManyContacts($contacts);
        $this->machineRepository->addManyMachines($machines);
        $this->plateRepository->addManyPlates($plates);
        $this->aniloxRepository->addManyAnilox($rodillos);
        $this->productCustomerRepository->addManyProductCustomer($products);

        //Guardado de imagen en directorio
        if ($file_logo != null) {
            if (!file_exists(RUTA_LOGOS)) {
                mkdir(RUTA_LOGOS);
            }
            $validationRule = [
                'userfile' => [
                    'label' => 'Image File',
                    'rules' => 'uploaded[userfile]'
                        . '|is_image[userfile]'
                        . '|mime_in[userfile,image/jpg,image/jpeg,image/gif,image/png,image/bmp,image/svg,image/ico]'
                        . '|max_size[userfile,100]'
                        . '|max_dims[userfile,1024,768]',
                ],
            ];
            if (!$this->validate($validationRule)) {
                $data = ['errors' => $this->validator->getErrors()];
                return json_encode($data);
            }

            $nombreArchivo = 'cliente-' . $cliente . '.' . $file_logo->getExtension();
            $file_logo->move(RUTA_LOGOS, $nombreArchivo, true);
            $file_array_update = [
                'logo' => $nombreArchivo
            ];
            $this->customerRepository->updateCustomer($cliente, $file_array_update); //Se actualiza porque ahora ya se tiene el id de cliente
        }
        echo json_encode("OK");
    }

    /**
     * 
     *
     * Obtiene los datos de un cliente segun ID
     *
     * @param 
     * @return Array  Datos del cliente y sus agregados
     * @throws None
     **/
    public function getCustomerData()
    {
        $idCliente = $this->request->getVar('id_cliente');
        
        $data['Cliente'] = [
            'General' => $this->customerRepository->getCustomerById($idCliente),
            'Contactos' => $this->contactRepository->getContactsByCustomer($idCliente)
        ];
        header('Content-Type: application/json');
        echo json_encode($data);
    }
   

    public
    function inactiveCliente()
    { //cambiar Nombre
        $idCliente = $this->request->getVar('idCliente');
        $cliente = [
            'activo' => 'n'
        ];
        $cliente = $this->customerRepository->updateCustomer($idCliente, $cliente);
    }

    public
    function activeCliente()
    { //cambiar Nombre
        $idCliente = $this->request->getVar('idCliente');
        $cliente = [
            'activo' => 's'
        ];
        $cliente = $this->customerRepository->updateCustomer($idCliente, $cliente);
    }

    public
    function activeInactiveCustomer()
    { //cambiar Nombre
        $idCliente = $this->request->getVar('idCliente');
        $estado = $this->request->getVar('estado');
        $cliente = [
            'activo' => $estado
        ];

        $cliente = $this->customerRepository->updateCustomer($idCliente, $cliente);
    }

    public
    function editCustomer()
    {
        $idCliente = $this->request->getVar('idCliente');
        $cliente = [
            'pais' => $this->request->getVar('pais'),
            'codigo_cliente' => $this->request->getVar('codigo'),
            'nit' => $this->request->getVar('nit'),
            'credito' => $this->request->getVar('diasCredito'),
            'direccion' => $this->request->getVar('direccion'),
            'web' => $this->request->getVar('web'),
            'nombre' => $this->request->getVar('empresa')
        ];

        $cliente = $this->customerRepository->updateCustomer($idCliente, $cliente);

        $contactos = $this->request->getVar('contactos');
        foreach ($contactos as $contacto) {
            $contact = [
                'id_cliente_contacto' => $contacto["IdContacto"],
                'nombre' => $contacto["NombreContacto"],
                'cargo' => $contacto["CargoContacto"],
                'email' => $contacto["EmailContacto"],
                'tel_oficina' => $contacto["TelOficinaContacto"],
                'tel_directo' => $contacto["TelDirectoContacto"],
                'tel_celular' => $contacto["TelCelularContacto"],
                'id_cliente' => $this->request->getVar('idCliente'),
            ];

            if ($contact['id_cliente_contacto'] == 0) {
                $this->contactRepository->addContact($contact);
            } else {
                $this->contactRepository->updateContact($contact['id_cliente_contacto'], $contact);
            }
        }

        $maquinas = $this->request->getVar('maquinas');
        foreach ($maquinas as $maquina) {
            $machine = [
                'id_cliente_maquina' => $maquina["IdMaquina"],
                'maquina' => $maquina["NombreMaquina"],
                'colores' => $maquina["ColoresMaquina"],
                'id_cliente' => $this->request->getVar('idCliente'),
            ];
            if ($machine['id_cliente_maquina'] == 0) {
                $this->machineRepository->addMachine($machine);
            } else {
                $this->machineRepository->updateMachine($machine['id_cliente_maquina'], $machine);
            }
        }

        $planchas = $this->request->getVar('planchas');
        foreach ($planchas as $plancha) {
            $plate = [
                'id_cliente_placa' => $plancha["IdPlancha"],
                'altura' => $plancha["AlturaPlancha"],
                'lineaje' => $plancha["LineajePlancha"],
                'marca' => $plancha["MarcaPlancha"],
                'id_cliente' => $this->request->getVar('idCliente'),
            ];
            if ($plate['id_cliente_placa'] == 0) {
                $this->plateRepository->addPlate($plate);
            } else {
                $this->plateRepository->updatePlate($plate['id_cliente_placa'], $plate);
            }
        }

        $aniloxes = $this->request->getVar('aniloxes');
        foreach ($aniloxes as $anilox) {
            $rodillo = [
                'id_cliente_anilox' => $anilox["IdAnilox"],
                'anilox' => $anilox["LineajeAnilox"],
                'bcm' => $anilox["BcmAnilox"],
                'cantidad' => $anilox["CantidadAnilox"],
                'id_cliente' => $this->request->getVar('idCliente'),
            ];
            if ($rodillo['id_cliente_anilox'] == 0) {
                $this->aniloxRepository->addAnilox($rodillo);
            } else {
                $this->aniloxRepository->updateAnilox($rodillo['id_cliente_anilox'], $rodillo);
            }
        }
        $productos = $this->request->getVar('productos');
        foreach ($productos as $producto) {
            $product = [
                'id_prod_clie' => $producto["IdProducto"],
                'id_grupo' => 1,
                'id_cliente' => $this->request->getVar('idCliente'),
                'codigo_cliente' => $this->request->getVar('codigo'),
                'id_producto' => $producto["CodigoProducto"],
                'precio' => $producto["PrecioProducto"],
                'cantidad' => 1,
                'concepto' => $producto["UnidadProducto"],
                'activo' => $producto["AsignarProducto"],
            ];
            if ($product['id_prod_clie'] == 0) {
                $this->productCustomerRepository->addProductCustomer($product);
            } else {
                $this->productCustomerRepository->updateProductCustomer($product['id_prod_clie'], $product);
            }
        }
        echo json_encode("Ok");
    }

    public
    function deleteCustomer()
    {
        $id = $this->request->getVar('idCliente');
        $data = [
            'activo' => 'n',
        ];
        $this->customerRepository->updateCustomer($id, $data);
        echo json_encode("Ok");
    }

    public function getCustomerProducts()
    {
        $data['data'] = $this->customerRepository->getProductsByCostumers($this->request->getVar('idCliente'));
        header('Content-Type: application/json');
        return json_encode($data);
    }
}
