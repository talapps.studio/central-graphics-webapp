<?php

namespace App\Controllers;
use App\Repositories\ReportRepository;
use App\Repositories\CustomerRepository;

class Reports extends BaseController
{
    
    protected $reportRepository;
    protected $customerRepository;
    function __construct()
    {
        $this->reportRepository = new ReportRepository();
        $this->customerRepository = new CustomerRepository();
    }

    public function compliance() //Cumplimiento
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $data['customers'] = $this->customerRepository->getActiveOrInactiveCustomers(1);
        return view('Reports/compliance',$data);
    }

    public function getComplianceData(){
        $customerId = $this->request->getVar('customerId');
        $daterange = explode('-',$this->request->getVar('dateRange'));
        $startDate = $daterange[0];
        $endDate = $daterange[1];
        $data['compliance'] = $this->reportRepository->getComplianceByDates($startDate,$endDate,$customerId);
        $data['complianceDetails'] = $this->reportRepository->getComplianceDetailsByDates($startDate,$endDate,$customerId);
        echo json_encode($data);

    }

    public function tracking() //Seguimiento
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
         $data['customers'] = $this->customerRepository->getActiveOrInactiveCustomers('s');
         $data['departments'] = $this->reportRepository->getFilteredJobPositions(['2','3','5','9','10','15','28']);
        return view('Reports/tracking', $data);
    }

    public function getJobByDepartmentData(){
        $daterange = explode('-',$this->request->getVar('dateRange'));
        $startDate = $daterange[0];
        $endDate = $daterange[1];
        $data['jobs'] = $this->reportRepository->getJobsByDepartment($startDate,$endDate);
        //$data['complianceDetails'] = $this->reportRepository->getComplianceDetailsByDates($startDate,$endDate,$customerId);
        echo json_encode($data);
    }


    
}
