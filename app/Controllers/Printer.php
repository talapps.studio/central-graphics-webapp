<?php

namespace App\Controllers;

use App\Repositories\PrinterRepository;
use App\Repositories\CustomerRepository;
use App\Repositories\ColorRepository;
use App\Repositories\ColorImpresoraRepository;


class Printer extends BaseController
{
    protected $printerRepository;
    protected $customerRepository;
    protected $colorRepository;
    protected $colorImpresoraRepository;
    function __construct()
    {
        $this->printerRepository = new PrinterRepository();
        $this->customerRepository = new CustomerRepository();
        $this->colorRepository = new ColorRepository();
        $this->colorImpresoraRepository = new ColorImpresoraRepository();
    }
    public function printerCustomer($idCustomer) 
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
        }
        $data['Cliente'] = $this->customerRepository->getCustomerById($idCustomer);
        $data['Colores'] = $this->colorRepository->getAllColors();
        return view('Printers/printerCustomer',$data);
    }

    public function getAllPrintersByCustomer(){
        $idCustomer = $this->request->getVar('idCustomer');
        $printers = $this->printerRepository->getPrinterByCustomer($idCustomer);
        $data['data'] =  is_null($printers) ? [] : $printers;
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function addPrinterForCustomer(){
        $printer = [
            'maquina' => $this->request->getVar('nombre'),
            'id_cliente' => $this->request->getVar('cliente'),
            'esConfigurado' => $this->request->getVar('esConfigurado'),
            'colores' => $this->request->getVar('numeroColores'),
        ];

        $newPrinter = $this->printerRepository->addPrinter($printer);

        $colors = json_decode($this->request->getVar('colores'));
        foreach ($colors as $color) {
            $colores[] = array(
                'id_color' => $color->{"id"},
                'id_impresora' => $newPrinter
            );
        }
        $this->colorImpresoraRepository->addManyColors($colores);
        $respuesta = [
            'status' => 200,
            'mensaje'=> 'maquina impresora agregada correctamente',
        ];
        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    
}
