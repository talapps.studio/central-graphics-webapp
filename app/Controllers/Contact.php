<?php

namespace App\Controllers;

use App\Repositories\ContactRepository;

class Contact extends BaseController
{
    protected $contactRepository;

    function __construct(){
        $this->contactRepository = new ContactRepository();
        
    }
    public function contacts()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //mostrar lista de contactos
        return view ('Contacts/managmentContacts');
    }
    
    public function getAllContacts(){
        $data['data'] = $this->contactRepository->getAll();
        header('Content-Type: application/json');
        echo json_encode($data);

    }
    public function addNewContact(){
       
        $data = [
            'nombre' => $this->request->getVar('nombre'),
            'cargo' => $this->request->getVar('cargo'),
            'email'=> $this->request->getVar('email'),
            'tel_oficina'=>$this->request->getVar('telOficina'),
            'tel_directo'=>$this->request->getVar('telDirecto'),
            'tel_celular'=>$this->request->getVar('telCelular')
        ];
        $this->contactRepository->addContact($data);
        echo json_encode("OK");
    }

    public function editContact(){
        $id = $this->request->getVar('idContacto');
        $data = [
            'nombre' => $this->request->getVar('nombre'),
            'cargo' => $this->request->getVar('cargo'),
            'email'=> $this->request->getVar('email'),
            'tel_oficina'=>$this->request->getVar('telOficina'),
            'tel_directo'=>$this->request->getVar('telDirecto'),
            'tel_celular'=>$this->request->getVar('telCelular')
        ];
        $this->contactRepository->updateContact($id,$data);
        echo json_encode("Ok");
    }
    
    // public function deleteContact(){
    //     $id = $this->request->getVar('idContacto');
    //     $this->contactRepository->updateContact($id);
    //     echo json_encode("Ok");
    // }
}

