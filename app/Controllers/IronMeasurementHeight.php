<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementHeightRepository;
use App\Helpers\Validadores;

class IronMeasurementHeight extends BaseController
{

    private $ironMeasurementHeightRepository;
    private $help;
    public function __construct()
    {
        $this->ironMeasurementHeightRepository = new IronMeasurementHeightRepository();
        $this->help = new Validadores();
    }

    public function ironMeasurementHeightList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('IronMeasurement/ironMeasurementHeightList');
    }
    public function getAllIronMeasurementHeight()
    {
        $data['data'] = $this->ironMeasurementHeightRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
    public function addNewIronMeasurementHeight()
    {
        $data = [
            'altura' => $this->request->getVar('altura'),
            'activo' => 1
        ];
        if( ! $this->help->longitud($this->request->getVar('altura')) ){
            $respuesta = [
                'status' =>403,
                'error' => '',
                'mensaje' => 'El formato debe ser sin coma y separar con . los decimales y mayor que cero'
            ];
        }else{
            try{
                $this->ironMeasurementHeightRepository->create($data);
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Plancha medicion altura agregada correctamente',
                ];
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al guardar'
                ];
            }
        }
        
        return json_encode($respuesta);
    }
    public function editIronMeasurementHeight(){
        $data = [
            'altura' => $this->request->getVar('alturaEdit')
        ];
        if( ! $this->help->longitud($this->request->getVar('alturaEdit')) ){
            $respuesta = [
                'status' =>403,
                'error' => '',
                'mensaje' => 'El formato debe ser sin coma y separar con . los decimales y mayor que cero'
            ];
        }else{
            try{
                $this->ironMeasurementHeightRepository->update($data, $this->request->getVar('id'));
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Plancha medicion altura actualizada correctamente',
                ];
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al actualizar'
                ];
            }
        }
        return json_encode($respuesta);
    }
    public function inactiveIronMeasurementHeight()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $route = $this->ironMeasurementHeightRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion altura desactivado correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }
}