<?php

namespace App\Controllers;

use App\Repositories\ProductRepository;

class Product extends BaseController
{
    protected $productRepository;

    function __construct()
    {
        $this->productRepository = new ProductRepository();

    }
    public function products(){
        
    }
    public function getAllProductos(){
        $data['data'] =$this->productRepository->getAll();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    
}