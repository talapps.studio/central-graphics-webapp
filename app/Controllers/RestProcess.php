<?php

namespace App\Controllers;

use App\Models\Cliente;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class RestProcess extends ResourceController
{

    use ResponseTrait;

    public function getAllCustomers()
    {
        $model = new Cliente();
        $data = $model->findAll();
        return $this->respond($data);
    }

}