<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementLineageRepository;

class IronMeasurementLineage extends BaseController
{

    private $ironMeasurementLineageRepository;

    public function __construct()
    {
        $this->ironMeasurementLineageRepository = new IronMeasurementLineageRepository();
    }

    public function ironMeasurementLineageList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('IronMeasurement/ironMeasurementLineageList');
    }
    public function getAllIronMeasurementLineage()
    {
        $data['data'] = $this->ironMeasurementLineageRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewIronMeasurementLineage()
    {
        $data = [
            'lineaje' => $this->request->getVar('lineaje'),
            'activo' => 1
        ];
        try{
            $this->ironMeasurementLineageRepository->create($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion lineaje agregada correctamente',
            ];
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
        }
        return json_encode($respuesta);
    }
    public function editIronMeasurementLineage(){
        $data = [
            'lineaje' => $this->request->getVar('lineajeEdit')
        ];

        if(is_numeric( $this->request->getVar('lineajeEdit') )){
            try{
                $this->ironMeasurementLineageRepository->update($data, $this->request->getVar('id'));
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Plancha medicion lineaje actualizada correctamente',
                ];
                return json_encode($respuesta);
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al actualizar'
                ];
                return json_encode($respuesta);
    
            }
        }else{
            $respuesta = [
                'status' =>500,
                'error' => 'Se necesita un número',
                'mensaje' => 'Ocurrio un error al actualizar'
            ];
            return json_encode($respuesta);
        }
    }
    public function inactiveIronMeasurementLineage()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $this->ironMeasurementLineageRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion lineaje desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }
}