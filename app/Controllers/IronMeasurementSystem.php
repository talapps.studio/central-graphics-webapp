<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementSystemRepository;

class IronMeasurementSystem extends BaseController
{

    private $ironMeasurementSystemRepository;

    public function __construct()
    {
        $this->ironMeasurementSystemRepository = new IronMeasurementSystemRepository();
    }

    public function ironMeasurementSystemList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('IronMeasurement/ironMeasurementSystemList');
    }
    public function getAllIronMeasurementSystem()
    {
        $data['data'] = $this->ironMeasurementSystemRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewIronMeasurementSystem()
    {
        $data = [
            'sistema' => $this->request->getVar('sistema'),
            'activo' => 1
        ];
        try{
            $this->ironMeasurementSystemRepository->create($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion sistema agregada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
            return json_encode($respuesta);

        }

    }
    public function editIronMeasurementSystem(){
        $data = [
            'sistema' => $this->request->getVar('sistemaEdit')
        ];
        try{
            $this->ironMeasurementSystemRepository->update($data, $this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion sistema actualizada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizar'
            ];
            return json_encode($respuesta);

        }

    }
    public function inactiveIronMeasurementSystem()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $route = $this->ironMeasurementSystemRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion sistema desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }

}