<?php

namespace App\Controllers;

use App\Models\Cliente;
use App\Models\Proceso;
use App\Repositories\CustomerRepository;

class OrderManager extends BaseController
{

    protected $customerRepository;

    public function __construct()
    {
        $this->customerRepository = new CustomerRepository();
    }

    public function processes()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('OrderManager/processes');
    }

    public function customerProcess($id)
    {
        $model = new Proceso();
        $cModel = new Cliente();
        $data['processes'] = $model->where('id_cliente', $id)->findAll();
        $data['customer'] = $cModel->where('id_cliente', $id)->first();
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('OrderManager/customerProcess', $data);
    }

    public function newProcess($id)
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $model = new Cliente();
        $data['customer'] = $model->where('id_cliente', $id)->first();
        
        return view('OrderManager/newProcess', $data);
    }

}