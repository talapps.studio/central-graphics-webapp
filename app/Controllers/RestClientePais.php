<?php

namespace App\Controllers;

use App\Models\ClientePais;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class RestClientePais extends ResourceController
{

    use ResponseTrait;

    public function getPaises()
    {
        $model = new ClientePais();
        $data = $model->findAll();
        return $this->respond($data);
    }

}