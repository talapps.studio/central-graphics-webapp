<?php
namespace App\Controllers;
use App\Repositories\JobTypeRepository;

class JobType extends BaseController
{
    protected $jobTypeRepository;

    function __construct(){
        $this->jobTypeRepository = new JobTypeRepository();
    }
    public function jobTypeList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //mostrar la lista de tipos de trabajo
        return view('JobType/jobList');
    }
    public function getAllJobType(){
        $data['data'] = $this->jobTypeRepository->getAll();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewJobType(){
        $data = [
            'trabajo' => $this->request->getVar('nombre'),
        ];
        $this->jobTypeRepository->addJobType($data);
        
        $respuesta = [
            'status' => 200,
            'mensaje'=> 'Tipo de trabajo agregado correctamente',
        ];
        return json_encode($respuesta);
    }
    public function editJobType(){
        $id = $this->request->getVar('idJobType');
        $data = [
            'trabajo' => $this->request->getVar('nombre'),
        ];
        $this->jobTypeRepository->updateJobType($id,$data);
        $respuesta = [
            'status'=>200,
            'mensaje'=>'Tipo trabajo actualizado correctamente',
        ];
        return json_encode($respuesta);
    }
    public function inactiveJob(){
        
            $idJob = $this->request->getVar('idJobType');
            $jobType = [
                'activo' => 'n'
            ];
            $jobType = $this->jobTypeRepository->updateJobType($idJob,$jobType);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Tipo de Trabajo inactivado correctamente'
            ];
            return json_encode($respuesta);
       
    }
    public function activeJob(){
        $idJob = $this->request->getVar('idJobType');
        $jobType = [
            'activo' => 's'
        ];
        $jobType = $this->jobTypeRepository->updateJobType($idJob,$jobType);
        $respuesta = [
            'status' => 200,
            'mensaje'=>'Tipo de trabajo activado correctamente'
        ];
        return json_encode($respuesta);
    } 

}