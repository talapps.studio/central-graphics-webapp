<?php

namespace App\Controllers;

use App\Repositories\CountryRepository;

class Country extends BaseController
{
    protected $countryRepository;
    function __construct(){
        $this->countryRepository = new CountryRepository();
    }

    public function countriesList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //mostrar la lista de clientes
       return view('Countries/countriesList');
    }

    public function getAllContries(){
        $data['data'] = $this->countryRepository->getCountries();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewCountry(){
        $data = [
            'abreviatura' => $this->request->getVar('abreviatura'),
            'nombre_pais' => $this->request->getVar('nombre'),
        ];
        try{
            $this->countryRepository->addCountry($data);
            $respuesta = [
                'status' => 200,
                'mensaje'=> 'País agregado correctamente',
            ];
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
        }
        return json_encode($respuesta);
    }
    public function editCountry(){
        $id = $this->request->getVar('idCountry');
        $data = [
            'abreviatura' => $this->request->getVar('abreviatura'),
            'nombre_pais' => $this->request->getVar('nombre'),
        ];
        $this->countryRepository->updateCountry($id,$data);
        $respuesta = [
            'status'=>200,
            'mensaje'=>'País actualizado correctamente',
        ];
        return json_encode($respuesta);
    }  
    public function inactiveCountry(){
        $idCountry = $this->request->getVar('idCountry');
        $count = [
            'activo' => 0
        ];
        $count = $this->countryRepository->updateCountry($idCountry,$count);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Pais desactivado correctamente'
        ];
        return json_encode($respuesta);
    }  
    public function activeCountry(){
        $idCountry = $this->request->getVar('idCountry');
        $count = [
            'activo' => 1
        ];
        $count = $this->countryRepository->updateCountry($idCountry,$count);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Pais activado correctamente'
        ];
        return json_encode($respuesta);
    }       
    
}