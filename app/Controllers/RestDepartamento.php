<?php

namespace App\Controllers;

use App\Models\Departamento;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class RestDepartamento extends ResourceController
{

    use ResponseTrait;

    public function getAllDepartamentos()
    {
        $model = new Departamento();
        $data = $model->findAll();
        return $this->respond($data);
    }

}