<?php

namespace App\Controllers;

use App\Repositories\AuthenticationRepository;
use App\Helpers\Notification;
use DateTime;

class Authentication extends BaseController
{
    protected $authenticationRepository;
    function __construct()
    {
        $this->authenticationRepository = new AuthenticationRepository();
    }

    public function login()
    {
        $session = session();
        if($session->get('isLoggedIn')){
            return redirect()->route('/');
        }
        return view('Authentication/login');
    }
    public function userAuth()
    {
        $session = session();
        
        $user = $this->authenticationRepository->getUserByUsername($this->request->getVar('username'));

        if (!$user) {
            $session->setFlashdata('msg', 'Usuario no encontrado');
            return redirect()->route('login');
        }

        if ($user['activo'] === 'n') {
            $session->setFlashdata('msg', 'Usuario inactivo');
            return redirect()->route('login');
        }
        $isVerifiedPassword = password_verify($this->request->getVar('password'), $user['contrasena']);
        if ($isVerifiedPassword) {
            $sesion_data = [
                'id_usuario' => $user['id_usuario'],
                'usuario' => $user['usuario'],
                'email' => $user['email'],
                'nombre' => $user['nombre'],
                'puesto' => $user['puesto'],
                'id_grupo' => $user['id_grupo'],
                'id_dpto' => $user['id_dpto'],
                'preferencias' => $user['preferencias'],
                'pais' => $user['upais'],
                'isLoggedIn' => TRUE
            ];
            $session->set($sesion_data);
            return redirect()->route('/');
        } else {
            $session->setFlashdata('msg', 'Usuario o Contraseña incorrectos');
            return redirect()->route('login');
        }
    }

    public function logout()
    {
        session()->destroy();
        return redirect()->to('/Authentication/login');
    }

    public function forgotPassword(){
        $session = session();
        if($session->get('isLoggedIn') != null){
            return redirect()->route('/');
        }
        return view('Authentication/forgotPassword');
    }

    public function changePassword(){
        $session = session();
        $password = $this->request->getVar('password1');
        $confirmation = $this->request->getVar('password2');
        if($password != $confirmation){
            $session->setFlashdata('msg', 'Las contraseñas ingresadas no coinciden entre si');
            return redirect()->to('Authentication/resetPassword/'.$session->get('token'));
        }
        $result = $this->authenticationRepository->changePassword($session->get('id_usuario'), $password);
        if($result == 0){
            $session->setFlashdata('msg', 'Las contraseñas ingresadas no pudieron validarse debido a un error');
            return redirect()->to('Authentication/resetPassword/'.$session->get('token'));
        }
        if(!$result){
            $session->setFlashdata('msg', 'La contraseña no ha podido ser actualizada, por favor intente nuevamente');
            return redirect()->to('Authentication/resetPassword/'.$session->get('token'));
        }
        $session->setFlashdata('msg', 'Contraseña actualizada correctamente');
        return redirect()->route('login');
    }

    public function sendEmailForReset(){
        $session = session();

        $token = sha1(uniqid(rand()));
        $expired = date ("Y-m-d H:i:s", strtotime (date_create()->format('Y-m-d H:i:s') ."+5 days"));

        $user = $this->authenticationRepository->getUserByUsername($this->request->getVar('email'));
        if (!$user) {
            $session->setFlashdata('msg', 'Usuario no encontrado');
            return redirect()->to('Authentication/forgotPassword');
        }
        $this->authenticationRepository->setToken($user['id_usuario'], $token, $expired);
        $message = base_url().'/Authentication/resetPassword/'.$token;
        $notification = new Notification();
        $isSend = $notification->sendNotification($user['email'], 'example@gmail.com', 'Reset Password', 'Reset Password', $message);
        if($isSend) 
		{
            $session->setFlashdata('msg', "Se ha enviado un enlace para el cambio de contrasena a su direccion de correo electronico");
        }else{
            $session->setFlashdata('msg', "Ha ocurrido un error, por favor intentelo nuevamente");
        } 
        return redirect()->to('Authentication/forgotPassword');
    }

    public function resetPassword($token){
        $result = $this->authenticationRepository->validateToken($token);
        $session = session();
        if(!$result){
            return view('Authentication/invalidToken');
        }else{
            $sesion_data = [
                'id_usuario' => $result['id_usuario'],
                'token' => $token,
            ];
            $session->set($sesion_data);
            return view('Authentication/resetPassword');
        }  
    }

    
}
