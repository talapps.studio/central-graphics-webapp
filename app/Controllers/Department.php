<?php

namespace App\Controllers;

use App\Repositories\DepartmentRepository;
use App\Helpers\Validadores;

class Department extends BaseController
{
    protected $departmentRepository;

    function __construct()
    {
        $this->departmentRepository = new DepartmentRepository();
        $this->help = new Validadores();
    }

    public function departmentList()
    { //debe haber iniciado session
        $session = session();
        if (!$session->get('isLoggedIn')) {
            return redirect()->route('login');
        }
        //mostrar la lista de departamentos
        return view('Department/departmentList');
    }

    public function getAllDepartments()
    {
        $data['data'] = $this->departmentRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function addNewDepartment()
    {
        $data = [
            'codigo' => $this->request->getVar('codigo'),
            'departamento' => $this->request->getVar('nomDpto'),
            'tipo_inv' => $this->request->getVar('tipoInv'),
            'cant_mensual' => $this->request->getVar('cantMensual'),
            'iniciales' => $this->request->getVar('iniciales')
        ];
        if($this->help->cantidadEntera($this->request->getVar('cantMensual'))){
            try{
                $this->departmentRepository->create($data);
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Departamento agregado correctamente',
                ];
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al guardar'
                ];
            }
        }else{
            $respuesta = [
                'status' =>403,
                'error' => '',
                'mensaje' => 'la cantidad debe ser entera y mayor a cero'
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function editDepartment()
    {
        $id = $this->request->getVar('idDepartment');
        $data = [
            'codigo' => $this->request->getVar('codigo'),
            'departamento' => $this->request->getVar('nomDpto'),
            'tipo_inv' => $this->request->getVar('tipoInv'),
            'cant_mensual' => $this->request->getVar('cantMensual'),
            'iniciales' => $this->request->getVar('iniciales')
        ];
        if($this->help->cantidadEntera($this->request->getVar('cantMensual'))){
            $this->departmentRepository->update($data, $id);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Departamento actualizado correctamente',
            ];
        }else{
            $respuesta = [
                'status' =>403,
                'error' => '',
                'mensaje' => 'La cantidad debe ser entera y mayor a cero'
            ];
        }
        header('Content-Type: application/json');
        echo json_encode($respuesta);
    }

    public function inactiveDepto()
    {
        $idDpto = $this->request->getVar('idDepartment');
        $dpto = [
            'activo' => 'n'
        ];
        $dpto = $this->departmentRepository->update($dpto, $idDpto);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Departamento inactivado correctamente'
        ];
        return json_encode($respuesta);
    }

    public function activeDepto()
    {
        $idDpto = $this->request->getVar('idDepartment');
        $dpto = [
            'activo' => 's'
        ];
        $dpto = $this->departmentRepository->update($dpto, $idDpto);
        $respuesta = [
            'status' => 200,
            'mensaje' => 'Departamento activado correctamente'
        ];
        return json_encode($respuesta);
    }
}