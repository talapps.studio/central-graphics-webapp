<?php

namespace App\Controllers;

class EmailController extends \CodeIgniter\RESTful\ResourceController
{

    /**
     * @var \App\Repositories\EmailService
     */
    private $mailService;

    public function __construct()
    {
        $this->mailService = new \App\Repositories\EmailService();
    }

    public function sendMail() {
        try {
            $email = $this->request->getVar('email');
            $password = $this->request->getVar('password');
            $response = $this->mailService->sendNewUserEmail($password, $email);
            return $this->respond(['status' => 'ok', 'result' => $response]);
        } catch (\Exception $e) {
            return $this->fail($e->getMessage());
        }
    }

}