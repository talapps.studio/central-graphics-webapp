<?php
namespace App\Controllers;

use App\Repositories\ServiceRepository;
use App\Helpers\Validadores;

class Service extends BaseController
{
    protected $serviceRepository;
    protected $help;
    function __construct(){
        $this->serviceRepository = new ServiceRepository();
        $this->help = new Validadores();
    }
    public function serviceList(){
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //mostrar lista de servicios
        return view('DefaultService/serviceList');
    }
    public function getAllServices(){
        $data['data'] = $this->serviceRepository->getAll();
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function addNewService(){
        if($this->help->montoMonetario($this->request->getVar('costo'))){
            try{
                $data = [
                    'descripcion' => $this->request->getVar('descripcion'),
                    'costo' => $this->request->getVar('costo')
                ];
                $this->serviceRepository->addService($data);
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Servicio agregado correctamente',
                ];
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al guardar'
                ];
            }
        }else{
            $respuesta = [
                'status' => 403,
                'mensaje' => 'Error en el formato del precio',
            ];
        }
        return  $this->response
                    ->setStatusCode($respuesta['status'], $respuesta['mensaje'])
                    ->setJSON(json_encode($respuesta))
                ;
    }

    public function editService()
    {
        if($this->help->montoMonetario($this->request->getVar('costo'))){
            try{
                $id = $this->request->getVar('idService');
                $data = [
                    'descripcion' => $this->request->getVar('descripcion'),
                    'costo' => $this->request->getVar('costo')
                ];
                $this->serviceRepository->updateService($id,$data);
                $respuesta = [
                    'status' => 200,
                    'mensaje' => 'Servicio actualizado correctamente',
                ];
            }catch(\Exception $e){
                $respuesta = [
                    'status' =>500,
                    'error' => $e->getMessage(),
                    'mensaje' => 'Ocurrio un error al guardar'
                ];
            }
        }else{
            $respuesta = [
                'status' => 403,
                'mensaje' => 'Error en el formato del precio',
            ];
        }
        return 
            $this->response
                ->setStatusCode($respuesta['status'], $respuesta['mensaje'])
                ->setJSON(json_encode($respuesta))
        ;
    }
    public function inactiveService()
    {
        $idService = $this->request->getVar('idService');
        $data = [
            'activo' => 0
        ];
        $data = $this->serviceRepository->updateService($idService,$data);
        $respuesta = [
            'status' =>200,
            'mensaje' =>'Servicio desactivado correctamente'
        ];
        return json_encode($respuesta);
    }
    public function activeService()
    {
        $idService = $this->request->getVar('idService');
        $data = [
            'activo' => 1
        ];
        $data = $this->serviceRepository->updateService($idService,$data);
        $respuesta = [
            'status' =>200,
            'mensaje' =>'Servicio activado correctamente'
        ];
        return json_encode($respuesta);
    }

}