<?php

namespace App\Controllers;

use App\Repositories\DepartmentRepository;
use App\Repositories\GrupoRepository;
use App\Repositories\PaisRepository;

class User extends BaseController
{

    protected $paisRepository;
    protected $departamentoRepository;
    protected $grupoRepository;

    public function __construct()
    {
        $this->paisRepository = new PaisRepository();
        $this->departamentoRepository = new DepartmentRepository();
        $this->grupoRepository = new GrupoRepository();
    }

    public function index() {
        $data = [
            'counties' => $this->paisRepository->get(),
            'departamentos' => $this->departamentoRepository->get(),
            'grupos' => $this->grupoRepository->get(),
        ];
        return view('User/users', $data);
    }

    public function online()
    {
        $data = [
            'departamentos' => $this->departamentoRepository->orderBy('departamento', 'asc')->get(),
            'grupos' => $this->grupoRepository->orderBy('nombre_grupo', 'asc')->get(),
        ];
        return view('User/online', $data);
    }

}