<?php
 namespace App\Controllers;
 use App\Repositories\CustomerRepository;
 use App\Repositories\ProcessRepository;
 use App\Repositories\OrderRepository;
 use App\Repositories\JobTypeRepository;
 use App\Repositories\OrderProductRepository;
 class Quotation extends BaseController
 {
    protected $customerRepository;
    protected $proccessRepository;
    protected $orderRepository;
    protected $jobTypeRepository;
    protected $orderProductRepository;

    function __construct()
    {
        $this->customerRepository = new CustomerRepository();
        $this->proccessRepository = new ProcessRepository();
        $this->orderRepository = new OrderRepository();
        $this->jobTypeRepository = new JobTypeRepository();
        $this->orderProductRepository = new OrderProductRepository();
    }
    public function quotationList($id=null){
       
         $data['clientes']= $this->customerRepository->getActiveOrInactiveCustomers('s');
         $data['tiposTrabajo'] = $this->jobTypeRepository->getAll();
        
        //vista de cotizaciones 
        return view('Quotation/quotationList',$data);
    }
    public function getQuotationProcessByCustomer(){
         

        $data['data']= $this->proccessRepository->ProcessByCustomer($this->request->getVar('idCliente'));
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function getQuotationOderByProcess(){
         

        $data['data']= $this->proccessRepository->getAllOrdersByProcess($this->request->getVar('idProceso'));
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewQuotation(){
        $fechaIngProceso = date("Y-m-d");
        $fechaEntreProces= date("Y-m-d");
        $fechaEntreReale= date("Y-m-d");
        $productos = $this->request->getVar('productos');
        try{
        $pedido = [
            'id_proceso' => $this->request->getVar('idProceso'),
            'id_tipo_trabajo' => $this->request->getVar('tipoTrabajo'),
            'fecha_entrada' => $fechaIngProceso,
            'fecha_entrega' => $fechaEntreProces,
            'fecha_reale' =>  $fechaEntreReale,
            'prioridad'=>'No',
            'id_responsable'=>0,
            'id_proyecto'=>0,
            'ped_prioridad'=>0,
            'id_repro_deta'=>0,
            'de'
        ];
        $idPedido= $this->orderRepository->addNewOrder($pedido);

        //cotizacion

            foreach($productos as $producto){
                $productoPedido = [
                    'id_producto'=>$producto['id'],
                    'id_pedido'=>$idPedido,
                    'precio'=>$producto['precio'],
                    'pulgadas'=>$producto['pulgadas'],
                    'cantidad'=>$producto['cantidad'],
                    'ancho'=>$producto['ancho'],
                    'alto'=>$producto['alto']
                ];
                $this->orderProductRepository->create($productoPedido);
            }
        
        

        $respuesta = [
                'status' => 200,
                'mensaje' => 'Cotizacion agregada correctamente'
            ];
         return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' => 500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al ingresar Cotizacion: '.' '.$e->getMessage(),
            ];
            return json_encode($respuesta);
        }
    }
 }