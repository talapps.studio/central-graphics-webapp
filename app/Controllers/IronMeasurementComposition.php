<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementCompositionRepository;

class IronMeasurementComposition extends BaseController
{
    private $ironMeasurementCompositionRepository;

    function __construct()
    {
        $this->ironMeasurementCompositionRepository = new IronMeasurementCompositionRepository();
    }

    public function ironMeasurementCompositionList()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('IronMeasurement/ironMeasurementCompositionList');
    }
    public function getAllIronMeasurementComposition()
    {
        $data['data'] = $this->ironMeasurementCompositionRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewIronMeasurementComposition()
    {
        try{
            $data = [
                'compensacion' => $this->request->getVar('compensacion'),
                'activo' => 1
            ];
            $this->ironMeasurementCompositionRepository->create($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Compensaci&oacute;n agregada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
            return json_encode($respuesta);

        }

    }
    public function editIronMeasurementComposition(){

        try{
            $data = [
                'compensacion' => $this->request->getVar('compensacionEdit')
            ];
            $this->ironMeasurementCompositionRepository->update($data, $this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Compensaci&oacute;n actualizada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizar'
            ];
            return json_encode($respuesta);

        }

    }
    public function inactiveIronMeasurementComposition()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $this->ironMeasurementCompositionRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Compensaci&oacute;n desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }
}