<?php

namespace App\Controllers;
use App\Repositories\OrderRepository;
use App\Repositories\OrderUserRepository;
use App\Repositories\OrderProductRepository;
use App\Repositories\ObservationRepository;
use App\Repositories\OrderAttachedRepository;
use App\Repositories\PolimeroRepository;
use App\Repositories\RutaDepartamentoRepository;
use App\Repositories\StickybackRepository;

class Order extends BaseController
{
    protected $orderRepository;
    protected $orderUserRepository;
    protected $orderProductRepository;
    protected $observationRepository;
    protected $orderAttachedRepository;
    protected $polimeroRepository;
    protected $stickybackRepository;
    protected $rutaDepartamentoRepository;

    function __construct()
    {
        $this->orderRepository = new OrderRepository();
        $this->orderUserRepository = new OrderUserRepository();
        $this->orderProductRepository = new OrderProductRepository();
        $this->observationRepository = new ObservationRepository();
        $this->orderAttachedRepository = new OrderAttachedRepository();
        $this->polimeroRepository = new PolimeroRepository();
        $this->stickybackRepository = new StickybackRepository();
        $this->rutaDepartamentoRepository=new RutaDepartamentoRepository();
    }

    public function assignments()
    {
        
        $data['pedidos'] = $this->orderRepository->getAssignments();
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        return view('Orders/assignments', $data);
    }

    public function details($id){
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
        }
        $data['cabecera'] = $this->orderRepository->getOrderHeader($id);
        $data['rutas'] = $this->orderRepository->getOrderWorkFlow($id);
        $data['observaciones'] = $this->orderRepository->getOrderObservations($id);
        return view('Orders/details',$data);
    }

    public function planningSheet($id){
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
        }
        $data['cabecera'] = $this->orderRepository->getOrderHeader($id);
        $data['polimeros'] = $this->polimeroRepository->getAllConstants();
        $data['stickybacks'] = $this->stickybackRepository->getAllConstants();
        return view('Orders/planning_sheet',$data);
    }

    public function planningSheetView($id){
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
        }
        $data['cabecera'] = $this->orderRepository->getOrderHeader($id);
        $data['polimeros'] = $this->polimeroRepository->getAllConstants();
        $data['stickybacks'] = $this->stickybackRepository->getAllConstants();
        return view('Orders/planning_sheet_view',$data);
    }

    public function getOrderByProcess(){
        $id_proceso = $this->request->getVar('id_proceso');
        $id_cliente = $this->request->getVar('id_cliente');
        $data['data'] = $this->orderRepository->getOrdersByProcess($id_proceso,$id_cliente);
        header('Content-Type: application/json');
        echo json_encode($data);
    }

    public function addNewOderByProcess(){
        $fechaIngProceso = date_create($this->request->getVar('fechaIngProceso'));
        $fechaEntreProces= date_create($this->request->getVar('fechaEntreProceso'));
        $fechaEntreReale= date_create($this->request->getVar('fechaEntreProceso'));
        $productos =json_decode($this->request->getVar('productos'));
        $observaciones =$this->request->getVar('observaciones');
        try{
        $pedido = [
            'id_proceso' => $this->request->getVar('idProceso'),
            'id_tipo_trabajo' => $this->request->getVar('tipoTrabajo'),
            'fecha_entrada' => date_format($fechaIngProceso, 'Y-m-d '),
            'fecha_entrega' => date_format($fechaEntreProces, 'Y-m-d ') ,
            'fecha_reale' => date_format($fechaEntreReale, 'Y-m-d '),
            'prioridad'=>'No',
            'id_responsable'=>0,
            'id_proyecto'=>0,
            'ped_prioridad'=>0,
            'id_repro_deta'=>0,
            'de'
        ];
        $idPedido= $this->orderRepository->addNewOrder($pedido);

        //ADJUNTOS
        $imageFile = $this->request->getFile('fileImage');
        if ($imageFile != null) {
                if (!file_exists(RUTA_PEDIDOS)) {
                    mkdir(RUTA_PEDIDOS);
                }
                $nombreArchivo = 'cliente-'.$this->request->getVar('idCliente').'pedido-' . $idPedido . '.' . $imageFile->getExtension();
                $imageFile->move(RUTA_PEDIDOS, $nombreArchivo, true);

                $pedidoAdjunto=['id_pedido'=>$idPedido,
                    'url'=>RUTA_PEDIDOS.$nombreArchivo,
                    'nombre_adjunto'=> $imageFile->getName(),
                    'tipo_ajunto'=>$imageFile->getClientMimeType(),
                    'mime_type'=>$imageFile->getMimeType()
                ];
                $this->orderAttachedRepository->create($pedidoAdjunto);
        }

        //ruta usuario
        $rutaDepto=['id_ruta_grupo'=> $this->request->getVar('grupo'),
            'id_dpto'=> $this->request->getVar('departamento')];
        $idRutaDepto=$this->rutaDepartamentoRepository->create($rutaDepto);
        $pedidoUsuario = [
            'id_ruta_dpto'=>$idRutaDepto,
            'id_pedido'=>$idPedido,
            'id_usuario'=>$this->request->getVar('idUsuario'),
            'fecha_asignado'=>date_format($fechaIngProceso, 'Y-m-d '),
            'fecha_inicio'=>date_format($fechaIngProceso, 'Y-m-d '),
            'fecha_fin'=>date_format($fechaEntreProces, 'Y-m-d '),
            'tiempo_asignado'=>$this->request->getVar('tiempo_usuario'),
            'estado'=>'Agregado',
            'pprioridad'=>99,
            'tipo'=>0
        ];
        $this->orderUserRepository->create($pedidoUsuario);
        //cotizacion

        if(!empty($productos)){
            foreach($productos as $producto){
                $productoPedido = [
                    'id_producto'=>$producto->id,
                    'id_pedido'=>$idPedido,
                    'precio'=>$producto->precio,
                    'pulgadas'=>$producto->pulgadas,
                    'cantidad'=>$producto->cantidad,
                    'ancho'=>$producto->ancho,
                    'alto'=>$producto->alto
                ];
                $this->orderProductRepository->create($productoPedido);
            }
        }
        if(!empty($observaciones)) {
            $observacion = [
                'id_pedido'=>$idPedido,
                'id_usuario'=>$this->request->getVar('idUsuario'),
                'fecha_hora'=>date('Y-m-d h:i:s'),
                'observacion'=>$this->request->getVar('observaciones'),
                'req_aprobacion'=>1
            ];
            $this->observationRepository->create($observacion);
        }

        $respuesta = [
                'status' => 200,
                'mensaje' => 'Pedido agregado correctamente'
            ];
         return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' => 500,
                'error' => '',
                'mensaje' => 'Ocurrio un error al ingresar Pedido: '.' '.$e->getMessage(),
            ];
            return json_encode($respuesta);
        }
    }

}
