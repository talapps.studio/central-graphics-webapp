<?php

namespace App\Controllers;

use App\Models\Grupo;
use CodeIgniter\API\ResponseTrait;
use CodeIgniter\RESTful\ResourceController;

class RestGrupo extends ResourceController
{

    use ResponseTrait;

    public function getAllGrupos()
    {
        $model = new Grupo();
        $data = $model->findAll();
        return $this->respond($data);
    }

}