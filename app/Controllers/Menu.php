<?php
namespace App\Controllers;
use App\Repositories\MenuRepository;


class Menu extends BaseController
{    
    protected $menuRepository;
    protected $session;

    function __construct(){
        $this->menuRepository = new MenuRepository();
        $this->session = \Config\Services::session();
        $this->session->start();
    }
    public function menuList(){
         //validacion de session
         $session = session();
         if(!$session->get('isLoggedIn')){
             return redirect()->route('login');
          }
        $data['menus'] = $this->menuRepository->getMenuPadre();
        //mostrar la vista de menu
        return view ('Menu/menuList',$data);
    }
    public function menuPadreList(){
        $data['menus'] = $this->menuRepository->getMenuPadre();
        header('Content-type: application/json');
        return json_encode($data);
    }
    public function subMenuList($idPadre){

        $data['submenus']=$this->menuRepository->getSubMenu($idPadre);
        header('Content-type: application/json');
        return json_encode($data);

    }
    
    public function getAllMenu(){
        $activo = $this->request->getVar('menuActivo');
        $data['data'] = $this->menuRepository->getMenus($activo);
        
        header('Content-type: application/json');
        echo json_encode($data);
    }
   public function editMenu(){
        $id = $this->request->getVar('idMenu');
        $menu = [
            'etiqueta' => $this->request->getVar('etiqueta'),
            'id_menu_padre' => $this->request->getVar('menuPadre'),
            'descripcion' => $this->request->getVar('descripcion'),
            'icono'=>$this->request->getVar('icono')
        ];
        $this->menuRepository->updateMenu($id,$menu);

        $respuesta = [
            'status' => 200,
            'mensaje' => 'Menu actualizado correctamente'
        ];
        return json_encode($respuesta);
   }

   public function inactiveMenu(){
       $idMenu = $this->request->getVar('idMenu');
       $menu = [
           'activo'=>'n'
       ];
       $menu = $this->menuRepository->updateMenu($idMenu,$menu);

       $respuesta = [
        'status' => 200,
        'mensaje' => 'Menu inactivado correctamente'
    ];
    return json_encode($respuesta);

   }
   Public function activeMenu(){
      $idMenu = $this->request->getVar('idMenu');
      $menu = [
          'activo' =>'s'
      ];
      $menu = $this->menuRepository->updateMenu($idMenu,$menu);

      $respuesta = [
        'status' => 200,
        'mensaje' => 'Menu activado correctamente'
    ];
    return json_encode($respuesta);
   }
   public function getMenuUsuario()
   {   
       $session = session();
       
       $id_dpto = $session->get('id_dpto');
       $id_user = $session->get('id_usuario');
       $menuDepartamentos = $this->menuRepository->getMenuDept($id_dpto);
       $menuUsuarios = $this->menuRepository->getMenuUser($id_user);
       $menusAsignados = [];
       foreach ($menuDepartamentos as $departamento){
            array_push($menusAsignados,$departamento);
        }
       foreach($menuUsuarios as $usuario){
            array_push($menusAsignados,$usuario);
       }

         header('Content-type: application/json');
        return json_encode($menusAsignados);
   }
  
}