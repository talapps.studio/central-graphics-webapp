<?php

namespace App\Controllers;

use App\Repositories\IronMeasurementPlotRepository;

class IronMeasurementPlot extends BaseController
{
    private $ironMeasurementPlotRepository;

    public function __construct()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        $this->ironMeasurementPlotRepository = new IronMeasurementPlotRepository();
    }

    public function ironMeasurementPlotList()
    {
        return view('IronMeasurement/ironMeasurementPlotList');
    }
    public function getAllIronMeasurementPlot()
    {
        $data['data'] = $this->ironMeasurementPlotRepository->get();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewIronMeasurementPlot()
    {
        $data = [
            'trama' => $this->request->getVar('trama'),
            'activo' => 1
        ];
        try{
            $this->ironMeasurementPlotRepository->create($data);
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion trama agregada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
            return json_encode($respuesta);

        }

    }
    public function editIronMeasurementPlot(){
        $data = [
            'trama' => $this->request->getVar('tramaEdit')
        ];
        try{
            $this->ironMeasurementPlotRepository->update($data, $this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion trama actualizada correctamente',
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al actualizar'
            ];
            return json_encode($respuesta);

        }

    }
    public function inactiveIronMeasurementPlot()
    {
        $data = [
            'activo' => 0
        ];
        try{
            $route = $this->ironMeasurementPlotRepository->update($data,$this->request->getVar('id'));
            $respuesta = [
                'status' => 200,
                'mensaje' => 'Plancha medicion trama desactivada correctamente'
            ];
            return json_encode($respuesta);
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al desactivar elemento'
            ];
            return json_encode($respuesta);

        }

    }

}