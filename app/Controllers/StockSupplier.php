<?php
namespace App\Controllers;

use App\Repositories\StockSupplierRepository;
use App\Models\InventarioProveedor;

class StockSupplier extends BaseController
{
    protected $stockSupplierRepository;

    function __construct(){
        $this->stockSupplierRepository = new StockSupplierRepository();
    }
    public function stockSuppliers()
    {
        $session = session();
        if(!$session->get('isLoggedIn')){
            return redirect()->route('login');
         }
        //mostrar lista de inventario de proveedores
        return view ('StockSuppliers/stockSuppliersList');
    }
    public function getAllStockSuppliers(){
        $data['data'] = $this->stockSupplierRepository->getAll();
        header('Content-Type: application/json');
        echo json_encode($data);
    }
    public function addNewStockSupplier(){
        try{
            $data = [
                'proveedor_nombre' => $this->request->getVar('nombreProv')
            ];
            $this->stockSupplierRepository->addStockSupplier($data);
            $respuesta = [
                'status'=> 200,
                'mensaje' => 'Proveedor agregado correctamente'
            ];
        }catch(\Exception $e){
            $respuesta = [
                'status' =>500,
                'error' => $e->getMessage(),
                'mensaje' => 'Ocurrio un error al guardar'
            ];
        }
        return json_encode($respuesta);
    }
    public function editStockSupplier(){
        $id = $this->request->getVar('idInvProv');
    
        $data = [
            'proveedor_nombre' => $this->request->getVar('nombreProv')
        ];
        $this->stockSupplierRepository->updateStockSupplier($id,$data);
        
        $respuesta = [
            'status'=> 200,
            'mensaje' => 'Proveedor actualizado correctamente'
        ];
        return json_encode($respuesta);
    }
    
    public function deleteMaterial($id = null){
        $id = $this->request->getVar('idInvProv');
        $prov = new InventarioProveedor();
        $prov->delete($id);

        $respuesta = [
            'status'=>200,
            'mensaje'=>'Proveedor fue desactivado exitosamente'
        ];
        return json_encode($respuesta);
    }
   

}